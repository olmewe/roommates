import UIKit

class ConsoleGameListViewController: UIViewController {

	@IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var descText: UITextView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var playButton: UIButton!

    @IBOutlet weak var genreLbl: UILabel!
    @IBOutlet weak var modeLbl: UILabel!
    @IBOutlet weak var playersLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!

	var selected:String = ""
	private var appeared = false
	private var first = true

	var close:((Bool) -> Void)? = nil
	var select:((String) -> Void)? = nil
	var play:(() -> Void)? = nil

	var list:[String] = []
	var sfxCartridge:Data?

	override func viewDidLoad() {
		super.viewDidLoad()

		let nib = UINib(nibName: "GameCartridgeViewCell", bundle: nil)
		collectionView.register(nib, forCellWithReuseIdentifier: "Cell")

		sfxCartridge = Audio.data(of:"app/cartridge.wav")
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)

        collectionView.delegate = self
        collectionView.dataSource = self

		appeared = true
		select(game:selected,force:true)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

    @IBAction func closeButton(_ sender: Any) {
		close?(true)
    }

    @IBAction func playButton(_ sender: Any) {
		play?()
    }

	func select(game id:String,force:Bool = false) {
		guard appeared else {
			selected = id
			return
		}
		guard force || selected != id else { return }
		getCell(in:list.index(of:selected))?.set(active:false)
		self.selected = id
		guard let index = list.index(of:id) else {
			showDefaultValues()
			return
		}
		getCell(in:index)?.set(active:true)
		if first {
			first = false
		} else {
			Audio.play(sfxCartridge)
		}
		guard let game = GameInfo.games[id] else {
			showDefaultValues()
			return
		}
		showValues(for:game)
	}

	func showValues(for game:GameInfo) {
		view.backgroundColor = game.bgColour
		headerImage.image = UIImage(named:game.assetsFolder+"/header")
		headerImage.contentMode = .scaleAspectFit
		headerImage.backgroundColor = game.fgColour
        descText.text = game.description
        infoView.isHidden = false
        playButton.isHidden = false
		genreLbl.text = game.genre
		modeLbl.text = game.modes.map {
			(mode) -> String in
			switch mode {
			case .single: return "Single"
			case .coop: return "Cooperative"
			case .comp: return "Competitive"
			case .compTeams: return "Teams"
			case .compOne: return "1 x All"
			}
			}.joined(separator:", ")
		let playersMin = max(1,game.playersMin)
		if var playersMax = game.playersMax {
			playersMax = max(1,playersMax)
			if playersMin >= playersMax {
				if playersMin <= 1 {
					playersLbl.text = "1 player"
				} else {
					playersLbl.text = "\(playersMin) players"
				}
			} else {
				playersLbl.text = "\(playersMin)-\(playersMax) players"
			}
		} else {
			playersLbl.text = "\(playersMin)+ players"
		}
		timeLbl.text = "\(Int(game.averageTime)) min"
	}

	func showDefaultValues() {
		view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 0.6, alpha: 1)
		headerImage.image = UIImage(named:"console_view/header")
		headerImage.contentMode = .scaleAspectFill
        descText.text = ""
        infoView.isHidden = true
        playButton.isHidden = true
	}

	func getCell(in index:Int?) -> GameCartridgeViewCell? {
		guard let index = index else { return nil }
		return collectionView.cellForItem(at:IndexPath(item:index,section:0)) as? GameCartridgeViewCell
	}
}

extension ConsoleGameListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
	var items:Int {
		return Game.pad ? 5 : 3
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		guard let flow = collectionViewLayout as? UICollectionViewFlowLayout else { return CGSize.one }
		let totalWidth = flow.sectionInset.left+flow.sectionInset.right+(flow.minimumInteritemSpacing*CGFloat(items-1))
		let width = Int((collectionView.bounds.width-totalWidth)/CGFloat(items))
		return CGSize(width:width,height:width+60)
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return list.count
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return .zero
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GameCartridgeViewCell
		let game = list[indexPath.item]
		cell.set(game:game)
		cell.forceSet(active:selected == game)
		return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let game = list[indexPath.item]
		if selected != game {
			select?(game)
		}
    }
}
