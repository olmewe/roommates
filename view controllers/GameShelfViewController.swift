import UIKit

class GameShelfViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

	@IBOutlet weak var prateleira: UICollectionView!
	@IBOutlet weak var nomeCategoria: UILabel!
    @IBOutlet weak var wave: UIImageView!
    @IBOutlet weak var wave2: UIImageView!

	var list:[GameInfo] = []


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.//
        list = [GameInfo](GameInfo.games.values)

		self.wave.bounds.size = CGSize(width: self.wave.bounds.size.width, height: UIScreen.main.bounds.height * 0.45)
        self.wave.center = CGPoint(x: UIScreen.main.bounds.width - self.wave.bounds.size.width/2, y: UIScreen.main.bounds.height - self.wave.bounds.size.height/2)
//        UIView.animateKeyframes(withDuration: 10,
//                                       delay: 0,
//                                     options: [.calculationModeLinear, .repeat, .autoreverse],
//                                  animations: {self.wave.center.x += self.wave.bounds.size.width/2},
//                                  completion: nil)

        self.wave2.bounds.size = CGSize(width: self.wave2.bounds.size.width, height: UIScreen.main.bounds.height * 0.45)
        self.wave2.center = CGPoint(x: UIScreen.main.bounds.width - self.wave2.bounds.size.width/2, y: UIScreen.main.bounds.height - self.wave2.bounds.size.height/2)
//        UIView.animateKeyframes(withDuration: 10,
//                                delay: 0,
//                                options: [.calculationModeLinear, .repeat, .autoreverse],
//                                animations: {self.wave2.center.x += self.wave2.bounds.size.width/2},
//                                completion: nil)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return (list.count + 2) / 3
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

		let numberOfSections = (list.count + 2) / 3

		if section == numberOfSections - 1 {
			if list.count % 3 != 0 {
				return list.count % 3
			}
		}

		return 3
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GameShelfCollectionViewCell

		var id = indexPath.item

		if indexPath.section != 0 {
			id = indexPath.section * 3 + indexPath.item
		}

		cell.object.text = list[id].name
		cell.backgroundColor = #colorLiteral(red: 0.8238351941, green: 0.9405960441, blue: 0.9992766976, alpha: 1)

		return cell
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: UIScreen.main.bounds.width * 0.27, height: UIScreen.main.bounds.width * 0.27)
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		var id = indexPath.item

		if indexPath.section != 0 {
			id = indexPath.section * 3 + indexPath.item
		}

		let vc = self.storyboard?.instantiateViewController(withIdentifier: "GameDescription") as! GameDescriptionViewController
		vc.nome = list[id].name
		vc.descricao = list[id].description
        vc.id = list[id].id

		UI.push(vc)
	}

	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

		var reusableView = UICollectionReusableView()

		if kind == UICollectionElementKindSectionFooter {
			let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer", for: indexPath)

			footer.backgroundColor = #colorLiteral(red: 0.9995340705, green: 0.988355577, blue: 0.4726552367, alpha: 0.6997270976)

			reusableView = footer
			collectionView.sendSubview(toBack: reusableView)
		}

		return reusableView
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsetsMake(15, 20, -25, 20)
	}

 func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		collectionView.bringSubview(toFront: cell)
	}

    @IBAction func voltar(_ sender: Any) {
        UI.pop()
    }

}
