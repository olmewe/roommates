import UIKit

class HouseViewController: UIViewController {
	var setInfo:[String:Any]?
    var connecting = false {
        didSet {
            if oldValue != connecting && activity != nil {
                updateActivity()
            }
        }
    }
	var close:((Bool) -> Void)?

    @IBOutlet weak var houseName: UILabel!
    @IBOutlet weak var connected: UILabel!
    @IBOutlet weak var games: UILabel!
    @IBOutlet weak var gameList: UILabel!
    @IBOutlet weak var gamePlaying: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var yesButton: UIButton!

	private var yesButtonText:String?

    override func viewDidLoad() {
        super.viewDidLoad()

		if let info = setInfo {
            setInfo = nil
            set(info:info)
		}
		yesButtonText = yesButton.currentTitle
        updateActivity()
    }

	private func updateActivity() {
		if connecting {
			activity.startAnimating()
            yesButton.setTitle("",for:.normal)
		} else {
			activity.stopAnimating()
            yesButton.setTitle(yesButtonText,for:.normal)
		}
	}

	func updateText(with info:[String:Any]) {
        if houseName == nil {
            setInfo = info
        } else {
            set(info:info)
        }
	}

    private func set(info:[String:Any]) {
		guard let homeInfo = HomeInfo.s(info["info"]),let peerInfo = PeerInfo.s(info["peer"]) else { return }
        houseName.text = peerInfo.name
		switch info["peers"] as? Int ?? 0 {
		case let x where x <= 0: connected.text = "no friends connected"
		case 1: connected.text = "1 friend connected"
		case let x: connected.text = "\(x) friends connected"
		}
		let gameCount = homeInfo.games.count
		switch gameCount {
		case let x where x <= 0: games.text = "no games"
		case 1: games.text = "1 game"
		case let x: games.text = "\(x) games"
		}
		let names = homeInfo.games.compactMap { GameInfo.games[$0]?.name }.prefix(2)
        if names.isEmpty {
            gameList.text = ""
        } else {
            gameList.text = "including: "+names.joined(separator:", ")
        }
		if let game = info["game"] as? String,homeInfo.games.contains(game),let name = GameInfo.games[game]?.name {
			gamePlaying.text = "now playing: \(name)"
		} else {
			gamePlaying.text = ""
		}
    }

	@IBAction func yesPressed(_ sender: Any) {
		close?(true)
    }
	@IBAction func noPressed(_ sender: Any) {
		close?(false)
    }
}
