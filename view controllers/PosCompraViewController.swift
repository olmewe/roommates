import UIKit

class PosCompraViewController: UIViewController {

    @IBOutlet weak var imagemProduto: UIImageView!
    @IBOutlet weak var voltarLoja: UIButton!
    @IBOutlet weak var irCasa: UIButton!

	var imagem = UIImage()

	override func viewDidLoad() {
		super.viewDidLoad()

		voltarLoja.layer.borderWidth = 0.7
		voltarLoja.layer.borderColor = UIColor.black.cgColor
		voltarLoja.clipsToBounds = true

		irCasa.layer.borderWidth = 0.7
		irCasa.layer.borderColor = UIColor.black.cgColor
		irCasa.clipsToBounds = true

		imagemProduto.image = imagem

		// Do any additional setup after loading the view.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

    @IBAction func voltar(_ sender:Any) {
        UI.popToRoot()
    }

    @IBAction func casa(_ sender:Any) {
		GameState.goHome()
        Scene.present(HomeScene())
    }
}
