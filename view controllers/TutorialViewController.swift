import UIKit
import SpriteKit

class TutorialViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var exampleImage: UIImageView!
    @IBOutlet weak var chatText: UITextView!
    @IBOutlet weak var owner: UIImageView!
    @IBOutlet weak var telephone: UIButton!

    @IBOutlet weak var leftArrowButton: UIButton!
    @IBOutlet weak var rightArrowButton: UIButton!

    @IBOutlet weak var nickErrorLbl: UILabel!
    @IBOutlet weak var nickTxtField: UITextField!

	@IBOutlet weak var optionsView: UICollectionView!

	@IBOutlet weak var charImage: UIImageView!
	@IBOutlet weak var charLeftArrowButton: UIButton!
	@IBOutlet weak var charRightArrowButton: UIButton!
	@IBOutlet weak var charName: UILabel!
	@IBOutlet weak var charDesc: UILabel!

	var index = 0
	var indexStack:[Int] = []
	var list:TutorialList!
	var end:(() -> Void)?

	private var telephoneCounter = 0

	private var currentBg:String = ""
	private var currentSprite:String = ""

	private var options:[(String,Int)] = []

	private var nameType:TutorialEntry.NameType = .peer

	private var charIndex = 0
	private var charAtlas:[UIImage] = []
	private var charCounter = 0

	override func viewDidLoad() {
		super.viewDidLoad()

		leftArrowButton.transform = leftArrowButton.transform.rotated(by:.pi)
		charLeftArrowButton.transform = charLeftArrowButton.transform.rotated(by:.pi)
        chatText.font = chatText.font?.withSize(17 * (UIScreen.main.bounds.height/736))
        nickTxtField.delegate = self
        nickErrorLbl.adjustsFontSizeToFitWidth = true
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		animate()
		Timer.scheduledTimer(timeInterval: 1/12, target: self, selector: #selector(animate), userInfo: nil, repeats: true)

		index = 0
		showEntry()
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		end?()
	}

	@objc func animate() {
		telephone.setBackgroundImage(UIImage(named: "tutorial_view/phone\(telephoneCounter)"), for: .normal)
		telephoneCounter += 1
		if telephoneCounter > 2 {
			telephoneCounter = 0
		}
		if !charAtlas.isEmpty {
			charCounter += 1
			if charCounter >= charAtlas.count {
				charCounter = 0
			}
			updateCharImage()
		}
	}

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.resignFirstResponder()
        self.view.endEditing(true)
        if getName() != nil {
            self.nickErrorLbl.isHidden = true
        }
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
		super.touchesBegan(touches,with:event)
    }

    @IBAction func nextButton(_ sender: Any) {
		if !charImage.isHidden {
			GameData.peerInfo.char = PeerInfo.chars[charIndex]
			GameData.updatePeerInfo()
		}
		if !nickTxtField.isHidden {
			guard checkName() else {
				animateInvalidName()
				return
			}
		}
		index = list.entries[index].next ?? (index+1)
		showEntry()
    }

	@IBAction func previousButton(_ sender: Any) {
		guard indexStack.count > 1 else { return }
		indexStack.removeLast()
		index = indexStack.removeLast()
		showEntry()
	}

	func optionSelected(_ index:Int) {
		let i = options[index].1
		if i >= 0 && i < list.entries.count {
			self.index = options[index].1
			showEntry()
		} else {
			UI.pop(self)
		}
	}

	func checkName() -> Bool {
		guard let name = getName() else { return false }
		switch nameType {
		case .peer:
			GameData.peerInfo.name = name
			GameData.updatePeerInfo()
		}
		return true
	}

	func getName() -> String? {
		guard var name = nickTxtField.text else { return nil }
		name = name.trimmingCharacters(in:.whitespacesAndNewlines)
		return name.isEmpty ? nil : name
	}

	func showChar() {
		let char = PeerInfo.chars[charIndex]
		let atlas = SKTextureAtlas(named:"char_\(char)_sprites")
		charAtlas = (0...4).map { UIImage(cgImage:atlas.textureNamed("walk\($0)").cgImage()) }
		charCounter = 0
		updateCharImage()
		charName.text = PeerInfo.chars[charIndex].capitalized
		charDesc.text = PeerInfo.charInfo[PeerInfo.chars[charIndex]] ?? ""
	}

	func updateCharImage() {
		charImage.image = charAtlas[charCounter]
	}

	@IBAction func charChange(_ sender:Any) {
		let n = PeerInfo.chars.count
		if charLeftArrowButton.isEqual(sender) {
			charIndex -= 1
			if charIndex < 0 {
				charIndex = n-1
			}
		} else {
			charIndex += 1
			if charIndex >= n {
				charIndex = 0
			}
		}
		showChar()
	}

	func showEntry() {
		view.endEditing(true)

		let entry = list.entries[index]
		if entry.back {
			indexStack.append(index)
		} else {
			indexStack.removeAll()
		}
		let next = entry.next ?? (index+1)
		let hasNext = next >= 0 && next < list.entries.count

		leftArrowButton.isHidden = !entry.back || indexStack.count < 2
		rightArrowButton.isHidden = !hasNext
		telephone.isHidden = !entry.close && hasNext

		nickTxtField.isHidden = true
		nickErrorLbl.isHidden = true
		optionsView.isHidden = true
		charImage.isHidden = true
		charLeftArrowButton.isHidden = true
		charRightArrowButton.isHidden = true
		charName.isHidden = true
		charDesc.isHidden = true
		charAtlas.removeAll()

		options = []

		let screen:String?

		switch entry.type {
		case .text(let s):
			screen = s
		case .options(let o):
			screen = nil
			options = o
			optionsView.isHidden = false
			optionsView.reloadData()
		case .name(let n):
			screen = "x_name"
			nameType = n
			nickTxtField.isHidden = false
			switch n {
			case .peer: nickTxtField.text = GameData.peerInfo.name
			}
		case .char:
			screen = nil
			charIndex = PeerInfo.chars.index(of:GameData.peerInfo.char) ?? 0
			charImage.isHidden = false
			charLeftArrowButton.isHidden = false
			charRightArrowButton.isHidden = false
			charName.isHidden = false
			charDesc.isHidden = false
			showChar()
		}

		chatText.text = list.entries[index].message
			.replacingOccurrences(of:"{peer}",with:GameData.peerInfo.name)
			.replacingOccurrences(of:"{char}",with:GameData.peerInfo.char.capitalized)
		if let s = screen {
			exampleImage.image = UIImage(named:"tutorial_screens/"+s)
			exampleImage.isHidden = false
		} else {
			exampleImage.isHidden = true
		}
		let newBg = entry.bg
		if currentBg != newBg {
			currentBg = newBg
			backgroundImage.image = UIImage(named:"tutorial_view/bg_"+currentBg)
		}
		let newSprite = entry.bg+"_"+entry.sprite
		if currentSprite != newSprite {
			currentSprite = newSprite
			owner.image = UIImage(named:"tutorial_char/"+currentSprite)
		}
	}

    func animateInvalidName() {
		nickErrorLbl.isHidden = false
		let x = view.center.x
		let w = view.frame.width*0.05
        UIView.animate(withDuration: 0.1, animations: {self.nickErrorLbl.center.x = x+w}, completion: { (complete: Bool) in
            UIView.animate(withDuration: 0.1, animations: {self.nickErrorLbl.center.x = x-w}, completion: { (complete: Bool) in
                UIView.animate(withDuration: 0.1, animations: {self.nickErrorLbl.center.x = x}, completion: nil)
            })
        })
    }

    @IBAction func closeButton(_ sender: Any) {
        UI.pop(self)
    }
}

extension TutorialViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView:UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAt indexPath:IndexPath) -> CGSize {
		return CGSize(width:optionsView.bounds.width,height:optionsView.bounds.height/CGFloat(options.count+1))
	}

	func numberOfSections(in collectionView:UICollectionView) -> Int {
		return 1
	}

	func collectionView(_ collectionView:UICollectionView,numberOfItemsInSection section:Int) -> Int {
		return options.count
	}

	func collectionView(_ collectionView:UICollectionView,layout collectionViewLayout:UICollectionViewLayout,insetForSectionAt section:Int) -> UIEdgeInsets {
		return .zero
	}

	func collectionView(_ collectionView:UICollectionView,cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
		let i = indexPath.item
		let cell = optionsView.dequeueReusableCell(withReuseIdentifier:"cell",for:indexPath) as! TutorialOptionViewCell
		cell.index = i
		cell.vc = self
		cell.button.setTitle(options[i].0,for:.normal)
		return cell
	}

}

class TutorialOptionViewCell:UICollectionViewCell {
	@IBOutlet weak var button:UIButton!
	var index:Int = 0
	var vc:TutorialViewController!

	@IBAction func press(_ sender:Any) {
		vc.optionSelected(index)
	}
}

class TutorialList {
	var entries:[TutorialEntry]

	init(entries:[TutorialEntry]) {
		self.entries = entries
	}

	init(parse e:String) {
		var rawEntries:[TutorialEntry] = []

		var bg = ""
		var sprite = ""
		var message = ""
		var next:Int?
		var back = true
		var close = true

		var screen:String?
		var options:[(String,Int)] = []
		var name:TutorialEntry.NameType?
		var char = false

		var labels:[String:Int] = [:]
		var strings:[String] = []

		func defaults() {
			message = ""
			next = nil
			options.removeAll()
			name = nil
			char = false
		}
		func checkAdd() {
			if !message.isEmpty {
				let type:TutorialEntry.EntryType
				if !options.isEmpty {
					type = .options(options)
					next = -1
				} else if let n = name {
					type = .name(n)
				} else if char {
					type = .char
				} else {
					type = .text(screen)
				}
				rawEntries.append(TutorialEntry(
					bg:bg,
					sprite:sprite,
					message:message,
					next:next,
					back:back,
					close:close,
					type:type
				))
				defaults()
			}
		}
		for var s in e.split(separator:"\n").map({ $0.trimmingCharacters(in:.whitespacesAndNewlines) }) {
			if s.isEmpty { continue }
			while s.hasPrefix("[") {
				checkAdd()
				let ss = s.dropFirst().split(separator:"]",maxSplits:1)
				let m = ss[0].split(separator:":",maxSplits:1)
				let a = String(m[0].trimmingCharacters(in:.whitespacesAndNewlines))
				let b = m.count > 1 ? String(m[1].trimmingCharacters(in:.whitespacesAndNewlines)) : ""
				switch a {
				case "bg": bg = b
				case "sp": sprite = b
				case "sc": screen = b.isEmpty ? nil : b
				case "back":
					switch b {
					case "on": back = true
					case "off": back = false
					default: break
					}
				case "close":
					switch b {
					case "on": close = true
					case "off": close = false
					default: break
					}
				case "name":
					switch b {
					case "peer": name = .peer
					default: break
					}
				case "char": char = true
				default: break
				}
				s = ss.count > 1 ? ss[1].trimmingCharacters(in:.whitespacesAndNewlines) : ""
			}
			if s.hasPrefix("-") {
				let m = s.dropFirst().split(separator:":",maxSplits:1)
				let a = String(m[0].trimmingCharacters(in:.whitespacesAndNewlines))
				let b = m.count > 1 ? String(m[1].trimmingCharacters(in:.whitespacesAndNewlines)) : a
				let i = strings.count
				strings.append(a)
				options.append((b,i))
			} else if s.hasPrefix("+") {
				checkAdd()
				let n = String(s.dropFirst().trimmingCharacters(in:.whitespacesAndNewlines))
				labels[n] = rawEntries.count
			} else if s.hasPrefix(">") {
				let n = String(s.dropFirst().trimmingCharacters(in:.whitespacesAndNewlines))
				next = strings.count
				strings.append(n)
			} else {
				checkAdd()
				message = s.split(separator:"\\").map { $0.trimmingCharacters(in:.whitespacesAndNewlines) }.joined(separator:"\n")
			}
		}
		checkAdd()
		entries = rawEntries.map { e in
			let next:Int?
			if let n = e.next {
				next = n >= 0 && n < strings.count ? labels[strings[n]] : n
			} else {
				next = nil
			}
			var type = e.type
			if case let .options(o) = type {
				type = .options(o.map { ($0.0,labels[strings[$0.1]] ?? -1) })
			}
			return TutorialEntry(
				bg:e.bg,
				sprite:e.sprite,
				message:e.message,
				next:next,
				back:e.back,
				close:e.close,
				type:type
			)
		}
	}
}

struct TutorialEntry {
	var bg:String
	var sprite:String
	var type:EntryType
	var message:String
	var next:Int?
	var back:Bool
	var close:Bool

	init(bg:String,sprite:String,message:String,next:Int?,back:Bool,close:Bool,type:EntryType) {
		self.bg = bg
		self.sprite = sprite
		self.message = message
		self.next = next
		self.back = back
		self.close = close
		self.type = type
	}

	enum EntryType {
		case text(String?)
		case name(NameType)
		case options([(String,Int)])
		case char
	}

	enum NameType {
		case peer
	}
}
