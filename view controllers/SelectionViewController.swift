import UIKit
import SpriteKit

class SelectionViewController:UIViewController {
	@IBOutlet weak var skView:SKView!
	@IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var errorLabel:UILabel!
    @IBOutlet weak var joinButton:UIButton!
    @IBOutlet weak var playButton:UIButton!

	let client = SelectionViewClient()
	let scene = SelectionSofaScene()

	var minPeers:Int = 1
	var maxPeers:Int? = nil
	var end:((Bool,[String]) -> Void)? = nil

    var joinMode = false
	var startGame = false

    var joinImage:[UIImage?] = []
    var leaveImage:[UIImage?] = []
    var charImage:[String:(Int,[UIImage])] = [:]
    var imageIndex:Int = 0
    var frameTimer:Timer? = nil

	var sfxJoin:Data?
	var sfxLeave:Data?

    override func viewDidLoad() {
        super.viewDidLoad()

        joinImage = (0...2).map { UIImage(named:"join_view/join\($0)") }
        leaveImage = (0...2).map { UIImage(named:"join_view/leave\($0)") }

		sfxJoin = Audio.data(of:"app/selection_join.wav")
		sfxLeave = Audio.data(of:"app/selection_leave.wav")

		skView.ignoresSiblingOrder = true
		skView.showsFPS = false
		skView.showsNodeCount = false
		skView.showsDrawCount = false
		scene.set()
		skView.presentScene(scene)

        updatePlayers()
        frameTimer = Timer.scheduledTimer(timeInterval:1/12,target:self,selector:#selector(updateTimer),userInfo:nil,repeats:true)
    }

	override func viewWillAppear(_ animated:Bool) {
		super.viewWillAppear(animated)

		client.delegate = self
		GameState.par.start(client:client)
		scene.sofaNode.register()
	}

	override func viewWillDisappear(_ animated:Bool) {
		super.viewWillDisappear(animated)

		end?(startGame,client.players)
		GameState.par.stop(client:client)
		scene.sofaNode.deregister()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

	func playAudio(join:Bool) {
		Audio.play(join ? sfxJoin : sfxLeave,volume:0.3)
	}

    func updatePlayers() {
		updateInsets()
        collectionView.reloadData()
        if client.players.count < minPeers {
            playButton.isHidden = true
            errorLabel.isHidden = false
            errorLabel.text = "not enough players! (min. \(minPeers))"
        } else if let maxPeers = maxPeers,client.players.count > maxPeers {
            playButton.isHidden = true
            errorLabel.isHidden = false
            errorLabel.text = "too many players! (max. \(maxPeers))"
        } else {
            playButton.isHidden = false
            errorLabel.isHidden = true
        }
        if client.players.contains(GameData.id) {
            joinMode = true
            joinButton.setTitle("leave",for:.normal)
        } else {
            joinMode = false
            joinButton.setTitle("join!",for:.normal)
        }
        updateSprites()
    }

    @objc func updateTimer() {
        imageIndex += 1
        if imageIndex > 2 { imageIndex = 0 }
        updateSprites()
    }

    func updateSprites(_ cell:SelectionCollectionViewCell? = nil) {
        joinButton.setBackgroundImage((joinMode ? leaveImage : joinImage)[imageIndex],for:.normal)
		let cells = (collectionView.visibleCells.map { $0 as? SelectionCollectionViewCell }+[cell]).compactMap { $0 }
        for cell in cells {
            cell.charImage.image = charImage[cell.char]?.1[imageIndex]
        }
    }

    func updateTextures(char:String,add:Bool) {
        if add {
            if var img = charImage[char] {
                img.0 += 1
                charImage[char] = img
            } else {
                let atlas = SKTextureAtlas(named:"char_\(char)_sprites")
                charImage[char] = (1,(0...2).map { UIImage(cgImage:atlas.textureNamed("still\($0)").cgImage()) })
            }
        } else if var img = charImage[char] {
            if img.0 > 1 {
                img.0 -= 1
                charImage[char] = img
            } else {
                charImage[char] = nil
            }
        }
    }

	func updateInsets() {
		let totalHeight = CGFloat(numberOfSections(in:collectionView)*cellSize.height)
		let y = totalHeight < collectionHeight ? (collectionHeight-totalHeight)*0.5 : 0
		collectionView.contentInset = UIEdgeInsets(top:y,left:0,bottom:y,right:0)
	}

    @IBAction func backPressed(_ sender: Any) {
		startGame = false
        UI.pop(self)
    }

    @IBAction func joinPressed(_ sender: Any) {
        let join = client.toggle()
        playAudio(join:join)
        updateTextures(char:GameData.peerInfo.char,add:join)
        updatePlayers()
    }

    @IBAction func playPressed(_ sender: Any) {
        if client.players.count < minPeers { return }
        if let maxPeers = maxPeers,client.players.count > maxPeers { return }
		client.sendStart()
		startGame = true
		UI.pop(self)
    }
}

extension SelectionViewController:SelectionViewClientDelegate {
	func selectionViewClient(_ client:SelectionViewClient,peer:String,connected:Bool) {
		playAudio(join:connected)
        if let char = GameState.peerInfo(for:peer)?.char {
            updateTextures(char:char,add:connected)
        }
		updatePlayers()
	}

	func selectionViewClient(_ client:SelectionViewClient,start:Bool) {
		guard start else { return }
		startGame = false
		UI.pop(self)
	}
}

extension SelectionViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var items:Int {
        return Game.pad ? 5 : 3
    }

	var collectionWidth:CGFloat {
		return collectionView.bounds.width
	}

	var collectionHeight:CGFloat {
		return collectionView.bounds.height
	}

	var cellSize:(width:Int,height:Int) {
		let w = Int(collectionWidth*0.93/CGFloat(items))
        return (w,11*w/10)
    }

	func collectionView(_ collectionView:UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAt indexPath:IndexPath) -> CGSize {
		let size = cellSize
		return CGSize(width:size.width,height:size.height)
    }

    func numberOfSections(in collectionView:UICollectionView) -> Int {
		return (client.players.count + items - 1) / items
    }

	func collectionView(_ collectionView:UICollectionView,numberOfItemsInSection section:Int) -> Int {
		return (client.players.count - section * items).clamp(0,items)
	}

    func collectionView(_ collectionView:UICollectionView,layout collectionViewLayout:UICollectionViewLayout,insetForSectionAt section:Int) -> UIEdgeInsets {
		let totalWidth = CGFloat(self.collectionView(collectionView,numberOfItemsInSection:section)*cellSize.width)
		let x = (collectionWidth-totalWidth)*0.5
        return UIEdgeInsets(top:0,left:x,bottom:0,right:x)
    }

    func collectionView(_ collectionView:UICollectionView,cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"SelectionCell",for:indexPath) as! SelectionCollectionViewCell
		let peer = client.players[(indexPath.item+items*indexPath.section).clamp(0,client.players.count-1)]
        let info = GameState.peerInfo(for:peer)
        cell.peerName.text = info?.name ?? "..."
        cell.char = info?.char ?? PeerInfo.chars[Random.range(PeerInfo.chars.count)]
		cell.backgroundColor = .clear
        updateSprites(cell)
		return cell
    }

}

class SelectionCollectionViewCell:UICollectionViewCell {
    @IBOutlet weak var peerName:UILabel!
    @IBOutlet weak var charImage:UIImageView!
    var char:String = ""
}

class SelectionSofaScene:SKScene {
	let sofaNode = HomeSofaNode()
	let cam = SKCameraNode()

	func set() {
		backgroundColor = .clear
		size = CGSize(width:720,height:600*720/1242)
		scaleMode = .aspectFit
		camera = cam

		addChild(sofaNode)
		sofaNode.texture = SKTextureAtlas(named:"home_theme_"+GameState.homeInfo.theme).textureNamed("sofa")
		sofaNode.position = CGPoint(x:0,y:-50)
	}

	override func update(_ currentTime:TimeInterval) {
		updateRoutine()
	}
}
