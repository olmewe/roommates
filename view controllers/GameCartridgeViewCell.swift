import UIKit

class GameCartridgeViewCell:UICollectionViewCell {
	@IBOutlet weak var icon:UIImageView!
    @IBOutlet weak var cartridge:UIImageView!
    @IBOutlet weak var name:UILabel!

	private static var cartridgeFrames:[UIImage?] = []
	private static var transformFrames:[CGAffineTransform] = []
	private static var offsetFrames:[CGPoint?] = []

	private var active = false
	private var currentFrame:Int = 0
	private var frameTimer:Timer? = nil

	override func awakeFromNib() {
		super.awakeFromNib()
		GameCartridgeViewCell.populateFrames()
	}

	private static func populateFrames() {
		guard cartridgeFrames.isEmpty else { return }
		cartridgeFrames = (0...3).map { UIImage(named:"cartridge/cartridge\($0)") }
		transformFrames = [
			CGAffineTransform(a:1,b:0,c:0.2,d:1,tx:0,ty:0).scaledBy(x:0.7,y:0.85),
			CGAffineTransform(a:1,b:0,c:0.0875,d:1,tx:0,ty:0).scaledBy(x:0.75,y:0.87),
			CGAffineTransform(scaleX:0.93,y:0.93),
			CGAffineTransform.identity,
		]
		offsetFrames = [
			CGPoint(x:0.105,y:0),
			CGPoint(x:0.0475,y:0),
			nil,
			nil,
		]

	}

	func set(game id:String) {
		if let game = GameInfo.games[id] {
			name.text = game.name
			icon.image = UIImage(named:game.assetsFolder+"/icon")
		} else {
			name.text = "?"
			icon.image = UIImage(named:"console_view/icon")
		}
	}

	func forceSet(active:Bool) {
		self.active = active
		currentFrame = active ? (GameCartridgeViewCell.cartridgeFrames.count-1) : 0
		stopTimer()
		applyCurrentFrame()
	}

	func set(active:Bool) {
		self.active = active
		if frameTimer == nil {
			updateFrame()
		}
	}

	@objc func updateFrame() {
		if active {
			currentFrame += 1
			if currentFrame >= GameCartridgeViewCell.cartridgeFrames.count-1 {
				currentFrame = GameCartridgeViewCell.cartridgeFrames.count-1
				stopTimer()
			} else {
				startTimer()
			}
		} else {
			currentFrame -= 1
			if currentFrame <= 0 {
				currentFrame = 0
				stopTimer()
			} else {
				startTimer()
			}
		}
		applyCurrentFrame()
	}

	func startTimer() {
		if frameTimer == nil {
			frameTimer = Timer.scheduledTimer(timeInterval:0.05,target:self,selector:#selector(updateFrame),userInfo:nil,repeats:true)
		}
	}

	func stopTimer() {
		if let timer = frameTimer {
			timer.invalidate()
			frameTimer = nil
		}
	}

	private func applyCurrentFrame() {
		let tr = GameCartridgeViewCell.transformFrames[currentFrame]
		if let offset = GameCartridgeViewCell.offsetFrames[currentFrame] {
			icon.transform = tr.translatedBy(x:offset.x*frame.size.width,y:offset.y*frame.size.width)
		} else {
			icon.transform = tr
		}
		cartridge.image = GameCartridgeViewCell.cartridgeFrames[currentFrame]
	}
}
