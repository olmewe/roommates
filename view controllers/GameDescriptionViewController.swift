import UIKit

class GameDescriptionViewController: UIViewController {

	@IBOutlet weak var nomeProduto: UILabel!
	@IBOutlet weak var gameImage: UIImageView!
	@IBOutlet weak var valorProduto: UILabel!
	@IBOutlet weak var viewDescricao: UIView!
	@IBOutlet weak var descriptGame: UITextView!
	@IBOutlet weak var botaoVoltar: UIButton!
	@IBOutlet weak var botaoComprar: UIButton!

	var nome = String()
	var imagemProduto = UIImage()
	var valor = String()
	var descricao = String()
    var id = String()

	var comprar = false

	override func viewDidLoad() {
		super.viewDidLoad()

		nomeProduto.text = nome
		descriptGame.text = descricao

		valorProduto.layer.cornerRadius = 10
		valorProduto.layer.masksToBounds = true

		viewDescricao.layer.borderWidth = 0.7
		viewDescricao.layer.borderColor = UIColor.black.cgColor
		viewDescricao.clipsToBounds = true

		botaoVoltar.layer.borderWidth = 0.7
		botaoVoltar.layer.borderColor = UIColor.black.cgColor
		botaoVoltar.clipsToBounds = true

		botaoComprar.layer.borderWidth = 0.7
		botaoComprar.layer.borderColor = UIColor.black.cgColor
		botaoComprar.clipsToBounds = true

		if !comprar {
			botaoVoltar.isUserInteractionEnabled = true
			botaoComprar.isUserInteractionEnabled = true
		}

		// Do any additional setup after loading the view.
	}

    override func viewWillAppear(_ animated: Bool) {
        if GameData.homeInfo.games.contains(id) {
            valor = "Voce possui esse biriguinaite!"
            valorProduto.text = "Voce possui esse biriguinaite!"

            botaoComprar.isUserInteractionEnabled = false
        }
    }

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@IBAction func comprar(_ sender: Any) {

		let vc = self.storyboard?.instantiateViewController(withIdentifier: "Popup_confimacao") as! ConfirmationViewController
		vc.imagem = gameImage.image!
        vc.id = id

		UI.push(vc)
	}

	@IBAction func voltar(_ sender: Any) {
		UI.popToRoot()
	}
}
