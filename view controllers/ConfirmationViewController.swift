import UIKit

class ConfirmationViewController: UIViewController {

	@IBOutlet weak var viewConfirmacao: UIView!

	var imagem = UIImage()
    var id = String()

	override func viewDidLoad() {
		super.viewDidLoad()

		viewConfirmacao.layer.cornerRadius = 10
		viewConfirmacao.layer.masksToBounds = true

		// Do any additional setup after loading the view.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@IBAction func voltar(_ sender: Any) {
		UI.pop()
	}

	@IBAction func confirmar(_ sender: Any) {
		let vc = storyboard?.instantiateViewController(withIdentifier: "GameDescription") as! GameDescriptionViewController
		vc.comprar = true

        UI.pop(animated:true) {
			GameData.acquire(game:self.id)

			let pos = self.storyboard?.instantiateViewController(withIdentifier: "posCompra") as! PosCompraViewController

			pos.imagem = self.imagem

			UI.push(pos)
		}
	}
}
