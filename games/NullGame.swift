import SpriteKit

class NullGameClient:GameClient {
	init() {
		super.init(name:"nullgame")
	}

	override func presentDefaultScene() {
		let scene = GameIdleScene()
		scene.message = .gameNotFound
		Scene.present(scene)
	}
}
