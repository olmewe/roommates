import Foundation

class SheepServer:ParServer {
	var state:State = .idle
	enum State {
		case idle
		case loaded
		case playing
		case end
	}
	var player1:String = ""
	var player2:String = ""
	var turnPlayer1 = false
	var readyPlayer1 = false
	var readyPlayer2 = false
	var loadedGame:SaveGame? = nil
	class SaveGame {
		var turnPlayer1 = false
		let gridPlayer1 = BoolGrid(width:Sheep.width,height:Sheep.height)
		let gridPlayer2 = BoolGrid(width:Sheep.width,height:Sheep.height)
		let hitPlayer1 = BoolGrid(width:Sheep.width,height:Sheep.height)
		let hitPlayer2 = BoolGrid(width:Sheep.width,height:Sheep.height)
	}
	let gridPlayer1 = BoolGrid(width:Sheep.width,height:Sheep.height)
	let gridPlayer2 = BoolGrid(width:Sheep.width,height:Sheep.height)
	let hitPlayer1 = BoolGrid(width:Sheep.width,height:Sheep.height)
	let hitPlayer2 = BoolGrid(width:Sheep.width,height:Sheep.height)

	init() {
		super.init(name:Sheep.serviceName)
	}

	override func started() {
		resetState()
	}

	override func stopped() {
		resetState()
	}

	func resetState() {
		state = .idle
		player1 = ""
		player2 = ""
		turnPlayer1 = false
		readyPlayer1 = false
		readyPlayer2 = false
		loadedGame = nil
		gridPlayer1.clear()
		gridPlayer2.clear()
		hitPlayer1.clear()
		hitPlayer2.clear()
	}

	override func connected(peer:String) {
		sendState(to:[peer])
	}

	override func disconnected(peer:String) {
		if state != .idle && (player1 == peer || player2 == peer) {
			let add:[String:Any] = state == .end ? [:] : ["flag":"disconnect"]
			resetState()
			sendState(add)
		}
	}

	override func received(data:Data,from peer:String) {
		guard let json = Json.parse(data),let name = json["?"] as? String else { return }
		switch name {
		case "start":
			guard state == .idle else { break }
			guard let p1 = json["p1"] as? String,let p2 = json["p2"] as? String else { break }
			let peerComparison = p1.compare(p2)
			guard peerComparison != .orderedSame && peers.contains(p1) && peers.contains(p2) else { break }
			resetState()
			if peerComparison == .orderedAscending {
				player1 = p1
				player2 = p2
			} else {
				player1 = p2
				player2 = p1
			}
			loadGame()
			state = loadedGame == nil ? .playing : .loaded
			sendState()
		case "stop":
			guard state != .idle else { break }
			let add:[String:Any] = state == .end ? [:] : ["flag":"close"]
			resetState()
			sendState(add)
		case "load":
			guard state == .loaded && playerMatch(peer) != nil,let s = loadedGame else { break }
			state = .playing
			if json["load"] as? Bool == true {
				turnPlayer1 = s.turnPlayer1
				gridPlayer1.copy(s.gridPlayer1)
				gridPlayer2.copy(s.gridPlayer2)
				hitPlayer1.copy(s.hitPlayer1)
				hitPlayer2.copy(s.hitPlayer2)
				readyPlayer1 = true
				readyPlayer2 = true
			} else {
				deleteGame()
			}
			loadedGame = nil
			sendState()
		case "ready":
			guard state == .playing && !(readyPlayer1 && readyPlayer2),let p1 = playerMatch(peer) else { break }
			let grid = json["grid"]
			if p1 {
				readyPlayer1 = gridPlayer1.set(grid) && gridPlayer1.width == Sheep.width && gridPlayer1.height == Sheep.height
			} else {
				readyPlayer2 = gridPlayer2.set(grid) && gridPlayer2.width == Sheep.width && gridPlayer2.height == Sheep.height
			}
			if readyPlayer1 && readyPlayer2 {
				turnPlayer1 = Random.bool
				saveGame()
			}
			sendState()
		case "attack":
			guard state == .playing && readyPlayer1 && readyPlayer2,let p1 = playerMatch(peer),p1 == turnPlayer1 else { break }
			guard let x = json["x"] as? Int,x >= 0 && x < Sheep.width else { break }
			guard let y = json["y"] as? Int,y >= 0 && y < Sheep.height else { break }
			let grid:BoolGrid
			let hit:BoolGrid
			if p1 {
				grid = gridPlayer2
				hit = hitPlayer2
			} else {
				grid = gridPlayer1
				hit = hitPlayer1
			}
			guard !hit[x,y] else { break }
			hit[x,y] = true
			if fix(grid:grid,hit:hit) {
				state = .end
				deleteGame()
			} else {
				if !grid[x,y] {
					turnPlayer1 = !turnPlayer1
				}
				saveGame()
			}
			sendState(["p1attack":p1,"ax":x,"ay":y])
		case "selection":
			if let peers = peers(excluding:peer),let data = Json.data(["?":"selection"]) {
				send(data:data,to:peers,reliable:true)
			}
		default: break
		}
	}

	func fix(grid:BoolGrid,hit:BoolGrid) -> Bool {
		var finished = true
		for sheep in Sheep.findSheep(in:grid) {
			if sheep.length == 1 {
				if hit[sheep.x,sheep.y] {
					hit[sheep.x-1,sheep.y-1] = true
					hit[sheep.x-1,sheep.y  ] = true
					hit[sheep.x-1,sheep.y+1] = true
					hit[sheep.x  ,sheep.y+1] = true
					hit[sheep.x+1,sheep.y+1] = true
					hit[sheep.x+1,sheep.y  ] = true
					hit[sheep.x+1,sheep.y-1] = true
					hit[sheep.x  ,sheep.y-1] = true
				} else {
					finished = false
				}
			} else if sheep.rotated {
				var completed = true
				for i in 0..<sheep.length {
					if hit[sheep.x,sheep.y+i] {
						hit[sheep.x-1,sheep.y+i-1] = true
						hit[sheep.x+1,sheep.y+i-1] = true
						hit[sheep.x+1,sheep.y+i+1] = true
						hit[sheep.x-1,sheep.y+i+1] = true
					} else {
						completed = false
					}
				}
				if completed {
					hit[sheep.x,sheep.y-1] = true
					hit[sheep.x,sheep.y+sheep.length] = true
				} else {
					finished = false
				}
			} else {
				var completed = true
				for i in 0..<sheep.length {
					if hit[sheep.x+i,sheep.y] {
						hit[sheep.x+i-1,sheep.y-1] = true
						hit[sheep.x+i+1,sheep.y-1] = true
						hit[sheep.x+i+1,sheep.y+1] = true
						hit[sheep.x+i-1,sheep.y+1] = true
					} else {
						completed = false
					}
				}
				if completed {
					hit[sheep.x-1,sheep.y] = true
					hit[sheep.x+sheep.length,sheep.y] = true
				} else {
					finished = false
				}
			}
		}
		return finished
	}

	func sendState(_ add:[String:Any] = [:]) {
		sendState(to:peers,add)
	}

	func sendState(to peers:[String],_ add:[String:Any] = [:]) {
		var json:[String:Any] = ["?":"state"]
		if state != .idle {
			json["p1"] = player1
			json["p2"] = player2
			if state == .loaded {
				json["state"] = "loaded"
			} else {
				json["state"] = state == .playing ? "playing" : "end"
				if readyPlayer1 {
					if readyPlayer2 {
						json["p1turn"] = turnPlayer1
						json["p1grid"] = gridPlayer1.s
						json["p1hit"] = hitPlayer1.s
						json["p2grid"] = gridPlayer2.s
						json["p2hit"] = hitPlayer2.s
					} else {
						json["p1grid"] = gridPlayer1.s
					}
				} else if readyPlayer2 {
					json["p2grid"] = gridPlayer2.s
				}
			}
		}
		for (k,v) in add {
			json[k] = v
		}
		if let data = Json.data(json) {
			send(data:data,to:peers,reliable:true)
		}
	}

	func playerMatch(_ peer:String) -> Bool? {
		switch peer {
		case player1: return true
		case player2: return false
		default: return nil
		}
	}

	func loadGame() {
		func parse() -> SaveGame? {
			guard let json = GameData.loadJson(forKey:saveKey()) else { return nil }
			guard let turn = json["turn"] as? Bool else { return nil }
			let s = SaveGame()
			s.turnPlayer1 = turn
			guard s.gridPlayer1.set(json["grid1"]) else { return nil }
			guard s.gridPlayer2.set(json["grid2"]) else { return nil }
			guard s.hitPlayer1.set(json["hit1"]) else { return nil }
			guard s.hitPlayer2.set(json["hit2"]) else { return nil }
			return s
		}
		loadedGame = parse()
	}

	func saveGame() {
		GameData.saveJson(["turn":turnPlayer1,"grid1":gridPlayer1.s,"grid2":gridPlayer2.s,"hit1":hitPlayer1.s,"hit2":hitPlayer2.s],forKey:saveKey())
	}

	func deleteGame() {
		GameData.deleteJson(forKey:saveKey())
	}

	func saveKey() -> String {
		return "sheep_savedata_\(player1)_\(player2)"
	}
}
