import UIKit

class SheepConfirmViewController:UIViewController {
	@IBOutlet weak var text:UILabel!
	@IBOutlet weak var image:UIImageView!

	var load = false
	var yes:(() -> Void) = {}
	var no:(() -> Void) = {}

	override func viewWillAppear(_ animated:Bool) {
		super.viewWillAppear(animated)
		if load {
			text.text = "do you wanna continue\nthe previous game?"
			if let img = Sheep.atlas?.textureNamed("load").cgImage() {
				image.image = UIImage(cgImage:img)
			}
		} else {
			text.text = "are you sure you\nwanna leave this game?"
			if let img = Sheep.atlas?.textureNamed("confirm").cgImage() {
				image.image = UIImage(cgImage:img)
			}
		}
	}

	@IBAction func pressYes(_ sender:Any) {
		yes()
	}

	@IBAction func pressNo(_ sender:Any) {
		no()
	}
}
