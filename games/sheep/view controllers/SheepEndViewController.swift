import UIKit

class SheepEndViewController:UIViewController {
	@IBOutlet weak var text:UILabel!
	@IBOutlet weak var image:UIImageView!

	var win:WinFlag = .player
	enum WinFlag {
		case player
		case opponent
		case other(String)
	}
	var end:(() -> Void) = {}

	override func viewWillAppear(_ animated:Bool) {
		super.viewWillAppear(animated)
		let i:String
		switch win {
		case .player:
			text.text = "you win!"
			i = "win"
		case .opponent:
			text.text = "opponent wins!"
			i = "lose"
		case .other(let name):
			text.text = "\(name) wins!"
			i = "win"
		}
		if let img = Sheep.atlas?.textureNamed(i).cgImage() {
			image.image = UIImage(cgImage:img)
		}
	}

	@IBAction func press(_ sender:Any) {
		end()
	}
}

