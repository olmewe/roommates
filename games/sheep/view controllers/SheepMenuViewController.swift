import UIKit

class SheepMenuViewController:UIViewController {
    @IBOutlet weak var text:UILabel!
    @IBOutlet weak var image:UIImageView!

    var flag:Sheep.Flag = .none
    var end:(() -> Void) = {}

    override func viewWillAppear(_ animated:Bool) {
		super.viewWillAppear(animated)
        if flag == .disconnect {
            text.text = "connection was lost!"
            if let img = Sheep.atlas?.textureNamed("disconnect").cgImage() {
                image.image = UIImage(cgImage:img)
            }
        } else {
            text.text = "game was closed!"
            if let img = Sheep.atlas?.textureNamed("close").cgImage() {
                image.image = UIImage(cgImage:img)
            }
        }
	}

    @IBAction func press(_ sender:Any) {
        end()
    }
}
