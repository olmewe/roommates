import Foundation

class SheepClient:GameClient,SceneNode {
	init() {
		super.init(name:Sheep.serviceName)
	}

	override func gameStarted() {
		Sheep.load()
		Sheep.state = .loading
		Sheep.scene = .none
	}

	override func gameStopped() {
		Sheep.unload()
		Sheep.state = .loading
		Sheep.scene = .none
	}

	func lateUpdate() {
		Sheep.selection = false
		Sheep.attack = nil
		Sheep.flag = .none
	}

	override func received(data:Data) {
		guard let json = Json.parse(data),let name = json["?"] as? String else { return }
		print("[sheep client] \(json)")
		switch name {
		case "state":
			if let state = json["state"] as? String,let p1 = json["p1"] as? String,let p2 = json["p2"] as? String {
				switch GameData.id {
				case p1: Sheep.playerMatch = true
				case p2: Sheep.playerMatch = false
				default: Sheep.playerMatch = nil
				}
				Sheep.player1 = p1
				Sheep.player2 = p2
				switch state {
				case "loaded": Sheep.state = .loaded
				case "playing": Sheep.state = .playing
				case "end": Sheep.state = .end
				default: break
				}
				if let turnPlayer1 = json["p1turn"] as? Bool {
					Sheep.turnPlayer1 = turnPlayer1
				}
				if Sheep.gridPlayer1.set(json["p1grid"]) {
					Sheep.readyPlayer1 = true
					_ = Sheep.hitPlayer1.set(json["p1hit"])
				} else {
					Sheep.readyPlayer1 = false
					Sheep.gridPlayer1.clear()
					Sheep.hitPlayer1.clear()
				}
				if Sheep.gridPlayer2.set(json["p2grid"]) {
					Sheep.readyPlayer2 = true
					_ = Sheep.hitPlayer2.set(json["p2hit"])
				} else {
					Sheep.readyPlayer2 = false
					Sheep.gridPlayer2.clear()
					Sheep.hitPlayer2.clear()
				}
				if let p1attack = json["p1attack"] as? Bool,let x = json["ax"] as? Int,let y = json["ay"] as? Int {
					Sheep.attack = (p1:p1attack,x:x,y:y)
				}
				Sheep.flag = .none
				Sheep.stateUpdate = true
			} else {
				Sheep.player1 = ""
				Sheep.player2 = ""
				Sheep.state = .idle
				if let flag = json["flag"] as? String {
					switch flag {
					case "disconnect": Sheep.flag = .disconnect
					case "close": Sheep.flag = .close
					default: break
					}
				}
				Sheep.attack = nil
			}
			let oldScene = Sheep.scene
			if Sheep.state == .idle {
				Sheep.scene = .menu
			} else if Sheep.playerMatch != nil || (Sheep.readyPlayer1 && Sheep.readyPlayer2) {
				Sheep.scene = .game
			} else {
				Sheep.scene = .arranging
			}
			if oldScene == .none {
				_ = Sheep.changeScene(from:.none)
			}
		case "selection": Sheep.selection = true
		default: break
		}
	}

	func sendStart(p1:String,p2:String) {
		if let data = Json.data(["?":"start","p1":p1,"p2":p2]) {
			send(data:data,reliable:true)
		}
	}

	func sendStop() {
		if let data = Json.data(["?":"stop"]) {
			send(data:data,reliable:true)
		}
	}

	func sendLoad(load:Bool) {
		if let data = Json.data(["?":"load","load":load]) {
			send(data:data,reliable:true)
		}
	}

	func sendReady(grid:BoolGrid?) {
		if let grid = grid {
			if let data = Json.data(["?":"ready","grid":grid.s]) {
				send(data:data,reliable:true)
			}
		} else {
			if let data = Json.data(["?":"ready"]) {
				send(data:data,reliable:true)
			}
		}
	}

	func sendAttack(x:Int,y:Int) {
		if let data = Json.data(["?":"attack","x":x,"y":y]) {
			send(data:data,reliable:true)
		}
	}

	func sendSelection() {
		if let data = Json.data(["?":"selection"]) {
			send(data:data,reliable:true)
		}
	}
}
