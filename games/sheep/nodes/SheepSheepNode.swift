import SpriteKit

class SheepSheepNode:SKNode,SceneNode {
	let length:Int
	var width:Int {
		return rotated ? 1 : length
	}
	var height:Int {
		return rotated ? length : 1
	}

	var rotated:Bool
	var x:Int = 0
	var y:Int = 0
	var hit:[Bool]

	var mapOffset:CGPoint = .zero
	var restPosition:CGPoint = .zero
	var dragPosition:CGPoint = .zero
	var snapToPosition = true
	var snapToRest = false
	var badPlace = false {
		didSet {
			guard badPlace != oldValue else { return }
			sprite.colorBlendFactor = badPlace ? 0.75 : 0
		}
	}

	private var rotation:CGFloat = 0
	private var rotationOffset:CGFloat = 0
	private var rotationScale:CGFloat = 0

	private let sprite = SKSpriteNode()

	init(length:Int,rotated:Bool) {
		self.length = length
		self.rotated = rotated
		hit = Array(repeating:false,count:length)

		super.init()

		sprite.color = SKColor.red
		addChild(sprite)
		updateSprite()
		snap()
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func snap() {
		position = getPosition()
		rotation = getRotation()
		sprite.zRotation = getScaledRotation()
		updateZ()
	}

	func lateUpdate() {
		position = (Time.deltaTime*15).lerp(position,getPosition())
		rotation = (Time.deltaTime*15).lerp(rotation,getRotation())
		sprite.zRotation = getScaledRotation()
		updateZ()
	}

	func select(at pos:CGPoint) -> Bool {
		let margin:CGFloat = 0.1
		let w = Sheep.unit*0.5*(CGFloat(width)+margin)
		let h = Sheep.unit*0.5*(CGFloat(height)+margin)
		return pos.x >= -w && pos.x <= w && pos.y >= -h && pos.y <= h
	}

	private func getPosition() -> CGPoint {
		if snapToPosition {
			if snapToRest {
				return restPosition
			}
		} else {
			return dragPosition
		}
		let x = CGFloat(self.x)+CGFloat(width-Sheep.width)*0.5
		let y = CGFloat(self.y)+CGFloat(height-Sheep.height)*0.5
		return CGPoint(x:mapOffset.x+x*Sheep.unit,y:mapOffset.y-y*Sheep.unit)
	}

	private func getRotation() -> CGFloat {
		return rotated ? 1 : 0
	}

	private func getScaledRotation() -> CGFloat {
		return (rotation+rotationOffset)*rotationScale
	}

	private func updateZ() {
		zPosition = snapToPosition ? (0.5-position.y*0.0001) : 1
	}

	func updateSprite() {
		let char:String,w:CGFloat,h:CGFloat
		if length == 1 {
			rotationOffset = 0
			rotationScale = 0
			char = ""
			w = 1
			h = 1
		} else {
			rotationScale = -CGFloat.pi*0.5
			if rotated {
				rotationOffset = -1
				char = "y"
				w = 1
				h = CGFloat(length)
			} else {
				rotationOffset = 0
				char = "x"
				w = CGFloat(length)
				h = 1
			}
		}
		sprite.size = CGSize(width:Sheep.unit*(w+0.5),height:Sheep.unit*(h+0.5))
		sprite.texture = Sheep.atlasSheeps?.textureNamed(char+hit.map { $0 ? "1" : "0" }.joined())
	}
}
