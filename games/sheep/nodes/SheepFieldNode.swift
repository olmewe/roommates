import SpriteKit

class SheepFieldNode:SKNode,SceneNode {
	let grid:BoolGrid
	let hit:BoolGrid

	var mapNode:SheepMapNode!
	var mode:Mode = .none
	enum Mode {
		case none
		case edit
		case select
	}

	var selected:(x:Int,y:Int)?
	private var clipperTempo:CGFloat = 0
	private var clipperPos:CGPoint = .zero
	private var clipperWool = false

	let selectedNode = SKSpriteNode()
	let clipperNode = SKSpriteNode()

	var sfxShear:Data?

	var ready:Bool {
		return !sheeps.contains { !$0.snapToPosition || $0.snapToRest || $0.badPlace }
	}

	private(set) var sheeps:[SheepSheepNode] = []
	private(set) var arrangement:[SheepMapNode] = []

	private var currentWidth:Int = 0
	private var currentHeight:Int = 0
	private var currentShowSheeps = false
	private var currentSheepCount:[Int] = []

	private var touches:[Int:SheepTouch] = [:]
	class SheepTouch {
		let sheep:SheepSheepNode
		let offset:CGPoint
		let start:(x:Int,y:Int)?
		var last:(x:Int,y:Int)?
		var rotate:Bool
		init(sheep:SheepSheepNode,offset:CGPoint,start:(x:Int,y:Int)?,rotate:Bool) {
			self.sheep = sheep
			self.offset = offset
			self.start = start
			last = start
			self.rotate = rotate
		}
	}

	convenience init(p1:Bool) {
		if p1 {
			self.init(grid:Sheep.gridPlayer1,hit:Sheep.hitPlayer1)
		} else {
			self.init(grid:Sheep.gridPlayer2,hit:Sheep.hitPlayer2)
		}
	}

	init(grid:BoolGrid,hit:BoolGrid) {
		self.grid = grid
		self.hit = hit
		super.init()

		selectedNode.texture = Sheep.atlasTiles?.textureNamed("select")
		selectedNode.zPosition = 2
		selectedNode.size = CGSize(width:Sheep.unit*1.25,height:Sheep.unit*1.25)

		clipperNode.texture = Sheep.atlasTiles?.textureNamed("clipper")
		clipperNode.zPosition = 3
		clipperNode.size = CGSize(width:Sheep.unit*1.25,height:Sheep.unit*1.25)
		clipperNode.anchorPoint = CGPoint(x:0.125,y:0.125)

		setSize()

		sfxShear = Audio.data(of:"sheeps/shear.wav")
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func update() {
		if mode == .edit {
			for i in Touch.events {
				if let t = touches[i.id] {
					if i.state == .up || i.state == .miss {
						t.sheep.snapToPosition = true
						if t.last == nil {
							t.sheep.snapToRest = true
							t.sheep.rotated = false
							t.sheep.badPlace = false
						} else {
							t.sheep.snapToRest = false
							if t.rotate {
								let d = (t.sheep.length-1)/2
								if t.sheep.rotated {
									t.sheep.rotated = false
									t.sheep.x = (t.sheep.x-d).clamp(0,Sheep.width-t.sheep.length)
									t.sheep.y = (t.sheep.y+d).clamp(0,Sheep.height-1)
								} else {
									t.sheep.rotated = true
									t.sheep.x = (t.sheep.x+d).clamp(0,Sheep.width-1)
									t.sheep.y = (t.sheep.y-d).clamp(0,Sheep.height-t.sheep.length)
								}
								t.sheep.updateSprite()
							}
							markBadPlaceSheeps()
						}
						t.sheep.updateSprite()
						touches[i.id] = nil
					} else {
						let tempPos = i.position(in:self)
						let pos = CGPoint(x:tempPos.x+t.offset.x,y:tempPos.y+t.offset.y)
						let mapPos:CGPoint
						if t.sheep.rotated {
							mapPos = CGPoint(x:pos.x,y:pos.y+CGFloat(t.sheep.length-1)*0.5*Sheep.unit)
						} else {
							mapPos = CGPoint(x:pos.x-CGFloat(t.sheep.length-1)*0.5*Sheep.unit,y:pos.y)
						}
						var sheepPos:(x:Int,y:Int)?
						if let (x,y) = mapNode.getCoord(for:convert(mapPos,to:mapNode)) {
							let inArea:Bool
							if t.sheep.rotated {
								inArea = x < Sheep.width && y+t.sheep.length <= Sheep.height
							} else {
								inArea = x+t.sheep.length <= Sheep.width && y < Sheep.height
							}
							if inArea {
								sheepPos = (x,y)
							}
						}
						if let sheepPos = sheepPos {
							let changed:Bool
							if let lastPos = t.last {
								changed = lastPos.x != sheepPos.x || lastPos.y != sheepPos.y
							} else {
								changed = true
							}
							if changed {
								t.last = sheepPos
								t.rotate = false
								t.sheep.snapToPosition = true
								t.sheep.x = sheepPos.x
								t.sheep.y = sheepPos.y
								t.sheep.updateSprite()
								markBadPlaceSheeps()
							}
						} else {
							if t.last != nil {
								t.last = nil
								t.rotate = false
								t.sheep.snapToPosition = false
								t.sheep.updateSprite()
								markBadPlaceSheeps()
							}
							t.sheep.dragPosition = pos
							t.sheep.zPosition = 1
						}
					}
				} else if i.state == .down,let sheep = sheeps.first(where:{ $0.select(at:i.position(in:$0)) }) {
					let pos = i.position(in:self)
					let offset = CGPoint(x:sheep.position.x-pos.x,y:sheep.position.y-pos.y)
					if sheep.snapToRest {
						sheep.snapToRest = false
						sheep.snapToPosition = false
						sheep.dragPosition = sheep.position
						touches[i.id] = SheepTouch(sheep:sheep,offset:offset,start:nil,rotate:false)
					} else {
						touches[i.id] = SheepTouch(sheep:sheep,offset:offset,start:(sheep.x,sheep.y),rotate:true)
					}
				}
			}
		} else if !touches.isEmpty {
			touches.removeAll()
		}
		if mode == .select {
			for i in Touch.events where i.state == .down {
				if let pos = mapNode.getCoord(for:i.position(in:mapNode)),!hit[pos.x,pos.y] {
					select(x:pos.x,y:pos.y)
				}
			}
		} else {
			deselect()
		}
		if clipperTempo > 0 {
			let old = clipperTempo
			clipperTempo -= Time.deltaTime
			if Int(old*10) != Int(clipperTempo*10) {
				let particle = ParticleNode(wool:clipperWool,velocityX:Sheep.unit*Random.range(-1,1),velocityY:Sheep.unit*Random.range(1,1.5))
				addChild(particle)
				particle.position = clipperPos
				particle.zPosition = clipperNode.zPosition-0.1
			}
			if clipperTempo <= 0 {
				if clipperNode.parent != nil {
					clipperNode.removeFromParent()
				}
			} else {
				let r = Sheep.unit*0.05
				clipperNode.position = CGPoint(x:clipperPos.x+Random.range(-r,r),y:clipperPos.y+Random.range(-r,r))
			}
		}
	}

	func select(x:Int,y:Int) {
		selected = (x:x,y:y)
		if selectedNode.parent == nil {
			addChild(selectedNode)
		}
		selectedNode.position = convert(mapNode.getPos(x:x,y:y),from:mapNode)
	}

	func deselect() {
		guard selected != nil else { return }
		selected = nil
		if selectedNode.parent != nil {
			selectedNode.removeFromParent()
		}
	}

	func clip(x:Int,y:Int) {
		deselect()
		if clipperNode.parent == nil {
			addChild(clipperNode)
		}
		clipperPos = convert(mapNode.getPos(x:x,y:y),from:mapNode)
		clipperNode.position = clipperPos
		clipperTempo = 0.7
		clipperWool = grid[x,y]
		Audio.play(sfxShear)
	}

	func setSize() {
		guard currentWidth != Sheep.width || currentHeight != Sheep.height else { return }
		currentWidth = Sheep.width
		currentHeight = Sheep.height
		if mapNode != nil {
			mapNode.removeFromParent()
		}
		mapNode = SheepMapNode(width:Sheep.width,height:Sheep.height)
		addChild(mapNode)
		mapNode.position = CGPoint(x:0,y:Scene.current.size.height*0.1)
	}

	func setSheeps(show:Bool) {
		guard currentShowSheeps != show || currentSheepCount != Sheep.sheepCount else { return }
		currentSheepCount = Sheep.sheepCount
		currentShowSheeps = show
		if !sheeps.isEmpty {
			for i in sheeps {
				i.removeFromParent()
			}
			sheeps.removeAll()
		}
		if show {
			for (length,count) in Sheep.sheepCount.enumerated() {
				guard count > 0 else { continue }
				for _ in 0..<count {
					let sheep = SheepSheepNode(length:length+1,rotated:false)
					sheeps.append(sheep)
					addChild(sheep)
					sheep.mapOffset = mapNode.position
					sheep.zPosition = 1
					sheep.snapToPosition = true
					sheep.snapToRest = true
					sheep.rotated = false
				}
			}
		}
		if !arrangement.isEmpty {
			for i in arrangement {
				i.removeFromParent()
			}
			arrangement.removeAll()
		}
		func sheepArrangement() -> [[Int]] {
			var a:[[Int]] = []
			let limit = CGFloat(Sheep.width)
			let margin:CGFloat = 0.75
			var rem = Sheep.sheepCount
			var totalRem = rem.reduce(0,+)
			var currentLine:[Int] = []
			var currentSum:CGFloat = 0
			while totalRem > 0 {
				if let (length,_) = rem.enumerated().reversed().first(where:{ $0.element > 0 && currentSum+CGFloat($0.offset)+1 <= limit }) {
					currentLine.append(length+1)
					currentSum += CGFloat(length+1)+margin
					rem[length] -= 1
					totalRem -= 1
				} else if currentSum > 0 {
					a.append(currentLine)
					currentLine = []
					currentSum = 0
				} else {
					return a
				}
			}
			if currentSum > 0 {
				a.append(currentLine)
			}
			return a
		}
		let halfWidth = CGFloat(Sheep.width)*0.5
		for (n,line) in sheepArrangement().enumerated() {
			let y = -Scene.current.size.height*0.2-CGFloat(n)*Sheep.unit*1.3
			func create(length:Int,x:CGFloat) {
				let node = SheepMapNode(width:length,height:1)
				arrangement.append(node)
				addChild(node)
				node.position = CGPoint(x:x*Sheep.unit,y:y)
			}
			if line.count > 1 {
				let spacing = CGFloat(Sheep.width-line.reduce(0,+))/CGFloat(line.count-1)
				var offset = -halfWidth
				for i in line {
					let ci = CGFloat(i)
					create(length:i,x:offset+ci*0.5)
					offset += ci+spacing
				}
			} else {
				create(length:line[0],x:0)
			}
		}
		if show {
			var maps = arrangement
			for sheep in sheeps {
				guard let (index,map) = maps.enumerated().first(where:{ $0.element.width == sheep.length }) else { continue }
				maps.remove(at:index)
				sheep.restPosition = CGPoint(x:map.position.x+map.cornerOffset,y:map.position.y)
				sheep.snap()
			}
		}
	}

	func markBadPlaceSheeps() {
		var grid:[Int] = Array(repeating:-1,count:Sheep.width*Sheep.height)
		for (n,sheep) in sheeps.enumerated() {
			sheep.badPlace = false
			guard !sheep.snapToRest && sheep.snapToPosition else { continue }
			let xMin = max(sheep.x-1,0)
			let yMin = max(sheep.y-1,0)
			let xMax = min(sheep.x+sheep.width,Sheep.width-1)
			let yMax = min(sheep.y+sheep.height,Sheep.height-1)
			for x in xMin...xMax {
				for y in yMin...yMax {
					let m = grid[x+y*Sheep.width]
					if m >= 0 {
						sheep.badPlace = true
						sheeps[m].badPlace = true
					}
				}
			}
			let index = sheep.x+sheep.y*Sheep.width
			if sheep.rotated {
				for i in 0..<sheep.length {
					grid[index+i*Sheep.width] = n
				}
			} else {
				for i in 0..<sheep.length {
					grid[index+i] = n
				}
			}
		}
	}

	func setGridFromSheeps() {
		grid.clear()
		for sheep in sheeps {
			if sheep.rotated {
				for i in 0..<sheep.length {
					grid[sheep.x,sheep.y+i] = true
				}
			} else {
				for i in 0..<sheep.length {
					grid[sheep.x+i,sheep.y] = true
				}
			}
		}
	}

	func setEmptyGrid() {
		for x in 0..<Sheep.width {
			for y in 0..<Sheep.height {
				mapNode[x,y] = (false,.none)
			}
		}
		for map in arrangement {
			for x in 0..<map.width {
				map[x,0] = (false,.none)
			}
		}
	}

	func setGrid() {
		var maps = arrangement
		var gridSheeps = Sheep.findSheep(in:grid)
		if !sheeps.isEmpty {
			for x in 0..<Sheep.width {
				for y in 0..<Sheep.height {
					mapNode[x,y] = (hit[x,y],.none)
				}
			}
			for sheep in sheeps {
				guard let mapIndex = maps.index(where:{ $0.width == sheep.length }) else { continue }
				guard let gridIndex = gridSheeps.index(where:{ $0.length == sheep.length }) else { continue }
				let map = maps[mapIndex]
				maps.remove(at:mapIndex)
				let gridSheep = gridSheeps[gridIndex]
				gridSheeps.remove(at:gridIndex)
				var updated = false
				if sheep.rotated != gridSheep.rotated {
					sheep.rotated = gridSheep.rotated
					updated = true
				}
				sheep.x = gridSheep.x
				sheep.y = gridSheep.y
				sheep.snapToPosition = true
				sheep.snapToRest = false
				sheep.snap()
				var c = sheep.length
				if sheep.rotated {
					for i in 0..<sheep.length {
						if hit[sheep.x,sheep.y+i] {
							c -= 1
							if !sheep.hit[i] {
								sheep.hit[i] = true
								updated = true
							}
						} else {
							if sheep.hit[i] {
								sheep.hit[i] = false
								updated = true
							}
						}
					}
				} else {
					for i in 0..<sheep.length {
						if hit[sheep.x+i,sheep.y] {
							c -= 1
							if !sheep.hit[i] {
								sheep.hit[i] = true
								updated = true
							}
						} else {
							if sheep.hit[i] {
								sheep.hit[i] = false
								updated = true
							}
						}
					}
				}
				if updated {
					sheep.updateSprite()
				}
				if c <= 0 {
					for i in 0..<sheep.length {
						map[i,0] = (false,.none)
					}
				} else if c < sheep.length {
					for i in 0..<c {
						map[i,0] = (false,.partial)
					}
					for i in c..<sheep.length {
						map[i,0] = (false,.none)
					}
				} else {
					for i in 0..<sheep.length {
						map[i,0] = (false,.full)
					}
				}
			}
		} else {
			for x in 0..<Sheep.width {
				for y in 0..<Sheep.height {
					if !hit[x,y] {
						mapNode[x,y] = (false,.none)
					} else if !grid[x,y] {
						mapNode[x,y] = (true,.none)
					}
				}
			}
			for sheep in gridSheeps {
				guard let mapIndex = maps.index(where:{ $0.width == sheep.length }) else { continue }
				let map = maps[mapIndex]
				maps.remove(at:mapIndex)
				var c:Int = 0
				if sheep.rotated {
					for i in 0..<sheep.length where hit[sheep.x,sheep.y+i] {
						c += 1
					}
				} else {
					for i in 0..<sheep.length where hit[sheep.x+i,sheep.y] {
						c += 1
					}
				}
				if c < sheep.length {
					for i in c..<sheep.length {
						map[i,0] = (false,.none)
					}
				} else {
					for i in 0..<sheep.length {
						map[i,0] = (false,.full)
					}
				}
				if sheep.rotated {
					for i in 0..<sheep.length where hit[sheep.x,sheep.y+i] {
						mapNode[sheep.x,sheep.y+i] = (true,(c < sheep.length) ? .partial : .full)
					}
				} else {
					for i in 0..<sheep.length where hit[sheep.x+i,sheep.y] {
						mapNode[sheep.x+i,sheep.y] = (true,(c < sheep.length) ? .partial : .full)
					}
				}
			}
		}
	}

	private class ParticleNode:SKSpriteNode,SceneNode {
		private let duration:CGFloat = 1
		private let gravity:CGFloat = -2
		private let fadeStart:CGFloat = 0.8

		private var tempo:CGFloat = 0
		private let velocityX:CGFloat
		private var velocityY:CGFloat

		init(wool:Bool,velocityX:CGFloat,velocityY:CGFloat) {
			self.velocityX = velocityX
			self.velocityY = velocityY
			super.init(texture:Sheep.atlasTiles?.textureNamed(wool ? "particle_wool" : "particle_grass"),color:SKColor.white,size:CGSize(width:Sheep.unit*0.5,height:Sheep.unit*0.5))
		}

		required init?(coder aDecoder:NSCoder) {
			return nil
		}

		func lateUpdate() {
			tempo += Time.deltaTime
			guard tempo < duration else {
				removeFromParent()
				return
			}
			if tempo <= duration*fadeStart {
				alpha = 1
			} else {
				alpha = ((tempo-1)/(duration*fadeStart-1)).easeOut
			}
			velocityY += Sheep.unit*gravity*Time.deltaTime
			position = CGPoint(x:position.x+velocityX*Time.deltaTime,y:position.y+velocityY*Time.deltaTime)
		}
	}
}
