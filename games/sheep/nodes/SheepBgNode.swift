import SpriteKit

class SheepBgNode:SKSpriteNode,SceneNode {
	let bgScene:Scene!
	let offset:CGFloat = 0.04
	let posOffset:CGPoint

	init(scene:Scene) {
		bgScene = scene
		posOffset = CGPoint(x:scene.size.width*offset,y:scene.size.height*offset)
		let m = 1+offset*2
		let finalSize = CGSize(width:scene.size.width*m,height:scene.size.height*m)
		super.init(texture:Sheep.atlas?.textureNamed("bg"),color:SKColor.clear,size:finalSize)
		zPosition = -1
		colorBlendFactor = 1
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func update() {
		let vel:CGFloat = 0.4
		position = CGPoint(x:cos(Time.time*vel)*posOffset.x,y:sin(Time.time*vel)*posOffset.y)
	}

	func set(bg:SKColor,fg:SKColor) {
		bgScene.backgroundColor = bg
		color = fg
	}
}
