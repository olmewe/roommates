import SpriteKit

class SheepMapNode:SKNode,SceneNode,SceneNodeHalt {
	let width:Int,height:Int
	let cornerOffset:CGFloat
	private var tiles:[TileNode] = []
	private var woolUpdate:[TileNode] = []

	init(width:Int,height:Int) {
		let width = max(width,1)
		let height = max(height,1)
		self.width = width
		self.height = height
		cornerOffset = -CGFloat(width-1)*0.5
		super.init()
		for y in 0..<height {
			for x in 0..<width {
				let node = TileNode(mown:false,wool:.none)
				node.position = getPos(x:x,y:y)
				tiles.append(node)
				addChild(node)
			}
		}
		let texture = Sheep.atlasTiles?.textureNamed("base")
		let size = CGSize(width:Sheep.unit,height:Sheep.unit*0.25)
		for x in 0..<width {
			let node = SKSpriteNode(texture:texture,size:size)
			node.position = CGPoint(x:getPos(x:x,y:0).x,y:(-CGFloat(height)-0.25)*0.5*Sheep.unit)
			addChild(node)
		}
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	subscript(x:Int,y:Int) -> (mown:Bool,wool:WoolState) {
		get {
			guard x >= 0 && x < width && y >= 0 && y < height else { return (false,.none) }
			let node = tiles[x+y*width]
			return (node.mown,node.wool)
		}
		set {
			guard x >= 0 && x < width && y >= 0 && y < height else { return }
			let node = tiles[x+y*width]
			node.mown = newValue.mown
			if node.wool != newValue.wool {
				node.wool = newValue.wool
				if newValue.wool != .none {
					if !woolUpdate.contains(node) {
						woolUpdate.append(node)
					}
				} else if let index = woolUpdate.index(of:node) {
					woolUpdate.remove(at:index)
				}
			}
		}
	}

	func lateUpdate() {
		for i in woolUpdate {
			i.updateWool()
		}
	}

	func getCoord(for pos:CGPoint) -> (x:Int,y:Int)? {
		let x = Int(pos.x/Sheep.unit+CGFloat(width)*0.5+1)-1
		let y = Int(CGFloat(height)*0.5-pos.y/Sheep.unit+1)-1
		guard x >= 0 && x < width && y >= 0 && y < height else { return nil }
		return (x:x,y:y)
	}

	func getPos(x:Int,y:Int) -> CGPoint {
		return CGPoint(x:(CGFloat(x)-CGFloat(width-1)*0.5)*Sheep.unit,y:(CGFloat(height-1)*0.5-CGFloat(y))*Sheep.unit)
	}

	enum WoolState {
		case none
		case partial
		case full
	}

	class TileNode:SKSpriteNode {
		var mown:Bool {
			didSet {
				if oldValue != mown {
					setMown()
				}
			}
		}
		var wool:WoolState {
			didSet {
				if oldValue != wool {
					setWool()
				}
			}
		}
		private var woolNode:SKSpriteNode?
		private var woolAnim:CGFloat = 0
		private var woolTexture:Int = -1

		init(mown:Bool,wool:WoolState) {
			self.mown = mown
			self.wool = wool
			super.init(texture:nil,color:SKColor.white,size:CGSize(width:Sheep.unit,height:Sheep.unit))
			setMown()
			setWool()
		}

		required init?(coder aDecoder:NSCoder) {
			return nil
		}

		func updateWool() {
			guard let wool = woolNode else { return }
			if woolAnim > 0 {
				woolAnim -= Time.deltaTime*1.5
				if woolAnim <= 0 { woolAnim = 0 }
				let y:CGFloat
				if woolAnim > 0.25 {
					y = ((4/3)*(woolAnim-0.25)).parabola
				} else {
					y = (woolAnim*4).parabola*0.25
				}
				wool.position = CGPoint(x:0,y:(0.2+y*0.2)*Sheep.unit)
			}
			if self.wool == .partial {
				let newTexture = Int(Time.time*6)%3
				if woolTexture != newTexture {
					woolTexture = newTexture
					wool.texture = Sheep.atlasTiles?.textureNamed("woolpartial\(newTexture)")
				}
			}
		}

		private func setMown() {
			texture = Sheep.atlasTiles?.textureNamed(mown ? "mown" : "grass")
		}

		private func setWool() {
			if wool == .none {
				if let w = woolNode {
					w.removeFromParent()
					woolNode = nil
				}
				woolAnim = 0
			} else {
				let woolNode:SKSpriteNode
				if let w = self.woolNode {
					woolNode = w
				} else {
					woolNode = SKSpriteNode(texture:nil,size:CGSize(width:Sheep.unit*1.25,height:Sheep.unit*1.25))
					woolNode.zPosition = 0.1
					addChild(woolNode)
					self.woolNode = woolNode
				}
				if wool == .full {
					woolNode.texture = Sheep.atlasTiles?.textureNamed("wool")
				} else {
					woolTexture = -1
				}
				woolAnim = 1
			}
		}
	}
}
