import SpriteKit

class Sheep {
	static let serviceName = "game_sheep"
	static let server = SheepServer()
	static let client = SheepClient()
	static let info = GameInfo(id:"sheep") {
		$0.value = 0

		$0.name = "Battle Sheep"
		$0.description = "shear your friend's sheeps while they try to do the same to yours!"
		$0.players = 2
		$0.genre = "Guessing"
		$0.averageTime = 10
		$0.modes = [.comp]

		$0.assetsFolder = "game_sheep_meta"
		$0.bgColour = #colorLiteral(red: 0.9098039216, green: 0.9843137255, blue: 0.6509803922, alpha: 1)
		$0.fgColour = #colorLiteral(red: 0.6078431373, green: 0.9568627451, blue: 0.6235294118, alpha: 1)

		$0.server = server
		$0.client = client
	}

	static let colourBg = #colorLiteral(red: 0.9098039216, green: 0.9843137255, blue: 0.6509803922, alpha: 1)
	static let colourFg = CGFloat(0.5).lerp(#colorLiteral(red: 0.6078431373, green: 0.9568627451, blue: 0.6235294118, alpha: 1),colourBg)

	static let width = 10
	static let height = 10
	static let sheepCount:[Int] = [4,3,2,1]

	static let unit:CGFloat = 64
	static var atlas:SKTextureAtlas?
	static var atlasSheeps:SKTextureAtlas?
	static var atlasTiles:SKTextureAtlas?

	static var state:State = .loading
	enum State {
		case loading
		case idle
		case loaded
		case playing
		case end
	}
	static var scene:GameScene = .none
	enum GameScene {
		case none
		case menu
		case game
		case arranging
	}

	static var playerMatch:Bool? = nil
	static var player1:String = ""
	static var player2:String = ""
	static var turnPlayer1 = false
	static var readyPlayer1 = false
	static var readyPlayer2 = false
	static var ready:Bool {
		return readyPlayer1 && readyPlayer2
	}

	static var selection = false
	static var stateUpdate = false
	static var attack:(p1:Bool,x:Int,y:Int)? = nil
	static var flag:Flag = .none
	enum Flag {
		case none
		case disconnect
		case close
	}

	static let gridPlayer1 = BoolGrid(width:Sheep.width,height:Sheep.height)
	static let gridPlayer2 = BoolGrid(width:Sheep.width,height:Sheep.height)
	static let hitPlayer1 = BoolGrid(width:Sheep.width,height:Sheep.height)
	static let hitPlayer2 = BoolGrid(width:Sheep.width,height:Sheep.height)

	static func load() {
		atlas = SKTextureAtlas(named:"game_sheep")
		atlasSheeps = SKTextureAtlas(named:"game_sheep_sheeps")
		atlasTiles = SKTextureAtlas(named:"game_sheep_tiles")
	}

	static func unload() {
		atlas = nil
		atlasSheeps = nil
		atlasTiles = nil
	}

	static func changeScene(from scene:GameScene) -> Bool {
		guard Sheep.scene != scene else { return false }
		let newScene:Scene
		switch Sheep.scene {
		case .none: return false
		case .menu: newScene = SheepMenuScene()
		case .game: newScene = SheepGameScene()
		case .arranging: newScene = SheepArrangingScene()
		}
		if scene == .none {
			Scene.present(newScene)
		} else {
			Scene.present(newScene,transition:.fade(with:colourBg,duration:0.5))
		}
		return true
	}

	static func findSheep(in grid:BoolGrid) -> [(x:Int,y:Int,length:Int,rotated:Bool)] {
		var list:[(x:Int,y:Int,length:Int,rotated:Bool)] = []
		let check = BoolGrid(grid)
		for x in 0..<width {
			for y in 0..<height {
				guard check[x,y] else { continue }
				check[x,y] = false
				var length:Int = 1
				var rotated = false
				while check[x+length,y] {
					check[x+length,y] = false
					length += 1
				}
				if length == 1 {
					while check[x,y+length] {
						check[x,y+length] = false
						length += 1
					}
					rotated = length > 1
				}
				list.append((x:x,y:y,length:length,rotated:rotated))
			}
		}
		return list
	}
}
