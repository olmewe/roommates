import SpriteKit

class SheepGameScene:Scene {
	var bgNode:SheepBgNode!

	let closeNode = CloseNode()
	let titleNode = PanelNode(size:CGSize(width:470,height:90),bg:#colorLiteral(red: 0.2980392157, green: 0.7019607843, blue: 0.568627451, alpha: 1),border:#colorLiteral(red: 0.1490196078, green: 0.462745098, blue: 0.4078431373, alpha: 1),colour:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
	let hintNode = SKLabelNode(fontNamed:"Nunito-Light")
	let actionNode = PanelNode(size:CGSize(width:570,height:90),bg:#colorLiteral(red: 0.9647058824, green: 1, blue: 0.8666666667, alpha: 1),border:#colorLiteral(red: 0.4588235294, green: 0.6470588235, blue: 0.431372549, alpha: 1),colour:#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1))

	var fieldPlayer1:SheepFieldNode!
	var fieldPlayer2:SheepFieldNode!

	var slideToRight = false
	var slideDelayed = false
	var slideAnimation:CGFloat = 0
	var actionDelay:CGFloat = 0

	var openView:UIViewController? = nil

	var titlePeer = ""

	let selector = ButtonSelector()

	var sfxSelect:Data?

	override func load() {
		size = CGSize(width:720,height:1280)
		bgNode = SheepBgNode(scene:self)
		addChild(bgNode)
		bgNode.set(bg:Sheep.colourBg,fg:Sheep.colourFg)

		addChild(closeNode)
		closeNode.resize(size)

		addChild(titleNode)
		titleNode.zPosition = 10
		titleNode.position = CGPoint(x:0,y:555)

		addChild(hintNode)
		hintNode.zPosition = 9
		hintNode.fontSize = 36
		hintNode.fontColor = #colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1)
		hintNode.position = CGPoint(x:0,y:-555)
		hintNode.horizontalAlignmentMode = .center
		hintNode.verticalAlignmentMode = .center

		addChild(actionNode)
		actionNode.zPosition = 10
		actionNode.position = CGPoint(x:0,y:-555)

		fieldPlayer1 = SheepFieldNode(p1:true)
		addChild(fieldPlayer1)

		fieldPlayer2 = SheepFieldNode(p1:false)
		addChild(fieldPlayer2)

		selector.nodes = [closeNode,actionNode]

		sfxSelect = Audio.data(of:"sheeps/select.wav")
	}

	override func show() {
		Sheep.stateUpdate = true
		_ = updateState()
		slideAnimation = slideToRight ? 1 : 0
		updateSlideAnimation()
	}

	override func update() {
		guard updateState() else { return }
		let selected = selector.update()
		if Sheep.state == .playing && selected?.isEqual(to:closeNode) == true {
			if Sheep.playerMatch == nil {
				GameState.closeGame()
			} else {
				Audio.play(sfxSelect)
				if openView == nil,let view = UI.load("sheepConfirm") as? SheepConfirmViewController {
					openView = view
					view.load = false
					view.yes = {
						Sheep.client.sendStop()
					}
					view.no = {
						UI.popAll()
						self.openView = nil
					}
					UI.push(view)
				}
			}
		}
		var actionText:String?
		var hintText:String?
		let actionSelected = selected?.isEqual(to:actionNode) == true
		if let p1 = Sheep.playerMatch {
			if Sheep.state == .playing && actionDelay <=~ 0 {
				if Sheep.ready {
					let field:SheepFieldNode
					let turn:Bool
					if p1 {
						field = fieldPlayer2
						turn = Sheep.turnPlayer1
					} else {
						field = fieldPlayer1
						turn = !Sheep.turnPlayer1
					}
					if turn {
						titleNode.text = "your turn"
						if let pos = field.selected {
							actionText = "attack!"
							if actionSelected {
								Sheep.client.sendAttack(x:pos.x,y:pos.y)
								Audio.play(sfxSelect)
							}
						} else {
							hintText = "select a square to attack"
						}
					} else {
						titleNode.text = "opponent's turn"
					}
				} else {
					let field:SheepFieldNode
					let ready:Bool
					if p1 {
						field = fieldPlayer1
						ready = Sheep.readyPlayer1
					} else {
						field = fieldPlayer2
						ready = Sheep.readyPlayer2
					}
					if ready {
						titleNode.text = "waiting for opponent..."
						actionText = "cancel"
						if actionSelected {
							Sheep.client.sendReady(grid:nil)
						}
					} else {
						titleNode.text = "arrange your sheeps"
						if field.ready {
							actionText = "touch to confirm!"
							if actionSelected {
								field.setGridFromSheeps()
								Sheep.client.sendReady(grid:field.grid)
							}
						} else {
							hintText = "drag your sheeps to the field above"
						}
					}
					if actionSelected {
						Audio.play(sfxSelect)
						actionDelay = 0.5
					}
				}
				titleNode.icon = nil
			}
		} else {
			hintText = "spectating!"
			if actionDelay <=~ 0 {
				let id = Sheep.turnPlayer1 ? Sheep.player1 : Sheep.player2
				if titlePeer != id {
					titlePeer = id
					if let info = GameState.peerInfo(for:id) {
						titleNode.text = "\(info.name)'s turn"
						titleNode.icon = SKTexture(imageNamed:"char_\(info.char)_meta/icon")
					}
				}
			}
		}
		if let text = actionText {
			actionNode.text = text
			actionNode.isHidden = false
		} else {
			actionNode.isHidden = true
		}
		hintNode.text = hintText
		if actionDelay > 0 {
			actionDelay -= Time.deltaTime
			if actionDelay <= 0 {
				actionDelay = 0
				slideDelayed = slideToRight
				if Sheep.state == .end {
					if openView != nil {
						UI.popAll()
						openView = nil
					}
					if let view = UI.load("sheepEnd") as? SheepEndViewController {
						openView = view
						if let p1 = Sheep.playerMatch {
							if Sheep.turnPlayer1 == p1 {
								view.win = .player
							} else {
								view.win = .opponent
							}
						} else if Sheep.turnPlayer1 {
							view.win = .other(GameState.peerInfo(for:Sheep.player1)?.name ?? "player 1")
						} else {
							view.win = .other(GameState.peerInfo(for:Sheep.player2)?.name ?? "player 2")
						}
						view.end = {
							Sheep.client.sendStop()
						}
						UI.push(view)
					}
				}
			}
		} else {
			slideDelayed = slideToRight
		}
		if slideDelayed {
			if slideAnimation < 1 {
				slideAnimation += Time.deltaTime*1.25
				if slideAnimation > 1 {
					slideAnimation = 1
				}
				updateSlideAnimation()
			}
		} else {
			if slideAnimation > 0 {
				slideAnimation -= Time.deltaTime*1.25
				if slideAnimation < 0 {
					slideAnimation = 0
				}
				updateSlideAnimation()
			}
		}
	}

	func updateState() -> Bool {
		if Sheep.changeScene(from:.game) { return false }
		guard Sheep.stateUpdate else { return true }
		Sheep.stateUpdate = false
		fieldPlayer1.setSheeps(show:Sheep.playerMatch != false)
		fieldPlayer2.setSheeps(show:Sheep.playerMatch != true)
		if Sheep.readyPlayer1 {
			fieldPlayer1.setGrid()
		} else {
			fieldPlayer1.setEmptyGrid()
		}
		if Sheep.readyPlayer2 {
			fieldPlayer2.setGrid()
		} else {
			fieldPlayer2.setEmptyGrid()
		}
		if let attack = Sheep.attack {
			(attack.p1 ? fieldPlayer2 : fieldPlayer1)?.clip(x:attack.x,y:attack.y)
			actionDelay = Sheep.state == .end ? 2 : attack.p1 == Sheep.turnPlayer1 ? 1 : 2
		}
		if Sheep.ready {
			slideToRight = Sheep.turnPlayer1
		} else {
			slideToRight = Sheep.playerMatch == false
		}
		if Sheep.state == .playing,let p1 = Sheep.playerMatch {
			if Sheep.ready {
				if p1 {
					fieldPlayer1.mode = .none
					fieldPlayer2.mode = Sheep.turnPlayer1 ? .select : .none
				} else {
					fieldPlayer1.mode = Sheep.turnPlayer1 ? .none : .select
					fieldPlayer2.mode = .none
				}
			} else {
				if p1 {
					fieldPlayer1.mode = Sheep.readyPlayer1 ? .none : .edit
					fieldPlayer2.mode = .none
				} else {
					fieldPlayer1.mode = .none
					fieldPlayer2.mode = Sheep.readyPlayer2 ? .none : .edit
				}
				actionDelay = 0
			}
		} else {
			fieldPlayer1.mode = .none
			fieldPlayer2.mode = .none
		}
		if Sheep.state == .loaded {
			if openView != nil {
				UI.popAll()
				openView = nil
			}
			if let view = UI.load("sheepConfirm") as? SheepConfirmViewController {
				openView = view
				view.load = true
				view.yes = {
					Sheep.client.sendLoad(load:true)
				}
				view.no = {
					Sheep.client.sendLoad(load:false)
				}
				UI.push(view)
			}
		} else if openView is SheepConfirmViewController {
			UI.popAll()
			openView = nil
		}
		return true
	}

	func updateSlideAnimation() {
		let t = slideAnimation.ease
		let offset = t*(-size.width)
		fieldPlayer1.position = CGPoint(x:offset,y:0)
		fieldPlayer2.position = CGPoint(x:size.width+offset,y:0)
		bgNode.set(bg:t.lerp(Sheep.colourBg,Sheep.colourFg),fg:t.lerp(Sheep.colourFg,Sheep.colourBg))
	}
}
