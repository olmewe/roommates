import SpriteKit

class SheepArrangingScene:Scene {
	let closeNode = CloseNode()

	let player1 = CharNameNode(showIcon:true)
	let player2 = CharNameNode(showIcon:true)
	let readyPlayer1 = SKLabelNode(fontNamed:"Nunito-Light")
	let readyPlayer2 = SKLabelNode(fontNamed:"Nunito-Light")

	let selector = ButtonSelector()

	override func load() {
		size = CGSize(width:720,height:1280)
		let bgNode = SheepBgNode(scene:self)
		addChild(bgNode)
		bgNode.set(bg:CGFloat(0.05).lerp(#colorLiteral(red: 0.1490196078, green: 0.462745098, blue: 0.4078431373, alpha: 1),Sheep.colourBg),fg:#colorLiteral(red: 0.1490196078, green: 0.462745098, blue: 0.4078431373, alpha: 1))

		addChild(closeNode)
		closeNode.resize(size)

		let labelNode = SKLabelNode(fontNamed:"Nunito-Light")
		addChild(labelNode)
		labelNode.fontSize = 48
		labelNode.horizontalAlignmentMode = .center
		labelNode.text = "players are getting ready..."
		labelNode.position = CGPoint(x:0,y:300)

		addChild(player1)
		player1.setScale(1.15)
		player1.color = SKColor.white
		player1.position = CGPoint(x:0,y:150)

		addChild(player2)
		player2.setScale(1.15)
		player2.color = SKColor.white
		player2.position = CGPoint(x:0,y:-50)

		addChild(readyPlayer1)
		readyPlayer1.fontSize = 36
		readyPlayer1.horizontalAlignmentMode = .center
		readyPlayer1.position = CGPoint(x:0,y:75)

		addChild(readyPlayer2)
		readyPlayer2.fontSize = 36
		readyPlayer2.horizontalAlignmentMode = .center
		readyPlayer2.position = CGPoint(x:0,y:-125)

		selector.nodes = [closeNode]
	}

	override func show() {
		Sheep.stateUpdate = true
		_ = updateState()
	}

	override func update() {
		guard updateState() else { return }
		if let i = selector.update() {
			switch i {
			case closeNode: GameState.closeGame()
			default: break
			}
		}
	}

	func updateState() -> Bool {
		if Sheep.changeScene(from:.arranging) { return false }
		guard Sheep.stateUpdate else { return true }
		Sheep.stateUpdate = false
		player1.set(peer:Sheep.player1)
		player2.set(peer:Sheep.player2)
		readyPlayer1.text = Sheep.readyPlayer1 ? "ready!" : "arranging..."
		readyPlayer2.text = Sheep.readyPlayer2 ? "ready!" : "arranging..."
		return true
	}
}
