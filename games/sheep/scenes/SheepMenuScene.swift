import SpriteKit

class SheepMenuScene:Scene {
	let logoNode = SKSpriteNode()

	let closeNode = CloseNode()
	let startNode = PanelNode(size:CGSize(width:570,height:140),bg:#colorLiteral(red: 1, green: 0.9137254902, blue: 0.3137254902, alpha: 1),border:#colorLiteral(red: 0.6392156863, green: 0.6117647059, blue: 0.4117647059, alpha: 1),colour:#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1),text:"start!",icon:SKTexture(imageNamed:"common/join"))
	let howNode = PanelNode(size:CGSize(width:570,height:90),bg:#colorLiteral(red: 0.9647058824, green: 1, blue: 0.8666666667, alpha: 1),border:#colorLiteral(red: 0.4588235294, green: 0.6470588235, blue: 0.431372549, alpha: 1),colour:#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1),text:"how to play")
	let switchNode = PanelNode(size:CGSize(width:570,height:90),bg:#colorLiteral(red: 0.9647058824, green: 1, blue: 0.8666666667, alpha: 1),border:#colorLiteral(red: 0.4588235294, green: 0.6470588235, blue: 0.431372549, alpha: 1),colour:#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1),text:"switch game")

	let selector = ButtonSelector()

	var selectionView:SelectionViewController?
	var flagView:SheepMenuViewController?
	var tutorialNode:TutorialNode!

	var sfxSelect:Data?

	var flag:Sheep.Flag = .none

	override func load() {
		size = CGSize(width:720,height:1280)
		let bgNode = SheepBgNode(scene:self)
		addChild(bgNode)
		bgNode.set(bg:Sheep.colourBg,fg:Sheep.colourFg)

		addChild(logoNode)
		logoNode.position = CGPoint(x:0,y:160)
		logoNode.texture = Sheep.atlas?.textureNamed("logo")
		logoNode.size = CGSize(width:870*720/1242,height:720)
		logoNode.alpha = 0

		addChild(closeNode)
		closeNode.resize(size)

		addChild(startNode)
		startNode.position = CGPoint(x:0,y:-300)

		addChild(howNode)
		howNode.position = CGPoint(x:0,y:-440)

		addChild(switchNode)
		switchNode.position = CGPoint(x:0,y:-555)

		tutorialNode = TutorialNode(size:size,showIcon:false)
		addChild(tutorialNode)

		selector.nodes = [closeNode,startNode,howNode,switchNode]

		sfxSelect = Audio.data(of:"sheeps/select.wav")

		flag = Sheep.flag
	}

	override func show() {
		if flag != .none,let view = UI.load("sheepMenu") as? SheepMenuViewController {
			flagView = view
			view.flag = flag
			view.end = {
				UI.popAll()
				self.flagView = nil
			}
			UI.push(view)
		}
	}

	override func update() {
		if Sheep.changeScene(from:.menu) { return }
		if logoNode.alpha < 1 {
			logoNode.alpha += Time.deltaTime
			if logoNode.alpha > 1 {
				logoNode.alpha = 1
			}
		}
		if Sheep.selection {
			openSelection()
		}
		if let i = selector.update() {
			switch i {
			case closeNode: GameState.closeGame()
			case startNode:
				Audio.play(sfxSelect)
				Sheep.client.sendSelection()
				openSelection()
			case howNode:
				Audio.play(sfxSelect)
				openTutorial()
			case switchNode: GameState.switchGame()
			default: break
			}
		}
	}

	func openSelection() {
		guard selectionView == nil && flagView == nil && tutorialNode.view == nil else { return }
		selectionView = UI.showSelection(count:2) {
			start,players in
			self.selectionView = nil
			if start && players.count == 2 {
				Sheep.client.sendStart(p1:players[0],p2:players[1])
			}
		}
	}

	func openTutorial() {
		guard selectionView == nil && flagView == nil else { return }
		if tutorialNode.list == nil {
			tutorialNode.list = TutorialList(parse:"""
			[bg:creator]
			[sc:sheep_intro] [sp:shock] Oh hi! \\ \\ This is Battle Sheep, a guessing game where each player tries to shear the other player's sheep.
			[sc:sheep_drag] [sp:idle] First, each player arranges their sheep in place. \\ You can drag and drop them on your field, and touch one to rotate it.
			[sc:sheep_think] [sp:idle] After both are done arranging, the players take turns to guess where the other player's sheep are!
			[sc:sheep_find] [sp:idle] During each turn, a player selects a square on the field and tries to shear. \\ If they find wool, they can keep shearing!
			[sc:sheep_complete] [sp:idle] A tiny wool indicates that this sheep is still not done shearing. \\ A full-sized one indicates that it's been fully sheared.
			[sc:sheep_win] [sp:shock] When one player shears all the wool from the other player's field, they win!
			[sc:sheep_end] [sp:idle] That's it! Have fun with Battle Sheep!
			""")
		}
		tutorialNode.open()
	}
}
