import UIKit

class BaseballServer: ParServer {

	var state: State = .idle
	enum State {
		case idle
		case loaded
		case playing
		case end
	}

	var player1: String = ""
	var player2: String = ""
	var isPitcher = false
	var readyPlayer1 = false
	var readyPlayer2 = false

	init() {
		super.init(name: Baseball.serviceName)
	}

	override func started() {
		resetState()
	}

	override func stopped() {
		resetState()
	}

	func resetState() {
		state = .idle
		player1 = ""
		player2 = ""
		isPitcher = false
		readyPlayer1 = false
		readyPlayer2 = false
	}

	override func connected(peer: String) {
		sendState(to: [peer])
	}

	override func disconnected(peer: String) {
		if state != .idle && (player1 == peer || player2 == peer) {
			let add: [String : Any] = state == .end ? [:] : ["flag" : "disconnect"]
			resetState()
			sendState(add)
		}
	}

	override func received(data: Data, from peer: String) {
		guard let json = Json.parse(data), let name = json["?"] as? String else { return }

		switch name {
		case "start":
			guard state == .idle else { break }
			guard let p1 = json["p1"] as? String, let p2 = json["p2"] as? String else { break }
			let peerComparison = p1.compare(p2)
			guard peerComparison != .orderedSame && peers.contains(p1) && peers.contains(p2) else { break }
			resetState()

			if peerComparison == .orderedAscending {
				player1 = p1
				player2 = p2
			}

			else {
				player1 = p2
				player2 = p1
			}

			state = .playing
			sendState()

		case "stop":
			guard state != .idle else { break }
			let add: [String : Any] = state == .end ? [:] : ["flag" : "close"]
			resetState()
			sendState(add)

//		case "load":
//			guard state == .loaded && playerMatch(peer) != nil, let s = loadedGame else { break }
//			state = .playing
//			if json["load"] as? Bool == true {
//				isPitcher = s.isPitcher
//				gridPlayer1.copy(s.gridPlayer1)
//				gridPlayer2.copy(s.gridPlayer2)
//				hitPlayer1.copy(s.hitPlayer1)
//				hitPlayer2.copy(s.hitPlayer2)
//				readyPlayer1 = true
//				readyPlayer2 = true
//			}
//			loadedGame = nil
//			sendState()

		case "ready":
			guard state == .playing && !(readyPlayer1 && readyPlayer2), let p1 = playerMatch(peer) else { break }
			if p1 {
				readyPlayer1 = true
			} else {
				readyPlayer2 = true
			}

			if readyPlayer1 && readyPlayer2 {
				isPitcher = Random.bool
			}

			sendState()

		case "ball":
			guard state == .playing && readyPlayer1 && readyPlayer2 else { break }

			if let vel = json["vel"] as? Double {
				sendState(["vel" : vel])
			}

			else if let set = json["set"] as? Int {
				sendState(["set" : set])
			}

		case "end":
			guard state == .playing && readyPlayer1 && readyPlayer2, let p1 = playerMatch(peer) else { break }
			guard let pts = json["pts"] as? Int else { break }

			if p1 {
				sendState(["p1End" : pts])
			} else {
				sendState(["p2End" : pts])
			}

		case "selection":
			if let peers = peers(excluding: peer), let data = Json.data(["?" : "selection"]) {
				send(data: data, to: peers, reliable: true)
			}

		default: break
		}
	}

	func sendState(_ add: [String : Any] = [:]) {
		sendState(to: peers, add)
	}

	func sendState(to peers: [String], _ add: [String : Any] = [:]) {
		var json: [String : Any] = ["?" : "state"]

		if state != .idle {
			json["p1"] = player1
			json["p2"] = player2

			if state == .loaded {
				json["state"] = "loaded"
			}

			else {
				json["state"] = state == .playing ? "playing" : "end"

				if readyPlayer1 {
					if readyPlayer2 {
						json["p1turn"] = isPitcher
						json["p1Ready"] = true
						json["p2Ready"] = true
					}

					else {
						json["p1Ready"] = true
						json["p2Ready"] = false
					}
				}

				else if readyPlayer2 {
					json["p2Ready"] = true
					json["p1Ready"] = false
				}
			}
		}

		for (k, v) in add {
			json[k] = v
		}

		if let data = Json.data(json) {
			send(data: data, to: peers, reliable: true)
		}
	}

	func playerMatch(_ peer: String) -> Bool? {
		switch peer {
		case player1: return true
		case player2: return false
		default: return nil
		}
	}
}
