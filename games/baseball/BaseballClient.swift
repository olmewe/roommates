import UIKit

class BaseballClient: GameClient, SceneNode {
	init() {
		super.init(name: Baseball.serviceName)
	}

	override func gameStarted() {
		Baseball.load()
		Baseball.state = .loading
	}

	override func gameStopped() {
		Baseball.unload()
		Baseball.state = .loading
	}

	func lateUpdate() {
		Baseball.selection = false
		Baseball.flag = .none
	}

	override func received(data: Data) {
		guard let json = Json.parse(data), let name = json["?"] as? String else { return }
		switch name {
		case "state":
			let prevState = Baseball.state
			if let state = json["state"] as? String, let p1 = json["p1"] as? String, let p2 = json["p2"] as? String {

				switch GameData.id {
				case p1: Baseball.playerMatch = true
				case p2: Baseball.playerMatch = false
				default: Baseball.playerMatch = nil
				}

				Baseball.player1 = p1
				Baseball.player2 = p2

				switch state {
				case "loaded": Baseball.state = .loaded
				case "playing": Baseball.state = .playing
				case "end": Baseball.state = .end
				default: break
				}

				if let p1Ready = json["p1Ready"] as? Bool, let p2Ready = json["p2Ready"] as? Bool {
					Baseball.readyPlayer1 = p1Ready
					Baseball.readyPlayer2 = p2Ready
				}

				if let isPitcher = json["p1turn"] as? Bool {

					if Baseball.round % 2 != 0 {
						if Baseball.playerMatch == true {
							Baseball.isPitcher = isPitcher
						}

						else if Baseball.playerMatch == false {
							Baseball.isPitcher = !isPitcher
						}
					}
				}

				if let vel = json["vel"] as? Double {
					Baseball.velocity = vel
					if Baseball.isPitcher {
						Baseball.messageSent = true
						Baseball.messageReceived = true
					}

					else {
						Baseball.messageSent = false
						Baseball.messageReceived = true
					}
				}

				if let set = json["set"] as? Int {
					Baseball.set = set
					if !Baseball.isPitcher {
						Baseball.messageSent = true
						Baseball.messageReceived = false
					}

					else {
						Baseball.messageSent = false
						Baseball.messageReceived = true

						if Baseball.set == 5 && Baseball.round != 4 {
							Baseball.change = true
						}

						else if Baseball.set == 5 && Baseball.round == 4 {
							Baseball.end = true
						}
					}
				}

				if let pts = json["p1End"] as? Int {
					guard let pl1 = Baseball.playerMatch else { break }

					if !pl1 {
						Baseball.opponentScore = pts
					}
				}

				if let pts = json["p2End"] as? Int {
					guard let pl1 = Baseball.playerMatch else { break }

					if pl1 {
						Baseball.opponentScore = pts
					}
				}

				Baseball.flag = .none
			}

			else {
				Baseball.player1 = ""
				Baseball.player2 = ""
				Baseball.state = .idle

				if let flag = json["flag"] as? String {

					switch flag {
					case "disconnect": Baseball.flag = .disconnect
					case "close": Baseball.flag = .close
					default: break
					}
				}
			}

			if prevState == .loading {
				if Baseball.state == .idle {
					Scene.present(BaseballMenuScene(), transition: .fade(with: Baseball.colourBg, duration: 0.5))
				}

				else if Baseball.ready {
					if GameData.id != Baseball.player1 &&
						GameData.id != Baseball.player2 {
						Scene.present(BaseballLoadingScene(), transition: .fade(with: Baseball.colourBg, duration: 0.5))
					}

					else if GameData.id == Baseball.player1 ||
						GameData.id == Baseball.player2 {
						Scene.present(BaseballScene(), transition: .fade(with: Baseball.colourBg, duration: 0.5))
					}
				}

				else {
					Scene.present(BaseballLoadingScene(), transition: .fade(with: Baseball.colourBg, duration: 0.5))
				}
			}

			else if prevState == .playing {
				if Baseball.state == .idle {
					Scene.present(BaseballMenuScene(), transition: .fade(with: Baseball.colourBg, duration: 0.5))
					Baseball.clearAll()
				}
			}

		case "selection": Baseball.selection = true

		default: break
		}
	}

	func sendStart(p1: String, p2: String) {
		if let data = Json.data(["?" : "start", "p1" : p1, "p2" : p2]) {
			send(data: data, reliable: true)
		}
	}

	func sendReady() {
		if let data = Json.data(["?" : "ready"]) {
			send(data: data, reliable: true)
		}
	}

	func sendStop() {
		if let data = Json.data(["?" : "stop"]) {
			send(data: data, reliable: true)
		}
	}

	func sendLoad(load: Bool) {
		if let data = Json.data(["?" : "load", "load" : load]) {
			send(data: data, reliable: true)
		}
	}

	func sendBall(set: Int) {
		if let data = Json.data(["?" : "ball",
								 "set" : set]) {
			send(data: data, reliable: true)
			Baseball.messageSent = true
			Baseball.messageReceived = false
		}
	}

	func sendBall(vel: Double) {
		if let data = Json.data(["?" : "ball",
								 "vel" : vel]) {
			send(data: data, reliable: true)
			Baseball.messageSent = true
			Baseball.messageReceived = false
		}
	}

	func sendSelection() {
		if let data = Json.data(["?" : "selection"]) {
			send(data: data, reliable: true)
		}
	}

	func sendPoints(pts: Int) {
		if let data = Json.data(["?" : "end",
								 "pts" : pts]) {
			send(data: data, reliable: true)
		}
	}
}
