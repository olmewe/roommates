import Foundation
import SpriteKit

class Baseball {
    static let serviceName = "game_baseball"
    static let server = BaseballServer()
    static let client = BaseballClient()

	// ---------------------------------- //
    static let info = GameInfo(id: "baseball") {
        $0.value = 0

        $0.name = "Baseball"
        $0.description = "switch places between being the hitter and fielder, and time your swing!"
        $0.players = 2
        $0.genre = "Sports"
        $0.averageTime = 2
        $0.modes = [.comp]

        $0.assetsFolder = "game_baseball_meta"
        $0.bgColour = #colorLiteral(red: 1, green: 0.9991900325, blue: 0.7986494899, alpha: 1)
		$0.fgColour = #colorLiteral(red: 0.3960784314, green: 0.8039215686, blue: 1, alpha: 1)

        $0.server = server
        $0.client = client
    }

	static let contentSize = CGSize(width: 720, height: 1280)
    static var atlas: SKTextureAtlas?
	static let colourBg = #colorLiteral(red: 1, green: 0.9991900325, blue: 0.7986494899, alpha: 1)

    static var state: State = .loading
    enum State: String {
        case loading
        case idle
        case loaded
        case playing
        case end
    }

    static var playerMatch: Bool? = nil
    static var player1: String = ""
    static var player2: String = ""
    static var readyPlayer1 = false
    static var readyPlayer2 = false
	static var turnPlayer1 = false
    static var selection = false
	static var ready = false

    static var flag: Flag = .none
    enum Flag {
        case none
        case disconnect
        case close
    }

	// ----------------------------- //
	static var messageReceived = false
	static var precisionVelocity = 20
	static var messageSent = false
	static var loadControl = false
	static var isPitcher = false
	static var opponentScore = 0
	static var velocity = 20.0
	static var change = false
	static var begin = true
	static var end = false
	static var round = 1
	static var score = 0
	static var set = 1
	static var aux = 0

	// ----------------------------- //
    static func load() {
        atlas = SKTextureAtlas(named: "game_baseball")
    }

    static func unload() {
		selection = false
        atlas = nil
		endGame()
    }

	static func endGame() {
		clearVariables()
		clearAll()
	}

	static func clearVariables() {
		messageReceived = false
		messageSent = false
		velocity = 20.0
		change = false
		set = 1
	}

	static func clearAll() {
		readyPlayer1 = false
		readyPlayer2 = false
		ready = false

		precisionVelocity = 20
		messageSent = false
		loadControl = false
		isPitcher = false
		opponentScore = 0
		velocity = 20.0
		change = false
		begin = true
		end = false
		round = 1
		score = 0
		set = 1
		aux = 0
	}
}
