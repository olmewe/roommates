import UIKit
import SpriteKit

class BaseballMenuScene: Scene {
	let logoNode = SKSpriteNode()
	let closeNode = CloseNode()
	let startNode = PanelNode(size:CGSize(width:570,height:140),bg:#colorLiteral(red: 1, green: 1, blue: 0.6, alpha: 1),border:#colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1),colour:#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1),text:"start!",icon:SKTexture(imageNamed:"common/join"))
	let howNode = PanelNode(size:CGSize(width:570,height:90),bg:#colorLiteral(red: 0.8, green: 1, blue: 1, alpha: 1),border:#colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1),colour:#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1),text:"how to play")
	let switchNode = PanelNode(size:CGSize(width:570,height:90),bg:#colorLiteral(red: 0.8, green: 1, blue: 1, alpha: 1),border:#colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1),colour:#colorLiteral(red: 0.2117647059, green: 0.2117647059, blue: 0.2117647059, alpha: 1),text:"switch game")
	let selector = ButtonSelector()
	let margin: CGFloat = 40

	// ----------------------------- //
	var selectionView: SelectionViewController?
	var tutorialNode:TutorialNode!

	// ----------------------------- //
	var sfxSelect: Data?

	// ----------------------------- //
	override func load() {
		Baseball.load()
		size = Baseball.contentSize
		scaleMode = .aspectFit

		addChild(logoNode)
		logoNode.texture = Baseball.atlas?.textureNamed("baseball_telainicial_fundo")
		logoNode.size = CGSize(width: size.width, height: size.height)
		logoNode.zPosition = -1

		addChild(closeNode)
		closeNode.resize(size)

		addChild(startNode)
		startNode.position = CGPoint(x:0,y:-300)

		addChild(howNode)
		howNode.position = CGPoint(x:0,y:-440)

		addChild(switchNode)
		switchNode.position = CGPoint(x:0,y:-555)

		tutorialNode = TutorialNode(size:size,showIcon:false)
		addChild(tutorialNode)

		selector.nodes = [closeNode,startNode,howNode,switchNode]

		sfxSelect = Audio.data(of: "baseball/select.wav")
	}

	override func update() {
		if Baseball.state != .idle {
			if GameData.id != Baseball.player1 &&
				GameData.id != Baseball.player2 {
				Scene.present(BaseballLoadingScene(), transition: .fade(with: Baseball.colourBg, duration: 0.5))
				return
			}

			else if GameData.id == Baseball.player1 ||
				GameData.id == Baseball.player2 {
				Scene.present(BaseballScene(), transition: .fade(with: Baseball.colourBg, duration: 0.5))
				return
			}
			return
		}

		if Baseball.selection {
			openSelection()
		}

		if let i = selector.update() {
			switch i {
			case closeNode: GameState.closeGame()
			case startNode:
				Audio.play(sfxSelect)
				Baseball.client.sendSelection()
				openSelection()
			case howNode:
				Audio.play(sfxSelect)
				openTutorial()
			case switchNode: GameState.switchGame()
			default: break
			}
		}
	}

	func openSelection() {
		guard selectionView == nil && tutorialNode.view == nil else { return }
		selectionView = UI.showSelection(count: 2) {
			start, players in
			self.selectionView = nil
			if start && players.count == 2 {
				Baseball.client.sendStart(p1: players[0], p2: players[1])
			}
		}
	}

	func openTutorial() {
		guard selectionView == nil else { return }
		if tutorialNode.list == nil {
			tutorialNode.list = TutorialList(parse:"""
			[bg:creator]
			[sc:baseball_intro] [sp:shock] Oh hi! \\ \\ Welcome to Baseball, a game where each player must time their hits, switching between Pitcher and Hitter.
			[sc:baseball_position] [sp:idle] It's cooler when both players hold their phones like they're pointing to each other!
			[sc:baseball_pitcher] [sp:idle] Playing as the Pitcher, you must touch the screen when the precision bar is moving.
			[sc:baseball_hitter] [sp:idle] Playing as the Hitter, you must touch the screen when the ball is inside the circular precision area...
			[sc:baseball_sweet] [sp:idle] For both players, the closer you are to the sweet spot, the more points you get during that round!
			[sc:baseball_win] [sp:shock] In the end, whoever gets more points is the winner!
			[sc:baseball_end] [sp:idle] That's it! Have fun with Baseball!
			""")
		}
		tutorialNode.open()
	}
}
