import UIKit
import SpriteKit

class BaseballLoadingScene: Scene {
	let selector = ButtonSelector()
	var nameArray = Array<String>()
	let p1LabelNode = SKLabelNode()
	let p2LabelNode = SKLabelNode()
	let closeNode = SKSpriteNode()
	let labelNode = SKLabelNode()
	let aux2Node = SKSpriteNode()
	let auxNode = SKSpriteNode()
	let andNode = SKLabelNode()
	var p1Name = "P1"
	var p2Name = "P2"

	// ----------------------------- //
	override func load() {
		size = Baseball.contentSize
		scaleMode = .aspectFit
		backgroundColor = Baseball.colourBg

		for i in GameState.peers { nameArray.append(i.value.name) }

		if nameArray.count == 2 {
			if nameArray[0] != "" { p1Name = nameArray[0] }
			if nameArray[1] != "" { p2Name = nameArray[1] }
		}

		auxNode.position = CGPoint(x: 0, y: 150)
		auxNode.size = CGSize(width: size.width * 0.9, height: 50)
		auxNode.zPosition = 1

		p1LabelNode.position = CGPoint(x: auxNode.position.x, y: auxNode.position.y - auxNode.size.height / 2)
		p1LabelNode.fontSize = 45
		p1LabelNode.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		p1LabelNode.fontName = "AmericanTypewriter-Bold"
		p1LabelNode.text = "\(p1Name)"
		p1LabelNode.zPosition = 2

		andNode.position = CGPoint(x: 0, y: p1LabelNode.position.y - 90)
		andNode.fontSize = 45
		andNode.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		andNode.fontName = "AmericanTypewriter-Bold"
		andNode.text = "and"
		andNode.zPosition = 2

		aux2Node.position = CGPoint(x: 0, y: andNode.position.y - 90)
		aux2Node.size = CGSize(width: size.width * 0.9, height: 50)
		aux2Node.zPosition = 1

		p2LabelNode.position = CGPoint(x: aux2Node.position.x, y: aux2Node.position.y - aux2Node.size.height / 2)
		p2LabelNode.fontSize = 45
		p2LabelNode.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		p2LabelNode.fontName = "AmericanTypewriter-Bold"
		p2LabelNode.text = "\(p2Name)"
		p2LabelNode.zPosition = 2

		labelNode.position = CGPoint(x: 0, y: p2LabelNode.position.y - 90)
		labelNode.fontSize = 45
		labelNode.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		labelNode.fontName = "AmericanTypewriter-Bold"
		labelNode.text = "are playing now..."
		labelNode.zPosition = 2

		closeNode.position = CGPoint(x: size.width * 0.4 + 15, y: (size.height) * 0.4 + 70)
		closeNode.texture = SKTexture(imageNamed: "common/close")
		closeNode.size = CGSize(width: 114, height: 126)
		closeNode.zPosition = 3

		adjustLabelFontSizeToFitRect(p1LabelNode, auxNode)
		adjustLabelFontSizeToFitRect(p2LabelNode, aux2Node)

		addChild(aux2Node)
		addChild(auxNode)
		addChild(p1LabelNode)
		addChild(andNode)
		addChild(p2LabelNode)
		addChild(labelNode)
		addChild(closeNode)

		selector.nodes = [closeNode]
	}

	override func update() {
		if let i = selector.update() {
			switch i {
			case closeNode:
				Baseball.clearAll()
				GameState.closeGame()
			default: break
			}
		}
	}

	func adjustLabelFontSizeToFitRect(_ labelNode: SKLabelNode, _ node: SKSpriteNode) {

		let rect = node.frame

		// Determine the font scaling factor that should let the label text fit in the given rectangle.
		let scalingFactor = min(rect.width / labelNode.frame.width, rect.height / labelNode.frame.height)

		// Change the fontSize.
		if scalingFactor * 45 <= 45 { labelNode.fontSize *= scalingFactor }

		// Optionally move the SKLabelNode to the center of the rectangle.
		labelNode.position = CGPoint(x: rect.midX, y: rect.midY - labelNode.frame.height / 2.0)
	}
}
