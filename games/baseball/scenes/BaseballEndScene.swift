import UIKit
import SpriteKit

class BaseballEndScene: Scene {

	// ------------------ ATLAS ------------------ //
	let ravioliAtlas = SKTextureAtlas(named: "char_ravioli_sprites")
	let matildeAtlas = SKTextureAtlas(named: "char_matilde_sprites")
	let confettiAtlas = SKTextureAtlas(named: "game_baseball_win")
	let peachAtlas = SKTextureAtlas(named: "char_peach_sprites")
	let endAtlas = SKTextureAtlas(named: "game_baseball")

	// ------------------ NODES ------------------ //
	var transitionBall = SKShapeNode(circleOfRadius: 1)

	var characterLeftNode = SKSpriteNode()
	let pointsLeftLabel = SKLabelNode()
	//	let pointsLeftNode = SKSpriteNode()
	let floorLeftNode = SKSpriteNode()
	var confettiNode = SKSpriteNode()
	let leftLabel = SKLabelNode()
	let leftNode = SKSpriteNode()

	var characterRightNode = SKSpriteNode()
	let pointsRightLabel = SKLabelNode()
	//	let pointsRightNode = SKSpriteNode()
	let backgroundNode = SKSpriteNode()
	let floorRightNode = SKSpriteNode()
	let rightLabel = SKLabelNode()
	let rightNode = SKSpriteNode()

	let playAgain = SKSpriteNode()
	let leave = SKSpriteNode()

	// ----------------- ACTIONS ----------------- //
	var characterRightAnimation = SKAction()
	var characterLeftAnimation = SKAction()
	var confettiAnimation = SKAction()

	// ------------------- VAR ------------------- //
	var characterRightSprites = Array<SKTexture>()
	var characterLeftSprites = Array<SKTexture>()
	var confettiSprites = Array<SKTexture>()
	let selector = ButtonSelector()

	let pointsOpponent = Baseball.opponentScore
	let isPitcher = Baseball.isPitcher
	let pointsYou = Baseball.score
	var opInfo: PeerInfo?
	var myInfo: PeerInfo?

	// ----------------------------- //
	var sfxSelect:Data?

	override func load() {
		size = Baseball.contentSize
		alpha = 0
		scaleMode = .aspectFit

		Baseball.end = false

		myInfo = GameState.peerInfo(for: GameData.id)
		opInfo = GameState.peerInfo(for: Baseball.playerMatch == true ? Baseball.player2 : Baseball.player1)

		backgroundNode.texture = endAtlas.textureNamed("background_win_lose")
		backgroundNode.size = size
		backgroundNode.zPosition = 0
		addChild(backgroundNode)

		backgroundColor = #colorLiteral(red: 0.4304351807, green: 0.8814089894, blue: 0.9992426038, alpha: 1)

		// ------------------ LEFT ------------------ //
		leftLabel.position = CGPoint(x: -(size.width / 4) + 10, y: (size.height / 4))
		leftLabel.fontSize = 45
		leftLabel.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		leftLabel.fontName = "AmericanTypewriter-Bold"
		leftLabel.text = "You"
		leftLabel.zPosition = 2

		leftNode.position = leftLabel.position
		leftNode.texture = endAtlas.textureNamed("name_winner")
		leftNode.size = CGSize(width: 276 * 0.95, height: 167 * 0.95)
		leftNode.zPosition = 1

		//		pointsLeftLabel.position = CGPoint(x: -(size.width / 4) + 50, y: (size.height / 4) - 220)
		pointsLeftLabel.position = CGPoint(x: leftNode.position.x, y: (size.height / 4) - 220)
		pointsLeftLabel.fontSize = 45
		pointsLeftLabel.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		pointsLeftLabel.fontName = "AmericanTypewriter-Bold"
		pointsLeftLabel.text = "\(999)"
		pointsLeftLabel.zPosition = 1

		//		pointsLeftNode.position = CGPoint(x: -(size.width / 4) - 50, y: (size.height / 4) - 200)
		//		pointsLeftNode.texture = endAtlas.textureNamed("points")
		//		pointsLeftNode.size = CGSize(width: 68, height: 104)
		//		pointsLeftNode.zPosition = 1

		characterLeftNode.texture = ravioliAtlas.textureNamed("walk0")
		characterLeftNode.position = CGPoint(x: leftNode.position.x, y: -120)
		characterLeftNode.size = CGSize(width: 360, height: 360)
		characterLeftNode.zPosition = 3

		floorLeftNode.texture = endAtlas.textureNamed("floor_winner")
		floorLeftNode.position = CGPoint(x: leftNode.position.x, y: characterLeftNode.position.y - 120)
		floorLeftNode.size = CGSize(width: 196, height: 67)
		floorLeftNode.zPosition = 2

		// ------------------ RIGHT ------------------ //

		rightLabel.position = CGPoint(x: -leftNode.position.x + 10, y: leftNode.position.y)
		rightLabel.fontSize = 45
		rightLabel.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		rightLabel.fontName = "AmericanTypewriter-Bold"
		rightLabel.text = "Opponent"
		rightLabel.zPosition = 2

		rightNode.position = rightLabel.position
		rightNode.texture = endAtlas.textureNamed("name_loser")
		rightNode.size = CGSize(width: 276 * 0.95, height: 167 * 0.95)
		rightNode.zPosition = 1

		//		pointsRightLabel.position = CGPoint(x: -pointsLeftLabel.position.x + 90, y: pointsLeftLabel.position.y)
		pointsRightLabel.position = CGPoint(x: rightNode.position.x, y: pointsLeftLabel.position.y)
		pointsRightLabel.fontSize = 45
		pointsRightLabel.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		pointsRightLabel.fontName = "AmericanTypewriter-Bold"
		pointsRightLabel.text = "\(999)"
		pointsRightLabel.zPosition = 1

		//		pointsRightNode.position = CGPoint(x: pointsRightLabel.position.x - 100, y: pointsLeftNode.position.y)
		//		pointsRightNode.size = pointsLeftNode.size
		//		pointsRightNode.texture = pointsLeftNode.texture
		//		pointsRightNode.zPosition = 1

		characterRightNode.texture = ravioliAtlas.textureNamed("walk0")
		characterRightNode.position = CGPoint(x: rightNode.position.x, y: characterLeftNode.position.y)
		characterRightNode.size = CGSize(width: 360, height: 360)
		characterRightNode.zPosition = 3

		floorRightNode.texture = endAtlas.textureNamed("floor_loser")
		floorRightNode.position = CGPoint(x: rightNode.position.x, y: floorLeftNode.position.y)
		floorRightNode.size = CGSize(width: 196, height: 67)
		floorRightNode.zPosition = 2

		// ------------------ BOTTOM ------------------ //

		playAgain.texture = endAtlas.textureNamed("again_button")
		playAgain.position = CGPoint(x: 0, y: size.height * (-0.312))
		playAgain.size = CGSize(width: 557 * 0.8, height: 200 * 0.8)
		playAgain.zPosition = 1

		leave.texture = endAtlas.textureNamed("leave_button")
		leave.position = CGPoint(x: 0, y: playAgain.position.y - 150)
		leave.size = CGSize(width: 377 * 0.8, height: 151 * 0.8)
		leave.zPosition = 1

		selector.nodes = [playAgain, leave]

		// ----------------------------------------- //
		setWinner()

		addChild(leftLabel)
		addChild(leftNode)
		addChild(confettiNode)
		addChild(pointsLeftLabel)
		//		addChild(pointsLeftNode)
		addChild(characterLeftNode)
		addChild(floorLeftNode)

		addChild(rightLabel)
		addChild(rightNode)
		addChild(pointsRightLabel)
		//		addChild(pointsRightNode)
		addChild(characterRightNode)
		addChild(floorRightNode)

		addChild(playAgain)
		addChild(leave)

		sfxSelect = Audio.data(of:"baseball/select.wav")
	}

	override func update() {
		if let i = selector.update(), alpha >= 1 {
			switch i {
			case playAgain:
				Audio.play(sfxSelect)
				Baseball.clearAll()
				Baseball.score = 0
				Baseball.round = 1
				Baseball.client.sendStop()

			case leave:
				Audio.play(sfxSelect)
				Baseball.clearAll()
				Baseball.score = 0
				Baseball.round = 1
				GameState.switchGame()

			default: break
			}
		}

		if alpha < 1 {
			alpha += 0.01
		}
	}

	func setWinner() {

		if myInfo != nil && opInfo != nil {

			if pointsYou > pointsOpponent {
				leftLabel.text = myInfo!.name
				pointsLeftLabel.text = "\(pointsYou)"

				rightLabel.text = opInfo!.name
				pointsRightLabel.text = "\(pointsOpponent)"

				switch myInfo!.char {
				case "ravioli":
					characterLeftSprites = (0...4).map { ravioliAtlas.textureNamed("walk\($0)") }
				case "peach":
					characterLeftSprites = (0...4).map { peachAtlas.textureNamed("walk\($0)") }
				case "matilde":
					characterLeftSprites = (0...4).map { matildeAtlas.textureNamed("walk\($0)") }
				default: break
				}

				switch opInfo!.char {
				case "ravioli":
					characterRightSprites = (0...4).map { ravioliAtlas.textureNamed("walk\($0)") }
				case "peach":
					characterRightSprites = (0...4).map { peachAtlas.textureNamed("walk\($0)") }
				case "matilde":
					characterRightSprites = (0...4).map { matildeAtlas.textureNamed("walk\($0)") }
				default: break
				}

				confettiSprites = (0...5).map { confettiAtlas.textureNamed("animation\($0)") }

				confettiAnimation = SKAction.animate(with: self.confettiSprites, timePerFrame: 0.1)
				let repeatConfettiFor = SKAction.repeatForever(confettiAnimation)

				confettiNode = SKSpriteNode(texture: confettiSprites[0])
				confettiNode.position = leftNode.position
				confettiNode.size = leftNode.size
				confettiNode.zPosition = 2
				confettiNode.run(repeatConfettiFor)
			}

			else if pointsOpponent > pointsYou {
				leftLabel.text = opInfo!.name
				pointsLeftLabel.text = "\(pointsOpponent)"

				rightLabel.text = myInfo!.name
				pointsRightLabel.text = "\(pointsYou)"

				switch opInfo!.char {
				case "ravioli":
					characterLeftSprites = (0...4).map { ravioliAtlas.textureNamed("walk\($0)") }
				case "peach":
					characterLeftSprites = (0...4).map { peachAtlas.textureNamed("walk\($0)") }
				case "matilde":
					characterLeftSprites = (0...4).map { matildeAtlas.textureNamed("walk\($0)") }
				default: break
				}

				switch myInfo!.char {
				case "ravioli":
					characterRightSprites = (0...4).map { ravioliAtlas.textureNamed("walk\($0)") }
				case "peach":
					characterRightSprites = (0...4).map { peachAtlas.textureNamed("walk\($0)") }
				case "matilde":
					characterRightSprites = (0...4).map { matildeAtlas.textureNamed("walk\($0)") }
				default: break
				}

				confettiSprites = (0...5).map { confettiAtlas.textureNamed("animation\($0)") }

				confettiAnimation = SKAction.animate(with: self.confettiSprites, timePerFrame: 0.1)
				let repeatConfettiFor = SKAction.repeatForever(confettiAnimation)

				confettiNode = SKSpriteNode(texture: confettiSprites[0])
				confettiNode.position = leftNode.position
				confettiNode.size = leftNode.size
				confettiNode.zPosition = 2
				confettiNode.run(repeatConfettiFor)
			}

			else if pointsYou == pointsOpponent {
				leftNode.texture = endAtlas.textureNamed("name_loser")
				rightNode.texture = endAtlas.textureNamed("name_loser")

				floorLeftNode.texture = endAtlas.textureNamed("floor_loser")
				floorRightNode.texture = endAtlas.textureNamed("floor_loser")

				leftLabel.text = myInfo!.name
				rightLabel.text = opInfo!.name

				pointsLeftLabel.text = "\(pointsYou)"
				pointsRightLabel.text = "\(pointsOpponent)"

				switch myInfo!.char {
				case "ravioli":
					characterLeftSprites = (0...4).map { ravioliAtlas.textureNamed("walk\($0)") }
				case "peach":
					characterLeftSprites = (0...4).map { peachAtlas.textureNamed("walk\($0)") }
				case "matilde":
					characterLeftSprites = (0...4).map { matildeAtlas.textureNamed("walk\($0)") }
				default: break
				}

				switch opInfo!.char {
				case "ravioli":
					characterRightSprites = (0...4).map { ravioliAtlas.textureNamed("walk\($0)") }
				case "peach":
					characterRightSprites = (0...4).map { peachAtlas.textureNamed("walk\($0)") }
				case "matilde":
					characterRightSprites = (0...4).map { matildeAtlas.textureNamed("walk\($0)") }
				default: break
				}

				leftNode.removeAllActions()
			}

			characterLeftAnimation = SKAction.animate(with: self.characterLeftSprites, timePerFrame: 0.1)
			let repeatLeftFor = SKAction.repeatForever(characterLeftAnimation)

			characterLeftNode.run(repeatLeftFor)

			characterRightAnimation = SKAction.animate(with: self.characterRightSprites, timePerFrame: 0.1)
			let repeatRightFor = SKAction.repeatForever(characterRightAnimation)

			characterRightNode.run(repeatRightFor)

			adjustLabelFontSizeToFitRect(leftLabel, leftNode)
			adjustLabelFontSizeToFitRect(rightLabel, rightNode)
		}
	}

	func adjustLabelFontSizeToFitRect(_ labelNode: SKLabelNode, _ node: SKSpriteNode) {

		let rect = node.frame

		// Determine the font scaling factor that should let the label text fit in the given rectangle.
		let scalingFactor = min(rect.width / labelNode.frame.width, rect.height / labelNode.frame.height)

		// Change the fontSize.
		if scalingFactor * 45 <= 45 { labelNode.fontSize *= scalingFactor }

		// Optionally move the SKLabelNode to the center of the rectangle.
		labelNode.position = CGPoint(x: rect.midX, y: rect.midY - labelNode.frame.height / 2.0)
	}
}
