import UIKit
import SpriteKit

class BaseballChangeScene: Scene {
	let pitcherAtlas = SKTextureAtlas(named: "game_baseball_pitcher")
	var pitcherSprites = Array<SKTexture>()
	var pitcherNode = SKSpriteNode()
	var animatePitcher = SKAction()

	// ----------------------------- //
	let hitterAtlas = SKTextureAtlas(named: "game_baseball_hitter")
	var hitterSprites = Array<SKTexture>()
	var hitterNode = SKSpriteNode()
	var animateHitter = SKAction()

	// ----------------------------- //
	var messageSent = Baseball.messageSent
	var isPitcher = Baseball.isPitcher
	var velocity = Baseball.velocity
	var sec = CGFloat(0)
	var aux = 1

	// ----------------------------- //
	override func load() {
		size = Baseball.contentSize

		if !isPitcher {
			backgroundColor = #colorLiteral(red: 0.4009025991, green: 0.8010060191, blue: 1, alpha: 1)

			pitcherSprites.append(pitcherAtlas.textureNamed("animation0"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation1"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation2"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation3"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation4"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation5"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation6"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation7"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation8"))

			pitcherNode = SKSpriteNode(texture: pitcherSprites[0])
			pitcherNode.position = CGPoint(x: 0, y: 0)
			pitcherNode.size = CGSize(width: 142, height: 170)
			pitcherNode.alpha = 0
			pitcherNode.zPosition = 1

			animatePitcher = SKAction.animate(with: self.pitcherSprites, timePerFrame: 0.1)
			let repeatAction = SKAction.repeatForever(animatePitcher)

			pitcherNode.run(repeatAction)

			addChild(pitcherNode)
		}

		else {
			backgroundColor = #colorLiteral(red: 1, green: 1, blue: 0.6012218595, alpha: 1)

			hitterSprites.append(hitterAtlas.textureNamed("animation0"))
			hitterSprites.append(hitterAtlas.textureNamed("animation1"))
			hitterSprites.append(hitterAtlas.textureNamed("animation2"))
			hitterSprites.append(hitterAtlas.textureNamed("animation3"))
			hitterSprites.append(hitterAtlas.textureNamed("animation4"))
			hitterSprites.append(hitterAtlas.textureNamed("animation5"))
			hitterSprites.append(hitterAtlas.textureNamed("animation6"))
			hitterSprites.append(hitterAtlas.textureNamed("animation7"))
			hitterSprites.append(hitterAtlas.textureNamed("animation8"))

			hitterNode = SKSpriteNode(texture: hitterSprites[0])
			hitterNode.position = CGPoint(x: 0, y: 0)
			hitterNode.size = CGSize(width: 142, height: 170)
			hitterNode.alpha = 0
			hitterNode.zPosition = 1

			animateHitter = SKAction.animate(with: self.hitterSprites, timePerFrame: 0.1)
			let repeatAction = SKAction.repeatForever(animateHitter)

			hitterNode.run(repeatAction)

			addChild(hitterNode)
		}

		Audio.play("baseball/switch.wav")
	}

	override func update() {

		if sec >= 2 {
			if pitcherNode.alpha > 0 {
				pitcherNode.alpha -= (0.01 + Time.deltaTime)
			}

			if hitterNode.alpha > 0 {
				hitterNode.alpha -= (0.01 + Time.deltaTime)
			}

			else if pitcherNode.alpha <= 0 && hitterNode.alpha <= 0 {
				Baseball.change = false
				Baseball.isPitcher = !isPitcher
				Baseball.clearVariables()
				Baseball.round += 1
				Baseball.aux = 0
				Baseball.begin = true

				let Base = BaseballScene()
				Scene.present(Base)
			}
		}

		else {
			sec += Time.deltaTime

			if !isPitcher && pitcherNode.alpha < 1 {
				pitcherNode.alpha += (0.01 + Time.deltaTime)
			}

			else if isPitcher && hitterNode.alpha < 1 {
				hitterNode.alpha += (0.01 + Time.deltaTime)
			}
		}
	}
}
