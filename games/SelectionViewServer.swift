import Foundation

class SelectionViewServer:ParServer {
	var players:[String] = []

	init() {
		super.init(name:"selectionView")
	}

	override func started() {
		players = []
	}

	override func stopped() {
		players = []
	}

	override func connected(peer:String) {
		if let data = Json.data(["?":players]) {
			send(data:data,to:peer,reliable:true)
		}
	}

	override func disconnected(peer:String) {
		if let index = players.index(of:peer) {
			players.remove(at:index)
			if let data = Json.data(["-":peer]) {
				send(data:data,to:peers,reliable:true)
			}
		}
	}

	override func received(data:Data,from peer:String) {
		guard let first = data.first else { return }
		switch first {
		case 0:
			guard let index = players.index(of:peer) else { break }
			players.remove(at:index)
			if let peers = peers(excluding:peer),let data = Json.data(["-":peer]) {
				send(data:data,to:peers,reliable:true)
			}
		case 1:
			guard !players.contains(peer) else { break }
			players.append(peer)
			if let peers = peers(excluding:peer),let data = Json.data(["+":peer]) {
				send(data:data,to:peers,reliable:true)
			}
		case 2:
			if let peers = peers(excluding:peer),let data = Json.data(["!":true]) {
				send(data:data,to:peers,reliable:true)
			}
		default: break
		}
	}
}
