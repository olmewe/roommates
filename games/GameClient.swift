import SpriteKit

class GameClient:ParClient {
	final override func started() {
		presentDefaultScene()
	}

	final override func stopped() {
		HomeScene.startTransition = nil
		Scene.present(HomeScene())
	}

	final override func connected() {
		print("[game client] started!")
		gameStarted()
	}

	final override func disconnected() {
		print("[game client] stopped!")
		gameStopped()
		presentDefaultScene()
	}

	func presentDefaultScene() {
		let scene = GameIdleScene()
		scene.message = .standBy
		Scene.present(scene)
	}

	func gameStarted() {}
	func gameStopped() {}

	class GameDefaultScene:Scene {
		override func load() {
			size = CGSize(width:720,height:1280)
			backgroundColor = #colorLiteral(red: 0.1676449158, green: 0.1676449158, blue: 0.1676449158, alpha: 1)
		}
	}
}
