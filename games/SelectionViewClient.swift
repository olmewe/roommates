import Foundation

class SelectionViewClient:ParClient {
	var players:[String] = []
	weak var delegate:SelectionViewClientDelegate? = nil

	init() {
		super.init(name:"selectionView")
	}

	override func connected() {
		players = []
	}

	override func disconnected() {
		players = []
	}

	override func received(data:Data) {
		guard let json = Json.parse(data) else { return }
		if let list = json["?"] as? [String] {
			while let peer = players.popLast() {
				delegate?.selectionViewClient(self,peer:peer,connected:false)
			}
			for peer in list {
				players.append(peer)
				delegate?.selectionViewClient(self,peer:peer,connected:true)
			}
		} else if let peer = json["+"] as? String {
			if !players.contains(peer) {
				players.append(peer)
				delegate?.selectionViewClient(self,peer:peer,connected:true)
			}
		} else if let peer = json["-"] as? String {
			if let index = players.index(of:peer) {
				players.remove(at:index)
				delegate?.selectionViewClient(self,peer:peer,connected:false)
			}
		} else if json["!"] as? Bool == true {
			delegate?.selectionViewClient(self,start:true)
		}
	}

	func sendJoin(join:Bool) {
		send(data:Data([join ? 1 : 0]),reliable:true)
	}

	func sendStart() {
		send(data:Data([2]),reliable:true)
	}

	func toggle() -> Bool {
		if let index = players.index(of:GameData.id) {
			players.remove(at:index)
			sendJoin(join:false)
			return false
		}
		players.append(GameData.id)
		sendJoin(join:true)
		return true
	}
}

protocol SelectionViewClientDelegate:AnyObject {
	func selectionViewClient(_ client:SelectionViewClient,peer:String,connected:Bool)
	func selectionViewClient(_ client:SelectionViewClient,start:Bool)
}
