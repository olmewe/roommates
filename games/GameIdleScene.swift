import SpriteKit

class GameIdleScene:Scene {
	let closeNode = CloseNode()

	enum Message {
		case standBy
		case gameNotFound
	}
	var message:Message = .standBy
	var tempo:CGFloat = 0

	let selector = ButtonSelector()

	override func load() {
		if Game.heightRatio > 16/9 {
			size = CGSize(width:720,height:720*Game.heightRatio)
		} else {
			size = CGSize(width:1280*Game.widthRatio,height:1280)
		}
		backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5725490196, blue: 0.5882352941, alpha: 1)

		addChild(closeNode)
		closeNode.resize(size)

		selector.nodes = [closeNode]
	}

	override func show() {
		let title:String
		let desc:[String]
		switch message {
		case .gameNotFound:
			title = "game not found!"
			desc = ["you need to update roommates","on the App Store to play it."]
		case .standBy:
			title = "please stand by..."
			desc = []
		}
		func y(for i:Int) -> CGFloat {
			guard desc.count > 0 else { return 0 }
			let h = CGFloat(desc.count)*30
			return (CGFloat(i+1)/CGFloat(desc.count)).lerp(h,-h)
		}

		let titleNode = SKLabelNode(fontNamed:"Nunito-SemiBold")
		addChild(titleNode)
		titleNode.horizontalAlignmentMode = .center
		titleNode.verticalAlignmentMode = .baseline
		titleNode.position = CGPoint(x:0,y:y(for:-1))
		titleNode.fontSize = 54
		titleNode.fontColor = SKColor.white
		titleNode.text = title

		for (n,i) in desc.enumerated() {
			let descNode = SKLabelNode(fontNamed:"Nunito-Light")
			addChild(descNode)
			descNode.horizontalAlignmentMode = .center
			descNode.verticalAlignmentMode = .baseline
			descNode.position = CGPoint(x:0,y:y(for:n))
			descNode.fontSize = 40
			descNode.fontColor = SKColor.white
			descNode.text = i
		}

		tempo = 0
		alpha = 0
	}

	override func update() {
		if selector.update()?.isEqual(to:closeNode) == true {
			GameState.closeGame()
		}
		if tempo < 1 {
			tempo += Time.deltaTime*2
			if tempo > 1 {
				tempo = 1
			}
			alpha = (2-tempo)*tempo
		}
	}
}
