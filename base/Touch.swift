import SpriteKit

/** touch class. takes care of user input and touch events. its instance stores an event. */
struct Touch {
	/** finger id. store this to remember which touch is which */
	let id:Int
	/** current state */
	fileprivate(set) var state:TouchState
	/** current position in scene */
	fileprivate(set) var position:CGPoint
	/** first position in scene */
	fileprivate(set) var startPosition:CGPoint
	/** previous position in scene */
	fileprivate(set) var lastPosition:CGPoint
	/** current pressure [0-1]. devices that don't support force touch default this to 0 */
	fileprivate(set) var pressure:CGFloat
	/** velocity in which this touch has left the screen */
	fileprivate(set) var velocity:CGPoint

	/**
	creates a new touch event.
	- parameter id: finger id
	- parameter state: current state
	- parameter position: current position in scene
	- parameter pressure: current pressure [0-1]
	*/
	init(id:Int,state:TouchState,position:CGPoint,lastPosition:CGPoint,pressure:CGFloat) {
		self.id = id
		self.state = state
		self.position = position
		startPosition = position
		self.lastPosition = lastPosition
		self.pressure = pressure
		velocity = .zero
	}

	/**
	gets the touch position in local space for a given node.
	- parameter node: node to convert
	- returns: touch position
	*/
	func position(in node:SKNode) -> CGPoint {
		return node.convert(position,from:Scene.current)
	}

	/**
	gets the previous touch position in local space for a given node.
	- parameter node: node to convert
	- returns: previous touch position
	*/
	func lastPosition(in node:SKNode) -> CGPoint {
		return node.convert(lastPosition,from:Scene.current)
	}
}

/** describes the state of a touch event. */
enum TouchState:CustomStringConvertible {
	/** this touch has just begun */
	case down
	/** this touch has moved */
	case move
	/** this touch has just left the screen */
	case up
	/** something happened and this touch ceased to exist, without necessarily leaving the screen */
	case miss
	/** B) */
	var description:String {
		switch self {
		case .down: return "down"
		case .move: return "move"
		case .up: return "up"
		case .miss: return "miss"
		}
	}
}

extension Touch {
	/** list of current touch events */
	static fileprivate(set) var events:[Touch] = []
	/** table of touch positions */
	static private var positions:[Int:CGPoint] = [:]
	/** table of touch first positions */
	static private var firstPositions:[Int:CGPoint] = [:]

	/** called to update the velocities. */
	static func update() {
		var newPositions:[Int:CGPoint] = [:]
		for (n,i) in events.enumerated() {
			switch i.state {
			case .down:
				newPositions[i.id] = i.position
				firstPositions[i.id] = i.position
			case .move:
				newPositions[i.id] = i.position
				if let pos = firstPositions[i.id] {
					var ni = i
					ni.startPosition = pos
					events[n] = ni
				} else {
					firstPositions[i.id] = i.position
				}
			default:
				newPositions[i.id] = nil
				var ni = i
				if let pos = positions[i.id] {
					ni.velocity = CGPoint(x:(i.position.x-pos.x)/Time.deltaTime,y:(i.position.y-pos.y)/Time.deltaTime)
					positions[i.id] = nil
				}
				if let pos = firstPositions[i.id] {
					ni.startPosition = pos
					firstPositions[i.id] = nil
				}
				events[n] = ni
			}
		}
		positions = newPositions
	}

	/** called to update the event list afterwards. */
	static func lateUpdate() {
		events.removeAll(keepingCapacity:true)
	}

	/**
	registers a touch event to the future event list.
	- parameter id: touch id
	- parameter state: new state
	- parameter position: new position
	- parameter pressure: new pressure
	*/
	static func register(event:Touch) {
		events.append(event)
	}
}
