import SpriteKit

/** time class. stores time info. */
class Time {
	/** current time in seconds */
	static private(set) var time:CGFloat = 0
	/** time in seconds since last frame. one over current frame rate, too */
	static private(set) var deltaTime:CGFloat = CGFloat(defaultDeltaTime)

	static private var firstTime = true
	static private var rawTime:TimeInterval = 0
	static private var timeDouble:TimeInterval = 0

	static private let defaultDeltaTime:TimeInterval = 1/60
	static private let maxDeltaTime:TimeInterval = 1/15

	/**
	called by the current scene to update time and deltaTime.
	- paramenter currentTime: current time given by spritekit
	*/
	static func update(_ currentTime:TimeInterval) {
		if firstTime {
			firstTime = false
			rawTime = currentTime
			deltaTime = CGFloat(defaultDeltaTime)
			timeDouble = 0
			time = 0
		} else {
			let delta = currentTime-rawTime
			rawTime = currentTime
			if delta >= maxDeltaTime {
				timeDouble += maxDeltaTime
				deltaTime = CGFloat(maxDeltaTime)
			} else {
				timeDouble += delta
				if delta <= 0 {
					deltaTime = CGFloat(defaultDeltaTime)
				} else {
					deltaTime = CGFloat(delta)
				}
			}
			time = CGFloat(timeDouble)
		}
	}

	/** resets the time counter. */
	static func reset() {
		firstTime = true
	}
}
