import Foundation
import AVFoundation

class Audio {
	var onStop:(() -> Void)?
	private(set) var playing = true
	private(set) var volume:CGFloat = 1

	private var player:AVAudioPlayer!

	private var timer:Timer?
	private let timerDuration:CGFloat = 0.1
	private var timerOffset:CGFloat = 0
	private var timerEnd:CGFloat = 0
	private var timerStop = false

	init(_ file:String,volume:CGFloat = 1,fade:CGFloat = 0) {
		self.volume = fade >~ 0 ? 0 : volume
		DispatchQueue.global(qos:.default).async {
			guard let data = Audio.data(of:file),let player = try? AVAudioPlayer(data:data) else { return }
			self.player = player
			self.begin(volume:volume,fade:fade)
		}
	}

	init(_ data:Data?,volume:CGFloat = 1,fade:CGFloat = 0) {
		self.volume = fade >~ 0 ? 0 : volume
		DispatchQueue.global(qos:.default).async {
			guard let data = data,let player = try? AVAudioPlayer(data:data) else { return }
			self.player = player
			self.begin(volume:volume,fade:fade)
		}
	}

	private func begin(volume:CGFloat = 1,fade:CGFloat = 0) {
		player.numberOfLoops = -1
		player.currentTime = 0
		if playing {
			if fade >~ 0 {
				player.volume = 0
				DispatchQueue.main.sync {
					self.set(volume:volume,fade:fade)
				}
			} else {
				player.volume = Float(volume)
			}
			player.play()
		} else {
			onStop?()
		}
	}

	func play() {
		guard !playing else { return }
		playing = true
		DispatchQueue.global(qos:.default).async {
			self.playUnt()
		}
	}

	func pause() {
		guard playing else { return }
		playing = false
		DispatchQueue.global(qos:.default).async {
			self.pauseUnt()
		}
	}

	func stop() {
		guard playing else { return }
		playing = false
		DispatchQueue.global(qos:.default).async {
			self.stopUnt()
		}
		onStop?()
	}

	private func playUnt() {
		guard playing && player != nil else { return }
		player.volume = Float(volume)
		player.play()
	}

	private func pauseUnt() {
		guard !playing && player != nil else { return }
		player.pause()
	}

	private func stopUnt() {
		guard !playing && player != nil else { return }
		player.stop()
	}

	func set(volume:CGFloat,fade:CGFloat = 0,stop:Bool = false) {
		timerInvalidate()
		guard volume !=~ self.volume else { return }
		if playing && fade >~ 0 {
			timerOffset = (volume-self.volume)*timerDuration/fade
			timerEnd = volume
			timerStop = stop
			timer = Timer.scheduledTimer(timeInterval:Double(timerDuration),target:self,selector:#selector(timerStep),userInfo:nil,repeats:true)
		} else {
			self.volume = volume
			DispatchQueue.global(qos:.default).async {
				guard self.player != nil else { return }
				self.player.volume = Float(volume)
			}
		}
	}

	func fadeOut(for fade:CGFloat) {
		set(volume:0,fade:fade,stop:true)
	}

	@objc private func timerStep() {
		guard playing else {
			volume = timerEnd
			timerInvalidate()
			return
		}
		volume += timerOffset
		if timerOffset > 0 ? (volume >=~ timerEnd) : (volume <=~ timerEnd) {
			volume = timerEnd
			timerInvalidate()
			if timerStop {
				stop()
				return
			}
		}
		DispatchQueue.global(qos:.default).async {
			guard self.player != nil else { return }
			self.player.volume = Float(self.volume)
		}
	}

	private func timerInvalidate() {
		if let timer = timer {
			timer.invalidate()
			self.timer = nil
		}
	}
}

extension Audio {
	static func data(of file:String) -> Data? {
		guard let url = Bundle.main.url(forResource:"Audio/"+file,withExtension:nil) else { return nil }
		return try? Data(contentsOf:url)
	}

	private static var once = Set<AVAudioPlayer>()
	private static var onceDelegate = OnceDelegate()
	private class OnceDelegate:NSObject,AVAudioPlayerDelegate {
		func audioPlayerDidFinishPlaying(_ player:AVAudioPlayer,successfully flag:Bool) {
			once.remove(player)
		}
	}

	static func play(_ file:String,volume:CGFloat = 1) {
		DispatchQueue.global(qos:.default).async {
			guard let data = data(of:file),let player = try? AVAudioPlayer(data:data) else { return }
			once.insert(player)
			player.delegate = onceDelegate
			player.numberOfLoops = 0
			player.currentTime = 0
			player.volume = Float(volume)
			player.play()
		}
	}

	static func play(_ data:Data?,volume:CGFloat = 1) {
		DispatchQueue.global(qos:.default).async {
			guard let data = data,let player = try? AVAudioPlayer(data:data) else { return }
			once.insert(player)
			player.delegate = onceDelegate
			player.numberOfLoops = 0
			player.currentTime = 0
			player.volume = Float(volume)
			player.play()
		}
	}
}

class AudioOld {
	static var sfxVolume:CGFloat = 1
	static var bgmVolume:CGFloat = 1
	static var sfxMute = false
	static var bgmMute = false

	private static var files:[String:AudioFile] = [:]
	private static var bgm:[String:Bgm] = [:]
	private static var bgmFade:[String:BgmFade] = [:]

	static func play(sfx:String,volume:CGFloat = 1) {
		guard let audio = files[sfx] else { return }
		DispatchQueue.global(qos:.default).async {
			if audio.player.isPlaying {
				audio.player.pause()
			}
			audio.player.numberOfLoops = 0
			audio.player.currentTime = 0
			audio.player.volume = Float(volume)*getSfxVolume()
			audio.player.play()
		}
	}

	static func play(bgm bgmNames:[String],volume:[CGFloat] = []) {
		for (n,i) in bgmNames.enumerated() {
			guard bgm[i] == nil else { continue }
			guard let audio = files[i] else { continue }
			let b = Bgm(audio:audio)
			b.volume = (n < volume.count) ? Float(volume[n]) : 1
			bgm[i] = b
			DispatchQueue.global(qos:.default).async {
				if audio.player.isPlaying {
					audio.player.pause()
				}
				audio.player.numberOfLoops = -1
				audio.player.currentTime = 0
				audio.player.volume = b.volume*getBgmVolume()
				audio.player.play()
			}
		}
	}

	static func play(bgm bgmName:String,volume:CGFloat = 1) {
		play(bgm:[bgmName],volume:[volume])
	}

	static func set(bgm bgmNames:[String],volume:[CGFloat],fade:CGFloat = 0) {
		for (n,i) in bgmNames.enumerated() {
			guard let b = bgm[i] else { continue }
			guard n < volume.count else { return }
			let fadeTo = Float(volume[n])
			if b.volume != fadeTo {
				if fade <= 0 {
					b.volume = fadeTo
					b.audio.player.volume = fadeTo*getBgmVolume()
				} else {
					bgmFade[i] = BgmFade(audio:i,currentVolume:b.volume,newVolume:fadeTo,duration:Float(fade))
				}
			} else {
				bgmFade[i] = nil
			}
		}
	}

	static func set(bgm bgmName:String,volume:CGFloat,fade:CGFloat = 0) {
		set(bgm:[bgmName],volume:[volume],fade:fade)
	}

	static func stop(bgm bgmName:String) {
		if let b = bgm[bgmName] {
			DispatchQueue.global(qos:.default).async {
				if b.audio.player.isPlaying {
					b.audio.player.pause()
				}
			}
			bgm[bgmName] = nil
		}
	}

	static func stopBgms() {
		for (_,i) in bgm {
			DispatchQueue.global(qos:.default).async {
				if i.audio.player.isPlaying {
					i.audio.player.pause()
				}
			}
		}
		bgm.removeAll()
	}

	static func updateVolume() {
		for (_,i) in bgm {
			DispatchQueue.global(qos:.default).async {
				i.audio.player.volume = i.volume*getBgmVolume()
			}
		}
	}

	static func update() {
		guard !bgmFade.isEmpty else { return }
		let fade = bgmFade.keys
		for i in fade {
			guard let fadeProps = bgmFade[i] else { continue }
			guard let b = bgm[fadeProps.audio] else {
				bgmFade[i] = nil
				continue
			}
			var nv = b.volume+fadeProps.velocity*Float(Time.deltaTime)
			if fadeProps.velocity < 0 {
				if nv <= fadeProps.volume {
					nv = fadeProps.volume
					bgmFade[i] = nil
				}
			} else {
				if nv >= fadeProps.volume {
					nv = fadeProps.volume
					bgmFade[i] = nil
				}
			}
			b.volume = nv
			DispatchQueue.global(qos:.default).async {
				b.audio.player.volume = nv*getBgmVolume()
			}
		}
	}

	private static func getSfxVolume() -> Float {
		return sfxMute ? 0 : Float(sfxVolume)
	}

	private static func getBgmVolume() -> Float {
		return bgmMute ? 0 : Float(bgmVolume)
	}

	private struct BgmFade {
		let audio:String
		let volume:Float
		let velocity:Float

		init(audio:String,currentVolume:Float,newVolume:Float,duration:Float) {
			self.audio = audio
			volume = newVolume
			velocity = (newVolume-currentVolume)/duration
		}
	}

	private class Bgm {
		let audio:AudioFile
		var volume:Float

		init(audio:AudioFile) {
			self.audio = audio
			self.volume = 1
		}
	}

	private class AudioFile {
		var path:String
		let player:AVAudioPlayer

		init?(path:String) {
			self.path = path
			guard let url = Bundle.main.url(forResource:"audio/"+path,withExtension:nil) else {
				return nil
			}
			do {
				try player = AVAudioPlayer(contentsOf:url)
			} catch {
				return nil
			}
		}
	}
}
