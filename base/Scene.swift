import SpriteKit

/** scene class. base to all scenes. */
class Scene:SKScene,SceneNode {
	/** current scene being shown */
	static var current:Scene {
		return Game.view.scene as! Scene
	}

	private var loaded = false
	private var shown = false

	var cam = SKCameraNode()

	/** overrides from skscene. use load() instead. */
	final override func sceneDidLoad() {
		tryLoad()
	}

	/** overrides from skscene. use show() instead. */
	final override func didMove(to view:SKView) {
		tryLoad()
		tryShow()
	}

	/** overrides from skscene. use hide() instead. */
	final override func willMove(from view:SKView) {
		tryHide()
	}

	/** loads the scene, if needed. */
	private func tryLoad() {
		guard !loaded else { return }
		loaded = true
		scaleMode = .aspectFit
		camera = cam
		addChild(cam)
		load()
	}

	/** shows the scene, if needed. */
	private func tryShow() {
		guard !shown else { return }
		shown = true
		show()
	}

	/** hides the scene, if needed. */
	private func tryHide() {
		guard shown else { return }
		shown = false
		hide()
	}

	/** overrides from skscene. use update() instead. */
	final override func update(_ currentTime:TimeInterval) {
		Time.update(currentTime)
		Touch.update()
		GameState.update()
		updateRoutine()
		GameState.lateUpdate()
		Touch.lateUpdate()
	}

	/** called on scene load (usually when an instance is created). */
	func load() {}
	/** called when this scene is shown to this view. */
	func show() {}
	/** called when this scene is hidden from this view. */
	func hide() {}
	/** called on game loop. */
	func update() {}
	/** called on game loop after the main loop. */
	func lateUpdate() {}

	/** overrides from skscene. use Touch.events instead. */
	final override func touchesBegan(_ touches:Set<UITouch>,with event:UIEvent?) {
		touchesUpdated(touches)
	}

	/** overrides from skscene. use Touch.events instead. */
	final override func touchesMoved(_ touches:Set<UITouch>,with event:UIEvent?) {
		touchesUpdated(touches)
	}

	/** overrides from skscene. use Touch.events instead. */
	final override func touchesEnded(_ touches:Set<UITouch>,with event:UIEvent?) {
		touchesUpdated(touches)
	}

	/** overrides from skscene. use Touch.events instead. */
	final override func touchesCancelled(_ touches:Set<UITouch>,with event:UIEvent?) {
		touchesUpdated(touches)
	}

	/**
	registers touch updates to Touch.
	- parameter touches: new touch events
	*/
	private func touchesUpdated(_ touches:Set<UITouch>) {
		for t in touches {
			let state:TouchState
			switch t.phase {
			case .began: state = .down
			case .ended: state = .up
			case .cancelled: state = .miss
			default: state = .move
			}
			let force:CGFloat
			if #available(iOS 9,*) {
				force = ((t.force-0.5)/6.16).clamp01
			} else {
				force = 0
			}
			Touch.register(event:Touch(id:t.hashValue,state:state,position:t.location(in:self),lastPosition:t.previousLocation(in:self),pressure:force))
		}
	}
}

extension SKScene {
	/** recursively updates all nodes using SceneNode. */
	func updateRoutine() {
		func recursiveUpdate(_ node:SKNode) {
			for i in node.children {
				(i as? SceneNode)?.update()
				if i as? SceneNodeHalt == nil {
					recursiveUpdate(i)
				}
			}
		}
		func recursiveLateUpdate(_ node:SKNode) {
			for i in node.children {
				(i as? SceneNode)?.lateUpdate()
				if i as? SceneNodeHalt == nil {
					recursiveLateUpdate(i)
				}
			}
		}
		(self as? SceneNode)?.update()
		recursiveUpdate(self)
		(self as? SceneNode)?.lateUpdate()
		recursiveLateUpdate(self)
	}
}

extension Scene {
	/**
	presents a new scene.
	- parameter scene: scene to be presented
	- parameter popUI: if true, all ui overlays are removed
	- parameter animated: if true, ui animations are enabled
	*/
	static func present(_ scene:Scene,popUI:Bool = true,animated:Bool = true) {
		if popUI {
			UI.popAll(animated:animated)
		}
		Game.view.presentScene(scene)
	}

	/**
	presents a new scene.
	- parameter scene: scene to be presented
	- parameter transition: how this scene should be presented, transitioning from the current one using the default spritekit transition style
	- parameter popUI: if true, all ui overlays are removed
	- parameter animated: if true, ui animations are enabled
	*/
	static func present(_ scene:Scene,transition:SKTransition,popUI:Bool = true,animated:Bool = true) {
		if popUI {
			UI.popAll(animated:animated)
		}
		Game.view.presentScene(scene,transition:transition)
	}
}

/** implement this to get your node being called during scene events! */
protocol SceneNode {
	/** called on game loop. */
	func update()
	/** called on game loop after the main loop. */
	func lateUpdate()
}

protocol SceneNodeHalt {
}

extension SceneNode {
	func update() {}
	func lateUpdate() {}
}
