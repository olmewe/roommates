import SpriteKit

/** game manager class. the Singleton™ */
class Game {
	static private let debug = false

	/** spritekit view being used */
	static private(set) var view:SKView!
	/** screen width ratio */
	static private(set) var widthRatio:CGFloat = 0
	/** screen height ratio */
	static private(set) var heightRatio:CGFloat = 0
	/** are we running on an ipad or something like that? */
	static private(set) var pad:Bool = false

	static var hidden = false

	/**
	starts the game with a given spritekit window.
	- parameter view: spritekit view
	- parameter viewController: game view controller instance
	- parameter width: window width
	- parameter height: window height
	*/
	static func start(view:SKView,vc:GameViewController,width:CGFloat,height:CGFloat,pad:Bool) {
		Game.view = view
		view.ignoresSiblingOrder = true
		view.showsFPS = debug
		view.showsNodeCount = debug
		view.showsDrawCount = debug

		let minRatio = CGSize(width:3,height:4)
		let maxRatio = CGSize(width:1125,height:2436)
		widthRatio = (width/height).clamp(maxRatio.width/maxRatio.height,minRatio.width/minRatio.height)
		heightRatio = (height/width).clamp(minRatio.height/minRatio.width,maxRatio.height/maxRatio.width)
		Game.pad = pad

		GameData.load()
		GameState.start()

		Time.reset()
		UI.start(vc:vc)

		HomeScene.startTransition = .welcome
		let firstScene = HomeScene()
		Scene.present(firstScene)
	}

	/** called when the window is hidden. e.g. user presses home button. */
	static func hide() {
		hidden = true
	}

	/** called when the window is shown. e.g. user reopens the app. */
	static func show() {
		hidden = false
		Time.reset()
	}

	/** called when the window is closed. e.g. user kills the app. */
	static func exit() {
		GameState.par.disable()
	}
}
