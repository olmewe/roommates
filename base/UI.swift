import UIKit

class UI {
	/** host view controller */
	static private(set) var viewController:GameViewController!
	/** view stack */
	static private(set) var stack:[UIViewController] = []

	/**
	starts the ui stack.
	- parameter vc: game view controller
	*/
	static func start(vc:GameViewController) {
		viewController = vc
		stack = []
	}

	/**
	loads a new view controller.
	- parameter id: view controller id
	*/
	static func load(_ id:String) -> UIViewController? {
		return viewController.storyboard?.instantiateViewController(withIdentifier:id)
	}

	/**
	pushes a view controller to the stack.
	- parameter vc: view controller to be pushed
	- parameter animated: if true, the presentation is animated
	- parameter completion: called after the presentation
	*/
	static func push(_ vc:UIViewController?,animated:Bool = true,completion:(() -> Void)? = nil) {
		guard let vc = vc else { return }
		(stack.last ?? viewController).present(vc,animated:animated,completion:completion)
		stack.append(vc)
	}

	/**
	pops a view controller from the stack.
	- parameter animated: if true, the presentation is animated
	- parameter completion: called after the presentation
	*/
	static func pop(animated:Bool = true,completion:(() -> Void)? = nil) {
		guard !stack.isEmpty else { return }
		stack.removeLast().dismiss(animated:animated,completion:completion)
	}

	/**
	pops a view controller based on its index from the stack.
	- parameter count: number of view controllers to be popped
	- parameter animated: if true, the presentation is animated
	- parameter completion: called after the presentation
	*/
	static func pop(index:Int,animated:Bool = true,completion:(() -> Void)? = nil) {
		guard index < stack.count else { return }
		let index = max(0,index)
		let count = stack.count-index
		guard count > 0 else { return }
		guard count > 1 else {
			pop(animated:animated,completion:completion)
			return
		}
		var views = [UIViewController](stack[index..<stack.count])
		stack.removeLast(count)
		func comp() {
			if views.count > 1 {
				views.removeLast().dismiss(animated:false,completion:comp)
			} else {
				views.removeLast().dismiss(animated:animated,completion:completion)
			}
		}
		comp()
	}
}

extension UI {
	/**
	pops all view controllers from the stack.
	- parameter animated: if true, the presentation is animated
	- parameter completion: called after the presentation
	*/
	static func popAll(animated:Bool = true,completion:(() -> Void)? = nil) {
		pop(index:0,animated:animated,completion:completion)
	}

	/**
	pops a number of view controlers from the stack.
	- parameter count: number of view controllers to be popped
	- parameter animated: if true, the presentation is animated
	- parameter completion: called after the presentation
	*/
	static func pop(count:Int,animated:Bool = true,completion:(() -> Void)? = nil) {
		pop(index:stack.count-count,animated:animated,completion:completion)
	}

	/**
	pops a view controller and all view controllers on top of it altogether from the stack.
	- parameter vc: view controller to be popped
	- parameter animated: if true, the presentation is animated
	- parameter completion: called after the presentation
	*/
	static func pop(_ vc:UIViewController,animated:Bool = true,completion:(() -> Void)? = nil) {
		guard let index = stack.index(where:{ $0 == vc }) else { return }
		pop(index:index,animated:animated,completion:completion)
	}

	/**
	pops all view controllers until a given view controller (exclusive) from the stack.
	- parameter vc: view controller to be on top
	- parameter animated: if true, the presentation is animated
	- parameter completion: called after the presentation
	*/
	static func pop(to vc:UIViewController,animated:Bool = true,completion:(() -> Void)? = nil) {
		guard let index = stack.index(where:{ $0 == vc }) else { return }
		pop(index:index+1,animated:animated,completion:completion)
	}

	/**
	pops all view controllers but the root one.
	- parameter animated: if true, the presentation is animated
	- parameter completion: called after the presentation
	*/
	static func popToRoot(animated:Bool = true,completion:(() -> Void)? = nil) {
		pop(index:1,animated:animated,completion:completion)
	}
}

extension UI {
	static func showSelection(min:Int,max:Int?,end:((Bool,[String]) -> Void)? = nil) -> SelectionViewController? {
		guard let view = UI.load("SelectionViewController") as? SelectionViewController else { return nil }
		view.minPeers = min
		view.maxPeers = max
		view.end = end
		UI.push(view,animated:true)
		return view
	}

	static func showSelection(min:Int,end:((Bool,[String]) -> Void)? = nil) -> SelectionViewController? {
		return showSelection(min:min,max:nil,end:end)
	}

	static func showSelection(count:Int,end:((Bool,[String]) -> Void)? = nil) -> SelectionViewController? {
		return showSelection(min:count,max:count,end:end)
	}

	static func showTutorial(list:TutorialList,end:(() -> Void)? = nil) -> TutorialViewController? {
		guard let view = UI.load("TutorialViewController") as? TutorialViewController else { return nil }
		view.list = list
		view.end = end
		UI.push(view,animated:true)
		return view
	}
}

extension UIView {
	@IBInspectable var borderWidth:CGFloat {
		get {
			return layer.borderWidth
		}
		set {
			layer.borderWidth = newValue
		}
	}

	@IBInspectable var borderColor:UIColor? {
		get {
			if let color = layer.borderColor {
				return UIColor(cgColor:color)
			}
			return nil
		}
		set {
			layer.borderColor = newValue?.cgColor
		}
	}
}
