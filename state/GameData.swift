import SpriteKit

class GameData {
	static var id:String = ""
	static var peerInfo = PeerInfo()
	static var homeInfo = HomeInfo()
	static var lastGame:String = ""
}

extension GameData {
	static func acquire(game id:String) {
		guard GameInfo.games[id] != nil else { return }
		var set = Set<String>(homeInfo.games)
		guard set.insert(id).inserted else { return }
		homeInfo.games = GameInfo.gamesSorted.filter(set.contains)
		updateHomeInfo()
	}
}

extension GameData {
	static func loadJson(forKey key:String) -> [String:Any]? {
		return UserDefaults.standard.dictionary(forKey:key)
	}

	static func saveJson(_ json:[String:Any],forKey key:String) {
		UserDefaults.standard.set(json,forKey:key)
	}

	static func deleteJson(forKey key:String) {
		UserDefaults.standard.removeObject(forKey:key)
	}

	static func load() {
		let ud = UserDefaults.standard
		let data = ud.dictionaryRepresentation()
		let reset:Bool

		let build = data["build"] as? Int ?? 1
		if build != 3 {
			set(flag:"phonecall",to:true)
			ud.setValue(3,forKey:"build")
		}

		if data["peer"] != nil {
			ud.removeObject(forKey:"peer")
			reset = true
		} else {
			reset = false
		}

		if let i = data["id"] as? String,!i.isEmpty {
			id = i
		} else {
			id = Par.randomId
			ud.setValue(id,forKeyPath:"id")
		}

		peerInfo = PeerInfo.s(data["peerInfo"],mode:.generate) ?? PeerInfo()
		if reset {
			peerInfo.name = ""
		}
		updatePeerInfo()

		homeInfo = HomeInfo.s(data["homeInfo"],mode:.generate) ?? HomeInfo()
		updateHomeInfo()

		if let e = data["lastGame"] as? String,homeInfo.games.contains(e) {
			lastGame = e
		} else {
			lastGame = homeInfo.games.first ?? ""
			updateLastGame()
		}
	}

	static func updatePeerInfo() {
		let ud = UserDefaults.standard
		ud.set(peerInfo.s,forKey:"peerInfo")
	}

	static func updateHomeInfo() {
		let ud = UserDefaults.standard
		ud.set(homeInfo.s,forKey:"homeInfo")
	}

	static func updateLastGame() {
		let ud = UserDefaults.standard
		ud.set(lastGame,forKey:"lastGame")
	}

	static func get(flag:String) -> Bool {
		let ud = UserDefaults.standard
		return ud.bool(forKey:flag)
	}

	static func set(flag:String,to value:Bool) {
		let ud = UserDefaults.standard
		if value {
			ud.set(true,forKey:flag)
		} else {
			ud.removeObject(forKey:flag)
		}
	}
}

/*
save data through builds

#3

build: Int
flag_[...]: Bool
id: String
peerInfo: [String:Any]
	name: String
	char: String
homeInfo: [String:Any]
	theme: String
	games: [String]
lastGame: String

#2

build: Int
id: String
peerInfo: [String:Any]
	name: String
	char: String
homeInfo: [String:Any]
	theme: String
	games: [String]
lastGame: String

#1

peer: Data (MCPeerID decodable)
peerInfo: [String:Any]
	name: String
	char: String
homeInfo: [String:Any]
	name: String
	theme: String
	games: [String]
lastGame: String

*/
