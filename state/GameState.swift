import SpriteKit

class GameState {
	static var par:ParService!
	private static let delegate = Internal()
	private static let server = StateServer()
	private static let client = StateClient()
	private static let field = StateField()

	static var sessionBrowseEvent:((String,[String:Any]?,Bool) -> Void)? {
		didSet {
			if let event = sessionBrowseEvent {
				for (peer,info) in par.sessions {
					event(peer,info,true)
				}
			}
		}
	}

	static var inMenu:Bool {
		return !par.sessionConnected && par.fieldChannel == nil
	}

	static var atStore:Bool {
		guard let channel = par.fieldChannel else { return false }
		return channel == "store"
	}

	static var house:String? {
		return par.session
	}

	static var houseConnecting:Bool {
		return !par.sessionConnected && par.session != nil
	}

	static var houseConnected:Bool {
		return par.sessionConnected
	}

	static var atHome:Bool {
		guard let session = par.session else { return false }
		return session == par.id
	}

	static func start() {
		par = ParService(id:GameData.id,serviceType:"dogroll-par")
		par.delegate = delegate
		goHome()
	}

	static func goToStore() {
		par.setFieldMode(channel:"store")
	}

	static func goToMenu() {
		if par.sessionMode {
			par.disconnect()
		} else {
			par.setSessionMode()
		}
	}

	static func visit(peer:String) {
		_ = par.connect(to:peer)
	}

	static func cancelVisit() {
		par.cancelConnect()
	}

	static func goHome() {
		//if !par.sessionMode {
		//	par.setSessionMode()
		//}
		_ = par.connect(to:par.id)
	}
}

extension GameState {
	static var peers:[String:PeerInfo] = [:]
	static var peerInfoUpdate:((String,PeerInfo?) -> Void)? = nil

	static var homeInfo:HomeInfo!
	static var homePeer:PeerInfo!
	static var houseVisible = false
	static var playingPeers:[String] = []
	static var playingPeersUpdate:((String,Int,Bool) -> Void)? = nil

	static var gameSelected:String = ""
	static var gameRunning = false
	static var gameOpen = false
	static var gameClient:ParClient? = nil
	static var gameClientName = ""

	static var charAreaServer:CharAreaServer?
	static var charAreaFirstSpawn = true

	static var loadingHouse:Bool {
		return houseConnected && homeInfo == nil
	}

	static func peerInfo(for id:String) -> PeerInfo? {
		if id == GameData.id {
			return GameData.peerInfo
		}
		return peers[id]
	}

	static func updateSessionInfo() {
		var info:[String:Any] = ["peers":peers.count+1,"info":homeInfo.s,"peer":GameData.peerInfo.s]
		if gameRunning {
			info["game"] = gameSelected
		}
		par.setSessionInfo(info)
	}

	private static func resetState() {
		peers = [:]

		homeInfo = nil
		homePeer = nil
		houseVisible = false
		playingPeers = []
		playingPeersUpdate = nil

		gameSelected = ""
		gameRunning = false
		gameOpen = false
		gameClient = nil
		gameClientName = ""

		charAreaServer = nil
		charAreaFirstSpawn = true
	}

	static func startGame(_ game:String) {
		if let client = gameClient {
			par.stop(client:client)
		}
		let client = GameInfo.games[game]?.client ?? NullGameClient()
		gameClient = client
		gameClientName = game
		par.start(client:client)
	}

	static func stopGame() {
		if let client = gameClient {
			gameClient = nil
			gameClientName = ""
			par.stop(client:client)
		}
	}

	static func showHouse() {
		houseVisible = true
		client.send(visible:true)
	}

	static func hideHouse() {
		houseVisible = false
		client.send(visible:false)
	}

	static func select(game:String,play:Bool = false) {
		if !gameRunning {
			gameSelected = game
			client.send(game:game,play:play)
		}
	}

	static func switchGame() {
		if gameRunning {
			client.send(game:"",play:false)
		}
	}

	static func openGame() {
		gameOpen = true
		client.send(playing:true)
	}

	static func closeGame() {
		gameOpen = false
		client.send(playing:false)
	}
}

extension GameState {
	static func update() {
		for (_,i) in par.servers {
			(i as? SceneNode)?.update()
		}
		for (_,i) in par.clients {
			(i as? SceneNode)?.update()
		}
		for (_,i) in par.fields {
			(i as? SceneNode)?.update()
		}
	}

	static func lateUpdate() {
		for (_,i) in par.servers {
			(i as? SceneNode)?.lateUpdate()
		}
		for (_,i) in par.clients {
			(i as? SceneNode)?.lateUpdate()
		}
		for (_,i) in par.fields {
			(i as? SceneNode)?.lateUpdate()
		}
		par.dequeueData()
		if gameClient != nil && (!gameOpen || !gameRunning || gameClientName != gameSelected) {
			stopGame()
		}
	}

	private class Internal:ParServiceDelegate {
		func parService(_ parService:ParService,field channel:String,connected:Bool) {
			resetState()
			if connected {
				GameState.par.start(field:GameState.field)
			}
		}

		func parService(_ parService:ParService,session peer:String,connected:Bool) {
			resetState()
			if connected {
				if peer == GameData.id {
					GameState.par.start(server:GameState.server)

					let charArea = CharAreaServer(id:"home")
					charArea.spawnMin = CGPoint(x:720*0.23,y:1280*0.18)
					charArea.spawnMax = CGPoint(x:720*0.27,y:1280*0.2)
					charAreaServer = charArea
					GameState.par.start(server:charArea)

					let selectionView = SelectionViewServer()
					GameState.par.start(server:selectionView)
				} else {
					GameState.houseVisible = true
				}
				GameState.par.start(client:GameState.client)
			}
		}

		func parService(_ parService:ParService,session peer:String,found info:[String:Any]?) {
			GameState.sessionBrowseEvent?(peer,info,false)
		}
	}
}
