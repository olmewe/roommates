import SpriteKit

class StateField:ParField {
	init() {
		super.init(name:"state",priority:100)
	}

	override func started() {
		//
	}

	override func stopped() {
		//
	}

	override func connected(peer:String) {
		if let data = Json.data(["?":"info","info":GameData.peerInfo.s]) {
			send(data:data,to:peer)
		}
	}

	override func disconnected(peer:String) {
		if GameState.peers[peer] != nil {
			GameState.peers[peer] = nil
			GameState.peerInfoUpdate?(peer,nil)
		}
	}

	override func received(data:Data,from peer:String) {
		guard let json = Json.parse(data),let name = json["?"] as? String else { return }
		switch name {
		case "info":
			guard let info = PeerInfo.s(json["info"]) else { break }
			GameState.peers[peer] = info
			GameState.peerInfoUpdate?(peer,info)
		default: break
		}
	}
}
