import Foundation

class HomeInfo {
	var theme = ""
	var games:[String] = []
}

extension HomeInfo {
	static let themes = ["green","yellow","pink","blue"]

	var s:[String:Any] {
		return ["theme":theme,"games":games]
	}

	static func s(_ value:Any?,mode:ParseMode = .intolerant) -> HomeInfo? {
		let info = HomeInfo()

		func parseTheme(_ theme:Any?) {
			if let theme = theme as? String,themes.contains(theme) {
				info.theme = theme
			} else {
				info.theme = mode == .generate ? themes[Random.range(themes.count)] : themes[0]
			}
		}

		func parseGames(_ games:Any?) {
			let games = games as? [String] ?? []
			if mode == .generate {
				let set = Set<String>(games)
				info.games = GameInfo.gamesSorted.filter { set.contains($0) || GameInfo.games[$0]?.value == 0 }
			} else {
				info.games = games
			}
		}

		if let data = value as? [String:Any] {
			parseTheme(data["theme"])
			parseGames(data["games"])
		} else if mode != .intolerant {
			parseTheme(nil)
			parseGames(nil)
		} else {
			return nil
		}
		return info
	}

	enum ParseMode {
		case intolerant
		case tolerant
		case generate
	}
}
