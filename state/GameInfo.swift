import SpriteKit

/** stores information about a given game. */
class GameInfo {
	let id:String

	var value:Int = 0

	var name:String = ""
	var description:String = ""
	var playersMin:Int = 1
	var playersMax:Int? = nil
	var players:Int? {
		get {
			if let max = playersMax,max == playersMin {
				return max
			}
			return nil
		}
		set {
			if let n = newValue {
				playersMin = n
				playersMax = n
			} else {
				playersMin = 1
				playersMax = nil
			}
		}
	}
	var genre:String = ""
	var averageTime:CGFloat = 0
	var modes:[GameMode] = []

	var assetsFolder:String = ""
	var fgColour:UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
	var bgColour:UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

	var server:ParServer? = nil
	var client:ParClient? = nil

	/**
	creates a new gameinfo instance with a given game id.
	- parameter id: game id
	*/
	init(id:String) {
		self.id = id
	}

	/**
	creates a new gameinfo instance with a given game id and properties.
	- parameter id: game
	*/
	init(id:String,props:((GameInfo) -> Void)) {
		self.id = id
		props(self)
	}
}

/** ways that a specific game can be played. */
enum GameMode {
	/** the game can be played with one person only */
	case single
	/** the game can be played with other people, while everyone cooperates to reach a certain goal */
	case coop
	/** the game can be played with other people, while competing against each other */
	case comp
	/** the game can be played with other people, divided into two teams that are competing against each other */
	case compTeams
	/** the game can be played with other people, while one specific player tries to win over everyone else and vice versa */
	case compOne
}

extension GameInfo {
	/** game list (key: game id, value: info about the game) */
	static let games:[String:GameInfo] = Dictionary(uniqueKeysWithValues:([

		Baseball.info,
		Sheep.info,

	] as [GameInfo]).map { ($0.id,$0) })
	/** game list (ids sorted by name) */
	static let gamesSorted:[String] = games.map { _,v in (v.id,v.name) }.sorted { a,b in a.1 < b.1 }.map { id,name in id }
}
