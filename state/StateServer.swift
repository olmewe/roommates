import SpriteKit

class StateServer:ParServer {
	private var peerInfo:[String:PeerInfo] = [:]
	private var gameSelected = ""
	private var gameServer:ParServer?
	private var playingPeers:[String] = []

	init() {
		super.init(name:"state",priority:100)
	}

	override func started() {
		peerInfo = [:]
		gameSelected = GameData.lastGame
		gameServer = nil
	}

	override func stopped() {
		peerInfo = [:]
		gameSelected = ""
		gameServer = nil
	}

	override func connected(peer:String) {
		if let data = Json.data([
			"?":"hi",
			"info":GameData.homeInfo.s,
			"peer":GameData.peerInfo.s,
			"peers":peerInfo.mapValues { $0.s },
			"game":gameSelected,
			"play":gameServer != nil,
			"visible":GameState.par.sessionVisible,
			"playing":playingPeers]) {
			send(data:data,to:peer,reliable:true)
		}
	}

	override func disconnected(peer:String) {
		if peerInfo[peer] != nil {
			if let index = playingPeers.index(of:peer) {
				playingPeers.remove(at:index)
				if let data = Json.data(["?":"playing","peer":peer,"index":index,"playing":false]) {
					send(data:data,to:peers,reliable:true)
				}
			}
			peerInfo[peer] = nil
			if !peers.isEmpty,let data = Json.data(["?":"peer","peer":peer]) {
				send(data:data,to:peers,reliable:true)
			}
		}
	}

	override func received(data:Data,from peer:String) {
		guard let json = Json.parse(data),let name = json["?"] as? String else { return }
		switch name {
		case "info":
			guard let info = PeerInfo.s(json["info"]) else { break }
			peerInfo[peer] = info
			if let peers = peers(excluding:peer),let data = Json.data(["?":"peer","peer":peer,"info":info.s]) {
				send(data:data,to:peers,reliable:true)
			}
			GameState.updateSessionInfo()
		case "game":
			if let game = json["game"] as? String,let info = GameInfo.games[game],GameData.homeInfo.games.contains(game) {
				if gameServer == nil {
					gameSelected = game
					if GameData.lastGame != game {
						GameData.lastGame = game
						GameData.updateLastGame()
					}
					if json["play"] as? Bool == true,let server = info.server {
						gameServer = server
						GameState.par.start(server:server)
					}
				}
			} else if let server = gameServer {
				gameServer = nil
				GameState.par.stop(server:server)
			}
			if let data = Json.data(["?":"game","game":gameSelected,"play":gameServer != nil]) {
				send(data:data,to:peers,reliable:true)
			}
			GameState.updateSessionInfo()
		case "visible":
			guard let visible = json["visible"] as? Bool,visible != GameState.par.sessionVisible else { break }
			if visible {
				GameState.par.showSession()
			} else {
				GameState.par.hideSession()
			}
			if let peers = peers(excluding:peer),let data = Json.data(["?":"visible","visible":visible]) {
				send(data:data,to:peers,reliable:true)
			}
		case "playing":
			guard let playing = json["playing"] as? Bool else { break }
			let index:Int
			if playing {
				guard !playingPeers.contains(peer) else { return }
				index = Random.range(playingPeers.count+1)
				playingPeers.insert(peer,at:index)
			} else {
				guard let i = playingPeers.index(of:peer) else { return }
				index = i
				playingPeers.remove(at:index)
			}
			if let data = Json.data(["?":"playing","peer":peer,"index":index,"playing":playing]) {
				send(data:data,to:peers,reliable:true)
			}
		default: break
		}
	}
}
