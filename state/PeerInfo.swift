import UIKit

class PeerInfo {
	var name = ""
	var char = ""
}

extension PeerInfo {
	static let charInfo = [
		"matilde": "Has a purrfect sense of style.",
		"peach": "Doesn't say much, but thinks too much.",
		"ravioli": "Likes to listen to the sound of the ocean.",
	]
	static let chars = [String](charInfo.keys).sorted()

	var s:[String:Any] {
		return ["name":name,"char":char]
	}

	static func s(_ value:Any?,mode:ParseMode = .intolerant) -> PeerInfo? {
		let info = PeerInfo()

		func parseName(_ name:Any?) {
			if let name = name as? String {
				info.name = name
			} else {
				info.name = mode == .generate ? "" : "?"
			}
		}

		func parseChar(_ char:Any?) {
			if let char = char as? String,chars.contains(char) {
				info.char = char
			} else {
				info.char = mode == .generate ? chars[Random.range(chars.count)] : chars[0]
			}
		}

		if let data = value as? [String:Any] {
			parseName(data["name"])
			parseChar(data["char"])
		} else if mode != .intolerant {
			parseName(nil)
			parseChar(nil)
		} else {
			return nil
		}
		return info
	}

	enum ParseMode {
		case intolerant
		case tolerant
		case generate
	}
}
