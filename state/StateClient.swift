import SpriteKit

class StateClient:ParClient {
	init() {
		super.init(name:"state",priority:100)
	}

	override func started() {
		//
	}

	override func stopped() {
		//
	}

	override func connected() {
		if let data = Json.data(["?":"info","info":GameData.peerInfo.s]) {
			send(data:data,reliable:true)
		}
	}

	override func disconnected() {
		//
	}

	override func received(data:Data) {
		guard let json = Json.parse(data),let name = json["?"] as? String else { return }
		switch name {
		case "hi":
			guard let info = HomeInfo.s(json["info"]),let peer = PeerInfo.s(json["peer"]) else { break }
			GameState.homeInfo = info
			GameState.homePeer = peer
			if let peers = json["peers"] as? [String:Any] {
				for (peer,info) in peers {
					if let info = PeerInfo.s(info) {
						GameState.peers[peer] = info
						GameState.peerInfoUpdate?(peer,info)
					}
				}
			}
			if let game = json["game"] as? String {
				GameState.gameSelected = game
				GameState.gameRunning = json["play"] as? Bool == true
			}
			if let visible = json["visible"] as? Bool {
				GameState.houseVisible = visible
			}
			if let playing = json["playing"] as? [String] {
				if let update = GameState.playingPeersUpdate {
					while let peer = GameState.playingPeers.popLast() {
						update(peer,GameState.playingPeers.count,false)
					}
					for (n,peer) in playing.enumerated() {
						GameState.playingPeers.append(peer)
						update(peer,n,true)
					}
				} else {
					GameState.playingPeers = playing
				}
			}
		case "peer":
			guard let peer = json["peer"] as? String else { break }
			if let info = PeerInfo.s(json["info"]) {
				GameState.peers[peer] = info
				GameState.peerInfoUpdate?(peer,info)
			} else if GameState.peers[peer] != nil {
				GameState.peers[peer] = nil
				GameState.peerInfoUpdate?(peer,nil)
			}
		case "game":
			guard let game = json["game"] as? String else { break }
			GameState.gameSelected = game
			GameState.gameRunning = json["play"] as? Bool == true
		case "visible":
			GameState.houseVisible = json["visible"] as? Bool == true
		case "playing":
			guard let peer = json["peer"] as? String,let i = json["index"] as? Int else { break }
			if json["playing"] as? Bool == true {
				let index = i.clamp(0,GameState.playingPeers.count)
				GameState.playingPeers.insert(peer,at:index)
				GameState.playingPeersUpdate?(peer,index,true)
			} else if !GameState.playingPeers.isEmpty {
				let index = i.clamp(0,GameState.playingPeers.count-1)
				GameState.playingPeers.remove(at:index)
				GameState.playingPeersUpdate?(peer,index,false)
			}
		default: break
		}
	}

	func send(game:String,play:Bool) {
		var json:[String:Any] = ["?":"game","game":game]
		if play {
			json["play"] = true
		}
		if let data = Json.data(json) {
			send(data:data,reliable:true)
		}
	}

	func send(visible:Bool) {
		if let data = Json.data(["?":"visible","visible":visible]) {
			send(data:data,reliable:true)
		}
	}

	func send(playing:Bool) {
		if let data = Json.data(["?":"playing","playing":playing]) {
			send(data:data,reliable:true)
		}
	}
}
