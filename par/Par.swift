import MultipeerConnectivity

/** connects pairs of peers nearby. */
class Par {
	/** my peer id */
	let id:String
	/** if true, par keeps looking for nearby peers */
	var searching = false {
		didSet {
			if searching {
				mcBrowser.startBrowsingForPeers()
				mcAdvertiser.startAdvertisingPeer()
			} else {
				foundPeers.removeAll()
				mcBrowser.stopBrowsingForPeers()
				mcAdvertiser.stopAdvertisingPeer()
			}
		}
	}
	/** all nearby peers */
	private(set) var peers:[String] = []
	/** uhh */
	weak var delegate:ParDelegate?

	private let mcPeer:MCPeerID
	private let mcBrowser:MCNearbyServiceBrowser
	private let mcAdvertiser:MCNearbyServiceAdvertiser

	private var intr:Internal!
	private var foundPeers = Set<String>()
	private var sessions:[String:Session] = [:]

	private let infoKey = "par"
	private let infoValue = "4.0b"

	/**
	initialises a par instance.
	- parameter id: peer id (a long, unique one is preferable; use Par.randomId to generate one)
	- parameter serviceType: service used by multipeer connectivity
	*/
	init(id:String,serviceType:String) {
		self.id = id

		mcPeer = MCPeerID(displayName:id)
		mcBrowser = MCNearbyServiceBrowser(peer:mcPeer,serviceType:serviceType)
		mcAdvertiser = MCNearbyServiceAdvertiser(peer:mcPeer,discoveryInfo:[infoKey:infoValue],serviceType:serviceType)

		intr = Internal(par:self)
		mcBrowser.delegate = intr
		mcAdvertiser.delegate = intr
	}

	/**
	disconnects from the given peer, only if searching is disabled.
	- parameter peer: peer to disconnect from
	*/
	func disconnect(peer:String) {
		guard !searching else { return }
		foundPeers.remove(peer)
		sessions[peer]?.disconnect()
	}

	/**
	disconnects from the given peers, only if searching is disabled.
	- parameter peers: peers to disconnect from
	*/
	func disconnect(peers:[String]) {
		guard !searching else { return }
		for peer in peers {
			foundPeers.remove(peer)
			sessions[peer]?.disconnect()
		}
	}

	/** disconnects from all current peers, only if searching is disabled. */
	func disconnectAll() {
		guard !searching else { return }
		for (i,session) in sessions {
			foundPeers.remove(i)
			session.disconnect()
		}
	}

	/**
	disconnects from everyone but the given peer, only if searching is disabled.
	- parameter peer: peer to keep connected to
	*/
	func disconnect(allBut peer:String) {
		guard !searching else { return }
		for (i,session) in sessions where i != peer {
			foundPeers.remove(i)
			session.disconnect()
		}
	}

	/**
	disconnects from everyone but the given peers, only if searching is disabled.
	- parameter peers: peers to keep connected to
	*/
	func disconnect(allBut peers:[String]) {
		guard !searching else { return }
		for (i,session) in sessions where !peers.contains(i) {
			foundPeers.remove(i)
			session.disconnect()
		}
	}

	/**
	sends data to a given connected peer.
	- parameter data: data to be sent
	- parameter peer: peer to receive such data
	- parameter reliable: kinda like the udp/tcp thing
	*/
	func send(data:Data,to peer:String,reliable:Bool = true) {
		Par.log(peer,"sending data")
		Par.sniffer(id,sending:data)
		sessions[peer]?.send(data:data,reliable:reliable)
	}

	private func set(peer:String,connected:Bool) -> Bool {
		if connected {
			if !peers.contains(peer) {
				if searching {
					peers.append(peer)
					Par.sniffer(peer,connected:true)
					delegate?.par(self,peer:peer,connected:true)
				} else {
					return false
				}
			}
		} else {
			if let index = peers.index(of:peer) {
				peers.remove(at:index)
				Par.sniffer(peer,connected:false)
				delegate?.par(self,peer:peer,connected:false)
			}
		}
		return true
	}

	private class Session:NSObject,MCSessionDelegate {
		let par:Par
		let otherPeer:MCPeerID
		let master:Bool
		let mcSession:MCSession
		var otherId:String {
			return otherPeer.displayName
		}

		var referenced = true
		var active = false {
			didSet {
				if !par.set(peer:otherId,connected:active) {
					disconnect()
				}
			}
		}

		var state:State = .trying
		enum State {
			case dead
			case trying
			case almost
			case connected
		}

		init(par:Par,otherPeer:MCPeerID,master:Bool) {
			self.par = par
			self.otherPeer = otherPeer
			self.master = master
			mcSession = MCSession(peer:par.mcPeer)
			super.init()
			mcSession.delegate = self
		}

		func connect() {
			state = .trying
			par.mcBrowser.invitePeer(otherPeer,to:mcSession,withContext:nil,timeout:2)
		}

		func send(data:Data,reliable:Bool) {
			guard active else { return }
			try? mcSession.send(data,toPeers:[otherPeer],with:reliable ? .reliable : .unreliable)
		}

		func disconnect() {
			active = false
			Par.log(otherId,"forcing disconnect from them")
			mcSession.disconnect()
			if referenced {
				referenced = false
				par.sessions[otherId] = nil
			}
		}

		func session(_ session:MCSession,peer peerID:MCPeerID,didChange state:MCSessionState) {
			DispatchQueue.main.async {
				let id = peerID.displayName
				guard id == self.otherId else { return }
				Par.log(id,"session \(state == .connected ? "connected" : state == .connecting ? "connecting" : "not connected")")
				let newState:State
				switch state {
				case .connected: newState = .connected
				case .connecting: newState = .almost
				case .notConnected: newState = .dead
				}
				guard newState != self.state else { return }
				self.state = newState
				switch newState {
				case .connected: self.active = true
				case .almost: break
				case .dead:
					self.active = false
					if self.referenced && self.master && self.par.foundPeers.contains(id) {
						self.connect()
					} else {
						self.disconnect()
					}
				case .trying: self.active = false
				}
			}
		}

		func session(_ session:MCSession,didReceive data:Data,fromPeer peerID:MCPeerID) {
			DispatchQueue.main.async {
				guard self.active else { return }
				let id = peerID.displayName
				guard id == self.otherId else { return }
				Par.sniffer(id,receiving:data)
				self.par.delegate?.par(self.par,peer:id,sent:data)
			}
		}

		func session(_ session:MCSession,didReceive stream:InputStream,withName streamName:String,fromPeer peerID:MCPeerID) {}
		func session(_ session:MCSession,didStartReceivingResourceWithName resourceName:String,fromPeer peerID:MCPeerID,with progress:Progress) {}
		func session(_ session:MCSession,didFinishReceivingResourceWithName resourceName:String,fromPeer peerID:MCPeerID,at localURL:URL?,withError error:Error?) {}
	}

	private class Internal:NSObject,MCNearbyServiceBrowserDelegate,MCNearbyServiceAdvertiserDelegate {
		let par:Par
		init(par:Par) {
			self.par = par
		}

		func browser(_ browser:MCNearbyServiceBrowser,foundPeer peer:MCPeerID,withDiscoveryInfo info:[String:String]?) {
			DispatchQueue.main.async {
				guard self.par.searching else { return }
				let id = peer.displayName
				guard self.par.id < id && info?[self.par.infoKey] == self.par.infoValue else { return }
				if let session = self.par.sessions[id] {
					guard session.otherPeer != peer else {
						self.par.foundPeers.insert(id)
						return
					}
					session.disconnect()
				}
				self.par.foundPeers.insert(id)
				Par.log(id,"inviting them")
				let session = Session(par:self.par,otherPeer:peer,master:true)
				self.par.sessions[id] = session
				session.connect()
			}
		}

		func browser(_ browser:MCNearbyServiceBrowser,lostPeer peer:MCPeerID) {
			DispatchQueue.main.async {
				let id = peer.displayName
				guard self.par.id < id else { return }
				self.par.foundPeers.remove(id)
			}
		}

		func advertiser(_ advertiser:MCNearbyServiceAdvertiser,didReceiveInvitationFromPeer peer:MCPeerID,withContext data:Data?,invitationHandler:@escaping(Bool,MCSession?) -> Void) {
			DispatchQueue.main.async {
				guard self.par.searching else {
					invitationHandler(false,nil)
					return
				}
				let id = peer.displayName
				guard self.par.id > id else {
					invitationHandler(false,nil)
					return
				}
				self.par.sessions[id]?.disconnect()
				Par.log(id,"got an invite from them")
				let session = Session(par:self.par,otherPeer:peer,master:false)
				self.par.sessions[id] = session
				invitationHandler(true,session.mcSession)
			}
		}
	}

	private static func log(_ id:String,_ m:String) {
		print("[par] \(strip(id:id)): \(m)")
	}

	private static func sniffer(_ id:String,_ p:String,_ m:String) {
		//print("[par sniffer] (\(p)) \(strip(id:id)): \(m)")
	}

	private static func sniffer(_ id:String,connected:Bool) {
		//sniffer(id,"!",connected ? "ON" : "OFF")
	}

	private static func sniffer(_ id:String,sending data:Data) {
		//sniffer(id,">",data.cuteString)
	}

	private static func sniffer(_ id:String,receiving data:Data) {
		//sniffer(id,"<",data.cuteString)
	}
}

extension Par {
	/**
	sends data to a given list of connected peers.
	- parameter data: data to be sent
	- parameter peers: peers to receive such data
	- parameter reliable: kinda like the udp/tcp thing
	*/
	func send(data:Data,to peers:[String],reliable:Bool = true) {
		for peer in peers {
			send(data:data,to:peer,reliable:reliable)
		}
	}

	/**
	strips a par id to 12 characters max.
	- parameter id: par id
	- returns: id trimmed down to 12 chars
	*/
	static func strip(id:String) -> String {
		let length = 12
		guard id.count > length else { return id }
		return String(id[id.startIndex..<id.index(id.startIndex,offsetBy:length)])
	}

	/** a random generated id to be used with par */
	static var randomId:String {
		let length = 48
		let name = String(format:"%02x",UIDevice.current.name.hashValue)
		var seconds = Date().timeIntervalSince1970
		let suffix = Data(buffer:UnsafeBufferPointer(start:&seconds,count:1)).reduce(name) { $0+String(format:"%02x",$1) }
		if suffix.count < length {
			let table = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
			let upper = UInt32(table.count)
			var partial = ""
			let add = arc4random_uniform(UInt32(length-suffix.count))
			if add > 0 {
				for _ in 0..<add {
					partial += table[Int(arc4random_uniform(upper))]
				}
			}
			return partial+suffix
		}
		if suffix.count > length {
			return String(suffix[suffix.startIndex..<suffix.index(suffix.startIndex,offsetBy:length)])
		}
		return suffix
	}
}

protocol ParDelegate:AnyObject {
	func par(_ par:Par,peer:String,connected:Bool)
	func par(_ par:Par,peer:String,sent data:Data)
}
