import Foundation

/** a wrapper on par group that manages services for both session and field modes. */
class ParService {
	/** current running servers (session owner only) */
	private(set) var servers:[String:ParServer] = [:]
	/** current running clients (session mode only) */
	private(set) var clients:[String:ParClient] = [:]
	/** current running fields (field mode only) */
	private(set) var fields:[String:ParField] = [:]
	/** uhh */
	weak var delegate:ParServiceDelegate?

	private var otherServers = Set<String>()
	private var otherClients:[String:Set<String>] = [:]
	private var otherFields:[String:Set<String>] = [:]

	private var dataQueue:[(String,Bool,Data)] = []

	private var mode:Mode = .none
	private enum Mode {
		case none
		case server
		case client
		case field
	}

	/** my peer id */
	let id:String
	private let pg:ParGroup

	private var intr:Internal!

	/**
	initialises a par service instance.
	- parameter id: peer id (a long, unique one is preferable; use Par.randomId to generate one)
	- parameter serviceType: service used by multipeer connectivity
	*/
	init(id:String,serviceType:String) {
		self.id = id
		pg = ParGroup(id:id,serviceType:serviceType)

		intr = Internal(ps:self)
		pg.delegate = intr
	}

	/**
	starts a par server. (session owner only)
	- parameter server: par server to be started
	*/
	func start(server:ParServer) {
		guard mode == .server else { return }
		stop(server:server)
		servers[server.name] = server
		server.start(with:self)
		send(parJson:["+":server.name],to:pg.peers)
		if let clients = otherClients[server.name] {
			for client in clients {
				server.connect(peer:client)
			}
		}
		if let client = clients[server.name] {
			server.connect(peer:id)
			client.connect()
		}
	}

	/**
	stops a par server. (session owner only)
	- parameter server: par server to be stopped
	*/
	func stop(server:ParServer) {
		stop(server:server.name)
	}

	/**
	stops a par server. (session owner only)
	- parameter server: par server name to be stopped
	*/
	func stop(server:String) {
		guard mode == .server else { return }
		guard let server = servers[server] else { return }
		servers[server.name] = nil
		server.stop()
		clients[server.name]?.disconnect()
		send(parJson:["-":server.name],to:pg.peers)
	}

	/**
	starts a par client. (session mode only)
	- parameter client: par client to be started
	*/
	func start(client:ParClient) {
		guard mode == .server || mode == .client else { return }
		stop(client:client)
		clients[client.name] = client
		client.start(with:self)
		if mode == .server {
			if let server = servers[client.name] {
				server.connect(peer:id)
				client.connect()
			}
		} else {
			if let session = session {
				send(parJson:["+":client.name],to:session)
			}
			if otherServers.contains(client.name) {
				client.connect()
			}
		}
	}

	/**
	stops a par client. (session mode only)
	- parameter client: par client to be stopped
	*/
	func stop(client:ParClient) {
		stop(client:client.name)
	}

	/**
	stops a par client. (session mode only)
	- parameter client: par client name to be stopped
	*/
	func stop(client:String) {
		guard mode == .server || mode == .client else { return }
		guard let client = clients[client] else { return }
		clients[client.name] = nil
		client.stop()
		if mode == .server {
			servers[client.name]?.disconnect(peer:id)
		} else if let session = session {
			send(parJson:["-":client.name],to:session)
		}
	}

	/**
	starts a par field. (field mode only)
	- parameter field: par field to be started
	*/
	func start(field:ParField) {
		guard mode == .field else { return }
		stop(field:field)
		fields[field.name] = field
		field.start(with:self)
		if let peers = otherFields[field.name] {
			for peer in peers {
				field.connect(peer:peer)
			}
		}
		send(parJson:["+":field.name],to:pg.peers)
	}

	/**
	stops a par field. (field mode only)
	- parameter field: par field to be stopped
	*/
	func stop(field:ParField) {
		stop(field:field.name)
	}

	/**
	stops a par field. (field mode only)
	- parameter field: par field name to be stopped
	*/
	func stop(field:String) {
		guard mode == .field else { return }
		guard let field = fields[field] else { return }
		fields[field.name] = nil
		field.stop()
		send(parJson:["-":field.name],to:pg.peers)
	}

	/**
	sends data to the current server. use a par client instead.
	- parameter data: data to be sent
	- parameter name: par service name
	- parameter reliable: kinda like the udp/tcp thing
	*/
	func send(data:Data,toServerOn name:String,reliable:Bool = true) {
		if servers[name] != nil {
			dataQueue.append((name,true,data))
		} else if let session = session,let data = generate(data:data,with:name) {
			pg.send(data:data,to:session,reliable:reliable)
		}
	}

	/**
	sends data to a given client (as a server) or field peer (as another one). use par services instead (par server, par field).
	- parameter data: data to be sent
	- parameter peer: peer to receive such data
	- parameter name: par service name
	- parameter reliable: kinda like the udp/tcp thing
	*/
	func send(data:Data,to peer:String,on name:String,reliable:Bool = true) {
		if peer == GameData.id {
			dataQueue.append((name,false,data))
		} else if let data = generate(data:data,with:name) {
			pg.send(data:data,to:peer,reliable:reliable)
		}
	}

	/**
	sends data to a given list of clients (as a server) or field peers (as another one). use par services instead (par server, par field).
	- parameter data: data to be sent
	- parameter peers: peers to receive such data
	- parameter name: par service name
	- parameter reliable: kinda like the udp/tcp thing
	*/
	func send(data:Data,to peers:[String],on name:String,reliable:Bool = true) {
		func send(to peers:[String]) {
			guard let data = generate(data:data,with:name) else { return }
			pg.send(data:data,to:peers,reliable:reliable)
		}
		if clients[name] != nil,let index = peers.index(of:id) {
			dataQueue.append((name,false,data))
			if peers.count > 1 {
				var peers = peers
				peers.remove(at:index)
				send(to:peers)
			}
		} else {
			send(to:peers)
		}
	}

	/** par messages to itself (when running a server and a client at the same time) aren't sent immediately; call this method to send pending data. */
	func dequeueData() {
		while !dataQueue.isEmpty {
			let (name,toServer,data) = dataQueue.removeFirst()
			if toServer {
				servers[name]?.receive(data:data,from:GameData.id)
			} else {
				clients[name]?.receive(data:data)
			}
		}
	}

	private func generate(data:Data,with name:String) -> Data? {
		guard let name = name.data(using:.utf8),name.count > 0 && name.count <= 255 else { return nil }
		var data = name+data
		data.insert(UInt8(name.count),at:0)
		return data
	}

	private func send(parJson json:[String:Any],to peers:[String]) {
		guard !peers.isEmpty,var data = Json.data(json) else { return }
		data.insert(0,at:0)
		pg.send(data:data,to:peers,reliable:true)
	}

	private func send(parJson json:[String:Any],to peer:String) {
		guard var data = Json.data(json) else { return }
		data.insert(0,at:0)
		pg.send(data:data,to:peer,reliable:true)
	}

	private class Internal:ParGroupDelegate {
		let ps:ParService
		init(ps:ParService) {
			self.ps = ps
		}

		func parGroup(_ parGroup:ParGroup,field channel:String,connected:Bool) {
			if connected {
				ps.mode = .field
			} else {
				if !ps.fields.isEmpty {
					for (_,i) in ps.fields {
						i.stop()
					}
					ps.fields.removeAll()
				}
			}
			ps.delegate?.parService(ps,field:channel,connected:connected)
		}

		func parGroup(_ parGroup:ParGroup,session peer:String,connected:Bool) {
			if connected {
				ps.mode = peer == ps.id ? .server : .client
			} else {
				if !ps.servers.isEmpty {
					for (_,i) in ps.servers {
						i.stop()
					}
					ps.servers.removeAll()
				}
				if !ps.clients.isEmpty {
					for (_,i) in ps.clients {
						i.stop()
					}
					ps.clients.removeAll()
				}
			}
			ps.delegate?.parService(ps,session:peer,connected:connected)
		}

		func parGroup(_ parGroup:ParGroup,session peer:String,found info:[String:Any]?) {
			ps.delegate?.parService(ps,session:peer,found:info)
		}

		func parGroup(_ parGroup:ParGroup,peer:String,connected:Bool) {
			if connected {
				let list:[String]
				switch ps.mode {
				case .server: list = [String](ps.servers.keys)
				case .client: list = [String](ps.clients.keys)
				case .field: list = [String](ps.fields.keys)
				default: return
				}
				ps.send(parJson:["+":list],to:peer)
			} else {
				switch ps.mode {
				case .server:
					for (name,var clients) in ps.otherClients {
						guard clients.remove(peer) != nil else { continue }
						ps.otherClients[name] = clients.isEmpty ? nil : clients
						ps.servers[name]?.disconnect(peer:peer)
					}
				case .client:
					if ps.session == peer {
						for name in ps.otherServers {
							ps.clients[name]?.disconnect()
						}
						ps.otherServers.removeAll()
					}
				case .field:
					for (name,var fields) in ps.otherFields {
						guard fields.remove(peer) != nil else { continue }
						ps.otherFields[name] = fields.isEmpty ? nil : fields
						ps.fields[name]?.disconnect(peer:peer)
					}
				default: break
				}
			}
		}

		func parGroup(_ parGroup:ParGroup,peer:String,sent data:Data) {
			var data = data
			guard let prefix = data.popFirst() else { return }
			guard prefix == 0 else {
				var nameData:Data
				if data.count == prefix {
					nameData = data
					data = Data()
				} else if data.count > prefix {
					nameData = Data()
					for _ in 0..<prefix {
						nameData.append(data.removeFirst())
					}
				} else {
					return
				}
				guard let name = String(data:nameData,encoding:.utf8) else { return }
				switch ps.mode {
				case .server: ps.servers[name]?.receive(data:data,from:peer)
				case .client: ps.clients[name]?.receive(data:data)
				case .field: ps.fields[name]?.receive(data:data,from:peer)
				default: break
				}
				return
			}
			guard let json = Json.parse(data) else { return }
			func list(_ key:String) -> [String] {
				if let obj = json[key] {
					if let list = obj as? [String] { return list }
					if let elem = obj as? String { return [elem] }
				}
				return []
			}
			var c:[(String,Int)] = []
			switch ps.mode {
			case .server:
				for name in list("+") {
					if var clients = ps.otherClients[name] {
						guard clients.insert(peer).inserted else { continue }
						ps.otherClients[name] = clients
					} else {
						ps.otherClients[name] = [peer]
					}
					if let i = ps.servers[name] {
						c.append((name,i.priority))
					}
				}
				for (name,_) in c.sorted(by:{ $0.1 > $1.1 }) {
					ps.servers[name]?.connect(peer:peer)
				}
				for name in list("-") {
					guard var clients = ps.otherClients[name],clients.remove(peer) != nil else { continue }
					ps.otherClients[name] = clients.isEmpty ? nil : clients
					ps.servers[name]?.disconnect(peer:peer)
				}
			case .client:
				for name in list("+") {
					guard ps.otherServers.insert(name).inserted else { continue }
					if let i = ps.clients[name] {
						c.append((name,i.priority))
					}
				}
				for (name,_) in c.sorted(by:{ $0.1 > $1.1 }) {
					ps.clients[name]?.connect()
				}
				for name in list("-") {
					guard ps.otherServers.remove(name) != nil else { continue }
					ps.clients[name]?.disconnect()
				}
			case .field:
				for name in list("+") {
					if var fields = ps.otherFields[name] {
						guard fields.insert(peer).inserted else { continue }
						ps.otherFields[name] = fields
					} else {
						ps.otherFields[name] = [peer]
					}
					if let i = ps.fields[name] {
						c.append((name,i.priority))
					}
				}
				for (name,_) in c.sorted(by:{ $0.1 > $1.1 }) {
					ps.fields[name]?.connect(peer:peer)
				}
				for name in list("-") {
					guard var fields = ps.otherFields[name],fields.remove(peer) != nil else { continue }
					ps.otherFields[name] = fields.isEmpty ? nil : fields
					ps.fields[name]?.disconnect(peer:peer)
				}
			default: break
			}
		}
	}
}

extension ParService {
	/** current connected channel (field mode only) */
	var fieldChannel:String? {
		return pg.fieldChannel
	}
	/** true if currently searching for sessions or if connected to one */
	var sessionMode:Bool {
		return pg.sessionMode
	}
	/** current connected peer session (session mode only) */
	var session:String? {
		return pg.session
	}
	/** true if the current peer session is fully established (session mode only) */
	var sessionConnected:Bool {
		return pg.sessionConnected
	}
	/** true if the current session is visible to other peers (session mode only) */
	var sessionVisible:Bool {
		return pg.sessionVisible
	}
	/** list of nearby sessions (session mode only) */
	var sessions:[String:[String:Any]] {
		return pg.sessions
	}
	/** information about our session that's announced to other peers */
	var sessionInfo:[String:Any] {
		return pg.sessionInfo
	}

	/** disables any connectivity. */
	func disable() {
		pg.disable()
	}

	/**
	enables field mode with a given channel name.
	- parameter channel: field channel
	*/
	func setFieldMode(channel:String) {
		pg.setFieldMode(channel:channel)
	}

	/** enables session mode and begins looking for nearby sessions. */
	func setSessionMode() {
		pg.setSessionMode()
	}

	/**
	connects to a nearby session.
	- parameter session: session peer id
	- returns: true if it's connecting i guess
	*/
	func connect(to session:String) -> Bool {
		return pg.connect(to:session)
	}

	/** cancels the current session connection. */
	func cancelConnect() {
		pg.cancelConnect()
	}

	/** disconnects from the current session. */
	func disconnect() {
		pg.disconnect()
	}

	/** shows the current session to nearby peers. */
	func showSession() {
		pg.showSession()
	}

	/** hides the current session from nearby peers. */
	func hideSession() {
		pg.hideSession()
	}

	/**
	sets a new info structure to be announced to nearby peers.
	- parameter info: info json structure
	*/
	func setSessionInfo(_ info:[String:Any]) {
		pg.setSessionInfo(info)
	}
}

protocol ParServiceDelegate:AnyObject {
	func parService(_ parService:ParService,field channel:String,connected:Bool)
	func parService(_ parService:ParService,session peer:String,connected:Bool)
	func parService(_ parService:ParService,session peer:String,found info:[String:Any]?)
}
