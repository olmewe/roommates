import Foundation

/** a wrapper on par that manages groups. */
class ParGroup {
	/** current connected channel (field mode only) */
	private(set) var fieldChannel:String?
	/** true if currently searching for sessions or if connected to one */
	private(set) var sessionMode = false
	/** current connected peer session (session mode only) */
	private(set) var session:String?
	/** true if the current peer session is fully established (session mode only) */
	private(set) var sessionConnected = false
	/** true if the current session is visible to other peers (session mode only) */
	private(set) var sessionVisible = false
	/** list of nearby sessions (session mode only) */
	private(set) var sessions:[String:[String:Any]] = [:]
	/** information about our session that's announced to other peers */
	private(set) var sessionInfo:[String:Any] = [:]
	/** list of connected peers (in channel or in session, depending on mode) */
	var peers:[String] {
		return [String](peersSet)
	}
	private var peersSet = Set<String>()
	/** uhh */
	weak var delegate:ParGroupDelegate?

	/** my peer id */
	let id:String
	private let par:Par

	private var intr:Internal!

	/**
	initialises a par group instance.
	- parameter id: peer id (a long, unique one is preferable; use Par.randomId to generate one)
	- parameter serviceType: service used by multipeer connectivity
	*/
	init(id:String,serviceType:String) {
		self.id = id
		par = Par(id:id,serviceType:serviceType)

		intr = Internal(pg:self)
		par.delegate = intr
	}

	/** disables any connectivity. */
	func disable() {
		if sessionConnected {
			disconnect(sessionRequested:false)
		}
		let oldChannel = fieldChannel

		fieldChannel = nil
		sessionMode = false
		session = nil
		sessionConnected = false
		sessionVisible = false
		clearSessions()
		clearPeers()

		par.searching = false
		par.disconnectAll()
		if let channel = oldChannel {
			delegate?.parGroup(self,field:channel,connected:false)
		}
	}

	/**
	enables field mode with a given channel name.
	- parameter channel: field channel
	*/
	func setFieldMode(channel:String) {
		if let c = fieldChannel {
			guard c != channel else { return }
		}
		if sessionConnected {
			disconnect(sessionRequested:false)
		}
		let oldChannel = fieldChannel

		fieldChannel = channel
		sessionMode = false
		session = nil
		sessionConnected = false
		sessionVisible = false
		clearSessions()
		clearPeers()

		par.searching = true
		resendPeerState()
		if let channel = oldChannel {
			delegate?.parGroup(self,field:channel,connected:false)
		}
		delegate?.parGroup(self,field:channel,connected:true)
		filterRawPeers()
	}

	/** enables session mode and begins looking for nearby sessions. */
	func setSessionMode() {
		guard !sessionMode else { return }
		let oldChannel = fieldChannel

		fieldChannel = nil
		sessionMode = true
		session = nil
		sessionConnected = false
		sessionVisible = false
		clearSessions()
		clearPeers()

		par.searching = true
		resendPeerState()
		if let channel = oldChannel {
			delegate?.parGroup(self,field:channel,connected:false)
		}
		filterRawPeers()
	}

	/**
	connects to a nearby session.
	- parameter session: session peer id
	- returns: true if it's connecting i guess
	*/
	func connect(to session:String) -> Bool {
		guard !sessionConnected else { return false }
		if session == id {
			let oldChannel = fieldChannel
			fieldChannel = nil
			sessionMode = true
			self.session = nil
			sessionConnected = false
			sessionVisible = false
			clearSessions()
			clearPeers()
			par.searching = false
			if let channel = oldChannel {
				delegate?.parGroup(self,field:channel,connected:false)
			}
			self.session = session
			sessionConnected = true
			par.disconnectAll()
			delegate?.parGroup(self,session:session,connected:true)
			return true
		}
		guard sessionMode else { return false }
		if sessions[session] != nil {
			self.session = session
			send(parJson:["par":"pls"],to:session)
			return true
		}
		return false
	}

	/** cancels the current session connection. */
	func cancelConnect() {
		guard !sessionConnected else { return }
		session = nil
	}

	/** disconnects from the current session. */
	func disconnect() {
		disconnect(sessionRequested:false)
	}

	private func disconnect(sessionRequested:Bool) {
		guard sessionConnected,let peer = session else { return }
		let allPeers = peer == id ? allReceivingPeers() : []

		session = nil
		sessionConnected = false
		sessionVisible = false
		clearPeers()

		par.searching = true
		if peer == id {
			send(parJson:["par":"bye"],to:allPeers)
		} else if !sessionRequested {
			send(parJson:["par":"bye"],to:peer)
		}
		resendPeerState()
		filterRawPeers()

		delegate?.parGroup(self,session:peer,connected:false)
	}

	/** shows the current session to nearby peers. */
	func showSession() {
		guard sessionConnected && !sessionVisible && session == id else { return }

		sessionVisible = true

		par.searching = true
		resendPeerState()
	}

	/** hides the current session from nearby peers. */
	func hideSession() {
		guard sessionVisible else { return }
		sessionVisible = false

		par.searching = false
		par.disconnect(allBut:allReceivingPeers())
		resendPeerState()
	}

	/**
	sets a new info structure to be announced to nearby peers.
	- parameter info: info json structure
	*/
	func setSessionInfo(_ info:[String:Any]) {
		sessionInfo = info
		resendPeerState()
	}

	/**
	sends data to a given connected peer.

	in a session, if you're not hosting it, you can only send data to the host.
	- parameter data: data to be sent
	- parameter peer: peer to receive such data
	- parameter reliable: kinda like the udp/tcp thing
	*/
	func send(data:Data,to peer:String,reliable:Bool = true) {
		if validDataPeer(peer) {
			var data = data
			data.insert(1,at:0)
			par.send(data:data,to:peer,reliable:reliable)
		}
	}

	/**
	sends data to a given list of connected peers.

	in a session, if you're not hosting it, you can only send data to the host.
	- parameter data: data to be sent
	- parameter peers: peers to receive such data
	- parameter reliable: kinda like the udp/tcp thing
	*/
	func send(data:Data,to peers:[String],reliable:Bool = true) {
		let peers = peers.filter(validDataPeer)
		guard !peers.isEmpty else { return }
		var data = data
		data.insert(1,at:0)
		par.send(data:data,to:peers,reliable:reliable)
	}

	private class Internal:ParDelegate {
		let pg:ParGroup
		init(pg:ParGroup) {
			self.pg = pg
		}

		func par(_ par:Par,peer:String,connected:Bool) {
			pg.event(peer:peer,connected:connected)
		}

		func par(_ par:Par,peer:String,sent data:Data) {
			pg.event(peer:peer,sent:data)
		}
	}

	private var peerState:PeerState? = nil
	private var rawPeers:[String:PeerState] = [:]
	private var pendingSessionPeers = Set<String>()
	private enum PeerState:Equatable {
		case session([String:Any])
		case field(String)

		static func ==(lhs:PeerState,rhs:PeerState) -> Bool {
			switch (lhs,rhs) {
			case (.field(let a),.field(let b)): return a == b
			default: return false
			}
		}
	}

	private func updatePeerState() -> Bool {
		let newPeerState:PeerState?
		if sessionMode {
			if sessionVisible {
				newPeerState = .session(sessionInfo)
			} else {
				newPeerState = nil
			}
		} else if let channel = fieldChannel {
			newPeerState = .field(channel)
		} else {
			newPeerState = nil
		}
		if peerState != newPeerState {
			peerState = newPeerState
			return true
		}
		return false
	}

	private func updateRawPeer(_ peer:String,to json:[String:Any]) {
		func getState() -> PeerState? {
			guard let jsonState = json["state"] as? String else { return nil }
			switch jsonState {
			case "session":
				guard let info = json["info"] as? [String:Any] else { return nil }
				return .session(info)
			case "field":
				guard let channel = json["channel"] as? String else { return nil }
				return .field(channel)
			default: return nil
			}
		}
		let state = getState()
		rawPeers[peer] = state
		if sessionMode {
			if !sessionConnected {
				if case .some(.session(let info)) = state {
					sessions[peer] = info
					delegate?.parGroup(self,session:peer,found:info)
				} else if sessions[peer] != nil {
					sessions[peer] = nil
					delegate?.parGroup(self,session:peer,found:nil)
				}
			}
		} else if let channel = fieldChannel {
			if case .some(.field(channel)) = state {
				if peersSet.insert(peer).inserted {
					delegate?.parGroup(self,peer:peer,connected:true)
				}
			} else if peersSet.remove(peer) != nil {
				delegate?.parGroup(self,peer:peer,connected:false)
			}
		}
	}

	private func filterRawPeers() {
		if sessionMode {
			if !sessionConnected {
				for (peer,state) in rawPeers {
					if case .session(let info) = state {
						sessions[peer] = info
						delegate?.parGroup(self,session:peer,found:info)
					}
				}
			}
		} else if let channel = fieldChannel {
			for (peer,state) in rawPeers {
				if case .field(channel) = state,peersSet.insert(peer).inserted {
					delegate?.parGroup(self,peer:peer,connected:true)
				}
			}
		}
	}

	private func clearSessions() {
		guard !sessions.isEmpty else { return }
		let list = [String](sessions.keys)
		for peer in list {
			sessions[peer] = nil
			delegate?.parGroup(self,session:peer,found:nil)
		}
	}

	private func clearPeers() {
		pendingSessionPeers.removeAll()
		while let peer = peersSet.popFirst() {
			delegate?.parGroup(self,peer:peer,connected:false)
		}
	}

	private func allReceivingPeers() -> [String] {
		return peers+[String](pendingSessionPeers)
	}

	private func event(peer:String,connected:Bool) {
		if connected {
			sendPeerState(to:peer)
		} else {
			updateRawPeer(peer,to:[:])
			if session == peer {
				if sessionConnected {
					disconnect(sessionRequested:true)
				} else {
					session = nil
					delegate?.parGroup(self,session:peer,connected:false)
				}
			} else if session == id {
				pendingSessionPeers.remove(peer)
				if peersSet.remove(peer) != nil {
					send(parJson:["par":"peer","peer":peer],to:allReceivingPeers())
					delegate?.parGroup(self,peer:peer,connected:false)
				}
			} else if fieldChannel != nil {
				if peersSet.remove(peer) != nil {
					delegate?.parGroup(self,peer:peer,connected:false)
				}
			}
		}
	}

	private func event(peer:String,sent data:Data) {
		guard let prefix = data.first else { return }
		guard prefix == 0 else {
			if validDataPeer(peer) {
				delegate?.parGroup(self,peer:peer,sent:data.count > 1 ? data.advanced(by:1) : Data())
			}
			return
		}
		guard data.count > 1,let json = Json.parse(data.advanced(by:1)),let type = json["par"] as? String else { return }
		switch type {
		case "state":
			updateRawPeer(peer,to:json)
		case "pls":
			guard sessionVisible else {
				send(parJson:["par":"bye"],to:peer)
				break
			}
			if !peersSet.contains(peer) {
				pendingSessionPeers.insert(peer)
			}
			send(parJson:["par":"welcome","peers":[id]+peers],to:peer)
		case "welcome":
			guard session == peer else {
				send(parJson:["par":"bye"],to:peer)
				break
			}
			if let allPeers = json["peers"] as? [String] {
				let notify = !sessionConnected
				sessionConnected = true
				par.searching = false
				par.disconnect(allBut:peer)
				clearSessions()
				send(parJson:["par":"thx"],to:peer)
				if notify {
					delegate?.parGroup(self,session:peer,connected:true)
				}
				for peer in allPeers where peersSet.insert(peer).inserted {
					delegate?.parGroup(self,peer:peer,connected:true)
				}
			} else {
				session = nil
				send(parJson:["par":"bye"],to:peer)
				delegate?.parGroup(self,session:peer,connected:false)
			}
		case "thx":
			pendingSessionPeers.remove(peer)
			if session == id && !peersSet.contains(peer) {
				let allPeers = allReceivingPeers()
				peersSet.insert(peer)
				send(parJson:["par":"peer","peer":peer,"connected":true],to:allPeers)
				delegate?.parGroup(self,peer:peer,connected:true)
			}
		case "bye":
			pendingSessionPeers.remove(peer)
			if session == peer {
				if sessionConnected {
					disconnect(sessionRequested:true)
				} else {
					session = nil
					delegate?.parGroup(self,session:peer,connected:false)
				}
			} else if session == id {
				if peersSet.remove(peer) != nil {
					send(parJson:["par":"peer","peer":peer],to:allReceivingPeers())
					delegate?.parGroup(self,peer:peer,connected:false)
				}
				if !sessionVisible {
					par.disconnect(peer:peer)
				}
			}
		case "peer":
			guard sessionConnected && session == peer,let peer = json["peer"] as? String else { break }
			if json["connected"] as? Bool == true {
				if peersSet.insert(peer).inserted {
					delegate?.parGroup(self,peer:peer,connected:true)
				}
			} else if peersSet.remove(peer) != nil {
				delegate?.parGroup(self,peer:peer,connected:false)
			}
		default: break
		}
	}

	private func send(parJson json:[String:Any],to peers:[String]) {
		guard !peers.isEmpty,var data = Json.data(json) else { return }
		data.insert(0,at:0)
		for peer in peers {
			par.send(data:data,to:peer,reliable:true)
		}
	}

	private func send(parJson json:[String:Any],to peer:String) {
		guard var data = Json.data(json) else { return }
		data.insert(0,at:0)
		par.send(data:data,to:peer,reliable:true)
	}

	private func peerStateJson() -> [String:Any] {
		var json:[String:Any] = ["par":"state"]
		if let state = peerState {
			switch state {
			case .session(let info):
				json["state"] = "session"
				json["info"] = info
			case .field(let channel):
				json["state"] = "field"
				json["channel"] = channel
			}
		}
		return json
	}

	private func resendPeerState() {
		if updatePeerState() {
			send(parJson:peerStateJson(),to:par.peers)
		}
	}

	private func sendPeerState(to peer:String) {
		send(parJson:peerStateJson(),to:peer)
	}

	private func validDataPeer(_ peer:String) -> Bool {
		if sessionConnected {
			if session == id {
				if peersSet.contains(peer) {
					return true
				}
			} else if session == peer {
				return true
			}
		} else if fieldChannel != nil {
			if peersSet.contains(peer) {
				return true
			}
		}
		return false
	}
}

protocol ParGroupDelegate:AnyObject {
	func parGroup(_ parGroup:ParGroup,field channel:String,connected:Bool)
	func parGroup(_ parGroup:ParGroup,session peer:String,connected:Bool)
	func parGroup(_ parGroup:ParGroup,session peer:String,found info:[String:Any]?)
	func parGroup(_ parGroup:ParGroup,peer:String,connected:Bool)
	func parGroup(_ parGroup:ParGroup,peer:String,sent data:Data)
}
