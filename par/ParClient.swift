import Foundation

/** overridable class for a par client structure. */
class ParClient {
	/** client name. server must have the same name */
	let name:String
	/** client priority. a higher number guarantees that this client will run before others */
	let priority:Int

	/** true if a par service is currently running this service */
	final var isRunning:Bool {
		return ps != nil
	}
	/** true if it's connected to the server */
	private(set) var isConnected = false

	private var ps:ParService?

	/**
	initialises a par client with a name.
	- parameter name: client name. server must have the same name
	*/
	init(name:String,priority:Int = 0) {
		self.name = name
		self.priority = priority
	}

	/**
	sends data to the server.
	- parameter data: data to be sent
	- parameter reliable: kinda like the udp/tcp thing
	*/
	final func send(data:Data,reliable:Bool = true) {
		log("sent \(data.count) bytes")
		ps?.send(data:data,toServerOn:name,reliable:reliable)
	}

	/**
	starts this client with a given par service instance.

	must be called by par service only; subclasses should override **started**.
	- parameter ps: par service instance
	*/
	final func start(with ps:ParService) {
		guard self.ps == nil else { return }
		self.ps = ps
		log("started")
		started()
	}

	/**
	stops this client.

	must be called by par service only; subclasses should override **stopped**.
	*/
	final func stop() {
		guard ps != nil else { return }
		ps = nil
		if isConnected {
			isConnected = false
			log("disconnected")
			disconnected()
		}
		log("stopped")
		stopped()
	}

	/**
	connects this client to the server.

	must be called by par service only; subclasses should override **connected**.
	- parameter peer: peer id
	*/
	final func connect() {
		guard ps != nil && !isConnected else { return }
		isConnected = true
		log("connected")
		connected()
	}

	/**
	disconnects this client from the server.

	must be called by par service only; subclasses should override **disconnected**.
	- parameter peer: peer id
	*/
	final func disconnect() {
		guard ps != nil && isConnected else { return }
		isConnected = false
		log("disconnected")
		disconnected()
	}

	/**
	receives data from the server.

	must be called by par service only; subclasses should override **received**.
	- parameter data: data to be received
	*/
	final func receive(data:Data) {
		guard ps != nil && isConnected else { return }
		log("received \(data.count) bytes")
		received(data:data)
	}

	func started() {}
	func stopped() {}
	func connected() {}
	func disconnected() {}
	func received(data:Data) {}

	final private func log(_ s:String) {
		print("[par client] \(name): \(s)")
	}
}
