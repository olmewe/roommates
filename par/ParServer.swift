import Foundation

/** overridable class for a par server structure. */
class ParServer {
	/** server name. clients must have the same name */
	let name:String
	/** server priority. a higher number guarantees that this server will run before others */
	let priority:Int

	/** true if a par service is currently running this service */
	final var isRunning:Bool {
		return ps != nil
	}
	/** list of peers in this server */
	final var peers:[String] {
		return [String](peersSet)
	}
	/** list of peers, excluding a given one */
	func peers(excluding peer:String) -> [String]? {
		var peers = peersSet
		peers.remove(peer)
		return peers.isEmpty ? nil : [String](peers)
	}

	private var peersSet = Set<String>()
	private var ps:ParService?

	/**
	initialises a par server with a name.
	- parameter name: server name. clients must have the same name
	*/
	init(name:String,priority:Int = 0) {
		self.name = name
		self.priority = priority
	}

	/**
	sends data to a given connected peer.
	- parameter data: data to be sent
	- parameter peer: peer to receive such data
	- parameter reliable: kinda like the udp/tcp thing
	*/
	final func send(data:Data,to peer:String,reliable:Bool = true) {
		log("sent \(data.count) bytes to \(Par.strip(id:peer))")
		ps?.send(data:data,to:peer,on:name,reliable:reliable)
	}

	/**
	sends data to a given list of connected peers.
	- parameter data: data to be sent
	- parameter peers: peers to receive such data
	- parameter reliable: kinda like the udp/tcp thing
	*/
	final func send(data:Data,to peers:[String],reliable:Bool = true) {
		log("sent \(data.count) bytes to \(peers.map(Par.strip))")
		ps?.send(data:data,to:peers,on:name,reliable:reliable)
	}

	/**
	starts this server with a given par service instance.

	must be called by par service only; subclasses should override **started**.
	- parameter ps: par service instance
	*/
	final func start(with ps:ParService) {
		guard self.ps == nil else { return }
		self.ps = ps
		log("started")
		started()
	}

	/**
	stops this server.

	must be called by par service only; subclasses should override **stopped**.
	*/
	final func stop() {
		guard ps != nil else { return }
		ps = nil
		peersSet.removeAll()
		log("stopped")
		stopped()
	}

	/**
	connects a new peer to this server.

	must be called by par service only; subclasses should override **connected**.
	- parameter peer: peer id
	*/
	final func connect(peer:String) {
		guard ps != nil && peersSet.insert(peer).inserted else { return }
		log("connected to \(Par.strip(id:peer))")
		connected(peer:peer)
	}

	/**
	disconnects a peer from this server.

	must be called by par service only; subclasses should override **disconnected**.
	- parameter peer: peer id
	*/
	final func disconnect(peer:String) {
		guard ps != nil && peersSet.remove(peer) != nil else { return }
		log("disconnected from \(Par.strip(id:peer))")
		self.disconnected(peer:peer)
	}

	/**
	receives data from a peer.

	must be called by par service only; subclasses should override **received**.
	- parameter data: data to be received
	- parameter peer: peer id
	*/
	final func receive(data:Data,from peer:String) {
		guard ps != nil && peersSet.contains(peer) else { return }
		log("received \(data.count) bytes from \(Par.strip(id:peer))")
		received(data:data,from:peer)
	}

	func started() {}
	func stopped() {}
	func connected(peer:String) {}
	func disconnected(peer:String) {}
	func received(data:Data,from peer:String) {}

	final private func log(_ s:String) {
		print("[par server] \(name): \(s)")
	}
}
