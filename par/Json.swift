import Foundation

/** a json helper class. */
class Json {
	/**
	converts a json structure to data.
	- parameter json: json structure
	- returns: resulting data, or nil if it couldn't convert
	*/
	static func data(_ json:[String:Any]?) -> Data? {
		guard let json = json else { return nil }
		return try? JSONSerialization.data(withJSONObject:json)
	}

	/**
	converts data to a json structure.
	- parameter data: json data
	- returns: resulting json, or nil if it couldn't convert
	*/
	static func parse(_ data:Data?) -> [String:Any]? {
		guard let data = data,let json = try? JSONSerialization.jsonObject(with:data) else { return nil }
		return json as? [String:Any]
	}
}
