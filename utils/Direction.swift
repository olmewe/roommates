import SpriteKit

/** direction indicator, for anything that needs a thing like this. */
enum Direction:Int {
	case up
	case down
	case left
	case right

	/** this direction in coord (int) form */
	var int:(x:Int,y:Int) {
		get {
			switch self {
			case .up: return (x:0,y:1)
			case .down: return (x:0,y:-1)
			case .left: return (x:-1,y:0)
			case .right: return (x:1,y:0)
			}
		}
	}

	/** this direction in cgpoint form */
	var float:CGPoint {
		get {
			switch self {
			case .up: return CGPoint(x:0,y:1)
			case .down: return CGPoint(x:0,y:-1)
			case .left: return CGPoint(x:-1,y:0)
			case .right: return CGPoint(x:1,y:0)
			}
		}
	}

	/** the opposite direction */
	var opposite:Direction {
		get {
			switch self {
			case .up: return .down
			case .down: return .up
			case .left: return .right
			case .right: return .left
			}
		}
	}
}
