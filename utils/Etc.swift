import SpriteKit

infix operator ==~:ComparisonPrecedence
infix operator !=~:ComparisonPrecedence
infix operator >~:ComparisonPrecedence
infix operator <~:ComparisonPrecedence
infix operator >=~:ComparisonPrecedence
infix operator <=~:ComparisonPrecedence

extension CGFloat {
	/**
	clamps this value between min and max.
	- parameter min: minimum value
	- parameter max: maximum value
	- returns: clamped value
	*/
	func clamp(_ min:CGFloat,_ max:CGFloat) -> CGFloat {
		if self <= min { return min }
		if self >= max { return max }
		return self
	}

	/** this value clamped between 0 and 1 */
	var clamp01:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 1 }
		return self
	}

	/** one subtracted by this value */
	var inv:CGFloat {
		return 1-self
	}

	/** this value eased in, for animations */
	var easeIn:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 1 }
		return self*self
	}

	/** this value eased out, for animations */
	var easeOut:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 1 }
		return (2-self)*self
	}

	/** this value eased, for animations */
	var ease:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 1 }
		return (3-2*self)*self*self
	}

	/** this value describing a parabola, for animations */
	var parabola:CGFloat {
		if self <= 0 { return 0 }
		if self >= 1 { return 0 }
		return 4*(self-self*self)
	}

	/**
	linearly interpolates two values using this one as a value between 0 and 1.
	- parameter a: value for 0
	- parameter b: value for 1
	- returns: interpolated value
	*/
	func lerp(_ a:CGFloat,_ b:CGFloat) -> CGFloat {
		if self <= 0 { return a }
		if self >= 1 { return b }
		return a+(b-a)*self
	}

	/**
	linearly interpolates 0 and 1 using this one as a value between a and b.
	it's like the opposite of lerp.
	- parameter a: value for 0
	- parameter b: value for 1
	- returns: interpolated value
	*/
	func lerpInv(_ a:CGFloat,_ b:CGFloat) -> CGFloat {
		if a == b { return 0 }
		if a < b {
			if self <= a { return 0 }
			if self >= b { return 1 }
		} else {
			if self >= a { return 0 }
			if self <= b { return 1 }
		}
		return (self-a)/(b-a)
	}

	/**
	linearly interpolates two points using this float as a value between 0 and 1.
	- parameter a: point for 0
	- parameter b: point for 1
	- returns: interpolated point
	*/
	func lerp(_ a:CGPoint,_ b:CGPoint) -> CGPoint {
		if self <= 0 { return a }
		if self >= 1 { return b }
		return CGPoint(x:lerp(a.x,b.x),y:lerp(a.y,b.y))
	}

	func lerp(_ a:SKColor,_ b:SKColor) -> SKColor {
		if self <= 0 { return a }
		if self >= 1 { return b }
		var ar:CGFloat = 0,ag:CGFloat = 0,ab:CGFloat = 0,aa:CGFloat = 0
		var br:CGFloat = 0,bg:CGFloat = 0,bb:CGFloat = 0,ba:CGFloat = 0
		a.getRed(&ar,green:&ag,blue:&ab,alpha:&aa)
		b.getRed(&br,green:&bg,blue:&bb,alpha:&ba)
		if aa <=~ 0 {
			return SKColor(red:br,green:bg,blue:bb,alpha:ba*self)
		}
		if ba <=~ 0 {
			return SKColor(red:ar,green:ag,blue:ab,alpha:aa*self)
		}
		return SKColor(red:lerp(ar,br),green:lerp(ag,bg),blue:lerp(ab,bb),alpha:lerp(aa,ba))
	}

	private static let minNormal:CGFloat = 1.17549435E-38
	private static let minDenormal:CGFloat = 1.401298E-45
	private static let flushToZeroEnabled = minDenormal == 0
	static var epsilon:CGFloat {
		return flushToZeroEnabled ? minNormal : minDenormal
	}

	static func ==~(left:CGFloat,right:CGFloat) -> Bool {
		return Swift.abs(left-right) < Swift.max(1E-06*Swift.max(Swift.abs(left),Swift.abs(right)),CGFloat.epsilon*8)
	}

	static func !=~(left:CGFloat,right:CGFloat) -> Bool {
		return !(left ==~ right)
	}

	static func <~(left:CGFloat,right:CGFloat) -> Bool {
		return left < right && left !=~ right
	}

	static func >~(left:CGFloat,right:CGFloat) -> Bool {
		return left > right && left !=~ right
	}

	static func <=~(left:CGFloat,right:CGFloat) -> Bool {
		return left < right || left ==~ right
	}

	static func >=~(left:CGFloat,right:CGFloat) -> Bool {
		return left > right || left ==~ right
	}
}

extension Int {
	/**
	clamps this value between min and max.
	- parameter min: minimum value
	- parameter max: maximum value
	- returns: clamped value
	*/
	func clamp(_ min:Int,_ max:Int) -> Int {
		if self <= min { return min }
		if self >= max { return max }
		return self
	}
}

extension CGPoint {
	/**
	clamps this point between min and max.
	- parameter min: minimum point
	- parameter max: maximum point
	- returns: clamped point
	*/
	func clamp(_ min:CGPoint,_ max:CGPoint) -> CGPoint {
		return CGPoint(x:x.clamp(min.x,max.x),y:y.clamp(min.y,max.y))
	}

	/**
	clamps this point inside a rect.
	- parameter rect: cgrect
	- returns: clamped point
	*/
	func clamp(_ rect:CGRect) -> CGPoint {
		return CGPoint(x:x.clamp(rect.minX,rect.maxX),y:y.clamp(rect.minY,rect.maxY))
	}

	/** squared magnitude of this vector */
	var sqrMagnitude:CGFloat {
		return x*x+y*y
	}

	/** magnitude of this vector */
	var magnitude:CGFloat {
		return sqrt(sqrMagnitude)
	}

	/**
	gets the square distance between this point and another.
	- parameter point: other point to compare
	- returns: squared distance
	*/
	func sqrDistance(to point:CGPoint) -> CGFloat {
		let dx = point.x-x
		let dy = point.y-y
		return dx*dx+dy*dy
	}

	/**
	gets the distance between this point and another.
	- parameter point: other point to compare
	- returns: distance
	*/
	func distance(to point:CGPoint) -> CGFloat {
		return sqrt(sqrDistance(to:point))
	}

	static func ==~(left:CGPoint,right:CGPoint) -> Bool {
		return left.x ==~ right.x && left.y ==~ right.y
	}

	static func !=~(left:CGPoint,right:CGPoint) -> Bool {
		return !(left ==~ right)
	}
}

extension CGRect {
	init(minX:CGFloat,maxX:CGFloat,minY:CGFloat,maxY:CGFloat) {
		self.init(x:minX,y:minY,width:maxX-minX,height:maxY-minY)
	}
}

extension CGSize {
	/** a 1x1 cgsize structure */
	static var one:CGSize {
		return CGSize(width:1,height:1)
	}
}

extension Data {
	var cuteString:String {
		return map {
			switch $0 {
			case 0...9: return $0.description
			case 10...31: return "_"
			case 127: return "_"
			default: return String(Character(Unicode.Scalar($0)))
			}
		}.joined()
	}
}

extension UIDevice {
	static var model:String {
		var systemInfo = utsname()
		uname(&systemInfo)
		let machineMirror = Mirror(reflecting: systemInfo.machine)
		let identifier = machineMirror.children.reduce("") { identifier, element in
			guard let value = element.value as? Int8, value != 0 else { return identifier }
			return identifier + String(UnicodeScalar(UInt8(value)))
		}
		return identifier
	}
}
