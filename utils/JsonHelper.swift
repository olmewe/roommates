import SpriteKit

extension Float {
	var s:Double {
		return Double(self)
	}

	static func s(_ value:Any?) -> Float? {
		guard let d = value as? Double else { return nil }
		return Float(d)
	}
}

extension CGFloat {
	var s:Double {
		return Double(self)
	}

	static func s(_ value:Any?) -> CGFloat? {
		guard let d = value as? Double else { return nil }
		return CGFloat(d)
	}
}

extension CGPoint {
	var s:[Double] {
		return [Double(x),Double(y)]
	}

	static func s(_ value:Any?) -> CGPoint? {
		guard let d = value as? [Double],d.count >= 2 else { return nil }
		return CGPoint(x:d[0],y:d[1])
	}
}

extension CGSize {
	var s:[Double] {
		return [Double(width),Double(height)]
	}

	static func s(_ value:Any?) -> CGSize? {
		guard let d = value as? [Double],d.count >= 2 else { return nil }
		return CGSize(width:d[0],height:d[1])
	}
}
