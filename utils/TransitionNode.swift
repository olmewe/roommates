import SpriteKit

class TransitionNode:SKNode,SceneNode {
	private var animTempo:CGFloat = 0
	private var transition:Transition = .slideFromLeft
	var completion:(() -> Void)? = nil
	private var velocity:CGFloat = 1

	private var transitionUpdate:((CGFloat) -> Void)? = nil
	private var transitionEnd:(() -> Void)? = nil

	static func make(transition:Transition,duration:CGFloat,completion:(() -> Void)? = nil) -> TransitionNode {
		let node = TransitionNode()
		Scene.current.cam.addChild(node)
		node.zPosition = 1000
		node.position = .zero
		node.set(transition:transition,duration:duration,completion:completion)
		return node
	}

	func set(transition:Transition,duration:CGFloat,completion:(() -> Void)? = nil) {
		guard duration > 0 else {
			completion?()
			if parent != nil {
				removeFromParent()
			}
			return
		}
		animTempo = 1
		self.transition = transition
		self.completion = completion
		velocity = 1/duration
		setTransition()
		transitionUpdate?(0)
	}

	deinit {
		transitionUpdate = nil
		if let comp = completion {
			completion = nil
			comp()
		}
		if let end = transitionEnd {
			transitionEnd = nil
			end()
		}
	}

	func update() {
		guard animTempo > 0 else {
			if parent != nil {
				removeFromParent()
			}
			if let end = transitionEnd {
				transitionEnd = nil
				end()
			}
			return
		}
		animTempo -= Time.deltaTime*velocity
		if animTempo <= 0 {
			animTempo = 0
			transitionUpdate?(1)
			if let comp = completion {
				completion = nil
				comp()
			}
		} else {
			transitionUpdate?(1-animTempo)
		}
	}

	enum Transition {
		case welcome
		case slideFromLeft
		case slideToLeft
		case slideFromRight
		case slideToRight
		case verticalOpen
		case verticalClose
	}

	private func setTransition() {
		transitionUpdate = nil
		transitionEnd = nil
		switch transition {
		case .slideFromLeft,.slideToLeft,.slideFromRight,.slideToRight:
			let sprite = SKSpriteNode(color:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),size:Scene.current.size)
			let s = Scene.current.size.width
			addChild(sprite)
			switch transition {
			case .slideFromLeft:
				transitionUpdate = {
					sprite.position = CGPoint(x:$0.inv.ease.lerp(0,-s),y:0)
				}
			case .slideToLeft:
				transitionUpdate = {
					sprite.position = CGPoint(x:$0.ease.lerp(0,-s),y:0)
				}
			case .slideFromRight:
				transitionUpdate = {
					sprite.position = CGPoint(x:$0.inv.ease.lerp(0,s),y:0)
				}
			case .slideToRight:
				transitionUpdate = {
					sprite.position = CGPoint(x:$0.ease.lerp(0,s),y:0)
				}
			default: break
			}
			transitionEnd = {
				sprite.removeFromParent()
			}
		case .welcome,.verticalOpen,.verticalClose:
			let c = transition == .welcome ? #colorLiteral(red: 1, green: 1, blue: 0.8, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
			let a = SKSpriteNode(color:c,size:Scene.current.size)
			let b = SKSpriteNode(color:c,size:Scene.current.size)
			let s = Scene.current.size.width
			addChild(a)
			addChild(b)
			switch transition {
			case .welcome:
				transitionUpdate = {
					let p = (($0.lerp(-1,1))*1.1).ease.lerp(s*0.475,s)
					a.position = CGPoint(x:-p,y:0)
					b.position = CGPoint(x:p,y:0)
				}
			case .verticalOpen:
				transitionUpdate = {
					let p = ($0*1.1).ease.lerp(s*0.475,s)
					a.position = CGPoint(x:-p,y:0)
					b.position = CGPoint(x:p,y:0)
				}
			case .verticalClose:
				transitionUpdate = {
					let p = ($0.inv*1.1).ease.lerp(s*0.475,s)
					a.position = CGPoint(x:-p,y:0)
					b.position = CGPoint(x:p,y:0)
				}
			default: break
			}
			transitionEnd = {
				a.removeFromParent()
				b.removeFromParent()
			}
		}
	}
}
