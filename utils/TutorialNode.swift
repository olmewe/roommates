import SpriteKit

class TutorialNode:SKNode,SceneNode {
	let icon:SKSpriteNode!
	let fade:SKSpriteNode
	private let textures:[SKTexture]
	private(set) var view:TutorialViewController?

	var list:TutorialList!

	private var tempo:CGFloat = 0

	init(size:CGSize,showIcon:Bool) {
		let fade = SKSpriteNode(color:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.59),size:size)
		fade.zPosition = 100
		self.fade = fade
		if showIcon {
			let icon = SKSpriteNode()
			icon.size = CGSize(width:155,height:125)
			icon.anchorPoint = CGPoint(x:1,y:0)
			icon.position = CGPoint(x:size.width*0.5-30,y:-size.height*0.5+30)
			icon.zPosition = 9
			self.icon = icon
			let atlas = SKTextureAtlas(named:"tutorial_phone")
			textures = (0...2).map { atlas.textureNamed($0.description) }
		} else {
			icon = nil
			textures = []
		}
		super.init()
		updateAlpha()
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func lateUpdate() {
		if icon != nil {
			icon.texture = textures[Int(Time.time*12)%textures.count]
		}
		if view != nil {
			if tempo < 1 {
				tempo += Time.deltaTime/0.3
				if tempo > 1 {
					tempo = 1
				}
				updateAlpha()
			}
		} else {
			if tempo > 0 {
				tempo -= Time.deltaTime/0.3
				if tempo < 0 {
					tempo = 0
				}
				updateAlpha()
			}
		}
	}

	private func updateAlpha() {
		if tempo >~ 0 {
			if fade.parent == nil {
				addChild(fade)
			}
			fade.alpha = tempo.ease
		} else {
			if fade.parent != nil {
				fade.removeFromParent()
			}
		}
		if icon != nil {
			let t = tempo.lerp(2,-1)
			if t >~ 0 {
				if icon.parent == nil {
					addChild(icon)
				}
				icon.alpha = t.ease
			} else {
				if icon.parent != nil {
					icon.removeFromParent()
				}
			}
		}
	}

	func open(_ end:(() -> Void)? = nil) {
		guard view == nil else { return }
		view = UI.showTutorial(list:list) {
			self.view = nil
			end?()
		}
	}
}
