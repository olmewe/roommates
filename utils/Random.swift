import SpriteKit

class Random {
	static var bool:Bool {
		return arc4random_uniform(2) == 0
	}

	static var value:CGFloat {
		return CGFloat(drand48()).clamp01
	}

	static func range(_ upper:Int) -> Int {
		guard upper > 1 else { return 0 }
		return Int(arc4random_uniform(UInt32(upper))).clamp(0,upper-1)
	}

	static func range(_ base:Int,_ upper:Int) -> Int {
		return base+range(upper-base)
	}

	static func range(_ max:CGFloat) -> CGFloat {
		guard max > 0 else { return 0 }
		return max*value
	}

	static func range(_ min:CGFloat,_ max:CGFloat) -> CGFloat {
		return min+range(max-min)
	}

	static func range(_ min:CGPoint,_ max:CGPoint) -> CGPoint {
		return CGPoint(x:min.x+range(max.x-min.x),y:min.y+range(max.y-min.y))
	}
}
