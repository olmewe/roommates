import Foundation

class BoolGrid {
	private(set) var data:Data
	var width:Int {
		return Int(data[0])+1
	}
	var height:Int {
		return Int(data[1])+1
	}

	subscript(x:Int,y:Int) -> Bool {
		get {
			guard let i = index(x:x,y:y) else { return false }
			return ((data[i.index] >> i.shift) & 1) != 0
		}
		set {
			guard let i = index(x:x,y:y) else { return }
			if newValue {
				data[i.index] |= 1 << i.shift
			} else {
				data[i.index] &= ~(1 << i.shift)
			}
		}
	}

	init(width:Int,height:Int) {
		let width = width.clamp(1,256)
		let height = height.clamp(1,256)
		data = Data(count:BoolGrid.length(width:width,height:height))
		data[0] = UInt8(width-1)
		data[1] = UInt8(height-1)
	}

	init?(_ data:Data) {
		guard data.count > 2 else { return nil }
		let count = BoolGrid.length(width:Int(data[0])+1,height:Int(data[1])+1)
		guard data.count >= count else { return nil }
		self.data = data
		while self.data.count > count {
			self.data.removeLast()
		}
	}

	init(_ grid:BoolGrid) {
		data = grid.data
	}

	convenience init(grid:[[Int]]) {
		self.init(width:max(grid.max { a,b in a.count < b.count }?.count ?? 1,1),height:max(grid.count,1))
		for y in 0..<height {
			if y < grid.count {
				let line = grid[y]
				if line.count >= width {
					for x in 0..<width {
						self[x,y] = line[x] != 0
					}
				} else if line.isEmpty {
					for x in 0..<width {
						self[x,y] = false
					}
				} else {
					for x in 0..<line.count {
						self[x,y] = line[x] != 0
					}
					for x in line.count..<width {
						self[x,y] = false
					}
				}
			} else {
				for x in 0..<width {
					self[x,y] = false
				}
			}
		}
	}

	func clear() {
		for i in 2..<data.count {
			data[i] = 0
		}
	}

	func set(_ value:Any?) -> Bool {
		guard let grid = BoolGrid.s(value),grid.width == width && grid.height == height else { return false }
		data = grid.data
		return true
	}

	func copy(_ grid:BoolGrid) {
		data = grid.data
	}

	private func index(x:Int,y:Int) -> (index:Int,shift:UInt8)? {
		guard x >= 0 && x < width && y >= 0 && y < height else { return nil }
		let index = x+y*width
		let rawIndex = index/8
		return (rawIndex+2,UInt8(index-rawIndex*8))
	}

	private static func length(width:Int,height:Int) -> Int {
		return 2+(width*height+7)/8
	}

	var s:String {
		return data.base64EncodedString()
	}

	static func s(_ value:Any?) -> BoolGrid? {
		guard let string = value as? String,let data = Data(base64Encoded:string) else { return nil }
		return BoolGrid(data)
	}
}
