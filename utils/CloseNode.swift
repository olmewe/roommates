import SpriteKit

class CloseNode:SKSpriteNode,ButtonSelectorHandler {
	init() {
		let s = CGFloat(Game.pad ? 124.2 : 189)/CGFloat(1242)
		super.init(texture:SKTexture(imageNamed:"common/close"),color:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),size:CGSize(width:s,height:s))
		anchorPoint = CGPoint(x:1,y:1)
		colorBlendFactor = 0
		zPosition = 10
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func resize(_ size:CGSize) {
		position = CGPoint(x:size.width*0.5,y:size.height*0.5)
		setScale(size.width)
	}

	func selected() {
		colorBlendFactor = 119/255
	}

	func deselected() {
		colorBlendFactor = 0
	}
}
