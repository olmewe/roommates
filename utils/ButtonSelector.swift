import SpriteKit

class ButtonSelector {
	var nodes:[SKNode] = []
	var selected:SKNode? = nil
	private(set) var touchID:Int? = nil

	func add(_ node:SKNode) {
		if !nodes.contains(node) {
			nodes.append(node)
		}
	}

	func remove(_ node:SKNode) {
		if let index = nodes.index(of:node) {
			nodes.remove(at:index)
		}
	}

	func update() -> SKNode? {
		for i in Touch.events {
			if touchID == nil && i.state == .down {
				select(at:i.position)
				if selected != nil {
					touchID = i.id
				}
			}
			if touchID == i.id {
				if i.state == .miss {
					touchID = nil
					cancel()
				} else if i.state == .up {
					touchID = nil
					return finalSelect(at:i.position)
				}
			}
		}
		return nil
	}

	func cancel() {
		(selected as? ButtonSelectorHandler)?.deselected()
		selected = nil
	}

	func select(at position:CGPoint) {
		for i in Scene.current.nodes(at:position) {
			for j in nodes where i.isEqual(to:j) {
				selected = i
				(i as? ButtonSelectorHandler)?.selected()
				return
			}
		}
		selected = nil
	}

	func finalSelect(at position:CGPoint) -> SKNode? {
		guard let j = selected else { return nil }
		(j as? ButtonSelectorHandler)?.deselected()
		for i in Scene.current.nodes(at:position) where i.isEqual(to:j) {
			return i
		}
		return nil
	}
}

protocol ButtonSelectorHandler {
	func selected()
	func deselected()
}
