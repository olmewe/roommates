import SpriteKit

class PanelNode:SKNode,ButtonSelectorHandler {
	var size:CGSize = CGSize.zero {
		didSet {
			shape.removeFromParent()
			shape = PanelNode.createShape(size:size)
			addChild(shape)
			format()
		}
	}
	var bg:SKColor {
		didSet {
			format()
		}
	}
	var border:SKColor {
		didSet {
			format()
		}
	}
	var colour:SKColor {
		didSet {
			format()
		}
	}
	var text:String {
		didSet {
			label.text = text
			reposition()
		}
	}
	var icon:SKTexture? {
		didSet {
			updateIcon()
			format()
			reposition()
		}
	}

	private var shape:SKShapeNode
	private let label = SKLabelNode(fontNamed:"Nunito-SemiBold")
	private var iconNode:SKSpriteNode! = nil

	private var hover = false
	private let iconActualSize:CGFloat = 96
	private let iconSize:CGFloat = 48
	private let iconMargin:CGFloat = 16

	init(size:CGSize,bg:SKColor,border:SKColor,colour:SKColor = SKColor.black,text:String = "",icon:SKTexture? = nil) {
		shape = PanelNode.createShape(size:size)
		self.bg = bg
		self.border = border
		self.colour = colour
		self.text = text
		self.icon = icon
		super.init()
		addChild(shape)
		addChild(label)
		label.horizontalAlignmentMode = .center
		label.verticalAlignmentMode = .center
		label.zPosition = 0.1
		label.fontSize = 40
		label.text = text
		updateIcon()
		format()
		reposition()
	}

	private func format() {
		let t:CGFloat = hover ? (136/255) : 1
		shape.fillColor = t.lerp(SKColor.black,bg)
		shape.strokeColor = t.lerp(SKColor.black,border)
		let labelColour = t.lerp(SKColor.black,colour)
		label.fontColor = labelColour
		if iconNode != nil {
			iconNode.color = labelColour
		}
	}

	private func updateIcon() {
		if let icon = icon {
			if iconNode == nil {
				iconNode = SKSpriteNode()
				addChild(iconNode)
				iconNode.zPosition = 0.1
				iconNode.size = CGSize(width:iconActualSize,height:iconActualSize)
				iconNode.colorBlendFactor = 1
			}
			iconNode.texture = icon
		} else {
			if iconNode != nil {
				iconNode.removeFromParent()
				iconNode = nil
			}
		}
	}

	private func reposition() {
		if iconNode != nil {
			let offset = -0.5*(label.frame.width+iconSize+iconMargin)
			iconNode.position = CGPoint(x:iconSize*0.5+offset,y:0)
			label.position = CGPoint(x:iconSize+iconMargin+offset,y:0)
			label.horizontalAlignmentMode = .left
		} else {
			label.position = CGPoint.zero
			label.horizontalAlignmentMode = .center
		}
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func selected() {
		hover = true
		format()
	}

	func deselected() {
		hover = false
		format()
	}

	private static func createShape(size:CGSize) -> SKShapeNode {
		let node = SKShapeNode(rectOf:size)
		node.lineWidth = 3
		return node
	}
}

