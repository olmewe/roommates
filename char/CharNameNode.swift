import SpriteKit

class CharNameNode:SKNode {
	var color:SKColor? {
		get {
			return label.fontColor
		}
		set {
			label.fontColor = newValue
		}
	}
	var alignment:SKLabelHorizontalAlignmentMode = .center {
		didSet {
			align()
		}
	}

	private let label = SKLabelNode(fontNamed:"Nunito-Regular")
	private let icon:SKSpriteNode!

	private let iconActualSize:CGFloat = 96
	private let iconSize:CGFloat = 64
	private let iconMargin:CGFloat = 12

	init(showIcon:Bool = true) {
		icon = showIcon ? SKSpriteNode() : nil
		super.init()
		addChild(label)
		label.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		label.fontSize = 48
		label.verticalAlignmentMode = .center
		if showIcon {
			addChild(icon)
			icon.color = SKColor.clear
			icon.size = CGSize(width:iconActualSize,height:iconActualSize)
		}
		align()
	}

	convenience init(peer:String,showIcon:Bool = true) {
		self.init(showIcon:showIcon)
		set(peer:peer)
	}

	convenience init(name:String,char:String) {
		self.init(showIcon:true)
		set(name:name,char:char)
	}

	convenience init(name:String,icon:SKTexture? = nil) {
		self.init(showIcon:icon != nil)
		set(name:name,icon:icon)
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func set(peer:String) {
		guard let info = GameState.peerInfo(for:peer) else { return }
		if info.name.isEmpty {
			set(name:"",icon:nil)
		} else {
			set(name:info.name,char:info.char)
		}
	}

	func set(name:String,char:String) {
		set(name:name,icon:icon != nil ? SKTexture(imageNamed:"char_\(char)_meta/icon") : nil)
	}

	func set(name:String,icon:SKTexture? = nil) {
		label.text = name
		if self.icon != nil {
			self.icon.texture = icon
		}
		align()
	}

	private func align() {
		if icon != nil {
			let offset:CGFloat
			switch alignment {
			case .left: offset = -(label.frame.width+iconSize+iconMargin)
			case .center: offset = -0.5*(label.frame.width+iconSize+iconMargin)
			case .right: offset = 0
			}
			self.icon.position = CGPoint(x:iconSize*0.5+offset,y:0)
			label.position = CGPoint(x:iconSize+iconMargin+offset,y:0)
			label.horizontalAlignmentMode = .left
		} else {
			label.position = CGPoint.zero
			label.horizontalAlignmentMode = alignment
		}
	}
}
