import SpriteKit

class CharNode:SKSpriteNode,SceneNode {
	var atlas:SKTextureAtlas! = nil
	var countFor:[State:Int] = [:]
	var state:State = .still

	private var currentState:State! = nil
	private var currentMax:Int = 0
	private var currentFrame:Int = 0
	private var lastTime:Int = -1

	static let defaultScale:CGFloat = 540*720/1242

	var scale:CGFloat {
		get {
			return size.width
		}
		set {
			size = CGSize(width:scale,height:scale)
		}
	}

	init(scale:CGFloat = defaultScale) {
		super.init(texture:nil,color:SKColor.clear,size:CGSize(width:scale,height:scale))
		anchorPoint = CGPoint(x:0.5,y:2/9)
		update()
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func update() {
		let time = Int(Time.time*12)
		if lastTime != time {
			lastTime = time
			if currentState != state {
				currentState = state
				currentFrame = 0
				currentMax = countFor[state] ?? 0
			} else if currentMax > 0 {
				currentFrame += 1
				if currentFrame >= currentMax {
					currentFrame = 0
				}
			}
			updateSprite()
		}
	}

	func updateSprite() {
		if currentMax <= 0 || atlas == nil || currentState == nil {
			texture = nil
		} else {
			texture = atlas.textureNamed("\(currentState.rawValue)\(currentFrame)")
		}
	}

	func set(char:String) {
		currentState = nil
		atlas = SKTextureAtlas(named:"char_\(char)_sprites")
		countFor = [:]
		let names = Set<String>(atlas.textureNames)
		for state in State.all {
			var frame:Int = 0
			while names.contains("\(state)\(frame)") {
				frame += 1
			}
			if frame > 0 {
				countFor[state] = frame
			}
		}
		currentMax = countFor[state] ?? 0
		if currentMax > 0 && currentFrame >= currentMax {
			currentFrame = 0
		}
		updateSprite()
	}

	enum State:String {
		case still = "still"
		case think = "think"
		case walk = "walk"
		case back = "back"

		static let all:[State] = [
			.still,
			.think,
			.walk,
			.back,
		]
	}
}
