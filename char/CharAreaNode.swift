import SpriteKit

class CharAreaNode:SKNode,SceneNode {
	private(set) var chars:[String:ControllerNode] = [:]
	private(set) var myChar:ControllerNode!

	private var zScale:CGFloat = 0
	private var zOffset:CGFloat = 0

	let collider = ColliderSystem()

	var sfxWalk:Data?

	override init() {
		super.init()
		myChar = ControllerNode(area:self,id:GameData.id,me:true)
		addChild(myChar)
		myChar.hide = true
		sfxWalk = Audio.data(of:"app/walk.wav")
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func debugCollider() {
		let maxOffset:CGFloat = 10000
		let weight:CGFloat = 8
		func colour(col colliders:[ColliderSystem.Collider],with colour:SKColor,vertical:Bool) {
			for col in colliders {
				let min = col.hasMin ? col.min : -maxOffset
				let max = col.hasMax ? col.max : maxOffset
				guard min < max else { continue }
				let node = SKSpriteNode()
				node.color = colour
				if vertical {
					node.size = CGSize(width:max-min,height:weight)
					node.position = CGPoint(x:(max+min)*0.5,y:col.value)
				} else {
					node.size = CGSize(width:weight,height:max-min)
					node.position = CGPoint(x:col.value,y:(max+min)*0.5)
				}
				node.zPosition = 10
				addChild(node)
			}
		}
		colour(col:collider.up,with:#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1),vertical:true)
		colour(col:collider.down,with:#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1),vertical:true)
		colour(col:collider.right,with:#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1),vertical:false)
		colour(col:collider.left,with:#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1),vertical:false)
	}

	func setZReference(min:CGFloat,max:CGFloat) {
		zScale = 1/(min-max)
		zOffset = max
	}

	func setZReferenceFromColliders() {
		guard let min = collider.down.min(by:{ a,b in a.value < b.value }),let max = collider.up.max(by:{ a,b in a.value < b.value }) else { return }
		setZReference(min:min.value,max:max.value)
	}

	func z(for y:CGFloat) -> CGFloat {
		return ((y-zOffset)*zScale).clamp01*0.9+0.05
	}

	func z(node:SKSpriteNode,below:CGFloat) {
		node.zPosition = z(for:node.position.y-node.size.height*(1-below)*0.5)
	}

	func show(peer:String,pos:CGPoint,target:CGPoint,autospawn:Bool = true) {
		if let char = chars[peer] {
			char.target = target
		} else if autospawn {
			let char = ControllerNode(area:self,id:peer)
			addChild(char)
			chars[peer] = char
			char.position = pos
			char.reset()
			char.target = target
		}
	}

	func show(peer:String,pos:CGPoint,autospawn:Bool = true) {
		show(peer:peer,pos:pos,target:pos,autospawn:autospawn)
	}

	func hide(peer:String) {
		if let c = chars[peer] {
			chars[peer] = nil
			c.hide = true
		}
	}

	func show(pos:CGPoint,target:CGPoint) {
		if !myChar.hide {
			myChar.target = target
		} else {
			myChar.hide = false
			myChar.position = pos
			myChar.reset()
			myChar.target = target
		}
	}

	func show(pos:CGPoint) {
		show(pos:pos,target:pos)
	}

	func show(target:CGPoint) {
		show(pos:myChar.position,target:target)
	}

	func hide() {
		myChar.hide = true
	}

	func hideAll() {
		myChar.hide = true
		for (_,c) in chars {
			c.hide = true
		}
		chars.removeAll()
	}

	func refreshChar() {
		myChar.nameNode.set(peer:GameData.id)
		myChar.sprite.set(char:GameData.peerInfo.char)
	}

	class ControllerNode:SKNode,SceneNode {
		let sprite = CharNode()
		let nameNode = CharNameNode(showIcon:false)
		let id:String
		let area:CharAreaNode
		let me:Bool
		let halfWidth:CGFloat
		let halfHeight:CGFloat

		var target:CGPoint {
			didSet {
				moving = position !=~ target
			}
		}
		private(set) var moving = false

		let vel:CGFloat = 500
		let halfWidthFactor:CGFloat = (240*720/(1242*2))/CharNode.defaultScale
		let halfHeightFactor:CGFloat = (80*720/(1242*2))/CharNode.defaultScale

		private var animationScale:CGFloat = 0
		var hide = false

		private var sfxTempo:CGFloat = 0
		private let sfxInterval:CGFloat = 0.2

		init(area:CharAreaNode,id:String,me:Bool = false,scale:CGFloat = CharNode.defaultScale) {
			self.area = area
			self.id = id
			self.me = me
			halfWidth = halfWidthFactor*scale
			halfHeight = halfHeightFactor*scale
			target = .zero
			super.init()
			position = .zero
			addChild(sprite)
			sprite.set(char:GameState.peerInfo(for:id)?.char ?? PeerInfo.chars[Random.range(PeerInfo.chars.count)])
			addChild(nameNode)
			nameNode.position = CGPoint(x:0,y:-halfHeight*1.5)
			nameNode.zPosition = 2
			nameNode.set(peer:id)
			nameNode.setScale(0.75)
			isHidden = true
		}

		required init?(coder aDecoder:NSCoder) {
			return nil
		}

		func reset() {
			target = position
			zPosition = area.z(for:target.y)
		}

		func update() {
			if sfxTempo > 0 {
				sfxTempo -= Time.deltaTime
				if sfxTempo < 0 {
					sfxTempo = 0
				}
			}
			if moving {
				let result = area.collider.solve(from:position,to:target,distance:vel*Time.deltaTime,halfWidth:halfWidth,halfHeight:halfHeight)
				if result.deadEnd || position ==~ result.pos {
					position = result.pos
					moving = false
					reset()
				} else {
					position = result.pos
					moving = true
					zPosition = area.z(for:result.pos.y)
				}
			}
			if moving {
				sprite.state = .walk
				if sfxTempo <=~ 0 {
					sfxTempo = sfxInterval
					Audio.play(area.sfxWalk)
				}
			} else {
				sprite.state = .still
			}
			if hide {
				if animationScale > 0 {
					animationScale -= Time.deltaTime*3
					if animationScale <= 0 {
						if me {
							animationScale = 0
							isHidden = true
						} else {
							removeFromParent()
						}
					} else {
						setScale(animationScale.easeOut)
					}
				}
			} else {
				if animationScale < 1 {
					animationScale += Time.deltaTime*3
					if animationScale >= 1 {
						animationScale = 1
						setScale(1)
					} else {
						setScale(animationScale.easeOut)
					}
					isHidden = false
				}
			}
		}

		func endAnimation() {
			if hide {
				if me {
					animationScale = 0
					isHidden = true
				} else {
					removeFromParent()
				}
			} else {
				animationScale = 1
				setScale(1)
				isHidden = false
			}
		}
	}
}
