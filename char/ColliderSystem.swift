import SpriteKit

class ColliderSystem {
	private(set) var up:[Collider] = []
	private(set) var down:[Collider] = []
	private(set) var left:[Collider] = []
	private(set) var right:[Collider] = []
	struct Collider {
		var value:CGFloat
		var hasMin:Bool
		var hasMax:Bool
		var min:CGFloat
		var max:CGFloat
		init(value:CGFloat,min:CGFloat?,max:CGFloat?) {
			self.value = value
			if let min = min {
				hasMin = true
				self.min = min
			} else {
				hasMin = false
				self.min = 0
			}
			if let max = max {
				hasMax = true
				self.max = max
			} else {
				hasMax = false
				self.max = 0
			}
		}
	}

	func add(whenGoing dir:Direction,at value:CGFloat,between min:CGFloat?,and max:CGFloat?) {
		switch dir {
		case .up: up.append(Collider(value:value,min:min,max:max))
		case .down: down.append(Collider(value:value,min:min,max:max))
		case .left: left.append(Collider(value:value,min:min,max:max))
		case .right: right.append(Collider(value:value,min:min,max:max))
		}
	}

	func add(whenGoing dir:Direction,at value:CGFloat) {
		add(whenGoing:dir,at:value,between:nil,and:nil)
	}

	func add(centre:CGPoint,size:CGSize) {
		let w = size.width*0.5
		let h = size.height*0.5
		let up = centre.y+h
		let down = centre.y-h
		let right = centre.x+w
		let left = centre.x-w
		add(whenGoing:.up,at:down,between:left,and:right)
		add(whenGoing:.down,at:up,between:left,and:right)
		add(whenGoing:.right,at:left,between:down,and:up)
		add(whenGoing:.left,at:right,between:down,and:up)
	}

	func add(node:SKSpriteNode,below:CGFloat,sides:CGFloat = 1) {
		add(
			centre:CGPoint(x:node.position.x,y:node.position.y-node.size.height*(1-below)*0.5),
			size:CGSize(width:node.size.width*sides,height:node.size.height*below)
		)
	}

	func solve(from origin:CGPoint,to end:CGPoint,distance:CGFloat,halfWidth:CGFloat,halfHeight:CGFloat) -> (pos:CGPoint,deadEnd:Bool) {
		if origin ==~ end || distance <=~ 0 { return (origin,true) }
		var origin = origin
		var distance = distance
		var prevDistance:CGFloat?
		while true {
			let dx = end.x-origin.x
			let dy = end.y-origin.y
			let totalDistance = sqrt(dx*dx+dy*dy)
			var target:CGPoint
			if distance >=~ totalDistance {
				distance = totalDistance
				target = end
			} else {
				let multiplier = distance/totalDistance
				target = CGPoint(x:origin.x+dx*multiplier,y:origin.y+dy*multiplier)
			}
			var result = collide(from:origin,to:target,halfWidth:halfWidth,halfHeight:halfHeight)
			guard let collider = result.collider else {
				return (result.pos,false)
			}
			distance -= origin.distance(to:result.pos)
			if distance <=~ 0 {
				return (result.pos,false)
			}
			origin = result.pos
			let stop:Bool,deadEnd:Bool
			let nextPos:CGFloat
			if result.vertical {
				if end.x >~ origin.x {
					if collider.hasMax && collider.max <=~ origin.x+distance {
						nextPos = collider.max
						stop = false
					} else {
						nextPos = origin.x+distance
						stop = true
					}
					if nextPos <=~ end.x {
						target = CGPoint(x:nextPos,y:origin.y)
						deadEnd = false
					} else {
						target = CGPoint(x:end.x,y:origin.y)
						deadEnd = true
					}
				} else if end.x <~ origin.x {
					if collider.hasMin && collider.min >=~ origin.x-distance {
						nextPos = collider.min
						stop = false
					} else {
						nextPos = origin.x-distance
						stop = true
					}
					if nextPos >=~ end.x {
						target = CGPoint(x:nextPos,y:origin.y)
						deadEnd = false
					} else {
						target = CGPoint(x:end.x,y:origin.y)
						deadEnd = true
					}
				} else {
					return (origin,true)
				}
			} else {
				if end.y >~ origin.y {
					if collider.hasMax && collider.max <=~ origin.y+distance {
						nextPos = collider.max
						stop = false
					} else {
						nextPos = origin.y+distance
						stop = true
					}
					if nextPos <=~ end.y {
						target = CGPoint(x:origin.x,y:nextPos)
						deadEnd = false
					} else {
						target = CGPoint(x:origin.x,y:end.y)
						deadEnd = true
					}
				} else if end.y <~ origin.y {
					if collider.hasMin && collider.min >=~ origin.y-distance {
						nextPos = collider.min
						stop = false
					} else {
						nextPos = origin.y-distance
						stop = true
					}
					if nextPos >=~ end.y {
						target = CGPoint(x:origin.x,y:nextPos)
						deadEnd = false
					} else {
						target = CGPoint(x:origin.x,y:end.y)
						deadEnd = true
					}
				} else {
					return (origin,true)
				}
			}
			result = collide(from:origin,to:target,halfWidth:halfWidth,halfHeight:halfHeight)
			if stop || deadEnd || result.collider != nil {
				return (result.pos,deadEnd)
			}
			distance -= origin.distance(to:result.pos)
			if distance <=~ 0 {
				return (result.pos,false)
			}
			if let prev = prevDistance,prev ==~ distance {
				return (result.pos,true)
			}
			prevDistance = distance
			origin = result.pos
		}
	}

	private func collide(from:CGPoint,to:CGPoint,halfWidth:CGFloat,halfHeight:CGFloat) -> (pos:CGPoint,collider:Collider?,vertical:Bool) {
		var pos = to
		var collider:Collider?
		var vertical = false
		func collideX(col:Collider) -> Bool {
			if from.x ==~ col.value && (!col.hasMin || from.y >=~ col.min) && (!col.hasMax || from.y <=~ col.max) {
				if col.hasMin && from.y ==~ col.min && pos.y <=~ from.y { return false }
				if col.hasMax && from.y ==~ col.max && pos.y >=~ from.y { return false }
				return true
			}
			let y = col.value.lerpInv(from.x,pos.x).lerp(from.y,pos.y)
			if (!col.hasMin || col.min <=~ y) && (!col.hasMax || col.max >=~ y) {
				pos = CGPoint(x:col.value,y:y)
				collider = col
				vertical = false
			}
			return false
		}
		func collideY(col:Collider) -> Bool {
			if from.y ==~ col.value && (!col.hasMin || from.x >=~ col.min) && (!col.hasMax || from.x <=~ col.max) {
				if col.hasMin && from.x ==~ col.min && pos.x <=~ from.x { return false }
				if col.hasMax && from.x ==~ col.max && pos.x >=~ from.x { return false }
				return true
			}
			let x = col.value.lerpInv(from.y,pos.y).lerp(from.x,pos.x)
			if (!col.hasMin || col.min <=~ x) && (!col.hasMax || col.max >=~ x) {
				pos = CGPoint(x:x,y:col.value)
				collider = col
				vertical = true
			}
			return false
		}
		if pos.y-from.y >~ 0 {
			for col in up {
				var col = col
				col.value -= halfHeight
				col.min -= halfWidth
				col.max += halfWidth
				guard from.y <=~ col.value && pos.y >~ col.value else { continue }
				if collideY(col:col) { return (from,col,true) }
			}
		} else if pos.y-from.y <~ 0 {
			for col in down {
				var col = col
				col.value += halfHeight
				col.min -= halfWidth
				col.max += halfWidth
				guard from.y >=~ col.value && pos.y <~ col.value else { continue }
				if collideY(col:col) { return (from,col,true) }
			}
		}
		if pos.x-from.x >~ 0 {
			for col in right {
				var col = col
				col.value -= halfWidth
				col.min -= halfHeight
				col.max += halfHeight
				guard from.x <=~ col.value && pos.x >~ col.value else { continue }
				if collideX(col:col) { return (from,col,false) }
			}
		} else if pos.x-from.x <~ 0 {
			for col in left {
				var col = col
				col.value += halfWidth
				col.min -= halfHeight
				col.max += halfHeight
				guard from.x >=~ col.value && pos.x <~ col.value else { continue }
				if collideX(col:col) { return (from,col,false) }
			}
		}
		return (pos,collider,vertical)
	}
}
