import SpriteKit

class CharAreaServer:ParServer {
	var spawnMin:CGPoint = .zero
	var spawnMax:CGPoint = .zero
	private var spawnPos:CGPoint {
		return Random.range(spawnMin,spawnMax)
	}

	private var chars:[String:CharState] = [:]
	private var inactiveChars:[String:CGPoint] = [:]
	private enum CharState {
		case moving(CGPoint,CGPoint)
		case still(CGPoint)
	}

	init(id:String) {
		super.init(name:"charArea_\(id)")
	}

	override func started() {
		chars = [:]
		inactiveChars = [:]
	}

	override func stopped() {
		chars = [:]
		inactiveChars = [:]
	}

	override func connected(peer:String) {
		var json:[String:Any] = [:]
		for (peer,state) in chars {
			switch state {
			case .moving(let pos,let target): json[peer] = ["A":true,"P":pos.s,"T":target.s]
			case .still(let pos): json[peer] = ["A":true,"S":pos.s]
			}
		}
		if let data = Json.data(json) {
			send(data:data,to:peer,reliable:true)
		}
	}

	override func disconnected(peer:String) {
		if let state = chars[peer] {
			switch state {
			case .moving(let pos,_): inactiveChars[peer] = pos
			case .still(let pos): inactiveChars[peer] = pos
			}
			chars[peer] = nil
			if let data = Json.data([peer:["A":false]]) {
				send(data:data,to:peers,reliable:true)
			}
		}
	}

	override func received(data:Data,from peer:String) {
		guard let json = Json.parse(data) else { return }
		if let newSpawn = json["A"] as? Bool {
			if newSpawn {
				let pos = spawnPos
				let spawn = chars[peer] == nil
				chars[peer] = .still(pos)
				if spawn {
					inactiveChars[peer] = nil
				} else if let peers = peers(excluding:peer),let data = Json.data([peer:["S":pos.s]]) {
					send(data:data,to:peers,reliable:false)
				}
				if let data = Json.data([peer:["A":true,"S":pos.s]]) {
					if spawn {
						send(data:data,to:peers,reliable:true)
					} else {
						send(data:data,to:peer,reliable:true)
					}
				}
			} else {
				if let state = chars[peer] {
					let json:[String:Any]
					switch state {
					case .moving(let pos,let target): json = [peer:["A":true,"P":pos.s,"T":target.s]]
					case .still(let pos): json = [peer:["A":true,"S":pos.s]]
					}
					if let data = Json.data(json) {
						send(data:data,to:peer,reliable:true)
					}
				} else {
					let pos:CGPoint
					if let i = inactiveChars[peer] {
						inactiveChars[peer] = nil
						pos = i
					} else {
						pos = spawnPos
					}
					chars[peer] = .still(pos)
					if let data = Json.data([peer:["A":true,"S":pos.s]]) {
						send(data:data,to:peers,reliable:true)
					}
				}
			}
		} else if let pos = CGPoint.s(json["S"]) {
			chars[peer] = .still(pos)
			if let peers = peers(excluding:peer),let data = Json.data([peer:["S":pos.s]]) {
				send(data:data,to:peers,reliable:false)
			}
		} else if let pos = CGPoint.s(json["P"]),let target = CGPoint.s(json["T"]) {
			chars[peer] = .moving(pos,target)
			if let peers = peers(excluding:peer),let data = Json.data([peer:["P":pos.s,"T":target.s]]) {
				send(data:data,to:peers,reliable:false)
			}
		}
	}
}
