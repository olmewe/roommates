import SpriteKit

class CharAreaField:ParField {
	var node:CharAreaNode?

	private var timer:Timer?
	private var lastPos:CGPoint?
	private var lastTarget:CGPoint?
	private var repeatCount = 0
	private let repeatLimit = 8

	init(id:String) {
		super.init(name:"charArea_\(id)")
	}

	override func started() {
		timer = Timer.scheduledTimer(timeInterval:1/16,target:self,selector:#selector(sendPos),userInfo:nil,repeats:true)
		lastPos = nil
		lastTarget = nil
		repeatCount = 0
	}

	override func stopped() {
		timer?.invalidate()
		timer = nil
		lastPos = nil
		lastTarget = nil
	}

	@objc private func sendPos() {
		guard !peers.isEmpty,let char = node?.myChar else { return }
		func send() {
			repeatCount = 0
			lastPos = char.position
			lastTarget = char.target
			guard let data = posData() else { return }
			self.send(data:data,to:peers,reliable:false)
		}
		if let lastTarget = lastTarget,let lastPos = lastPos {
			if lastTarget !=~ char.target || lastPos !=~ char.position {
				send()
			} else {
				repeatCount += 1
				if repeatCount >= repeatLimit {
					send()
				}
			}
		} else {
			send()
		}
	}

	override func connected(peer:String) {
		guard let data = posData() else { return }
		send(data:data,to:peer,reliable:true)
	}

	override func disconnected(peer:String) {
		node?.hide(peer:peer)
	}

	override func received(data:Data,from peer:String) {
		guard let node = node else { return }
		guard let json = Json.parse(data) else { return }
		if let pos = CGPoint.s(json["S"]) {
			node.show(peer:peer,pos:pos)
		} else if let pos = CGPoint.s(json["P"]),let target = CGPoint.s(json["T"]) {
			node.show(peer:peer,pos:pos,target:target)
		}
	}

	private func posData() -> Data? {
		guard let char = node?.myChar else { return nil }
		if char.moving {
			return Json.data(["P":char.position.s,"T":char.target.s])
		}
		return Json.data(["S":char.target.s])
	}
}
