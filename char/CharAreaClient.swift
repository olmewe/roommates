import SpriteKit

class CharAreaClient:ParClient {
	var node:CharAreaNode?
	var firstSpawn = true

	private(set) var spawned = false
	private var timer:Timer?
	private var lastPos:CGPoint?
	private var lastTarget:CGPoint?
	private var repeatCount = 0
	private let repeatLimit = 8

	init(id:String) {
		super.init(name:"charArea_\(id)")
	}

	override func started() {
		//
	}

	override func stopped() {
		//
	}

	override func connected() {
		spawned = false
		timer = Timer.scheduledTimer(timeInterval:1/16,target:self,selector:#selector(sendPos),userInfo:nil,repeats:true)
		lastPos = nil
		lastTarget = nil
		repeatCount = 0
		if let data = Json.data(["A":firstSpawn]) {
			send(data:data,reliable:true)
		}
	}

	override func disconnected() {
		spawned = false
		timer?.invalidate()
		timer = nil
		lastPos = nil
		lastTarget = nil
		node?.hideAll()
	}

	@objc private func sendPos() {
		guard spawned,let char = node?.myChar else { return }
		func send() {
			repeatCount = 0
			lastPos = char.position
			lastTarget = char.target
			guard let data = posData() else { return }
			self.send(data:data,reliable:false)
		}
		if let lastTarget = lastTarget,let lastPos = lastPos {
			if lastTarget !=~ char.target || lastPos !=~ char.position {
				send()
			} else {
				repeatCount += 1
				if repeatCount >= repeatLimit {
					send()
				}
			}
		} else {
			send()
		}
	}

	override func received(data:Data) {
		guard let node = node else { return }
		guard let json = Json.parse(data) as? [String:[String:Any]] else { return }
		for (peer,json) in json {
			let flag = json["A"] as? Bool
			let spawn = flag == true
			let hide = flag == false
			if peer == GameData.id {
				if !spawned && spawn {
					spawned = true
					firstSpawn = false
				}
				if spawned {
					if let pos = CGPoint.s(json["S"]) {
						node.show(pos:pos)
					} else if let pos = CGPoint.s(json["P"]),let target = CGPoint.s(json["T"]) {
						node.show(pos:pos,target:target)
					}
				}
			} else if hide {
				node.hide(peer:peer)
			} else if let pos = CGPoint.s(json["S"]) {
				node.show(peer:peer,pos:pos,autospawn:spawn)
			} else if let pos = CGPoint.s(json["P"]),let target = CGPoint.s(json["T"]) {
				node.show(peer:peer,pos:pos,target:target,autospawn:spawn)
			}
		}
	}

	private func posData() -> Data? {
		guard let char = node?.myChar else { return nil }
		if char.moving {
			return Json.data(["P":char.position.s,"T":char.target.s])
		}
		return Json.data(["S":char.target.s])
	}
}
