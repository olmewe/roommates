import SpriteKit

class StoreScene:Scene {
	static var startTransition:TransitionNode.Transition?
	var endTransition:TransitionNode?

	let charAreaNode = CharAreaNode()
	let charAreaField = CharAreaField(id:"store")

	let cashierNode = SKSpriteNode()
	let doorNode = SKSpriteNode()
	let shelfHouseNode = SKSpriteNode()
	let shelfGamesNode = SKSpriteNode()
	let shelfClothesNode = SKSpriteNode()

	let selector = ButtonSelector()
	private var touchID:Int?

	var cameraPan:CGFloat = 0
	var cameraLimits:CGFloat = 0
	var cameraIntro:CGFloat = 1

	var tutorialNode:TutorialNode!

	let contentSize = CGSize(width:720,height:1280)

	static var bgmStore:Audio?

	override func load() {
		if Game.heightRatio > 16/9 {
			size = CGSize(width:720,height:720*Game.heightRatio)
		} else {
			size = CGSize(width:1280*Game.widthRatio,height:1280)
		}
		backgroundColor = #colorLiteral(red: 0.1803921569, green: 0.1960784314, blue: 0.2509803922, alpha: 1)

		let atlas = SKTextureAtlas(named:"store_nav")

		let bgLeft = SKSpriteNode(texture:atlas.textureNamed("bg_left"),size:contentSize)
		addChild(bgLeft)
		bgLeft.position = CGPoint(x:-contentSize.width*0.5,y:0)
		bgLeft.zPosition = -1

		let bgRight = SKSpriteNode(texture:atlas.textureNamed("bg_right"),size:contentSize)
		addChild(bgRight)
		bgRight.position = CGPoint(x:contentSize.width*0.5,y:0)
		bgRight.zPosition = -1

		addChild(charAreaNode)
		charAreaNode.zPosition = 0
		charAreaNode.collider.add(whenGoing:.up,at:contentSize.height*0.25)
		charAreaNode.collider.add(whenGoing:.down,at:-contentSize.height*0.5)
		charAreaNode.collider.add(whenGoing:.left,at:-contentSize.width)
		charAreaNode.collider.add(whenGoing:.right,at:contentSize.width)
		charAreaNode.setZReferenceFromColliders()
		charAreaField.node = charAreaNode

		addChild(cashierNode)
		cashierNode.texture = atlas.textureNamed("cashier")
		cashierNode.size = CGSize(width:298,height:303)
		cashierNode.position = CGPoint(x:235,y:410)
		charAreaNode.z(node:cashierNode,below:0.5)
		charAreaNode.collider.add(node:cashierNode,below:0.5)

		addChild(doorNode)
		doorNode.position = CGPoint(x:600,y:430)
		doorNode.zPosition = 0
		doorNode.size = CGSize(width:250,height:230)

		addChild(shelfHouseNode)
		shelfHouseNode.texture = atlas.textureNamed("shelf_house")
		shelfHouseNode.size = CGSize(width:381,height:300)
		shelfHouseNode.position = CGPoint(x:-470,y:400)
		charAreaNode.z(node:shelfHouseNode,below:0.15)
		charAreaNode.collider.add(node:shelfHouseNode,below:0.15)

		addChild(shelfGamesNode)
		shelfGamesNode.texture = atlas.textureNamed("shelf_games")
		shelfGamesNode.size = CGSize(width:381,height:300)
		shelfGamesNode.position = CGPoint(x:-470,y:0)
		charAreaNode.z(node:shelfGamesNode,below:0.15)
		charAreaNode.collider.add(node:shelfGamesNode,below:0.15)

		addChild(shelfClothesNode)
		shelfClothesNode.texture = atlas.textureNamed("shelf_clothes")
		shelfClothesNode.size = CGSize(width:381,height:300)
		shelfClothesNode.position = CGPoint(x:-470,y:-400)
		charAreaNode.z(node:shelfClothesNode,below:0.15)
		charAreaNode.collider.add(node:shelfClothesNode,below:0.15)

		func addSoon(real:Bool,x:CGFloat,y:CGFloat,z:CGFloat) {
			let node = SKSpriteNode()
			addChild(node)
			node.texture = atlas.textureNamed(real ? "realsoon" : "soon")
			node.size = CGSize(width:153,height:153)
			node.position = CGPoint(x:x,y:y)
			node.zPosition = z+0.0001
		}
		addSoon(real:false,x:-484,y:422,z:shelfHouseNode.zPosition)
		addSoon(real:true,x:-398,y:22,z:shelfGamesNode.zPosition)
		addSoon(real:false,x:-548,y:-382,z:shelfClothesNode.zPosition)

		let cautionNode = SKSpriteNode()
		addChild(cautionNode)
		cautionNode.texture = atlas.textureNamed("caution")
		cautionNode.size = CGSize(width:200,height:165)
		cautionNode.position = CGPoint(x:530,y:-125)
		charAreaNode.z(node:cautionNode,below:0.25)
		charAreaNode.collider.add(node:cautionNode,below:0.25)

		func addCone(x:CGFloat,y:CGFloat) {
			let node = SKSpriteNode()
			addChild(node)
			node.texture = atlas.textureNamed("cone")
			node.size = CGSize(width:143,height:143)
			node.position = CGPoint(x:x,y:y)
			charAreaNode.z(node:node,below:0.3)
			charAreaNode.collider.add(node:node,below:0.3)
		}
		addCone(x:410,y:295)
		addCone(x:95,y:-380)
		addCone(x:170,y:-405)
		addCone(x:600,y:-515)
		addCone(x:-250,y:230)
		addCone(x:-235,y:170)
		addCone(x:-300,y:100)
		addCone(x:-250,y:-210)
		addCone(x:-260,y:-280)
		addCone(x:-260,y:-360)
		addCone(x:-210,y:-430)

		tutorialNode = TutorialNode(size:size,showIcon:true)
		cam.addChild(tutorialNode)

		selector.nodes = [doorNode,cashierNode,tutorialNode.icon]

		cameraLimits = contentSize.width*0.5
	}

	override func show() {
		cameraPan = 0
		cameraIntro = 1

		let spawnMin = CGPoint(x:contentSize.width*0.78,y:contentSize.height*0.18)
		let spawnMax = CGPoint(x:contentSize.width*0.82,y:contentSize.height*0.2)
		charAreaNode.show(pos:Random.range(spawnMin,spawnMax))
		GameState.par.start(field:charAreaField)

		let bgm = Audio("app/store.mp3",volume:1,fade:1)
		StoreScene.bgmStore = bgm
		bgm.onStop = {
			StoreScene.bgmStore = nil
		}

		if let transition = StoreScene.startTransition {
			_ = TransitionNode.make(transition:transition,duration:0.5)
		}
	}

	override func hide() {
		StoreScene.bgmStore?.fadeOut(for:1)
	}

	override func update() {
		let pan:CGFloat
		if charAreaNode.myChar.position.x > 0 {
			pan = (1-charAreaNode.myChar.position.x*1.5/contentSize.width).easeIn*0.175
		} else {
			pan = 1-(1+charAreaNode.myChar.position.x*4/contentSize.width).easeIn*0.175
		}
		if cameraIntro > 0 {
			cameraIntro -= Time.deltaTime*0.4
			if cameraIntro <= 0 {
				cameraIntro = 0
			}
			cameraPan = cameraIntro.ease.lerp(pan,1)
		} else {
			cameraPan = (Time.deltaTime*3).lerp(cameraPan,pan)
		}
		cam.position.x = cameraPan.lerp(cameraLimits,-cameraLimits)

		if cameraIntro <=~ 0 {
			var charAreaTarget:CGPoint?
			for i in Touch.events {
				if touchID == nil && i.state == .down {
					touchID = i.id
					selector.select(at:i.position)
					if selector.selected == nil {
						charAreaTarget = i.position
					}
				}
				if touchID == i.id {
					if selector.selected == nil {
						charAreaTarget = i.position
					}
					if i.state == .miss {
						touchID = nil
					} else if i.state == .up {
						touchID = nil
						if let node = selector.finalSelect(at:i.position) {
							press(node:node)
						}
					}
				}
			}
			if let target = charAreaTarget {
				charAreaNode.show(target:target)
			}
		}

		if endTransition == nil && !GameState.atStore {
			endTransition = TransitionNode.make(transition:GameState.inMenu ? .verticalClose : .slideFromRight,duration:0.5,completion:sceneEnd)
		}
	}

	func sceneEnd() {
		endTransition = nil
		if GameState.inMenu {
			MenuScene.startTransition = .verticalOpen
			Scene.present(MenuScene())
		} else if GameState.houseConnected {
			HomeScene.startTransition = .verticalOpen
			Scene.present(HomeScene())
		} else {
			_ = TransitionNode.make(transition:.verticalOpen,duration:0.5)
		}
	}

	func press(node i:SKNode) {
		switch i {
		case doorNode: GameState.goToMenu()
		case shelfGamesNode: UI.push(UI.load("GameShelf"))
		case cashierNode,tutorialNode.icon: openTutorial()
		default: break
		}
	}

	func openTutorial() {
		if tutorialNode.list == nil {
			tutorialNode.list = TutorialList(parse:"""
			[bg:salesman] [sp:idle]
			[sc:store_salesman] Hi! No need to call my brother while in here. \\ \\ I, the SALESMAN, can show you around!
			[sc:store_money] As you can see, the store is still under construction. \\ \\ Selling stuff sure is hard work!
			[sc:store_bros] That's right, my brother creates the games and I sell them. \\ \\ It's a family business.
			[sc:store_construction] Come back when the cones are gone! \\ \\ Thank you for your visit.
			[sc:store_love] Love ya...
			""")
		}
		tutorialNode.open()
	}
}
