import SpriteKit

class StarNode:SKSpriteNode,SceneNode {
	let frames:[SKTexture]

	init() {
		let atlas = SKTextureAtlas(named:"star")
		frames = [atlas.textureNamed("star0"),atlas.textureNamed("star1"),atlas.textureNamed("star2")]
		super.init(texture:frames[0],color:SKColor.white,size:CGSize(width:18,height:18))
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	func update() {
		texture = frames[Int(Time.time*4) % frames.count]
	}
}
