import SpriteKit

class MenuPeerNode:SKSpriteNode,SceneNode {
	let nameNode:CharNameNode
	private var animationScale:CGFloat = 0
	var posIndex:Int = 0
	var hide = false

	init(tex:SKTexture?,name:String,char:String) {
		nameNode = CharNameNode(name:name,char:char)
		super.init(texture:tex,color:SKColor.clear,size:CGSize(width:140,height:140))
		addChild(nameNode)
		nameNode.position = CGPoint(x:0,y:-100)
		nameNode.setScale(0.75)
		setScale(0)
	}

	required init?(coder aDecoder: NSCoder) {
		return nil
	}

	func endAnimation() {
		if hide {
			if parent != nil {
				removeFromParent()
			}
		} else {
			animationScale = 1
			setScale(1)
		}
	}

	func update() {
		if hide {
			if animationScale > 0 {
				animationScale -= Time.deltaTime*3
				if animationScale <= 0 {
					removeFromParent()
				} else {
					setScale(animationScale.easeOut)
				}
			}
		} else {
			if animationScale < 1 {
				animationScale += Time.deltaTime*3
				if animationScale >= 1 {
					animationScale = 1
					setScale(1)
				} else {
					setScale(animationScale.easeOut)
				}
			}
		}
	}
}
