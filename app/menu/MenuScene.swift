import SpriteKit

class MenuScene:Scene {
	static var startTransition:TransitionNode.Transition?
	var endTransition:TransitionNode?

	let scrollNode = SKNode()
	let mountainsNode = SKSpriteNode()
	var homeNode:MenuPeerNode!
	let storeNode = SKSpriteNode()
	let storeNameNode = CharNameNode(name:"store")
	let sunNode = SKSpriteNode()
	var peerNodes:[String:MenuPeerNode] = [:]

	var selectedPeer:String?
	var zoomLevel:CGFloat = 0
	var zoomAt:CGPoint?
	var zoomingIn = false
	let houseTransition:CGFloat = 0.5
	let houseZoom:CGFloat = 0.5

	var houseView:HouseViewController?
	let smokeNode = SKNode()
	let smokeSpriteNode = SKSpriteNode()
	var overlayAlpha:CGFloat = 0

	var tutorialNode:TutorialNode!

	var charNode:CharNode!
	var charHeight:CGFloat = 0

	let selector = ButtonSelector()

	var touchID:Int?
	var scroll:CGFloat = 0
	var scrollVel:CGFloat = 0
	var contentWidth:CGFloat = 0

	var reload = false

	var phoneTextures:[SKTexture] = []
	var peerTextures:[SKTexture] = []
	var smokeTextures:[SKTexture] = []

	let peerPos = [CGPoint](arrayLiteral:
		CGPoint(x:180,y:530),
		CGPoint(x:200,y:800),
		CGPoint(x:288,y:440),
		CGPoint(x:340,y:768),
		CGPoint(x:465,y:875),
		CGPoint(x:488,y:700),
		CGPoint(x:590,y:830),
		CGPoint(x:610,y:655),
		CGPoint(x:660,y:360),
		CGPoint(x:730,y:810),
		CGPoint(x:750,y:615),
		CGPoint(x:780,y:400),
		CGPoint(x:860,y:750),
		CGPoint(x:890,y:595),
		CGPoint(x:890,y:330),
		CGPoint(x:990,y:810),
		CGPoint(x:1040,y:595),
		CGPoint(x:1130,y:865),
		CGPoint(x:1170,y:320),
		CGPoint(x:1180,y:540),
		CGPoint(x:1270,y:880),
		CGPoint(x:1285,y:440),
		CGPoint(x:1360,y:570),
		CGPoint(x:1400,y:830),
		CGPoint(x:1505,y:605),
		CGPoint(x:1540,y:800),
		CGPoint(x:1550,y:325),
		CGPoint(x:1640,y:540),
		CGPoint(x:1685,y:785),
		CGPoint(x:1715,y:360),
		CGPoint(x:1825,y:765),
		CGPoint(x:1845,y:430),
		CGPoint(x:1985,y:785),
		CGPoint(x:1992,y:440)
	).sorted { $0.x < $1.x }
	var peerPosAvailable:[Int] = []

	var sfxHouseShow:Data?
	var sfxHouseHide:Data?
	var sfxHouseZoomIn:Data?
	var sfxHouseZoomOut:Data?
	static var bgmMenu:Audio?

	override func load() {
		if Game.heightRatio > 16/9 {
			size = CGSize(width:720,height:720*Game.heightRatio)
		} else {
			size = CGSize(width:1280*Game.widthRatio,height:1280)
		}
		backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

		let atlas = SKTextureAtlas(named:"menu_nav")
		peerTextures = (0...2).map { atlas.textureNamed("peer\($0)") }
		smokeTextures = (0...2).map { atlas.textureNamed("smoke\($0)") }

		addChild(scrollNode)

		scrollNode.addChild(mountainsNode)
		mountainsNode.texture = atlas.textureNamed("mountains")
		mountainsNode.size = CGSize(width:2172,height:860)
		mountainsNode.anchorPoint = CGPoint(x:0,y:0)
		mountainsNode.position = CGPoint(x:-size.width*0.5,y:-size.height*0.5)
		mountainsNode.zPosition = -1
		contentWidth = mountainsNode.size.width-size.width

		homeNode = MenuPeerNode(tex:peerTexture(for:GameData.id),name:GameData.peerInfo.name,char:GameData.peerInfo.char)
		scrollNode.addChild(homeNode)
		homeNode.position = housePos(x:430,y:380)
		homeNode.zPosition = z(for:homeNode)
		homeNode.endAnimation()

		scrollNode.addChild(storeNode)
		storeNode.texture = atlas.textureNamed("store")
		storeNode.size = CGSize(width:233,height:170)
		storeNode.position = housePos(x:180,y:200)
		storeNode.zPosition = z(for:storeNode)
		storeNode.addChild(storeNameNode)
		storeNameNode.position = CGPoint(x:0,y:-110)
		storeNameNode.setScale(0.75)

		tutorialNode = TutorialNode(size:size,showIcon:true)
		addChild(tutorialNode)

		addChild(sunNode)
		sunNode.texture = atlas.textureNamed("sun")
		sunNode.size = CGSize(width:47,height:47)
		sunNode.position = CGPoint(x:0,y:size.height*0.425)

		smokeNode.zPosition = 50
		smokeNode.addChild(smokeSpriteNode)
		smokeSpriteNode.size = CGSize(width:size.width,height:size.width*4/9)
		smokeSpriteNode.position = .zero
		smokeSpriteNode.anchorPoint = CGPoint(x:0.5,y:0)
		let smokeBgNode = SKSpriteNode(color:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),size:CGSize(width:size.width,height:size.height))
		smokeNode.addChild(smokeBgNode)
		smokeBgNode.position = CGPoint(x:0,y:smokeSpriteNode.size.height)
		smokeBgNode.anchorPoint = CGPoint(x:0.5,y:0)
		smokeBgNode.zPosition = -0.1

		let charHeightRatio = Game.heightRatio.lerpInv(4/3,16/9)
		charNode = CharNode(scale:charHeightRatio.lerp(330,400))
		charNode.zPosition = 50
		charNode.anchorPoint = CGPoint(x:0.5,y:1)
		charHeight = charHeightRatio.lerp(-345,-260)

		selector.nodes = [homeNode,storeNode,tutorialNode.icon]

		sfxHouseShow = Audio.data(of:"app/house_show.wav")
		sfxHouseHide = Audio.data(of:"app/house_hide.wav")
		sfxHouseZoomIn = Audio.data(of:"app/house_zoomin.wav")
		sfxHouseZoomOut = Audio.data(of:"app/house_zoomout.wav")
	}

	override func show() {
		scroll = 0
		scrollVel = 0
		scrollNode.position.x = 0

		selectedPeer = nil
		houseView = nil
		peerNodes = [:]
		peerPosAvailable = Array(0..<peerPos.count)
		GameState.sessionBrowseEvent = { peer,info,start in self.update(session:peer,to:info,animate:!start) }

		charNode.set(char:GameData.peerInfo.char)
		charNode.state = .think

		let bgm = Audio("app/menu.mp3")
		MenuScene.bgmMenu = bgm
		bgm.onStop = {
			MenuScene.bgmMenu = nil
		}

		if let transition = MenuScene.startTransition {
			_ = TransitionNode.make(transition:transition,duration:0.5)
		}
	}

	override func hide() {
		for (_,node) in peerNodes {
			node.removeFromParent()
			selector.remove(node)
		}
		peerNodes.removeAll()
		houseView = nil

		MenuScene.bgmMenu?.fadeOut(for:1)
	}

	override func update() {
		var addScroll:CGFloat = 0
		if zoomLevel <=~ 0 {
			for i in Touch.events {
				if touchID == nil && i.state == .down {
					touchID = i.id
					scrollVel = 0
					selector.select(at:i.position)
				}
				if touchID == i.id {
					var last = i.lastPosition.x
					if i.state == .move && selector.selected != nil && abs(i.position.x-i.startPosition.x) > size.width*0.02 {
						selector.cancel()
						last = i.startPosition.x
					}
					if selector.selected == nil {
						addScroll -= i.position.x-last
					}
					if i.state == .miss {
						touchID = nil
					} else if i.state == .up {
						touchID = nil
						if let node = selector.finalSelect(at:i.position) {
							press(node:node)
						} else {
							scrollVel = -i.velocity.x
						}
					}
				}
			}
		} else {
			touchID = nil
		}
		if abs(scrollVel) > 0 {
			addScroll += scrollVel*Time.deltaTime
			scrollVel = (Time.deltaTime*4).lerp(scrollVel,0)
			if abs(scrollVel) <= 0.01 {
				scrollVel = 0
			}
		}
		if addScroll > 0 {
			if scroll < contentWidth {
				scroll += addScroll
				if scroll > contentWidth {
					scroll = contentWidth
				}
				scrollNode.position.x = -scroll
			}
		} else if addScroll < 0 {
			if scroll > 0 {
				scroll += addScroll
				if scroll < 0 {
					scroll = 0
				}
				scrollNode.position.x = -scroll
			}
		}

		if endTransition != nil {
			selectedPeer = nil
		} else if let house = GameState.house {
			selectedPeer = house
		} else if let peer = selectedPeer {
			if GameState.par.sessions[peer] == nil {
				selectedPeer = nil
			}
		}

		if selectedPeer != nil {
			if !zoomingIn {
				zoomingIn = true
				Audio.play(sfxHouseZoomIn)
			}
			if zoomLevel < 1 {
				zoomLevel += Time.deltaTime/houseTransition
				if zoomLevel > 1 {
					zoomLevel = 1
				}
				updateZoom()
			}
		} else {
			if zoomingIn {
				zoomingIn = false
				Audio.play(sfxHouseZoomOut)
			}
			if zoomLevel > 0 {
				zoomLevel -= Time.deltaTime/houseTransition
				if zoomLevel < 0 {
					zoomLevel = 0
				}
				updateZoom()
			}
		}

		if zoomLevel >=~ 1 {
			if overlayAlpha < 1 {
				overlayAlpha += Time.deltaTime*5
				if overlayAlpha > 1 {
					overlayAlpha = 1
				}
				updateAlpha()
			}
		} else {
			if overlayAlpha > 0 {
				overlayAlpha -= Time.deltaTime*16
				if overlayAlpha < 0 {
					overlayAlpha = 0
				}
				updateAlpha()
			}
		}

		if smokeNode.parent != nil {
			smokeSpriteNode.texture = smokeTextures[Int(Time.time*6)%smokeTextures.count]
		}

		if endTransition == nil && !GameState.inMenu {
			endTransition = TransitionNode.make(transition:GameState.atStore ? .verticalClose : .slideFromRight,duration:0.5,completion:sceneEnd)
		}
	}

	func sceneEnd() {
		endTransition = nil
		if GameState.atStore {
			StoreScene.startTransition = .verticalOpen
			Scene.present(StoreScene())
		} else if GameState.houseConnected {
			HomeScene.startTransition = .slideToLeft
			Scene.present(HomeScene())
		} else {
			selectedPeer = nil
			_ = TransitionNode.make(transition:.slideToLeft,duration:0.5)
		}
	}

	func updateZoom() {
		if zoomLevel <=~ 0 {
			zoomAt = nil
			cam.position = .zero
			cam.setScale(1)
			if charNode.parent != nil {
				charNode.removeFromParent()
			}
		} else {
			let zoom:CGPoint
			if let z = zoomAt {
				zoom = z
			} else if let peer = selectedPeer,let z = peerNodes[peer]?.position {
				zoomAt = z
				zoom = z
			} else {
				zoom = .zero
			}
			let toPos = CGPoint(x:zoom.x+scrollNode.position.x,y:zoom.y+scrollNode.position.y)
			let t = zoomLevel.ease
			cam.position = t.lerp(CGPoint.zero,toPos)
			cam.setScale(t.lerp(1,houseZoom))
			if charNode.parent == nil {
				cam.addChild(charNode)
			}
			charNode.position = CGPoint(x:0,y:t.easeOut.lerp(-0.5*size.height,charHeight))
		}
		for node in (peerNodes.values.map { $0.nameNode }+[storeNameNode,homeNode.nameNode]) {
			node.alpha = 1-zoomLevel.ease
		}
	}

	func updateAlpha() {
		if overlayAlpha <=~ 0 {
			if smokeNode.parent != nil {
				smokeNode.removeFromParent()
			}
			if houseView != nil {
				houseView = nil
				UI.popAll(animated:false)
			}
		} else {
			if smokeNode.parent == nil {
				cam.addChild(smokeNode)
			}
			if houseView == nil,let peer = selectedPeer,let info = GameState.par.sessions[peer],let view = UI.load("houseView") as? HouseViewController {
				houseView = view
				view.close = { self.houseViewAction(confirmed:$0) }
				view.updateText(with:info)
				UI.push(view,animated:false)
			}
			let t = overlayAlpha.ease
			smokeNode.alpha = t
			if let view = houseView {
				view.view.alpha = t
				view.connecting = GameState.house != nil
			}
		}
	}

	func update(session peer:String,to info:[String:Any]?,animate:Bool) {
		if let node = peerNodes[peer] {
			if let info = info {
				if let p = selectedPeer,p == peer {
					houseView?.updateText(with:info)
				}
			} else {
				node.hide = true
				setPeerPosIndex(node.posIndex)
				selector.remove(node)
				peerNodes[peer] = nil
				if animate {
					Audio.play(sfxHouseHide)
				} else {
					node.endAnimation()
				}
			}
		} else if let info = info,let index = getPeerPosIndex() {
			let peerInfo = PeerInfo.s(info["peer"],mode:.tolerant) ?? PeerInfo()
			let node = MenuPeerNode(tex:peerTexture(for:peer),name:peerInfo.name,char:peerInfo.char)
			node.position = housePos(peerPos[index])
			node.zPosition = z(for:node)
			node.posIndex = index
			node.nameNode.alpha = 1-zoomLevel.ease
			scrollNode.addChild(node)
			selector.add(node)
			peerNodes[peer] = node
			if animate {
				Audio.play(sfxHouseShow)
			} else {
				node.endAnimation()
			}
		}
	}

	func press(node i:SKNode) {
		switch i {
		case homeNode: GameState.goHome()
		case storeNode: GameState.goToStore()
		case tutorialNode.icon: openTutorial()
		default:
			guard houseView == nil && UI.stack.isEmpty else { break }
			for (peer,node) in peerNodes where i == node {
				selectedPeer = peer
				break
			}
		}
	}

	func houseViewAction(confirmed:Bool) {
		guard GameState.inMenu,let peer = selectedPeer else { return }
		if confirmed {
			if GameState.houseConnecting {
				GameState.cancelVisit()
			} else {
				GameState.visit(peer:peer)
			}
		} else {
			if GameState.houseConnecting {
				GameState.cancelVisit()
			}
			selectedPeer = nil
		}
	}

	func getPeerPosIndex() -> Int? {
		guard !peerPosAvailable.isEmpty else { return nil }
		let availableIndex = Random.range(min(5,peerPosAvailable.count))
		let index = peerPosAvailable[availableIndex]
		peerPosAvailable.remove(at:availableIndex)
		return index
	}

	func setPeerPosIndex(_ index:Int) {
		if !peerPosAvailable.isEmpty {
			if let i = (0..<peerPosAvailable.count).filter({ peerPosAvailable[$0] > index }).first {
				peerPosAvailable.insert(index,at:i)
			}
		}
		peerPosAvailable.append(index)
	}

	func peerTexture(for peer:String) -> SKTexture {
		var i = peer.hashValue%peerTextures.count
		while i < 0 {
			i += peerTextures.count
		}
		return peerTextures[i]
	}

	func housePos(x:CGFloat,y:CGFloat) -> CGPoint {
		return CGPoint(x:x+mountainsNode.position.x,y:y+mountainsNode.position.y)
	}

	func housePos(_ p:CGPoint) -> CGPoint {
		return housePos(x:p.x,y:p.y)
	}

	func z(for node:SKNode) -> CGFloat {
		return node.position.y.lerpInv(size.height*0.5,-size.height*0.5).lerp(0.2,0.8)
	}

	func openTutorial() {
		if tutorialNode.list == nil {
			tutorialNode.list = TutorialList(parse:"""
			[bg:creator]
			[sc:menu_overview] [sp:shock] This is the world! \\ \\ Houses will pop on the mountains, revealing nearby friends who are available to play.
			[sc:menu_house] [sp:idle] That house in the corner, that's your house! \\ \\ By entering it, you can see your games and let others play with you.
			[sc:menu_houses] [sp:idle] Those are nearby players. \\ \\ For the best experience, you and other players should turn on both Wi-Fi and Bluetooth.
			[sc:menu_store] [sp:shock] And although it's under construction, you can already visit the store there to see how it's going.
			[sc:menu_touch] [sp:idle] That's it! \\ \\ Go visit a house and play some games.
			""")
		}
		tutorialNode.open()
	}
}
