import SpriteKit

class HomeScene:Scene {
	static var startTransition:TransitionNode.Transition?
	var endTransition:TransitionNode?

	let charAreaNode = CharAreaNode()
	let charAreaClient = CharAreaClient(id:"home")

	let bgNode = SKSpriteNode()
	let staticNode = SKSpriteNode()

	let windowNode = SKSpriteNode()
	let tvNode = SKSpriteNode()
	let phoneNode = SKSpriteNode()
	let ringNode = SKSpriteNode()
	let sofaNode = HomeSofaNode()
	let tvContentNode = SKSpriteNode()

	let doorNode = SKSpriteNode()

	let selector = ButtonSelector()
	private var touchID:Int?

	var consoleView:ConsoleGameListViewController?
	var tutorialNode:TutorialNode!

	var loadingHouse = false
	var loadingNode:SKSpriteNode!
	var currentGame:String? = nil

	var windowOpen = false
	var windowTempo:CGFloat = 0
	var windowTextureIndex:Int = 0
	var windowTextures:[SKTexture] = []

	var staticTempo:CGFloat = 0
	var staticForever = false
	var staticTextures:[SKTexture] = []
	let staticDuration:CGFloat = 0.3

	var phoneRing = false
	var phoneIndex:Int = -1
	var phoneTextures:[SKTexture] = []

	var sfxCurtainOpen:Data?
	var sfxCurtainClose:Data?
	var sfxStatic:Data?
	var bgmStatic:Audio?

	override func load() {
		let wideSize:CGSize
		if Game.heightRatio > 16/9 {
			size = CGSize(width:720,height:720*Game.heightRatio)
			wideSize = CGSize(width:size.height*9/16,height:size.height)
		} else {
			size = CGSize(width:1280*Game.widthRatio,height:1280)
			wideSize = CGSize(width:size.width,height:size.width*16/9)
		}
		backgroundColor = #colorLiteral(red: 0.1803921569, green: 0.1960784314, blue: 0.2509803922, alpha: 1)

		let atlas = SKTextureAtlas(named:"home_nav")

		addChild(bgNode)
		bgNode.size = CGSize(width:720,height:1280)
		bgNode.position = .zero
		bgNode.zPosition = -1

		staticNode.size = wideSize
		staticNode.position = .zero
		staticNode.zPosition = 100

		addChild(charAreaNode)
		charAreaNode.zPosition = 0
		charAreaClient.node = charAreaNode
		charAreaNode.collider.add(whenGoing:.up,at:320)
		charAreaNode.collider.add(whenGoing:.down,at:-640)
		charAreaNode.collider.add(whenGoing:.left,at:-360)
		charAreaNode.collider.add(whenGoing:.right,at:360)
		charAreaNode.setZReferenceFromColliders()

		addChild(windowNode)
		windowNode.size = CGSize(width:186,height:158)
		windowNode.position = CGPoint(x:-140,y:450)
		windowNode.zPosition = 0

		addChild(tvNode)
		tvNode.texture = atlas.textureNamed("tv")
		tvNode.size = CGSize(width:188,height:163)
		tvNode.position = CGPoint(x:-80,y:-215)
		tvNode.zPosition = charAreaNode.z(for:tvNode.position.y-tvNode.size.height*0.4)
		charAreaNode.collider.add(node:tvNode,below:0.5)

		addChild(phoneNode)
		phoneNode.size = CGSize(width:156,height:156)
		phoneNode.position = CGPoint(x:15,y:345)
		phoneNode.zPosition = 0
		charAreaNode.collider.add(node:phoneNode,below:1)

		addChild(ringNode)
		ringNode.texture = atlas.textureNamed("ring")
		ringNode.size = CGSize(width:156,height:38)
		ringNode.position = CGPoint(x:15,y:450)
		ringNode.zPosition = 0
		ringNode.isHidden = true

		addChild(tvContentNode)
		tvContentNode.size = CGSize(width:125,height:125)
		tvContentNode.position = CGPoint(x:-83,y:-222)
		tvContentNode.zPosition = tvNode.zPosition-0.00001
		tvContentNode.color = #colorLiteral(red: 1, green: 0.8, blue: 0.8, alpha: 1)

		addChild(sofaNode)
		sofaNode.position = CGPoint(x:0,y:-500)
		sofaNode.zPosition = 2
		charAreaNode.collider.add(node:sofaNode,below:0.5)

		addChild(doorNode)
		doorNode.size = CGSize(width:150,height:200)
		doorNode.position = CGPoint(x:200,y:410)
		doorNode.zPosition = 0

		tutorialNode = TutorialNode(size:size,showIcon:false)
		cam.addChild(tutorialNode)

		selector.nodes = [doorNode,windowNode,tvNode,phoneNode,sofaNode]

		let staticAtlas = SKTextureAtlas(named:"static")
		staticTextures = staticAtlas.textureNames.map {
			let tex = staticAtlas.textureNamed($0)
			tex.filteringMode = .nearest
			return tex
		}

		phoneTextures = (0...2).map { atlas.textureNamed("phone\($0)") }

		sfxCurtainOpen = Audio.data(of:"app/curtain_open.wav")
		sfxCurtainClose = Audio.data(of:"app/curtain_close.wav")
		sfxStatic = Audio.data(of:"app/static.wav")
	}

	override func show() {
		currentGame = GameState.gameRunning ? GameState.gameSelected : nil

		setTvTexture()
		if GameState.loadingHouse {
			apply(theme:HomeInfo.themes[Random.range(HomeInfo.themes.count)])
			loadingHouse = true
			loadingNode = SKSpriteNode()
			loadingNode.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.75)
			loadingNode.size = size
			loadingNode.position = .zero
			loadingNode.zPosition = 50
			cam.addChild(loadingNode)
			forceWindow()
		} else {
			applyHome()
		}

		if let transition = HomeScene.startTransition {
			_ = TransitionNode.make(transition:transition,duration:transition == .welcome ? 1 : 0.5)
		} else {
			showStatic()
		}

		phoneRing = GameData.peerInfo.name.isEmpty || GameData.get(flag:"phonecall")
		sofaNode.register()
	}

	func applyHome() {
		apply(theme:GameState.homeInfo.theme)
		forceWindow()
	}

	func startCharArea() {
		guard !charAreaClient.isRunning else { return }
		charAreaClient.firstSpawn = GameState.charAreaFirstSpawn
		GameState.charAreaFirstSpawn = false
		GameState.par.start(client:charAreaClient)
	}

	func stopCharArea() {
		guard charAreaClient.isRunning else { return }
		GameState.par.stop(client:charAreaClient)
	}

	override func hide() {
		sofaNode.deregister()
		stopCharArea()
		bgmStatic?.stop()
		bgmStatic = nil
	}

	override func update() {
		var shouldHideStatic = false
		if staticNode.parent != nil && !staticForever {
			staticTempo -= Time.deltaTime
			if staticTempo <=~ 0 {
				staticTempo = 0
				shouldHideStatic = true
			}
		}

		if !GameState.houseConnected {
			if endTransition == nil {
				if shouldHideStatic {
					goToMenu()
				} else if let view = consoleView {
					view.close?(false)
				} else if staticNode.parent == nil {
					endTransition = TransitionNode.make(transition:.slideFromLeft,duration:0.5,completion:goToMenu)
				}
			}
			return
		}

		if loadingHouse && !GameState.loadingHouse {
			loadingHouse = false
			if loadingNode != nil {
				loadingNode.removeFromParent()
				loadingNode = nil
			}
			applyHome()
		}

		if staticNode.parent == nil && consoleView == nil && tutorialNode.view == nil {
			var charAreaTarget:CGPoint?
			for i in Touch.events {
				if touchID == nil && i.state == .down {
					touchID = i.id
					selector.select(at:i.position)
					if selector.selected == nil && charAreaClient.spawned {
						charAreaTarget = i.position
					}
				}
				if touchID == i.id {
					if selector.selected == nil && charAreaClient.spawned {
						charAreaTarget = i.position
					}
					if i.state == .miss {
						touchID = nil
					} else if i.state == .up {
						touchID = nil
						if let node = selector.finalSelect(at:i.position) {
							press(node:node)
						}
					}
				}
			}
			if let target = charAreaTarget {
				charAreaNode.show(target:target)
			}
		}

		if GameState.houseVisible {
			if !windowOpen {
				windowOpen = true
				Audio.play(sfxCurtainOpen)
			}
			if windowTempo < 1 {
				windowTempo += Time.deltaTime*6
				if windowTempo > 1 {
					windowTempo = 1
				}
				updateWindow()
			}
		} else {
			if windowOpen {
				windowOpen = false
				Audio.play(sfxCurtainClose)
			}
			if windowTempo > 0 {
				windowTempo -= Time.deltaTime*6
				if windowTempo < 0 {
					windowTempo = 0
				}
				updateWindow()
			}
		}

		if GameState.gameRunning {
			if currentGame != GameState.gameSelected {
				currentGame = GameState.gameSelected
				setTvTexture()
			}
		} else if currentGame != nil {
			currentGame = nil
			setTvTexture()
		}

		let phoneIndex:Int
		if phoneRing && tutorialNode.view == nil && Int(Time.time) % 2 == 0 {
			phoneIndex = Int(Time.time*12) % phoneTextures.count
			ringNode.isHidden = false
		} else {
			phoneIndex = 0
			ringNode.isHidden = true
		}
		if self.phoneIndex != phoneIndex {
			self.phoneIndex = phoneIndex
			phoneNode.texture = phoneTextures[phoneIndex]
		}

		if GameState.gameOpen {
			if GameState.gameRunning {
				if let view = consoleView {
					view.close?(false)
				} else if shouldHideStatic {
					GameState.startGame(GameState.gameSelected)
				} else if staticNode.parent == nil {
					showStatic()
				}
			} else if let view = consoleView {
				if shouldHideStatic {
					showStatic(forever:true)
				}
				view.select(game:GameState.gameSelected)
			} else {
				if shouldHideStatic {
					if let view = UI.load("consoleView") as? ConsoleGameListViewController {
						view.list = GameState.homeInfo.games
						view.close = {
							self.consoleView = nil
							UI.popAll(animated:false)
							self.showStatic()
							if $0 {
								GameState.closeGame()
							}
						}
						view.select = {
							GameState.select(game:$0)
						}
						view.play = {
							GameState.select(game:GameState.gameSelected,play:true)
						}
						view.selected = GameState.gameSelected
						self.consoleView = view
						UI.push(view,animated:false)
						showStatic(forever:true)
					}
				} else if staticNode.parent == nil {
					showStatic()
				}
			}
		} else {
			if shouldHideStatic {
				hideStatic()
			} else if let view = consoleView {
				view.close?(false)
			}
		}

		if !GameState.loadingHouse && staticNode.parent == nil && consoleView == nil {
			startCharArea()
		} else {
			stopCharArea()
		}
	}

	override func lateUpdate() {
		if staticNode.parent != nil {
			updateStatic()
		}
	}

	func goToMenu() {
		MenuScene.startTransition = .slideToRight
		Scene.present(MenuScene(),popUI:true,animated:false)
	}

	func updateWindow(forceTexture:Bool = false) {
		let textureIndex:Int
		if windowTempo > 0 {
			if windowTempo < 1 {
				textureIndex = (windowTempo >= 0.5) ? 2 : 1
			} else {
				textureIndex = 3
			}
		} else {
			textureIndex = 0
		}
		if forceTexture || windowTextureIndex != textureIndex {
			windowTextureIndex = textureIndex
			windowNode.texture = windowTextures[textureIndex]
		}
	}

	func forceWindow() {
		windowOpen = GameState.houseVisible
		windowTempo = windowOpen ? 1 : 0
		updateWindow(forceTexture:true)
	}

	func press(node i:SKNode) {
		switch i {
		case doorNode:
			guard !phoneRing else { break }
			GameState.goToMenu()
		case windowNode:
			guard !GameState.loadingHouse && !phoneRing else { break }
			if GameState.houseVisible {
				GameState.hideHouse()
			} else {
				GameState.showHouse()
			}
		case tvNode,sofaNode:
			guard !GameState.loadingHouse && !phoneRing else { break }
			GameState.openGame()
		case phoneNode:
			guard !GameState.loadingHouse else { break }
			openTutorial()
		default: break
		}
	}

	func apply(theme:String) {
		let atlas = SKTextureAtlas(named:"home_theme_"+theme)
		bgNode.texture = atlas.textureNamed("bg")
		sofaNode.texture = atlas.textureNamed("sofa")
		windowTextures = (0...3).map { atlas.textureNamed("window\($0)") }
		updateWindow(forceTexture:true)
	}

	func setTvTexture() {
		if let game = currentGame,let assetsFolder = GameInfo.games[game]?.assetsFolder {
			tvContentNode.texture = SKTexture(imageNamed:assetsFolder+"/icon")
		} else {
			tvContentNode.texture = nil
		}
	}

	func showStatic(forever:Bool = false) {
		if forever {
			staticForever = true
			staticTempo = 0
			bgmStatic?.stop()
			bgmStatic = nil
		} else {
			staticForever = false
			staticTempo = staticDuration
			if bgmStatic == nil {
				bgmStatic = Audio(sfxStatic,volume:0.5)
			}
		}
		if staticNode.parent == nil {
			addChild(staticNode)
			updateStatic()
		}
	}

	func hideStatic() {
		bgmStatic?.stop()
		bgmStatic = nil
		if staticNode.parent != nil {
			staticNode.removeFromParent()
		}
	}

	func updateStatic() {
		staticNode.texture = staticTextures[Int(Time.time/0.06) % staticTextures.count]
	}

	func openTutorial() {
		let tuto = """

		[back:on] [close:on]

		[sc:home_walk] [sp:shock]
		This is your house! \\ \\ You can touch and drag around the house to move your character.

		[sc:home_window] [sp:idle]
		As the house owner, you can open the curtains to make the house visible for nearby players.

		[sc:home_play] [sp:idle]
		By touching the tv, you can select a game and start playing with whoever is in the house with you.

		[sc:home_leave] [sp:idle]
		You can also, of course, leave the house. \\ \\ If anyone is in your house, they leave with you.

		"""

		var e = ""
		if GameData.peerInfo.name.isEmpty {
			//first time using app! show basic tutorial
			e += """

			[bg:creator] [back:off] [close:off]

			[sc:intro_welcome] [sp:idle]
			Hi! Welcome to Roommates! \\ \\ Hang out and play games with nearby friends.

			[name:peer]
			It looks like you're new here! \\ \\ So first of all, how would you like to be called?

			[sc:intro_good] [sp:shock]
			{peer}! \\ \\ That's a beautiful name!

			[close:on]

			[sc] [sp:idle]
			I'll teach you real quick how to navigate around here before you can get started. \\ \\ If you don't wanna hear it, feel free to hang up!

			""" + tuto + """

			[sc:home_squad] [sp:idle]
			...aaand that's about it! If you have any doubts, you can always call me for help! \\ \\ Have fun!

			"""
		} else if phoneRing {
			//new update! show new features
			e += """

			[bg:creator] [back:off] [close:off]

			[sc] [sp:idle]
			Hi! Just calling to let you know about the new update!

			(this is where he talks about new features and whatnot)

			"""
		} else if GameState.atHome {
			//they're back! show options for changing stuff & tutorial
			e += """

			[bg:creator] [back:off] [close:true] [sp:idle]
			Hi! \\ \\ Is there anything I can help you with?
			- tuto: teach me again!
			- name: change name
			- char: change character
			- home: redecorate home

			+ again

			[back:off] [close:true] [sp:idle]
			Is there anything else I can help you with?
			- tuto: teach me again!
			- name: change name
			- char: change character
			- home: redecorate home

			+ tuto

			""" + tuto + """

			[sc:home_squad] [sp:idle]
			That's about it, again! \\ \\ Remember you can always call me wherever you are, if you need help with something more specific.

			> again

			"""
			if GameState.houseVisible {
				//house is currently visible, disable customisation
				e += """

				+ name
				+ char
				+ home

				[sc] [sp:shock]
				Oh sorry, you can only customize things with your curtains closed! \\ \\ Can't change without some privacy.

				> again

				"""
			} else {
				//house is invisible, allow customisation
				e += """

				+ name

				[name:peer] [sp:idle]
				Alright! \\ \\ How would you like to be called, then?

				[sc:intro_good] [sp:shock]
				{peer}! \\ \\ That's a beautiful name!

				> again

				+ char

				[char] [sp:idle]
				Alright! \\ \\ Select a character by tapping the arrows above.

				[sc:intro_good] [sp:shock]
				Oh! {char}! \\ \\ Great choice!

				> again

				+ home

				[sc] [sp:shock]
				(oh sorry, still building this feature.)

				> again

				"""
			}
		} else {
			//visiting a friend! show a basic tutorial
			e += """

			[bg:creator] [back:true] [close:true]

			[sc:home_walk2] [sp:shock]
			Hi! Apparently you're visiting a friend! You can touch and drag around the house to move your character, just like at home.

			[sc:home_window2] [sp:idle]
			You were able to visit because the curtains are open!\nThe house owner can open it to make their house visible.

			[sc:home_play] [sp:idle]
			By touching the tv, you can select a game and start playing with your friends.

			[sc:home_leave] [sp:idle]
			You can also, of course, leave the house.\n\nIf the house owner leaves their house, everyone leaves with them.

			[sc:home_squad] [sp:idle]
			That's it! You can call me at your house if you need anything else. \\ \\ Have fun!

			"""
		}
		tutorialNode.list = TutorialList(parse:e)
		tutorialNode.open() {
			self.charAreaNode.refreshChar()
			if self.phoneRing && !GameData.peerInfo.name.isEmpty {
				self.phoneRing = false
				GameData.set(flag:"phonecall",to:false)
			}
		}
	}
}
