import SpriteKit

class HomeSofaNode:SKSpriteNode,SceneNode {
	private var peers:[ControllerNode] = []

	init() {
		super.init(texture:nil,color:SKColor.white,size:CGSize(width:466,height:214))
	}

	func register() {
		for id in GameState.playingPeers {
			let node = ControllerNode(id:id)
			addChild(node)
			peers.append(node)
		}
		realign()
		GameState.playingPeersUpdate = {
			id,index,connect in
			if connect {
				let node = ControllerNode(id:id)
				self.addChild(node)
				self.peers.insert(node,at:index)
			} else {
				let node = self.peers.remove(at:index)
				node.hide = true
			}
			self.realign()
		}
	}

	func deregister() {
		if !peers.isEmpty {
			for node in peers {
				node.hide = true
			}
			peers.removeAll()
		}
		GameState.playingPeersUpdate = nil
	}

	func realign() {
		guard !peers.isEmpty else { return }
		guard peers.count > 1 else {
			peers[0].x = 0
			return
		}
		let halfWidth = (1-1/CGFloat(peers.count))*150
		for (n,node) in peers.enumerated() {
			node.x = (CGFloat(n)/CGFloat(peers.count-1)).lerp(-halfWidth,halfWidth)
		}
	}

	required init?(coder aDecoder:NSCoder) {
		return nil
	}

	class ControllerNode:SKNode,SceneNode {
		let sprite = CharNode()
		var hide = false
		var x:CGFloat = 0

		private var first = true

		init(id:String) {
			super.init()
			addChild(sprite)
			sprite.set(char:GameState.peerInfo(for:id)?.char ?? PeerInfo.chars[Random.range(PeerInfo.chars.count)])
			sprite.state = .back
		}

		required init?(coder aDecoder:NSCoder) {
			return nil
		}

		func update() {
			let t = Time.deltaTime*8
			let yMin:CGFloat = -100
			let yMax:CGFloat = 0
			if first {
				first = false
				position = CGPoint(x:x,y:yMin)
			} else {
				position.x = t.lerp(position.x,x)
			}
			zPosition = -(position.x*0.001+0.5).clamp01*0.001-0.001
			if hide {
				position.y = t.lerp(position.y,yMin)
				yScale = t.lerp(yScale,0.5)
				if position.y < -99 {
					removeFromParent()
				}
			} else {
				position.y = t.lerp(position.y,yMax)
				yScale = t.lerp(yScale,1)
			}
		}
	}
}
