import UIKit
import SpriteKit

class GameViewController:UIViewController {
    override func viewDidLoad() {
		super.viewDidLoad()
		let s = UIScreen.main.bounds.size
		Game.start(view:view as! SKView,vc:self,width:s.width,height:s.height,pad:UIDevice.current.userInterfaceIdiom == .pad)
	}

	override var shouldAutorotate:Bool {
		return false
	}

	override var supportedInterfaceOrientations:UIInterfaceOrientationMask {
		return .portrait
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}

	override var prefersStatusBarHidden:Bool {
		switch UIDevice.model {
		//case "i386","x86_64": return false
		case "iPhone10,3","iPhone10,6": return false
		default: return true
		}
	}
}
