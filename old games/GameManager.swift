import SpriteKit

protocol GameManager {
	static func load()
	static func unload()
	static func update()
	static func lateUpdate()
}

extension GameManager {
	static func load() {}
	static func unload() {}
	static func update() {}
	static func lateUpdate() {}

	static func presentFirstScene(_ scene:Scene) {
		Scene.present(scene,popUI:true,animated:false)
	}

	static func leaveScene() {
		presentFirstScene(HomeScene())
	}
}
