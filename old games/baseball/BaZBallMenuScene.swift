import UIKit
import SpriteKit

class BaZBallMenuScene: Scene {
	let logoNode = SKSpriteNode()
	let closeNode = SKSpriteNode()
	let switchNode = SKSpriteNode()
	let startNode = SKSpriteNode()
	let selector = ButtonSelector()
	let margin: CGFloat = 40

	// ----------------------------- //
	var selectionView : SelectionViewController?

	// ----------------------------- //
	var sfxSelect:Data?

	// ----------------------------- //
	override func load() {
		size = CGSize(width: BaZBall.contentSize.width, height: BaZBall.contentSize.height)

		addChild(logoNode)
		logoNode.texture = BaZBall.atlas?.textureNamed("baseball_telainicial_fundo")
		logoNode.size = CGSize(width:size.width,height:size.height)

		addChild(closeNode)
		closeNode.position = CGPoint(x:size.width*0.5,y:size.height*0.5)
		closeNode.texture = SKTexture(imageNamed:"common/close")
		closeNode.size = CGSize(width:171*720/1242,height:189*720/1242)
		closeNode.anchorPoint = CGPoint(x:1,y:1)
		closeNode.zPosition = 10

		addChild(switchNode)
		switchNode.position = CGPoint(x:0,y:-875*720/1242)
		switchNode.texture = BaZBall.atlas?.textureNamed("botao_goBack")
		switchNode.size = CGSize(width:990*720/1242,height:156*720/1242)

		addChild(startNode)
		startNode.position = CGPoint(x:0,y:-585*720/1242)
		startNode.texture = BaZBall.atlas?.textureNamed("botao_start")
		startNode.size = CGSize(width:990*720/1242,height:246*720/1242)

		selector.nodes = [closeNode, switchNode, startNode]

		sfxSelect = Audio.data(of:"baseball/select.wav")
	}

	override func update() {
		for i in BaZBall.par.events {
			switch i.event.name {
			case "openSelection":
				openSelection(request: false)
			default:
				break
			}
		}
		if let i = selector.update() {
			switch i {
			case closeNode:
				HomeState.leaveGame()
			case switchNode:
				HomeState.closeGame()
			case startNode:
				Audio.play(sfxSelect)
				openSelection(request: true)
			default:
				break
			}
		}
	}

	func openSelection(request: Bool) {
		if request {
			BaZBall.par.send(ParEvent(name: "openSelection"), reliable: true)
		}

		guard selectionView == nil else { return }
		selectionView = UI.showSelection(count: 2) {
			start,players in
			self.selectionView = nil

			if start {
				BaZBall.set(players: players)
				BaZBall.sendPlayers(to: BaZBall.par.peers)
			}
		}
	}
}
