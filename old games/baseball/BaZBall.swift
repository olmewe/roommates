import SpriteKit

class BaZBall: GameManager {
	static let contentSize = CGSize(width: 720, height: 720 * Game.heightRatio)
	static let par = ParContext(context: context)
	static let context = "game_" + id
	static let id = "baseball"

	// ----------------------------- //
	enum State {
		case loading
		case menu
		case queue
		case game
	}

	enum GameState {
		case loading
		case arranging
	}

	// ----------------------------- //
	static var gameState: GameState = .loading
	static var state: State = .loading
	static var confirmOpponent: Int?
	static var players: [Peer] = []
	static var confirmPlayer: Int?
	static var opponent: Peer!
	static var atlas:SKTextureAtlas?

	// ----------------------------- //
	static var messageReceived = false
	static var precisionVelocity = 10
	static var messageSent = false
	static var isPitcher = false
	static var opponentScore = 0
	static var velocity = 10.0
	static var change = false
	static var begin = true
	static var end = false
	static var round = 1
	static var score = 0
	static var set = 1
	static var aux = 0

	// ----------------------------- //
	static func load() {
		players = []
		opponent = Par.peer
		atlas = SKTextureAtlas(named:"game_baseball")

		par.subscribe()

		if par.peers.isEmpty {
			state = .menu
			presentFirstScene(BaZBallMenuScene())
		} else {
			state = .loading
			presentFirstScene(BaZBallLoadingScene())
		}
	}

	static func unload() {
		players = []
		clearVariables()
		score = 0
		round = 1
		par.unsubscribe()
		leaveScene()
	}

	static func update() {
		for i in par.events {
			switch i.event.name {
			case "players":
				set(players: Peer.decode(peers: i.event.data["list"]) ?? [])

			case "ready":
				confirmOpponent = i.event.data["confirm"] as? Int
				isPitcher = isOpponentStarting()
				par.send(ParEvent(name: "start", data: ["isPitcher": !isPitcher]), to: opponent)
//				Scene.present(BaZBallWinLostScene())
				Scene.present(BaZBallScene())

			case "start":
				isPitcher = i.event.data["isPitcher"] as! Bool
//				Scene.present(BaZBallWinLostScene())
				Scene.present(BaZBallScene())

			case "throw":
				guard let vel = i.event.data["velocity"] as? Double, let pts = i.event.data["pts"] as? Int else { break }
				opponentScore = pts
				velocity = vel
				messageReceived = true
				messageSent = false

			case "received":
				guard let s = i.event.data["set"] as? Int, let pts = i.event.data["pts"] as? Int else { break }
				opponentScore = pts
				set = s
				messageReceived = true
				messageSent = false

				if set == 5 && round != 4 {
					change = true
				}

				else if set == 5 && round == 4 {
					end = true
				}

			default:
				break
			}
		}

		if state != .loading {
			var newPeers: [Peer] = []
			for (peer,connected) in par.newPeers {
				if connected {
					newPeers.append(peer)
				} else if players.contains(peer) {
					set(players: [])
				}
			}
			if !newPeers.isEmpty {
				sendPlayers(to: newPeers)
			}
		}
	}

	static func endGame() {
		clearVariables()
		score = 0
		round = 1
		opponentScore = 0
		end = false

		set(players: [])
		sendPlayers(to: par.peers)
	}

	static func lateUpdate() {
		par.clearEvents()
	}

	static func sendConfirm() {
		let confirm = Random.range(1 << 16)
		confirmPlayer = confirm
		par.send(ParEvent(name: "ready", data: ["confirm": confirm]), to: opponent)
	}

	static func clearConfirm() {
		confirmPlayer = nil
		confirmOpponent = nil
	}

	static func clearVariables() {
		messageReceived = false
		messageSent = false
		change = false
		velocity = 10.0
		set = 1
	}

	static func set(players: [Peer]) {
		self.players = players
		let newState: State

		if players.count == 2 {
			if players[0] == Par.peer {
				if par.peers.contains(players[1]) {
					opponent = players[1]
					newState = .game
				}

				else {
					newState = .menu
				}
			}

			else if players[1] == Par.peer {
				if par.peers.contains(players[0]) {
					opponent = players[0]
					newState = .game
				}

				else {
					newState = .menu
				}
			}

			else if par.peers.contains(players[0]) && par.peers.contains(players[1]) {
				newState = .queue
			}

			else {
				newState = .menu
			}
		}

		else {
			newState = .menu
		}

		guard state != newState else { return }
		state = newState

		switch state {
		case .loading:
			clearConfirm()
			clearVariables()
			Scene.present(BaZBallLoadingScene())

		case .menu:
			clearConfirm()
			clearVariables()
			Scene.present(BaZBallMenuScene())

		case .queue:
			clearConfirm()
			clearVariables()
			Scene.present(BaZBallQueueScene())

		case .game:
			sendConfirm()
			clearVariables()
			score = 0
			round = 1

			if confirmOpponent != nil {
				gameState = .arranging
			} else {
				gameState = .loading
			}
		}
	}

	static func sendPlayers(to peers:[Peer]) {
		par.send(ParEvent(name: "players", data: ["list": Peer.encode(peers: players)]), to: peers)
	}

	static func isOpponentStarting() -> Bool {
		guard let p = confirmPlayer, let o = confirmOpponent else { return true }
		guard p != o else { return Par.peer.compare(to: opponent) }
		return p < o
	}

	static func sendVelocity(vel: Double, name: String, pts: Int) {
		//		messageReceived = false
		messageSent = true
		par.send(ParEvent(name: name, data:["velocity": vel, "pts": pts]), to: opponent)
	}

	static func sendMessage(name: String, s: Int, pts: Int) {
		messageSent = true
		par.send(ParEvent(name: name, data: ["set" : s, "pts": pts]), to: opponent)
	}
}
