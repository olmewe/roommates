import UIKit
import SpriteKit

class BaZBallQueueScene: Scene {
	let leaveNode = SKSpriteNode(color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), size: CGSize(width: 100, height: 100))
	let idleNode = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), size: CGSize(width: 50, height: 50))
	let selector = ButtonSelector()

	// ----------------------------- //
	override func load() {
		size = CGSize(width: BaZBall.contentSize.width, height: BaZBall.contentSize.height)
		backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)

		let margin: CGFloat = 40

		leaveNode.position = CGPoint(x: (size.width-leaveNode.size.width-margin) * 0.5, y: (size.height - leaveNode.size.height - margin) * 0.5)
		addChild(leaveNode)

		addChild(idleNode)

		selector.nodes = [leaveNode]
	}

	override func update() {
		if let i = selector.update() {
			switch i {
			case leaveNode: HomeState.leaveGame()
			default: break
			}
		}

		idleNode.position = CGPoint(x: cos(Time.time) * 300, y: sin(Time.time) * 300)
	}
}
