import UIKit
import SpriteKit
import Foundation

class BaZBallScene: Scene {
	//	let leaveNode = SKSpriteNode(color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), size: CGSize(width: 300, height: 100))
	let gameAtlas = SKTextureAtlas(named: "game_baseball")
	var transitionBall = SKShapeNode(circleOfRadius: 1)
	let scoreAnimationLabel = SKLabelNode()
	let backgroundNode = SKSpriteNode()
	let selector = ButtonSelector()
	let tableNode = SKSpriteNode()
	let closeNode = SKSpriteNode()

	// ------------------------------ //
	let pitcherAtlas = SKTextureAtlas(named: "game_baseball_pitcher")
	var pitcherSprites = Array<SKTexture>()
	var pitcherNode = SKSpriteNode()
	var animatePitcher = SKAction()
	let precision = SKSpriteNode()
	let scoreLabel = SKLabelNode()
	let roundLabel = SKLabelNode()
	let ballNode = SKSpriteNode()
	var auxNode = SKSpriteNode()
	let setLabel = SKLabelNode()
	let bar = SKSpriteNode()

	// ----------------------------- //
	let barNodeLowTop = SKSpriteNode()
	let barNodeMediumTop = SKSpriteNode()
	let barNodeAlmostHighTop = SKSpriteNode()
	let barNodeHigh = SKSpriteNode()
	let barNodeAlmostHighBot = SKSpriteNode()
	let barNodeMediumBot = SKSpriteNode()
	let barNodeLowBot = SKSpriteNode()

	// ----------------------------- //
	let hitterAtlas = SKTextureAtlas(named: "game_baseball_hitter")
	var precisionBall2 = SKShapeNode(circleOfRadius: 150)
	let precisionBall = SKShapeNode(circleOfRadius: 150)
	var hitterSprites = Array<SKTexture>()
	var hitterNode = SKSpriteNode()
	var animateHitter = SKAction()

	// ----------------------------- //
	var precisionVelocity = BaZBall.precisionVelocity
	var messageReceived = BaZBall.messageReceived
	var messageSent = BaZBall.messageSent
	var isPitcher = BaZBall.isPitcher
	var velocity = BaZBall.velocity
	var change = BaZBall.change
	var endGame = BaZBall.end
	var score = BaZBall.score
	var scoreNodeAnim = false
	var aux = CGSize.zero
	var missTouch = false
	var scoreAnim = false
	var back = false
	var anim = true
	var pts = 0

	// ----------------------------- //
	var sfxSelect:Data?
	var sfxAction:Data?

	// ----------------------------- //
	func endScene(_ control: Int) {

		var path = UIBezierPath()
		var radius = transitionBall.path?.boundingBox.height

		if control == 1 {
			BaZBall.isPitcher = isPitcher
			BaZBall.score = score

			if precisionVelocity < 0 {
				BaZBall.precisionVelocity = precisionVelocity * -1
			}

			if isPitcher {
				anim = false
			}

			transitionBall.fillColor = #colorLiteral(red: 0.4304351807, green: 0.8814089894, blue: 0.9992426038, alpha: 1)

			path = UIBezierPath(arcCenter: CGPoint (x: 0, y : size.height * (-0.15)), radius: 1, startAngle: 0.0, endAngle: CGFloat(Double.pi * 2), clockwise: false)
			transitionBall.path = path.cgPath

			transitionBall.zPosition = 6
			addChild(transitionBall)
			BaZBall.aux += 1
		}

		// ----------------------------- //
		if radius != nil {

			if radius! >= CGFloat(1700) {
				clearVariables()
				let eScene = BaZBallWinLostScene()
				Scene.present(eScene)
			}

			radius = radius! / 2

			path = UIBezierPath(arcCenter: CGPoint (x: 0, y : size.height * (-0.15)), radius: radius! + 10.0, startAngle: 0.0, endAngle: CGFloat(Double.pi * 2), clockwise: false)
			transitionBall.path = path.cgPath
		}

		else {
			clearVariables()
			let eScene = BaZBallWinLostScene()
			Scene.present(eScene)
		}
	}

	func changeScene(_ control: Int) {

		var path = UIBezierPath()
		var radius = transitionBall.path?.boundingBox.height

		if control == 1 {
			BaZBall.isPitcher = isPitcher
			BaZBall.score = score

			if precisionVelocity < 0 {
				BaZBall.precisionVelocity = precisionVelocity * -1
			}

			if isPitcher {
				transitionBall.fillColor = #colorLiteral(red: 1, green: 1, blue: 0.6012218595, alpha: 1)
				anim = false
			}

			else {
				transitionBall.fillColor = #colorLiteral(red: 0.4009025991, green: 0.8010060191, blue: 1, alpha: 1)
			}

			path = UIBezierPath(arcCenter: CGPoint (x: 0, y : size.height * (-0.15)), radius: 1, startAngle: 0.0, endAngle: CGFloat(Double.pi * 2), clockwise: false)
			transitionBall.path = path.cgPath

			transitionBall.zPosition = 6
			addChild(transitionBall)
			BaZBall.aux += 1
		}

		// ----------------------------- //
		if radius != nil {

			if radius! >= CGFloat(1700) {
				let cScene = BaZBallChangeScene()
				Scene.present(cScene)
			}

			radius = radius! / 2

			path = UIBezierPath(arcCenter: CGPoint (x: 0, y : size.height * (-0.15)), radius: radius! + 10.0, startAngle: 0.0, endAngle: CGFloat(Double.pi * 2), clockwise: false)
			transitionBall.path = path.cgPath
		}

		else {
			let cScene = BaZBallChangeScene()
			Scene.present(cScene)
		}
	}

	override func load() {
		size = CGSize(width: BaZBall.contentSize.width, height: BaZBall.contentSize.height)
		alpha = 0
		backgroundNode.size = size
		backgroundNode.zPosition = 0
		scaleMode = .aspectFit

		closeNode.position = CGPoint(x: size.width * 0.4 + 15, y: (size.height) * 0.4 + 70)
		closeNode.texture = SKTexture(imageNamed: "common/close")
		closeNode.size = CGSize(width: 114, height: 126)
		closeNode.zPosition = 1
		addChild(closeNode)

		tableNode.position = CGPoint(x: -200, y: size.height * (0.3) - 15)
		tableNode.size = CGSize(width: 328, height: 250)
		tableNode.zPosition = 2

		scoreLabel.position = CGPoint(x: -316, y: size.height * (0.3) - 45)
		scoreLabel.fontSize = 45
		scoreLabel.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		scoreLabel.fontName = "AmericanTypewriter-Bold"
		scoreLabel.text = "\(score)"
		scoreLabel.zPosition = 3
		addChild(scoreLabel)

		roundLabel.position = CGPoint(x: -205, y: size.height * (0.3) - 45)
		roundLabel.fontSize = 45
		roundLabel.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		roundLabel.fontName = "AmericanTypewriter-Bold"
		roundLabel.text = "\(BaZBall.round)"
		roundLabel.zPosition = 3
		addChild(roundLabel)

		setLabel.position = CGPoint(x: -100, y: size.height * (0.3) - 42)
		setLabel.fontSize = 45
		setLabel.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		setLabel.fontName = "AmericanTypewriter-Bold"
		setLabel.text = "\(BaZBall.set)"
		setLabel.zPosition = 3
		addChild(setLabel)

		scoreAnimationLabel.position = CGPoint(x: 0, y: size.height * (-0.15))
		scoreAnimationLabel.fontSize = 45
		scoreAnimationLabel.fontColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		scoreAnimationLabel.fontName = "AmericanTypewriter-Bold"
		scoreAnimationLabel.text = "+\(BaZBall.score)"
		scoreAnimationLabel.zPosition = 5
		scoreAnimationLabel.alpha = 0
		addChild(scoreAnimationLabel)

		selector.nodes = [closeNode]

		if isPitcher {
			backgroundColor = #colorLiteral(red: 0.4009025991, green: 0.8010060191, blue: 1, alpha: 1)
			backgroundNode.texture = gameAtlas.textureNamed("background_pitcher")
			tableNode.texture = gameAtlas.textureNamed("table_pitcher")

			pitcherSprites.append(pitcherAtlas.textureNamed("animation0"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation1"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation2"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation3"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation4"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation5"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation6"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation7"))
			pitcherSprites.append(pitcherAtlas.textureNamed("animation8"))

			pitcherNode = SKSpriteNode(texture: pitcherSprites[0])
			pitcherNode.position = CGPoint(x: 0, y: size.height * (-0.35))
			pitcherNode.size = CGSize(width: 142, height: 170)
			pitcherNode.zPosition = 4

			animatePitcher = SKAction.animate(with: self.pitcherSprites, timePerFrame: 0.05)

			ballNode.position = CGPoint(x: 0, y: size.height * (-0.35))
			ballNode.texture = gameAtlas.textureNamed("ball")
			ballNode.size = CGSize(width: 40, height: 40)
			ballNode.zPosition = 2

			bar.position = CGPoint(x: -210, y: size.height * (-0.2))
			bar.size = CGSize(width: 70, height: 500)
			bar.texture = gameAtlas.textureNamed("bar")
			bar.zPosition = 3

			precision.position = CGPoint(x: bar.position.x, y: -(bar.size.height / 2))
			precision.size = CGSize(width: 100, height: 10)
			precision.texture = gameAtlas.textureNamed("bar_force")
			precision.zPosition = 4

			BaZBall.messageReceived = true

			barNodeLowTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 439.5)
			barNodeLowTop.size = CGSize(width: 61, height: 99)
			barNodeLowTop.color = #colorLiteral(red: 0.9999546409, green: 0.8586017489, blue: 0.5406649709, alpha: 1)
			barNodeLowTop.alpha = 0
			barNodeLowTop.zPosition = 5

			barNodeMediumTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 365)
			barNodeMediumTop.size = CGSize(width: 61, height: 50)
			barNodeMediumTop.color = #colorLiteral(red: 1, green: 0.7987430692, blue: 0.5436424613, alpha: 1)
			barNodeMediumTop.alpha = 0
			barNodeMediumTop.zPosition = 5

			barNodeAlmostHighTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 315)
			barNodeAlmostHighTop.size = CGSize(width: 61, height: 50)
			barNodeAlmostHighTop.color = #colorLiteral(red: 0.9985727668, green: 0.6984573007, blue: 0.5732192397, alpha: 1)
			barNodeAlmostHighTop.alpha = 0
			barNodeAlmostHighTop.zPosition = 5

			barNodeHigh.position = CGPoint(x: bar.position.x, y: -bar.size.height + 255)
			barNodeHigh.size = CGSize(width: 61, height: 70)
			barNodeHigh.color = #colorLiteral(red: 0.999497354, green: 0.6211079955, blue: 0.6379546523, alpha: 1)
			barNodeHigh.alpha = 0
			barNodeHigh.zPosition = 5

			barNodeAlmostHighBot.position = CGPoint(x: bar.position.x, y: -bar.size.height + 190)
			barNodeAlmostHighBot.size = CGSize(width: 61, height: 60)
			barNodeAlmostHighBot.color = #colorLiteral(red: 0.9985727668, green: 0.6984573007, blue: 0.5732192397, alpha: 1)
			barNodeAlmostHighBot.alpha = 0
			barNodeAlmostHighBot.zPosition = 5

			barNodeMediumBot.position = CGPoint(x: bar.position.x, y: -bar.size.height + 137.5)
			barNodeMediumBot.size = CGSize(width: 61, height: 45)
			barNodeMediumBot.color = #colorLiteral(red: 1, green: 0.7987430692, blue: 0.5436424613, alpha: 1)
			barNodeMediumBot.alpha = 0
			barNodeMediumBot.zPosition = 5

			barNodeLowBot.position = CGPoint(x: bar.position.x, y: -bar.size.height + 57.5)
			barNodeLowBot.size = CGSize(width: 61, height: 115)
			barNodeLowBot.color = #colorLiteral(red: 0.9999546409, green: 0.8586017489, blue: 0.5406649709, alpha: 1)
			barNodeLowBot.alpha = 0
			barNodeLowBot.zPosition = 5

			auxNode.zPosition = 6

			// ----------------------------------------------------------------------- //

//			let lowBottom = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width: 200, height: 2))
//			lowBottom.position = CGPoint(x: bar.position.x, y: -bar.size.height)
//			lowBottom.zPosition = 5
//
//			let lowTop = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width: 200, height: 2))
//			lowTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 115)
//			lowTop.zPosition = 5
//
//			let mediumTop = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width: 200, height: 2))
//			mediumTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 160)
//			mediumTop.zPosition = 5
//
//			let almostHighTop = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width: 200, height: 2))
//			almostHighTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 220)
//			almostHighTop.zPosition = 5
//
//			let highTop = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width: 200, height: 2))
//			highTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 290)
//			highTop.zPosition = 5

			// ----------------------------------------------------------------------- //

//			let almostHighTopTop = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width: 200, height: 2))
//			almostHighTopTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 340)
//			almostHighTopTop.zPosition = 5
//
//			let mediumTopTop = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width: 200, height: 2))
//			mediumTopTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 390)
//			mediumTopTop.zPosition = 5
//
//			let lowTopTop = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width: 200, height: 2))
//			lowTopTop.position = CGPoint(x: bar.position.x, y: -bar.size.height + 489)
//			lowTopTop.zPosition = 5

			addChild(barNodeLowBot)
			addChild(barNodeMediumBot)
			addChild(barNodeAlmostHighBot)
			addChild(barNodeHigh)
			addChild(barNodeAlmostHighTop)
			addChild(barNodeMediumTop)
			addChild(barNodeLowTop)
//			addChild(lowTopTop)
//			addChild(mediumTopTop)
//			addChild(almostHighTopTop)
//			addChild(highTop)
//			addChild(almostHighTop)
//			addChild(mediumTop)
//			addChild(lowTop)
//			addChild(lowBottom)
			addChild(backgroundNode)
			addChild(tableNode)
			addChild(pitcherNode)
			addChild(ballNode)
			addChild(bar)
			addChild(precision)
			addChild(auxNode)

			sfxAction = Audio.data(of:"baseball/throw.wav")
		}

		else {
			backgroundColor = #colorLiteral(red: 1, green: 1, blue: 0.6012218595, alpha: 1)
			backgroundNode.texture = gameAtlas.textureNamed("background_hitter")
			tableNode.texture = gameAtlas.textureNamed("table_hitter")

			hitterSprites.append(hitterAtlas.textureNamed("animation0"))
			hitterSprites.append(hitterAtlas.textureNamed("animation1"))
			hitterSprites.append(hitterAtlas.textureNamed("animation2"))
			hitterSprites.append(hitterAtlas.textureNamed("animation3"))
			hitterSprites.append(hitterAtlas.textureNamed("animation4"))
			hitterSprites.append(hitterAtlas.textureNamed("animation5"))
			hitterSprites.append(hitterAtlas.textureNamed("animation6"))
			hitterSprites.append(hitterAtlas.textureNamed("animation7"))
			hitterSprites.append(hitterAtlas.textureNamed("animation8"))

			hitterNode = SKSpriteNode(texture: hitterSprites[0])
			hitterNode.position = CGPoint(x: -30, y: size.height * (-0.35))
			hitterNode.size = CGSize(width: 142, height: 170)
			hitterNode.zPosition = 5

			animateHitter = SKAction.animate(with: self.hitterSprites, timePerFrame: 0.05)

			ballNode.position = CGPoint(x: 0, y: (size.height / 2) + 20)
			ballNode.texture = gameAtlas.textureNamed("ball")
			ballNode.size = CGSize(width: 40, height: 40)
			ballNode.zPosition = 2

			precisionBall.position = CGPoint(x: 0, y: size.height * (-0.30))
			precisionBall.strokeColor = #colorLiteral(red: 0.999497354, green: 0.6211079955, blue: 0.6379546523, alpha: 1)
			precisionBall.glowWidth = 3.0
			precisionBall.zPosition = 3

			precisionBall2.position = precisionBall.position
			precisionBall2.strokeColor = #colorLiteral(red: 0.999497354, green: 0.6211079955, blue: 0.6379546523, alpha: 1)
			precisionBall2.glowWidth = 3.0
			precisionBall2.zPosition = 4

			BaZBall.messageReceived = false

			addChild(backgroundNode)
			addChild(tableNode)
			addChild(hitterNode)
			addChild(ballNode)
			addChild(precisionBall)
			addChild(precisionBall2)

			sfxAction = Audio.data(of:"baseball/hit.wav")
		}

		sfxSelect = Audio.data(of:"baseball/select.wav")
	}

	func clearVariables() {
		if velocity < 0 {
			BaZBall.velocity = -10.0
			velocity = BaZBall.velocity
		}

		else {
			BaZBall.velocity = 10.0
			velocity = BaZBall.velocity
		}

		missTouch = false
		BaZBall.change = false
		BaZBall.messageReceived = false
		BaZBall.messageSent = false
		BaZBall.aux = 0
		anim = true
		back = false
	}

	override func update() {
		if let i = selector.update() {
			switch i {

			case closeNode:
				Audio.play(sfxSelect)
				BaZBall.score = 0
				BaZBall.round = 1
				BaZBall.endGame()
			default: break
			}
		}

		if BaZBall.begin && alpha < 1 {
			alpha += 0.05
		}

		if BaZBall.change {
			BaZBall.aux += 1
			changeScene(BaZBall.aux)
		}

		if BaZBall.end {
			BaZBall.aux += 1
			endScene(BaZBall.aux)
		}

		if score <= 99 {
			scoreLabel.fontSize = 35
		}

		else if score > 99 {
			scoreLabel.position = CGPoint(x: -316, y: size.height * (0.3) - 45)
			scoreLabel.fontSize = 25
		}

		if BaZBall.isPitcher {
			for i in Touch.events {
				if i.state == .up {
					if anim {

						if !missTouch && !BaZBall.messageSent {
							Audio.play(sfxAction)

							self.pitcherNode.run(animatePitcher)

							anim = false

							pts = calculateScorePitcher()
							velocity = (Double(pts / 2))

							ballNode.isHidden = false
							scoreAnim = true
							scoreNodeAnim = true
							scoreAnimationLabel.position.y = size.height * (-0.15)
						}

						if score <= 99 {
							scoreLabel.fontSize = 35
						}

						else if score > 99 {
							scoreLabel.position = CGPoint(x: -316, y: size.height * (0.3) - 45)
							scoreLabel.fontSize = 25
						}

						scoreLabel.text = "\(score)"
					}
				}
			}

			if BaZBall.set == 5 {
				anim = false
			}

			ballAnimationPitcher(!anim)
			precisionAnimationPitcher(anim)
			scoreAnimation(pts)
			barAnimation(pts)

			if ballNode.position.y >= (size.height / 2) + 20 {
				if !BaZBall.messageSent {
					BaZBall.sendVelocity(vel: velocity, name: "throw", pts: score)
					missTouch = false
				}

				ballNode.isHidden = true
				ballNode.position.y = (size.height * (-0.35))
				velocity = BaZBall.velocity
				anim = true
			}
		}

		else {
			for i in Touch.events {
				if i.state == .up {
					if anim {

						if !BaZBall.messageReceived {
							missTouch = false
						}

						if !missTouch && BaZBall.messageReceived {
							Audio.play(sfxAction)
							self.hitterNode.run(animateHitter)

							pts = calculateScoreHitter()
							scoreAnim = true
							scoreAnimationLabel.position.y = size.height * (-0.15)

							velocity *= 1.3
						}

						if score <= 99 {
							scoreLabel.fontSize = 35
						}

						else if score > 99 {
							scoreLabel.position = CGPoint(x: -316, y: size.height * (0.3) - 45)
							scoreLabel.fontSize = 25
						}

						scoreLabel.text = "\(score)"
					}
				}
			}

			ballAnimationHitter(anim)
			precisionAnimationHitter(anim)
			scoreAnimation(pts)

			if ballNode.position.y <= -(size.height / 2) {
				// ERROOOOOOUUU!!!

				BaZBall.set += 1

				setLabel.text = "\(BaZBall.set)"

				if !BaZBall.messageSent {
					BaZBall.sendMessage(name: "received", s: BaZBall.set, pts: score)
				}

				ballNode.position = CGPoint(x: 0, y: (size.height / 2) + 20)
				clearVariables()

				if BaZBall.set == 5 && BaZBall.round != 4 {
					BaZBall.change = true
				}

				else if BaZBall.set == 5 && BaZBall.round == 4 {
					BaZBall.end = true
				}
			}

			if ballNode.position.y >= (size.height / 2) + 30 {
				// Pega Essa!

				BaZBall.set += 1

				setLabel.text = "\(BaZBall.set)"

				if !BaZBall.messageSent {
					BaZBall.sendMessage(name: "received", s: BaZBall.set, pts: score)
				}

				ballNode.position = CGPoint(x: 0, y: (size.height / 2) + 20)
				clearVariables()

				if BaZBall.set == 5 && BaZBall.round != 4 {
					BaZBall.change = true
				}

				else if BaZBall.set == 5 && BaZBall.round == 4 {
					BaZBall.end = true
				}
			}
		}
	}

	func grad() -> SKTexture {
		UIGraphicsBeginImageContext(bar.size)

		let context = UIGraphicsGetCurrentContext()
		let gradient = CAGradientLayer()

		gradient.frame = bar.frame
		gradient.colors = [#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1).cgColor, #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1).cgColor, #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1).cgColor, #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1).cgColor, #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1).cgColor, #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1).cgColor]

		gradient.render(in: context!)

		let img = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		let texture = SKTexture(cgImage: (img?.cgImage)!)

		return texture
	}

	func barAnimation(_ points: Int) {

		if scoreNodeAnim {
			let pos = precision.position.y

			switch points {
			case 10:
				if pos < (-bar.size.height + 115) {
					auxNode.position = barNodeLowBot.position
					auxNode.size = barNodeLowBot.size
					auxNode.color = barNodeLowBot.color
					aux = barNodeLowBot.size
				}

				else if pos >= (-bar.size.height + 390) {
					auxNode.position = barNodeLowTop.position
					auxNode.size = barNodeLowTop.size
					auxNode.color = barNodeLowTop.color
					aux = barNodeLowTop.size
				}

			case 30:
				if pos >= (-bar.size.height + 115) && pos < (-bar.size.height + 160) {
					auxNode.position = barNodeMediumBot.position
					auxNode.size = barNodeMediumBot.size
					auxNode.color = barNodeMediumBot.color
					aux = barNodeMediumBot.size
				}

				else if pos >= (-bar.size.height + 340) && pos < (-bar.size.height + 390) {
					auxNode.position = barNodeMediumTop.position
					auxNode.size = barNodeMediumTop.size
					auxNode.color = barNodeMediumTop.color
					aux = barNodeMediumTop.size
				}

			case 40:
				if pos >= (-bar.size.height + 160) && pos < (-bar.size.height + 220) {
					auxNode.position = barNodeAlmostHighBot.position
					auxNode.size = barNodeAlmostHighBot.size
					auxNode.color = barNodeAlmostHighBot.color
					aux = barNodeAlmostHighBot.size
				}

				else if pos >= (-bar.size.height + 290) && pos < (-bar.size.height + 340) {
					auxNode.position = barNodeAlmostHighTop.position
					auxNode.size = barNodeAlmostHighTop.size
					auxNode.color = barNodeAlmostHighTop.color
					aux = barNodeAlmostHighTop.size
				}

			case 50:
				if pos >= (-bar.size.height + 220) && pos < (-bar.size.height + 290) {
					auxNode.position = barNodeHigh.position
					auxNode.size = barNodeHigh.size
					auxNode.color = barNodeHigh.color
					aux = barNodeHigh.size
				}

			default:
				break
			}

			if auxNode.alpha < 1 {
				auxNode.alpha += 0.05
			}

			else if auxNode.alpha >= 1 {
				auxNode.alpha = 1
				scoreNodeAnim = false
			}

			auxNode.size = CGSize(width: (aux.width * 1.1), height: (aux.height * 1.1))
		}

		else {
			if auxNode.alpha > 0 {
				auxNode.alpha -= 0.05
			}

			else if auxNode.alpha <= 0 {
				auxNode.size = aux
				auxNode.alpha = 0
			}
		}
	}

	func scoreAnimation(_ points: Int) {

		if scoreAnim {

			scoreAnimationLabel.text = "+\(points)"

			if points == 0 {
				scoreAnimationLabel.text = "Miss"
			}

			scoreAnimationLabel.position.y += 8

			if scoreAnimationLabel.alpha < 1 {
				scoreAnimationLabel.alpha += 0.05
			}

			else if scoreAnimationLabel.alpha >= 1 {
				scoreAnim = false
			}
		}

		else {
			if scoreAnimationLabel.alpha > 0 {
				scoreAnimationLabel.alpha -= 0.05
			}
		}
	}

	func calculateScoreHitter() -> Int {
		let size = precisionBall2.frame.height
		var points = 0

		if size <= 80 {
			// Very High
			score += 50
			points = 50
			anim = false
		}

		else if size > 80 && size <= 120 {
			// Almost High
			score += 40
			points = 40
			anim = false
		}

		else if size > 120 && size <= 160 {
			// Medium
			score += 30
			points = 30
			anim = false
		}

		else if size > 160 && size <= 200 {
			// Low
			score += 20
			points = 20
			anim = false
		}

		else if size > 200 && size <= 240 {
			// Very Low
			score += 10
			points = 10
			anim = false
		}

		else if size > 300 {
			// Miss
			score += 0
//			points = 0
			anim = true
			missTouch = true
		}

		return points
	}

	func calculateScorePitcher() -> Int {
		let pos = (precision.position.y)
		var points = 10

		if (pos >= (-bar.size.height)) && (pos < (-bar.size.height + 115)) {
			// Low
			score += 10
			points = 10
		}

		else if (pos >= (-bar.size.height + 115)) && (pos < (-bar.size.height + 160)) {
			// Medium
			score += 30
			points = 30
		}

		else if (pos >= (-bar.size.height + 160)) && (pos < (-bar.size.height + 220)) {
			// Almost High
			score += 40
			points = 40
		}

		else if (pos >= (-bar.size.height + 220)) && (pos < (-bar.size.height + 290)) {
			// High
			score += 50
			points = 50
		}

		else if (pos >= (-bar.size.height + 290)) && (pos < (-bar.size.height + 340)) {
			// Almost High
			score += 40
			points = 40
		}

		else if (pos >= (-bar.size.height + 340)) && (pos < (-bar.size.height + 390)) {
			// Medium
			score += 30
			points = 30
		}

		else if (pos >= (-bar.size.height + 390)) {
			// Low
			score += 10
			points = 10
		}

		return points
	}

	func ballAnimationHitter(_ animate: Bool) {
		if BaZBall.messageReceived {

			velocity = BaZBall.velocity

			if animate {
				if velocity < 0 {
					ballNode.position.y += CGFloat(velocity)
				}

				else {
					ballNode.position.y -= CGFloat(velocity)
				}
			}

			else if !animate && !missTouch {
				if ballNode.position.y < (size.height / 2) + 40 {
					if velocity < 0 {
						ballNode.position.y -= CGFloat(velocity)
					}

					else {
						ballNode.position.y += CGFloat(velocity)
					}
				}

				else if ballNode.position.y > -(size.height / 2) - 20 {
					if velocity < 0 {
						ballNode.position.y += CGFloat(velocity)
					}

					else {
						ballNode.position.y -= CGFloat(velocity)
					}
				}
			}

			else if missTouch {
				anim = true
			}
		}
	}

	func ballAnimationPitcher(_ animate: Bool) {
		if animate && !change && !endGame {
			if velocity < 0 {
				ballNode.position.y -= CGFloat(velocity)
			}

			else {
				ballNode.position.y += CGFloat(velocity)
			}
		}
	}

	func precisionAnimationHitter(_ animate: Bool) {

		if animate == false {
			back = true
		}

		if precisionBall.contains(CGPoint(x: 0, y: ballNode.position.y + 20)) {

			var path = UIBezierPath()
			var lastHeight = precisionBall2.path?.boundingBox.height

			if lastHeight == nil {
				lastHeight = CGFloat(150)
			}

			if !back {
				lastHeight = (lastHeight! / 2) - CGFloat(velocity)

				if lastHeight! <= 20 {
					lastHeight = 20
					back = true
				}
			}

			else {
				lastHeight = (lastHeight! / 2) + CGFloat(velocity)

				if velocity >= 5 && velocity <= 15 {
					if lastHeight! >= 140 {
						lastHeight = 150
					}
				}

				else if velocity > 15 && velocity <= 25 {
					if lastHeight! >= 125 {
						lastHeight = 150
					}
				}
			}

			path = UIBezierPath(arcCenter: CGPoint (x: 0, y : 0), radius: lastHeight!, startAngle: 0.0, endAngle: CGFloat(Double.pi * 2), clockwise: false)
			precisionBall2.path = path.cgPath
		}

		else {
			let path = UIBezierPath(arcCenter: CGPoint (x: 0, y : 0), radius: 150, startAngle: 0.0, endAngle: CGFloat(Double.pi * 2), clockwise: false)
			precisionBall2.path = path.cgPath
		}
	}

	func precisionAnimationPitcher(_ animate: Bool) {
		if !BaZBall.messageSent {

			roundLabel.text = "\(BaZBall.round)"
			setLabel.text = "\(BaZBall.set)"

			if animate {

				ballNode.isHidden = true

				if precision.position.y >= (bar.position.y + (bar.size.height / 2)) - 15 {
					precisionVelocity *= -1
				}

				else if precision.position.y <= -(bar.size.height) {
					precisionVelocity *= -1
				}

				precision.position.y += CGFloat(precisionVelocity)
			}
		}
	}
}
