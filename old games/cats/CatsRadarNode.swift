import SpriteKit

class CatsRadarNode: SKNode, SceneNode {
	var ringSignal = SKNode()
	var ring = SKShapeNode()
	var ring0 = SKShapeNode()
	var ring1 = SKShapeNode()
	var ring2 = SKShapeNode()
	var ring3 = SKShapeNode()
	var ring4 = SKShapeNode()
	var ring5 = SKShapeNode()
	var ring6 = SKShapeNode()
	var ring7 = SKShapeNode()
	var ring8 = SKShapeNode()
	var ring9 = SKShapeNode()
	var ring10 = SKShapeNode()
	var ring11 = SKShapeNode()
	var ring12 = SKShapeNode()
	var ring13 = SKShapeNode()
	var ring14 = SKShapeNode()
	var colour = SKColor.white
	var thickness:CGFloat = 2.0


	func load(){
		ring = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.005)
		ring0 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.01)
		ring1 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.015)
		ring2 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.02)
		ring3 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.025)
		ring4 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.03)
		ring5 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.035)
		ring6 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.04)
		ring7 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.045)
		ring8 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.05)
		ring9 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.055)
		ring10 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.06)
		ring11 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.065)
		ring12 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.07)
		ring13 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.075)
		ring14 = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.08)

		ring14.fillColor = SKColor.lightGray
		ring.fillColor = colour
		ring14.alpha = 0.5

		ring.strokeColor = colour
		ring0.strokeColor = colour
		ring1.strokeColor = colour
		ring2.strokeColor = colour
		ring3.strokeColor = colour
		ring4.strokeColor = colour
		ring5.strokeColor = colour
		ring6.strokeColor = colour
		ring7.strokeColor = colour
		ring8.strokeColor = colour
		ring9.strokeColor = colour
		ring10.strokeColor = colour
		ring11.strokeColor = colour
		ring12.strokeColor = colour
		ring13.strokeColor = colour
		ring14.strokeColor = SKColor.clear

		ring.lineWidth = thickness
		ring0.lineWidth = thickness
		ring1.lineWidth = thickness
		ring2.lineWidth = thickness
		ring3.lineWidth = thickness
		ring4.lineWidth = thickness
		ring5.lineWidth = thickness
		ring6.lineWidth = thickness
		ring7.lineWidth = thickness
		ring8.lineWidth = thickness
		ring9.lineWidth = thickness
		ring10.lineWidth = thickness
		ring11.lineWidth = thickness
		ring12.lineWidth = thickness
		ring13.lineWidth = thickness

		ringSignal.addChild(ring14)
		ringSignal.addChild(ring)
		ringSignal.addChild(ring0)
		ringSignal.addChild(ring1)
		ringSignal.addChild(ring2)
		ringSignal.addChild(ring3)
		ringSignal.addChild(ring4)
		ringSignal.addChild(ring5)
		ringSignal.addChild(ring6)
		ringSignal.addChild(ring7)
		ringSignal.addChild(ring8)
		ringSignal.addChild(ring9)
		ringSignal.addChild(ring10)
		ringSignal.addChild(ring11)
		ringSignal.addChild(ring12)
		ringSignal.addChild(ring13)

		addChild(ringSignal)
		ringSignal.zPosition = 3
	}

	func updateRadar(dist: Float) {

		if dist < 100{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 1
			ring6.alpha = 1
			ring7.alpha = 1
			ring8.alpha = 1
			ring9.alpha = 1
			ring10.alpha = 1
			ring11.alpha = 1
			ring12.alpha = 1
			ring13.alpha = 1
		}else if dist < 200{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 1
			ring6.alpha = 1
			ring7.alpha = 1
			ring8.alpha = 1
			ring9.alpha = 1
			ring10.alpha = 1
			ring11.alpha = 1
			ring12.alpha = 1
			ring13.alpha = 0
		}else if dist < 300{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 1
			ring6.alpha = 1
			ring7.alpha = 1
			ring8.alpha = 1
			ring9.alpha = 1
			ring10.alpha = 1
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}else if dist < 400{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 1
			ring6.alpha = 1
			ring7.alpha = 1
			ring8.alpha = 1
			ring9.alpha = 1
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}else if dist < 500{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 1
			ring6.alpha = 1
			ring7.alpha = 1
			ring8.alpha = 1
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}else if dist < 600{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 1
			ring6.alpha = 1
			ring7.alpha = 1
			ring8.alpha = 0
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}
		else if dist < 700{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 1
			ring6.alpha = 1
			ring7.alpha = 0
			ring8.alpha = 0
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}else if dist < 800{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 1
			ring6.alpha = 0
			ring7.alpha = 0
			ring8.alpha = 0
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}
		else if dist < 900{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 1
			ring5.alpha = 0
			ring6.alpha = 0
			ring7.alpha = 0
			ring8.alpha = 0
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}
		else if dist < 1000{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 1
			ring4.alpha = 0
			ring5.alpha = 0
			ring6.alpha = 0
			ring7.alpha = 0
			ring8.alpha = 0
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}
		else if dist < 1100{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 1
			ring3.alpha = 0
			ring4.alpha = 0
			ring5.alpha = 0
			ring6.alpha = 0
			ring7.alpha = 0
			ring8.alpha = 0
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}
		else if dist < 1200{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 1
			ring2.alpha = 0
			ring3.alpha = 0
			ring4.alpha = 0
			ring5.alpha = 0
			ring6.alpha = 0
			ring7.alpha = 0
			ring8.alpha = 0
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0
		}
		else{
			ring.alpha = 1
			ring0.alpha = 1
			ring1.alpha = 0
			ring2.alpha = 0
			ring3.alpha = 0
			ring4.alpha = 0
			ring5.alpha = 0
			ring6.alpha = 0
			ring7.alpha = 0
			ring8.alpha = 0
			ring9.alpha = 0
			ring10.alpha = 0
			ring11.alpha = 0
			ring12.alpha = 0
			ring13.alpha = 0

		}

	}
}
