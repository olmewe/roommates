import UIKit
import SpriteKit

class CatsScene: Scene {

	/*GameNode Variables*/
	let cat = CatsCatNode()
	let radar = CatsRadarNode()
	let joystick = CatsJoystickNode()
	let leaveNode = SKSpriteNode(imageNamed: "common/close")
	let background = SKSpriteNode(imageNamed: "cenario")
	var players = [Peer: CatsStalkerNode]()
	let selector = ButtonSelector()
	let popUpTitle = SKLabelNode()
	let popUpText = SKLabelNode()
	var popUpBack = SKSpriteNode()


	/*Utils Variables*/
	private var column: Int = 0
	private var line: Int = 0
	private let timer = SKLabelNode()
	private var timeLimit:Int = 60
	private var time: CGFloat = 0
	private var fingerID: Int?
	private var dist:Float = 1300
	private var notPaused = false
	private var t: CGFloat = 1
	private var endTimer:CGFloat = 2.5
	private var movementTimer:CGFloat = 1/20
	private var gameEnded: Bool = false
	static var sceneSize = CGSize(width: 0, height: 0)

	var mapMatrix = [[Int]]()
	let map = SKNode()
	let popUp = SKNode()

	override func load() {
		mapMatrix =
		   [[1,1,1,1,1,1,1,1,1,1,1,1 ,1,1,1,0,0,0,0,0,0,1,1,1 ,1,1,1,1,1,1,1,1,1,0,0,0],
			[1,0,0,0,0,0,0,0,0,0,0,0 ,0,0,1,0,0,0,0,0,0,1,0,0 ,0,0,0,0,1,0,0,0,1,0,0,0],
			[1,0,0,0,0,0,0,0,0,0,0,0 ,0,0,1,0,0,0,0,0,0,1,0,0 ,0,0,0,0,1,0,0,0,1,0,0,0],
			[1,0,0,1,1,1,1,1,1,1,1,0 ,0,0,1,1,1,1,1,1,1,1,0,0 ,0,0,0,0,0,0,0,0,1,0,0,0],
			[1,0,0,1,0,0,0,0,0,0,1,0 ,0,0,0,0,0,0,0,0,0,0,0,0 ,0,0,0,0,0,0,0,0,1,0,0,0],
			[1,0,0,1,0,0,0,0,0,0,1,0 ,0,0,0,0,0,0,0,0,0,0,0,0 ,1,1,0,0,1,1,1,1,1,0,0,0],
			[1,0,0,1,1,1,1,1,1,1,1,0 ,0,0,0,0,0,0,0,0,0,0,0,0 ,1,0,0,0,0,0,0,0,1,1,1,1],

			[1,0,0,0,0,0,0,0,0,0,0,0 ,0,0,1,0,0,0,0,0,0,1,0,0 ,1,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,0,0,0,0,0,0,0 ,0,0,1,0,0,0,0,0,0,1,0,0 ,1,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,1,1,0,0,0,0,0,0,0 ,0,0,1,1,1,0,0,1,1,1,0,0 ,1,1,1,1,1,1,1,1,1,0,0,1],
			[1,0,0,1,1,0,0,1,1,1,1,1 ,0,0,1,0,0,0,0,0,0,1,0,0 ,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,0,0,1,0,0,0,1 ,0,0,0,0,0,0,0,0,0,0,0,0 ,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,0,0,1,0,0,0,1 ,0,0,0,0,0,0,0,0,0,0,0,0 ,1,1,1,1,1,1,1,1,1,0,0,1],

			[1,0,0,1,1,1,1,1,0,0,1,1 ,0,0,1,1,1,0,0,1,1,1,0,0 ,0,0,0,0,0,0,0,0,1,0,0,1],
			[1,0,0,1,0,0,0,0,0,0,0,0 ,0,0,1,0,0,0,0,0,0,1,0,0 ,0,0,0,0,0,0,0,0,1,0,0,1],
			[1,0,0,1,0,0,0,0,0,0,0,0 ,0,0,1,0,0,0,0,0,0,1,0,0 ,1,1,1,1,1,1,0,0,0,0,0,1],
			[1,0,0,1,1,1,1,1,0,0,1,1 ,0,0,1,0,0,0,0,0,0,1,0,0 ,1,0,0,0,0,1,0,0,0,0,0,1],
			[1,0,0,0,0,0,0,0,0,0,0,1 ,0,0,0,0,0,0,0,0,0,1,0,0 ,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,0,0,0,0,0,0,0,0,0,0,1 ,0,0,0,0,0,0,0,0,0,1,0,0 ,0,0,0,0,0,0,0,0,0,0,0,1],
			[1,1,1,1,1,1,1,1,1,1,1,1 ,1,1,1,1,1,1,1,1,1,1,1,1 ,1,1,1,1,1,1,1,1,1,1,1,1]]

		backgroundColor = #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
		size = CGSize(width: 720,height: 720*Game.heightRatio)
		popUpBack = SKSpriteNode(color: #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1), size: CGSize(width: size.width * 0.3, height: size.height * 0.6))
		popUpBack.position = CGPoint(x: -size.width * 0.05, y: 0)
		CatsScene.sceneSize = size
		background.size = size
		leaveNode.position = CGPoint(x: size.width * -0.55 + CatsScene.sceneSize.width * 0.07 ,y: size.height * 0.5 - CatsScene.sceneSize.width * 0.03)
		leaveNode.zPosition = 3
		leaveNode.zRotation = CGFloat.pi/2
		leaveNode.size = CGSize(width: CatsScene.sceneSize.width * 0.15, height: CatsScene.sceneSize.width * 0.15)
		radar.position = CGPoint(x: -CatsScene.sceneSize.width/3.2, y: CatsScene.sceneSize.height/2.2)

		time = CGFloat(timeLimit)
		timer.text = "\(timeLimit)"
		timer.fontSize = 65
		timer.zRotation = CGFloat.pi/2
		timer.zPosition = 3
		timer.fontColor = SKColor.black
		timer.position = CGPoint(x: -size.width/2.4, y: 0)

		popUpTitle.zPosition = 10
		popUpText.fontSize = 30
		popUpTitle.fontSize = popUpText.fontSize
		popUpTitle.position = CGPoint(x: -size.width * 0.1, y: 0)
		popUpTitle.zRotation = CGFloat.pi/2
		if Cats.opponent == Par.peer{
			popUpTitle.text = "You're the Cat!"
			popUpText.text = "The game will start as soon as you move"
		}else{
			popUpTitle.text = "You're the Stalker!"
			popUpText.text = "The game will start as soon as the cat makes its first move"
		}

		popUpBack.zPosition = 9
		popUpText.zPosition = 10
		popUpText.zRotation = CGFloat.pi/2


		addChild(timer)

		for index in stride(from: -self.size.height/2 + self.size.width * 0.0125, through: self.size.height/2, by: self.size.width * 0.05){
			for index2 in stride(from: -self.size.width/2 + self.size.width * 0.025, through: self.size.width/2 - self.size.width * 0.025, by: self.size.width * 0.05){
				if 	mapMatrix[column][line] == 1{
					let field = CatsMapBlockNode()
					field.load(x:CGFloat(index2), y:  CGFloat(index), width: self.size.width * 0.05, height: self.size.width * 0.05)
					map.addChild(field)
				}
				column += 1
			}
			line += 1
			column = 0
		}

		for player in Cats.players{
			players[player] = CatsStalkerNode()
			if Cats.opponent != player, let i = players[player] {
				addChild(i)
			}
		}

		cat.load(isCat: Cats.opponent == Par.peer)

		addChild(map)
		addChild(background)
		addChild(cat)
		addChild(joystick)
		addChild(leaveNode)
		if Cats.opponent != Par.peer{
			addChild(radar)
		}
		popUp.addChild(popUpBack)
		popUp.addChild(popUpTitle)
		popUp.addChild(popUpText)
		addChild(popUp)
		selector.nodes = [leaveNode]
	}

	override func update() {
		movementTimer -= Time.deltaTime
		if movementTimer < 0{
			movementTimer = 0.1
			if Cats.opponent == Par.peer {
				Cats.par.send(ParEvent(name:"pos", data:["pos": cat.sprite.position.s]),to: Cats.par.peers)
			}else if let p = players[Par.peer]{
				Cats.par.send(ParEvent(name:"pos", data:["pos": p.sprite.position.s]),to: Cats.par.peers)
			}
		}

		if let i = selector.update() {
			switch i {
			case leaveNode:
				Cats.endGame()
			default: break
			}
		}

		for i in Cats.par.events {
			switch i.event.name {
			case "pos":
				if Cats.opponent != i.peer,let pos = CGPoint.s(i.event.data["pos"]){
					players[i.peer]?.sprite.position = pos
				}else if Cats.opponent == i.peer,let pos = CGPoint.s(i.event.data["pos"]){
					cat.sprite.position = pos
				}
			case "time":
				timer.text = "\(Int( i.event.data["time"] as! CGFloat))"
			case "end":
					gameEnded = true
			case "pause":
				if let pause = i.event.data["pause"] as? Bool{
					notPaused = pause
					popUp.alpha = 0
				}
			default: break
			}
		}

		if Cats.opponent != Par.peer ,let j = players[Par.peer]{
			dist = hypotf(Float(cat.sprite.position.x - j.sprite.position.x), Float(cat.sprite.position.y - j.sprite.position.y))
			radar.updateRadar(dist: dist)
			if dist < 70{
				cat.alpha = 1 - CGFloat(dist/70)
				if dist < 50{
					notPaused = false
					gameEnded = true
					Cats.par.send(ParEvent(name:"pause", data:["pause": false]),to: Cats.par.peers)
				}
			}else{
				cat.alpha = 0
			}
		}

		if gameEnded {
			Cats.par.send(ParEvent(name:"end", data:["end": true]),to: Cats.par.peers)
			cat.alpha = 1
			t -= Time.deltaTime
			endTimer -= Time.deltaTime
			if t < 0{
				t = 0
			}
			let te = t.ease
			cam.setScale(te.lerp(0.3,1))
			cam.position = te.lerp(cat.sprite.position,CGPoint.zero)

			if endTimer < 0{
				if Int(time) == 0{
					Cats.setEndGame(win: true)
				}else{
					Cats.setEndGame(win: false)
				}
			}
		}

		if Cats.opponent == Par.peer{
			if Int(time) == 0 {
				gameEnded = true
				notPaused = false
				Cats.par.send(ParEvent(name:"pause", data:["pause": false]),to: Cats.par.peers)
			}
			if notPaused{
				time -= Time.deltaTime
			}
			Cats.par.send(ParEvent(name:"time", data:["time": time]),to: Cats.par.peers)
			timer.text = "\(Int(time))"
		}

		for i in Touch.events{
			if i.state == .down{
				if fingerID == nil {
					fingerID = i.id
				}
				if fingerID == i.id{
					joystick.updateOuterShape(alpha: 0.65, point: i.position)
					joystick.updateInnerShape(alpha: 1, point: i.position)
				}
			}
			if (i.state == .up || i.state == .miss) && fingerID == i.id{
				joystick.updateOuterShape(alpha: 0, point: CGPoint(x: i.position.x, y: i.position.y))
				joystick.updateInnerShape(alpha: 0, point: i.position)
				if Cats.opponent == Par.peer{
					cat.velX = 0
					cat.velY = 0
				}else{
					players[Par.peer]?.velX = 0
					players[Par.peer]?.velY = 0
				}
				fingerID = nil
			}
			if i.state == .move && fingerID == i.id && notPaused{
				joystick.updateInnerShape(alpha: 1, point: i.position)
				if Cats.opponent == Par.peer{
					cat.move(innerPos: joystick.innerShape.position, outerPoint: joystick.outerShape.position)
				}else{
					players[Par.peer]?.move(innerPos: joystick.innerShape.position, outerPoint: joystick.outerShape.position)
				}
			}else if i.state == .move && fingerID == i.id {
				if timeLimit == Int(time) && !notPaused && Cats.opponent == Par.peer && popUp.alpha == 1{
					popUp.alpha = 0
					Cats.par.send(ParEvent(name:"pause", data:["pause": true]),to: Cats.par.peers)
					notPaused = true
				}
			}
		}
	}
}
