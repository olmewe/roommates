import SpriteKit

class CatsLoadingScene:Scene {
	let leaveNode = SKSpriteNode(color:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),size:CGSize(width:100,height:100))

	let idleNode = SKSpriteNode(color:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),size:CGSize(width:50,height:50))

	let selector = ButtonSelector()

	override func load() {
		size = CGSize(width:720,height:720*Game.heightRatio)
		backgroundColor = #colorLiteral(red: 0.4464774547, green: 0.6130099826, blue: 0.3318156934, alpha: 1)

		let margin:CGFloat = 40

		addChild(leaveNode)
		leaveNode.position = CGPoint(x:(size.width-leaveNode.size.width-margin)*0.5,y:(size.height-leaveNode.size.height-margin)*0.5)

		addChild(idleNode)

		selector.nodes = [leaveNode]
	}

	override func update() {
		if let i = selector.update() {
			switch i {
			case leaveNode: HomeState.leaveGame()
			default: break
			}
		}
		idleNode.position = CGPoint(x:cos(Time.time)*300,y:sin(Time.time)*300)
	}
}
