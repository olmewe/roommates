import SpriteKit

class CatsEndScene: Scene{

	let label = SKLabelNode()
	let button = SKSpriteNode(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), size: CGSize(width:  CatsScene.sceneSize.width * 0.1 , height:  CatsScene.sceneSize.height * 0.1))
	let selector = ButtonSelector()

	override func load() {
		backgroundColor = #colorLiteral(red: 0.9995340705, green: 0.988355577, blue: 0.4726552367, alpha: 1)
		size = CGSize(width: 720,height: 720*Game.heightRatio)
		label.text = "GameOver!"
		label.fontColor = SKColor.black
		label.zRotation = CGFloat.pi/2
		button.position = CGPoint(x: CatsScene.sceneSize.width/4, y: 0)

		addChild(label)
		addChild(button)

		selector.nodes = [button]
	}

	override func update() {
		if selector.update() == button {
			Cats.endGame()
		}
	}
}
