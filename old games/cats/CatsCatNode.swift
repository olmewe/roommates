import SpriteKit

class CatsCatNode: SKNode, SceneNode {
	var sprite = SKSpriteNode()
	var velX:CGFloat = 0
	var velY:CGFloat = 0
	var size: CGSize = CGSize(width: 0, height: 0)
	var speedRate: CGFloat = 1
	var maxSpeed: CGFloat = 6
	var velocity: CGFloat = 0
	static var catMask: UInt32 = 0x1 << 1

	var target: CGPoint = CGPoint.zero

	func load(isCat: Bool) {
		sprite = SKSpriteNode(color: #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1), size: CGSize(width: 50, height: 50))
		sprite.zPosition = 1
		sprite.physicsBody = SKPhysicsBody(rectangleOf: sprite.size)
		sprite.physicsBody?.categoryBitMask = CatsCatNode.catMask
		sprite.physicsBody?.contactTestBitMask = CatsMapBlockNode.blockMask
		sprite.physicsBody?.collisionBitMask = CatsMapBlockNode.blockMask
		sprite.physicsBody?.affectedByGravity = false
		sprite.physicsBody?.isDynamic = true
		sprite.physicsBody?.allowsRotation = false
		size = sprite.size
//		sprite.alpha = 0
		sprite.position = CGPoint(x:300,y:600)

		addChild(sprite)
		if isCat{
			sprite.alpha = 1

			let positionArray = [CGPoint(x:-300,y:-600),
			                     CGPoint(x:-300,y: 500),
			                     CGPoint(x: 300,y:-600),
			                     CGPoint(x: 300,y: 600)]

			let R = Random.range(positionArray.count)

			sprite.position = positionArray[R]

			Cats.par.send(ParEvent(name:"pos", data:["pos": sprite.position.s]),to: Cats.par.peers, reliable: true)
		}

		speedRate = CGFloat(Cats.players.count)/7
		velocity = maxSpeed * speedRate
	}


	func update() {
		sprite.position.x += velX
		sprite.position.y += velY
	}

	func move(innerPos: CGPoint, outerPoint: CGPoint){
		self.velX = (innerPos.x - outerPoint.x)/10
		self.velY = (innerPos.y - outerPoint.y)/10


		if self.velX > velocity {
			self.velX = velocity
		}else if self.velX < -velocity {
				self.velX = -velocity
		}

		if self.velY > velocity {
			self.velY = velocity
		}else if self.velY < -velocity {
				self.velY = -velocity
		}
	}
}

