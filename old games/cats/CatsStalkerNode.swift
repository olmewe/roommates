import SpriteKit

class CatsStalkerNode: SKNode, SceneNode {
	var sprite = SKSpriteNode()
	var velX:CGFloat = 0
	var velY:CGFloat = 0
	var size: CGSize = CGSize(width: 0, height: 0)

	var target: CGPoint = CGPoint.zero

	func load() {
		sprite = SKSpriteNode(color: #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), size: CGSize(width: 50, height: 50))
		sprite.zPosition = 1
		sprite.physicsBody = SKPhysicsBody(rectangleOf: sprite.size)
		sprite.physicsBody?.categoryBitMask = CatsCatNode.catMask
		sprite.physicsBody?.contactTestBitMask = CatsMapBlockNode.blockMask
		sprite.physicsBody?.collisionBitMask = CatsMapBlockNode.blockMask
		sprite.physicsBody?.affectedByGravity = false
		sprite.physicsBody?.isDynamic = true
		sprite.physicsBody?.allowsRotation = false
		size = sprite.size

		addChild(sprite)
	}

	func update() {
		sprite.position.x += velX
		sprite.position.y += velY
	}

	func move(innerPos: CGPoint, outerPoint: CGPoint){
		self.velX = (innerPos.x - outerPoint.x)/10
		self.velY = (innerPos.y - outerPoint.y)/10


		if self.velX > 3 {
			self.velX = 3
		}else
			if self.velX < -3 {
				self.velX = -3
		}

		if self.velY > 3 {
			self.velY = 3
		}else

			if self.velY < -3 {
				self.velY = -3
		}
	}

}
