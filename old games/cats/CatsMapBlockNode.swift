import UIKit
import SpriteKit

class CatsMapBlockNode: SKNode, SceneNode {
	var sprite = SKSpriteNode()
	static let blockMask: UInt32 = 0x1 << 2

	func load(x: CGFloat, y: CGFloat,width : CGFloat, height: CGFloat){
		self.sprite = SKSpriteNode(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), size: CGSize(width: width, height: height))
		self.sprite.position = CGPoint(x: x, y:y)
		self.sprite.zPosition = 1

		self.sprite.physicsBody = SKPhysicsBody(rectangleOf: self.sprite.size)
		self.sprite.physicsBody?.categoryBitMask = CatsMapBlockNode.blockMask
		self.sprite.physicsBody?.collisionBitMask = CatsCatNode.catMask
		self.sprite.physicsBody?.contactTestBitMask = CatsCatNode.catMask
		self.sprite.physicsBody?.affectedByGravity = false
		self.sprite.physicsBody?.allowsRotation = false
		self.sprite.physicsBody?.isDynamic = false
		self.sprite.alpha = 0
		addChild(sprite)
	}

}

