import SpriteKit

class CatsMenuScene: Scene {
	let leaveNode = SKSpriteNode(color:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),size:CGSize(width:100,height:100))
	let closeNode = SKSpriteNode(color:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),size:CGSize(width:600,height:100))
	let startNode = SKSpriteNode(color:#colorLiteral(red: 0.9427354601, green: 0.8303493924, blue: 0.2666666806, alpha: 1),size:CGSize(width:600,height:100))

	let idleNode = SKSpriteNode(color:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),size:CGSize(width:50,height:50))

	let selector = ButtonSelector()

	var selectionView:SelectionViewController?

	override func load() {
		size = CGSize(width:720,height:720*Game.heightRatio)
		backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)

		let margin:CGFloat = 40

		addChild(leaveNode)
		leaveNode.position = CGPoint(x:(size.width-leaveNode.size.width-margin)*0.5,y:(size.height-leaveNode.size.height-margin)*0.5)

		addChild(closeNode)
		closeNode.position = CGPoint(x:0,y:-(size.height-closeNode.size.height-margin)*0.5)

		addChild(startNode)
		startNode.position = CGPoint(x:0,y:closeNode.position.y+(closeNode.size.height+margin+startNode.size.height)*0.5)

		addChild(idleNode)

		selector.nodes = [leaveNode,closeNode,startNode]
	}

	override func update() {
		for i in Cats.par.events {
			switch i.event.name {
			case "openSelection": openSelection(request:false)
			default: break
			}
		}
		if let i = selector.update() {
			switch i {
			case leaveNode: HomeState.leaveGame()
			case closeNode: HomeState.closeGame()
			case startNode: openSelection(request:true)
			default: break
			}
		}
		idleNode.position = CGPoint(x:cos(Time.time)*300,y:sin(Time.time)*300)
	}

	func openSelection(request:Bool) {
		if request {
			Cats.par.send(ParEvent(name:"openSelection"),reliable:true)
		}
		guard selectionView == nil else { return }
		selectionView = UI.showSelection(min: 2, max: 8) {
			start,players in
			self.selectionView = nil

			if start {
				Cats.set(players:players)
				Cats.sendPlayers(to:Cats.par.peers)
			}
		}
	}
}
