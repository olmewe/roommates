import SpriteKit

class Cats:GameManager {
	static let id = "meowingcat"
	static let context = "game_"+id
	static let par = ParContext(context:context)

	enum State {
		case loading
		case menu
		case queue
		case game
	}
	static var state:State = .loading

	enum GameState {
		case loading
		case playing
		case catWins
		case catLoses
	}
	static var gameState:GameState = .loading

	static var players:[Peer] = []
	static var opponent:Peer!
	static var controlEnabled = false

	static func load() {
		players = []
		opponent = Par.peer

		par.subscribe()
		if par.peers.isEmpty {
			state = .menu
			presentFirstScene(CatsMenuScene())
		} else {
			state = .loading
			presentFirstScene(CatsScene())
		}
	}

	static func unload() {
		players = []

		par.unsubscribe()
		leaveScene()
	}

	static func update() {
		for i in par.events {
			switch i.event.name {
			case "players":
				set(players:Peer.decode(peers:i.event.data["list"]) ?? [])
			case "start":
				guard i.peer == players[0] else { break }
				if let number = i.event.data["opponent"] as? Int{
					setOpponent(number: number)
				}
			case "end":
				guard state == .game && i.peer == opponent else { break }
				if let win = i.event.data["win"] as? Bool{
					setEndGame(win: win)
				}
			default: break
			}
		}
		if state != .loading {
			var newPeers:[Peer] = []
			for (peer,connected) in par.newPeers {
				if connected {
					newPeers.append(peer)
				} else if players.contains(peer) {
					set(players:[])
				}
			}
			if !newPeers.isEmpty {
				sendPlayers(to:newPeers)
			}
		}
	}

	static func lateUpdate() {
		par.clearEvents()
	}

	static func set(players:[Peer]) {
		self.players = players
		var newState:State
		if players.count >= 2  && players.count <= 8{
			newState = .queue
			for player in players{
				if player == Par.peer {
					newState = .game
				}else if !par.peers.contains(player){
					newState = .menu
					break
				}
			}
		} else {
			newState = .menu
		}
		guard state != newState else { return }
		state = newState
		switch state {
		case .loading:
			Scene.present(CatsLoadingScene())
		case .menu:
			Scene.present(CatsMenuScene())
		case .queue:
			Scene.present(CatsQueueScene())
		case .game:
			gameState = .playing
			opponent = players[0]
			Scene.present(CatsScene())
		}
	}

	static func endGame() {
		set(players:[])
		sendPlayers(to:par.peers)
	}

	static func sendPlayers(to peers:[Peer]) {
		par.send(ParEvent(name:"players",data:["list":Peer.encode(peers:players)]),to:peers)
	}

	static func sendOpponent(number: Int) {
		par.send(ParEvent(name:"start",data:["opponent":number]),to: players)
	}

	static func setOpponent(number: Int){
		guard number >= 0 && number < players.count else { return }
		Cats.opponent = players[number]
		gameState = .playing
	}

	static func setEndGame(win: Bool){
		if opponent == Par.peer {
			par.send(ParEvent(name:"end",data:["win":win]),to: players)
		}
		gameState = win ? .catWins : .catLoses
		Scene.present(CatsEndScene())
	}
}

