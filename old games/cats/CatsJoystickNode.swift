import SpriteKit

class CatsJoystickNode: SKNode, SceneNode {
	private(set) var outerShape = SKShapeNode()
	private(set) var innerShape = SKShapeNode()
	private var restraint = SKShapeNode()

	func load(){
		outerShape = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.1)
		outerShape.fillColor = SKColor.lightGray
		outerShape.strokeColor = SKColor.clear
		outerShape.zPosition = 1
		outerShape.alpha = 0
		outerShape.position = CGPoint(x: 0, y: 0)

		innerShape = SKShapeNode(circleOfRadius: CatsScene.sceneSize.width * 0.1)
		innerShape.fillColor = SKColor.white
		innerShape.zPosition = 2
		innerShape.alpha = 0

		addChild(outerShape)
		addChild(innerShape)
	}
	func updateOuterShape(alpha: CGFloat, point: CGPoint) {
		self.outerShape.alpha = alpha
		self.outerShape.position = point
	}

	func updateInnerShape(alpha: CGFloat, point: CGPoint){
		self.innerShape.alpha = alpha
		self.innerShape.position = point

		let range = SKRange(lowerLimit: 0, upperLimit: CatsScene.sceneSize.width * 0.1)

		let lockToCenter = SKConstraint.distance(range, to: outerShape.position)

		innerShape.constraints = [ lockToCenter ]
	}


}

